<?php
// use \Phalcon\PHPMailer;

class IndexController extends ControllerBase
{


     public function indexAction()
    {
    	 $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos_login.css');

    	$this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/index.js');



    }

     public function iniciarAction()
    {
    	$usuario = Usuarios::find(array(
    		"usu_ema='".$this->request->getPost("txt_usu")."' AND usu_con='".$this->request->getPost("txt_con")."' AND usu_est='1'"
    	));
    	if(count($usuario)==1)
    	{
    		foreach ($usuario as  $usu)
    		{
	    		$this->session->set("usu_cod", $usu->usu_cod);
	    		$this->session->set("usu_nom", $usu->usu_nom." ".$usu->usu_ape);
	    		echo "1";
    		}

    	}else{
    		echo "Usuario y/o Contraseña incorrectos";
    	}


    }

    public function driveAction($code = false) {
        if ($code) {
            $this->view->setVar("code", $code);
        }
    }

    public function apiAction() {    }

     public function olvidarAction()
    {

        if($this->request->getPost("txt_usu")=="")
        {
            echo "Debe escribir su correo electronico en el campo Usuario";
            return;
        }


        $usuario = Usuarios::find(array(
            "usu_ema='".$this->request->getPost("txt_usu")."'"
        ));
        if(count($usuario)==1)
        {
            foreach ($usuario as  $usu)
            {
               $element = new Elements();
               $body = "<p>Su usuario es: ".$usu->usu_ema."</p>";
               $body.= "<p>Su contraseña es: ".$usu->usu_con."</p>";
               $element->envioEmail($body, "Recuperación de Contraseña", $usu->usu_ema);
               echo "Se han enviado los datos de acceso a su correo electronico";
            }


        }else{
            echo "Su correo electronico no aparece registrado en nuestra base de datos";
        }




    }

    public function cerrarAction() {
        if ($this->session->has("per_cod")) {
            $url_cerrar_sesion = $this->url->get()."registro";
        }else{
            $url_cerrar_sesion = $this->url->get();
        }
        $this->session->destroy();
        header("Location: " . $url_cerrar_sesion);
    }

}

