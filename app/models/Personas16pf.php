<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Personas16pf extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $p16_cod;

    /**
     *
     * @var integer
     */
    public $per_cod;

    /**
     *
     * @var integer
     */
    public $req_cod;

    /**
     *
     * @var integer
     */
    public $usu_cod;

    /**
     *
     * @var string
     */
    public $p16_fch;

    /**
     *
     * @var string
     */
    public $p16_web;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('16pf_per');
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'p16_cod' => 'p16_cod', 
            'per_cod' => 'per_cod', 
            'req_cod' => 'req_cod', 
            'usu_cod' => 'usu_cod', 
            'p16_fch' => 'p16_fch', 
            'p16_web' => 'p16_web'
        );
    }

    public static function resultado($per_cod, $req_cod)
    {
        // A raw SQL statement
        $hoy = date("Y-m-d");
        $fecha = strtotime ( '-90 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
       $sql ="SELECT *
            FROM 16pf_per 
            WHERE p16_cod=(SELECT MAX(p16_cod) FROM 16pf_per WHERE per_cod=$per_cod) AND DATE(p16_fch) BETWEEN '$fecha' AND '$hoy'";

        // Base model
        $ipv = new Personas16pf();

        // Execute the query
        return new Resultset(null, $ipv, $ipv->getReadConnection()->query($sql));
    }

     public static function npruebas($fecha)
    {
        // A raw SQL statement

       $sql ="SELECT COUNT(*) as total
            FROM 16pf_per 
            WHERE DATE(p16_fch) = '$fecha'";

        // Base model
        $ipv = new Personas16pf();

        // Execute the query
        return new Resultset(null, $ipv, $ipv->getReadConnection()->query($sql));
    }

}
