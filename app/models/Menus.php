<?php

class Menus extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $men_cod;

    /**
     *
     * @var string
     */
    public $men_des;

    /**
     *
     * @var string
     */
    public $men_url;

    /**
     *
     * @var string
     */
    public $men_tip;

    /**
     *
     * @var string
     */
    public $men_est;

      public function initialize()
    {

         $this->hasMany("men_cod", "Permisos", "men_cod"); 
        
    }

}
