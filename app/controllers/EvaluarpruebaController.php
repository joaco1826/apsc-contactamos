<?php

class EvaluarPruebaController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/EvaluarPrueba.js');


         $this->view->setVar("preguntas", PreguntasPnl::find());
         
            
    }

     public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $this->view->setVar("req_cod", $req_cod);
    }

     public function guardarAction()
    {
        if ($this->request->isPost() == true) {
            
            $pru = new Pruebas();
            $pru->pos_cod = $this->request->getPost("per_cod");
            $pru->req_cod = $this->request->getPost("req_cod");
            $pru->usu_cod = $this->session->get("usu_cod");
            $pru->pru_fec = new \Phalcon\Db\RawValue('default');
            $pru->pru_rip = $this->request->getPost("pru_rip");
            $pru->pru_rip = $this->request->getPost("pru_rip");
            $pru->pru_ahn = $this->request->getPost("pru_ahn");
            $pru->pru_ada = $this->request->getPost("pru_ada");
            $pru->pru_com = $this->request->getPost("pru_com");
            $pru->pru_lid = $this->request->getPost("pru_lid");
            $pru->pru_mot = $this->request->getPost("pru_mot");
            $pru->pru_cap = $this->request->getPost("pru_cap");
            $pru->pru_obs = $this->request->getPost("pru_obs");
            

            if($pru->save() == false){
                $this->db->rollback();
                return;
            }

            echo "1";
        }
    }
}