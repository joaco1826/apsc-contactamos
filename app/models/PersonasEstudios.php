<?php

class PersonasEstudios extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $est_cod;

    /**
     *
     * @var string
     */
    public $est_ent;

    /**
     *
     * @var string
     */
    public $est_pro;

    /**
     *
     * @var string
     */
    public $est_fec;

    /**
     *
     * @var integer
     */
    public $per_cod;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'est_cod' => 'est_cod', 
            'est_ent' => 'est_ent', 
            'est_pro' => 'est_pro', 
            'est_fec' => 'est_fec', 
            'per_cod' => 'per_cod'
        );
    }

}
