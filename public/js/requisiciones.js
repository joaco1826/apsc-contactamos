
jQuery(document).ready(function($) {
	 
	$("#btn_guardar").click(function(event) {
		

		
		$.ajax({
			url: $("#url").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Registro Guardado");
				location.reload()
			}else{
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$(".eliminar").click(function(event) {
			

			$.ajax({
				url: $("#url").val(),
				type: 'POST',
				dataType: 'html',
				data: "cod="+$(this).attr('id'),
			})
			.done(function(datos) {
				console.log(datos)
				if(datos==1){
					alert("Registro Eliminado");
					location.reload()
				}else{
					alert(datos)
				}

				
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
	});
});