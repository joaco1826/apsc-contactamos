<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class RegistrarHvController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        $this->assets
                 ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
                  ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false)
                 ->addCss('css/estilos.css');

            $this->assets
                 ->addJs('js/jquery.js')
                 ->addJs('js/menu.js')
                 ->addJs('js/validate.js')
                 ->addJs('js/msj_validate.js')
                 ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false)
                ->addJs('js/registrar_hv.js');
    }

    public function indexAction()
    {
    	
    	


        $this->view->setVar("identificaciones", Tipos::find(array(
                "tip_tip = 'identificacion'"
        )));

        $this->view->setVar("grupos", Tipos::find(array(
                "tip_tip = 'sangre'"
        )));

        $this->view->setVar("sexos", Tipos::find(array(
                "tip_tip = 'sexo'"
        )));

        $this->view->setVar("civiles", Tipos::find(array(
                "tip_tip = 'civil'"
        )));

        $this->view->setVar("estaturas", Tipos::find(array(
                "tip_tip = 'est'"
        )));

        $this->view->setVar("estratos", Tipos::find(array(
                "tip_tip = 'eto'"
        )));

        $this->view->setVar("cargos", Cargos::find(array(
                "car_est = '1' ORDER BY car_des"
         )));

        $muni = new Muni();

        $this->view->setVar("municipios", $muni->listarMuniDpto());
            
    	 
    }

   

    

    
    	
   	public function guardarAction()
    {
        

        $validation = new Phalcon\Validation();

        $validation->add('car_cod', new PresenceOf(array(
            'message' => 'El campo cargo de interés Es Requerido',

        )));

        $validation->add('per_tid', new PresenceOf(array(
            'message' => 'El campo Tipo de identificacion Es Requerido',

        )));
       
        $validation->add('per_ide', new PresenceOf(array(
            'message' => 'El campo No. Identificacion Es Requerido',

        )));

        $validation->add('per_pno', new PresenceOf(array(
            'message' => 'El campo  Nombre Es Requerido',

        )));

        

        $validation->add('per_pap', new PresenceOf(array(
            'message' => 'El campo   Apellido Es Requerido',

        )));

        

        $validation->add('per_fex', new PresenceOf(array(
            'message' => 'El campo  Fecha Expedicion Es Requerido',

        )));

        $validation->add('per_cex', new PresenceOf(array(
            'message' => 'El campo  Lugar Expedicion Es Requerido',

        )));

        $validation->add('per_dir', new PresenceOf(array(
            'message' => 'El campo  Direccion Es Requerido',

        )));

        $validation->add('per_bar', new PresenceOf(array(
            'message' => 'El campo  Barrio Es Requerido',

        )));

        $validation->add('per_ciu', new PresenceOf(array(
            'message' => 'El campo  Lugar de Residencia Es Requerido',

        )));

        $validation->add('per_ema', new PresenceOf(array(
            'message' => 'El campo  Email Es Requerido',

        )));

        // $validation->add('per_tel', new PresenceOf(array(
        //     'message' => 'El campo  Telefono Es Requerido',

        // )));

        $validation->add('per_ato', new PresenceOf(array(
            'message' => 'El campo  Estrato Es Requerido',

        )));

        $validation->add('per_cel', new PresenceOf(array(
            'message' => 'El campo  Celular Es Requerido',

        )));

        $validation->add('per_fna', new PresenceOf(array(
            'message' => 'El campo  Fecha de Nacimiento Es Requerido',

        )));

        $validation->add('per_cna', new PresenceOf(array(
            'message' => 'El campo  Lugar de Nacimiento Es Requerido',

        )));


        $validation->add('per_gsa', new PresenceOf(array(
            'message' => 'El campo  Grupo Sanguineo Es Requerido',

        )));

        $validation->add('per_pro', new PresenceOf(array(
            'message' => 'El campo  Profesion Es Requerido',

        )));

        // $validation->add('per_mcf', new PresenceOf(array(
        //     'message' => 'El campo  Madre Cabeza de familia Es Requerido',

        // )));

        $validation->add('per_sex', new PresenceOf(array(
            'message' => 'El campo  Sexo Es Requerido',

        )));

        $validation->add('per_civ', new PresenceOf(array(
            'message' => 'El campo  Estado Civil Es Requerido',

        )));

        $validation->add('per_ura', new PresenceOf(array(
            'message' => 'El campo  Estatura Es Requerido',

        )));

        $validation->add('per_eda', new PresenceOf(array(
            'message' => 'El campo  Edad Es Requerido',

        )));

        $validation->add('per_pes', new PresenceOf(array(
            'message' => 'El campo  Peso Es Requerido',

        )));

        $validation->add('per_tca', new PresenceOf(array(
            'message' => 'El campo  Talla Camisa Es Requerido',

        )));

        $validation->add('per_tpa', new PresenceOf(array(
            'message' => 'El campo  Talla Pantalón Es Requerido',

        )));

        $validation->add('per_tza', new PresenceOf(array(
            'message' => 'El campo  Talla Zapato Es Requerido',

        )));

        $validation->add('per_hij', new PresenceOf(array(
            'message' => 'El campo  No. de Hijos Es Requerido',

        )));

        $validation->add('per_pac', new PresenceOf(array(
            'message' => 'El campo  Personas a Cargo Es Requerido',

        )));

        $validation->add('per_asp', new PresenceOf(array(
            'message' => 'El campo  Aspiración Salarial Es Requerido',

        )));

        $validation->add('per_tur', new PresenceOf(array(
            'message' => 'El campo  Disponibilidad para turnos Es Requerido',

        )));
        $validation->add('per_din', new PresenceOf(array(
            'message' => 'El campo  Disponibilidad inmediata Es Requerido',

        )));
        $validation->add('per_fre', new PresenceOf(array(
            'message' => 'El campo  Fuente de reclutamiento Es Requerido',

        )));

        

        


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $per           = new Personas();
        $per->car_cod  = $this->request->getPost("car_cod");
        $per->per_tid  = $this->request->getPost("per_tid");
        $per->per_ide  = $this->request->getPost("per_ide");
        $per->per_pno  = $this->request->getPost("per_pno");
        $per->per_sno  = $this->request->getPost("per_sno");
        $per->per_pap  = $this->request->getPost("per_pap");
        $per->per_sap  = $this->request->getPost("per_sap");
        $per->per_fex  = $this->request->getPost("per_fex");
        $per->per_cex  = $this->request->getPost("per_cex");
        $per->per_dir  = $this->request->getPost("per_dir");
        $per->per_bar  = $this->request->getPost("per_bar");
        $per->per_ciu  = $this->request->getPost("per_ciu");
        $per->per_ema  = $this->request->getPost("per_ema");
        $per->per_tel  = $this->request->getPost("per_tel");
        $per->per_ato  = $this->request->getPost("per_ato");
        $per->per_cel  = $this->request->getPost("per_cel");
        $per->per_fna  = $this->request->getPost("per_fna");
        $per->per_cna  = $this->request->getPost("per_cna");
        $per->per_gsa  = $this->request->getPost("per_gsa");
        $per->per_pro  = $this->request->getPost("per_pro");
        $per->per_mcf  = $this->request->getPost("per_mcf");
        $per->per_sex  = $this->request->getPost("per_sex");
        $per->per_civ  = $this->request->getPost("per_civ");
        $per->per_ura  = $this->request->getPost("per_ura");
        $per->per_eda  = $this->request->getPost("per_eda");
        $per->per_pes  = $this->request->getPost("per_pes");
        $per->per_tca  = $this->request->getPost("per_tca");
        $per->per_tpa  = $this->request->getPost("per_tpa");
        $per->per_tza  = $this->request->getPost("per_tza");
        $per->per_gso  = $this->request->getPost("per_gso");
        $per->per_get  = $this->request->getPost("per_get");
        $per->per_her  = $this->request->getPost("per_her");
        $per->per_hij  = $this->request->getPost("per_hij");
        $per->per_pac  = $this->request->getPost("per_pac");
        $per->per_asp  = $this->request->getPost("per_asp");
        $per->per_tur  = $this->request->getPost("per_tur");
        $per->per_din  = $this->request->getPost("per_din");
        $per->per_fre  = $this->request->getPost("per_fre");
        $per->per_nlm  = $this->request->getPost("per_nlm");
        $per->per_dis  = $this->request->getPost("per_dis");
        $per->per_lco  = $this->request->getPost("per_lco");
        $per->per_cat  = $this->request->getPost("per_cat");
        $per->per_cju  = $this->request->getPost("per_cju");

        
        if($per->save()){
            echo "1"."(/)".$per->per_cod;
        }else{
             foreach ($per->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
        

    
    	

    }
}

