<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class SectoresController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/sectores.js');
    }

    public function indexAction()
    {
       
            
            
    	 $this->view->setVar("sectores", Sectores::find(array(
		    	"sec_est = '1'"
		 )));
    }

     public function nuevoAction()
    {
    	
       
    }

     public function editarAction($sec_cod)
    {
    	

 		$this->view->setVar("sector", Sectores::findFirst($sec_cod));
 		
       
    }

     public function guardarAction()
    {
    	

		$validation = new Phalcon\Validation();
       
		$validation->add('sec_nom', new PresenceOf(array(
		    'message' => 'El campo Nombre Es Requerido',

		)));

        


		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$sector = new Sectores();
    	$sector->sec_nom  = $this->request->getPost("sec_nom");
    	$sector->sec_est  = new \Phalcon\Db\RawValue('default');

    	
    	if($sector->save()){
    		echo "1";
    	}else{
    		 foreach ($sector->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    	

    }

     public function actualizarAction()
    {
    	

		$validation = new Phalcon\Validation();
       
		$validation->add('sec_nom', new PresenceOf(array(
		    'message' => 'El campo Nombre Es Requerido',

		)));


		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$sector = Sectores::findFirst($this->request->getPost("sec_cod"));
    	$sector->sec_nom  = $this->request->getPost("sec_nom");
    	$sector->sec_est  = new \Phalcon\Db\RawValue('default');

    	
    	if($sector->save()){
    		echo "1";
    	}else{
    		 foreach ($sector->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    	

    }

     public function eliminarAction()
    {
    	
    	$sector = Sectores::findFirst($this->request->getPost("cod"));
    	$sector->sec_est="0";
    	if($sector->save()){
    		echo "1";
    	}else{
    		 foreach ($sector->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    }

}

