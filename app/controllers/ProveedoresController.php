<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class ProveedoresController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/proveedores.js');

    }

    public function indexAction()
    {
        
    	
    	 // $this->view->setVar("empresas", $this->modelsManager->executeQuery("SELECT e.emp_nit, p.pro_pre FROM Empresas As e JOIN Proveedores As p"));
             $this->view->setVar("proveedores", Empresas::find(array(
		    	"emp_est = '1' AND emp_tip = 'P'"
		 )));
    }

    public function nuevoAction()
    {
    	
    }

      public function editarAction($emp_cod)
    {
    	

 		$this->view->setVar("proveedor", Empresas::findFirst($emp_cod));
 		
       
    }

     public function guardarAction()
    {
    	

		$validation = new Phalcon\Validation();
       
		$validation->add('emp_nit', new PresenceOf(array(
		    'message' => 'El campo Nit Es Requerido',

		)));

		$validation->add('emp_raz', new PresenceOf(array(
		    'message' => 'el campo  Razon social es requerido'
		)));

		$validation->add('emp_con', new PresenceOf(array(
		    'message' => 'el campo  Contacto es requerido'
		)));

		$validation->add('emp_tel', new PresenceOf(array(
		    'message' => 'el campo  Telefono es requerido'
		)));

		$validation->add('emp_ema', new PresenceOf(array(
		    'message' => 'el campo  Email es requerido'
		)));

		$validation->add('emp_ema', new Email(array(
		    'message' => 'el formato de  Email no es valido'
		)));


		$validation->add('emp_pas', new PresenceOf(array(
		    'message' => 'el campo Contraseña es requerido'
		)));

		// $validation->add('pro_pre', new PresenceOf(array(
		//     'message' => 'el campo Prefijo es requerido'
		// )));

		// $validation->add('pro_con', new PresenceOf(array(
		//     'message' => 'el campo Consecutivo es requerido'
		// )));

		


		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}

		
		$this->db->begin();

    	$empresa = new Empresas();
    	$empresa->setEmpNit($this->request->getPost("emp_nit"));
    	$empresa->setEmpRaz($this->request->getPost("emp_raz"));
    	$empresa->setEmpCon($this->request->getPost("emp_con"));
    	$empresa->setEmpTel($this->request->getPost("emp_tel"));
    	$empresa->setEmpEma($this->request->getPost("emp_ema"));
        $empresa->setEmpDir($this->request->getPost("emp_dir"));
    	$empresa->setEmpPas($this->request->getPost("emp_pas"));
    	$empresa->setEmpEst(new \Phalcon\Db\RawValue('default'));
    	$empresa->setEmpTip("P");//proveedor

    	if ($empresa->save() == false) {
            $this->db->rollback();
            return;
        }
       
        

    	$proveedor = new Proveedores();
    	$proveedor->setEmpCod($empresa->getEmpCod());
    	$proveedor->setProPre($this->request->getPost("pro_pre"));
    	$proveedor->setProCon($this->request->getPost("pro_con"));
    	if ($proveedor->save() == false) {
            $this->db->rollback();
            return;
        }
         $this->db->commit();
         echo "1";
    	

    }

     public function actualizarAction()
    {
    	

		$validation = new Phalcon\Validation();
       
		$validation->add('emp_nit', new PresenceOf(array(
		    'message' => 'El campo Nit Es Requerido',

		)));

		$validation->add('emp_raz', new PresenceOf(array(
		    'message' => 'el campo  Razon social es requerido'
		)));

		$validation->add('emp_con', new PresenceOf(array(
		    'message' => 'el campo  Contacto es requerido'
		)));

		$validation->add('emp_tel', new PresenceOf(array(
		    'message' => 'el campo  Telefono es requerido'
		)));

		$validation->add('emp_ema', new PresenceOf(array(
		    'message' => 'el campo  Email es requerido'
		)));

		$validation->add('emp_ema', new Email(array(
		    'message' => 'el formato de  Email no es valido'
		)));


		$validation->add('emp_pas', new PresenceOf(array(
		    'message' => 'el campo Contraseña es requerido'
		)));

		// $validation->add('pro_pre', new PresenceOf(array(
		//     'message' => 'el campo Prefijo es requerido'
		// )));

		// $validation->add('pro_con', new PresenceOf(array(
		//     'message' => 'el campo Consecutivo es requerido'
		// )));

		


		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}

		
		$this->db->begin();

		$empresa = Empresas::findFirst($this->request->getPost("emp_cod"));
    	$empresa->setEmpNit($this->request->getPost("emp_nit"));
    	$empresa->setEmpRaz($this->request->getPost("emp_raz"));
    	$empresa->setEmpCon($this->request->getPost("emp_con"));
    	$empresa->setEmpTel($this->request->getPost("emp_tel"));
    	$empresa->setEmpEma($this->request->getPost("emp_ema"));
        $empresa->setEmpDir($this->request->getPost("emp_dir"));
    	$empresa->setEmpPas($this->request->getPost("emp_pas"));
    	$empresa->setEmpEst(new \Phalcon\Db\RawValue('default'));
    	$empresa->setEmpTip("P");//proveedor

    	if ($empresa->save() == false) {
            $this->db->rollback();
            return;
        }
       
        

    	$proveedor = Proveedores::findFirst("emp_cod=".$empresa->getEmpCod());
    	$proveedor->setProPre($this->request->getPost("pro_pre"));
    	$proveedor->setProCon($this->request->getPost("pro_con"));
    	if ($proveedor->save() == false) {
            $this->db->rollback();
            return;
        }
         $this->db->commit();
         echo "1";
    	

    }

     public function eliminarAction()
    {
    	
    	$empresa = Empresas::findFirst($this->request->getPost("cod"));
    	$empresa->setEmpEst("0");
    	if($empresa->save()){
    		echo "1";
    	}else{
    		 foreach ($empresa->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    }

}

