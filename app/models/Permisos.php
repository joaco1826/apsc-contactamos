<?php

class Permisos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $per_cod;

    /**
     *
     * @var integer
     */
    public $men_cod;

    /**
     *
     * @var integer
     */
    public $tus_cod;



      public function initialize()
    {
        $this->belongsTo("men_cod", "Menus", "men_cod");
        $this->belongsTo("tus_cod", "TipoUsuarios", "tus_cod");
    }

    public function getPermisosTipoUsuario($tus_cod){
        $sql = "SELECT IFNULL(tu.tus_cod, 0) permiso, tu.`tus_des` nombre_usuario, m.`men_cod`, m.`men_des`, m.men_tip
        FROM menus m 
        LEFT JOIN permisos p ON m.`men_cod`=p.`men_cod` AND p.`tus_cod`=$tus_cod
        LEFT JOIN tipo_usuarios tu ON p.`tus_cod`=tu.`tus_cod`
        WHERE men_est='1'";
        $registros = $this->db->query($sql);
        return $registros;
        
        
    }

    public function UsuariosCargos($usu_cod){
        $sql = "SELECT IFNULL(c.car_id, 0) permiso, e.`car_des`, e.`car_cod`, u.`usu_nom`, u.usu_ape, u.usu_cod
        FROM cargos e 
        LEFT JOIN consempresas c ON e.`car_cod`=c.`car_id` AND c.`usu_id`=$usu_cod
        LEFT JOIN usuarios u ON c.`usu_id`=u.`usu_cod`
        WHERE car_est='1'";
        $registros = $this->db->query($sql);
        return $registros;
        
        
    }
   

}
