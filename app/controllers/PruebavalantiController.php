<?php

class PruebaValantiController extends \Phalcon\Mvc\Controller
{
 	public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
              ->addCss('css/reloj.css')
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jpegcam/htdocs/webcam.js')
            ->addJs('js/countdown/jquery.countdown.min.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/pruebas.js');
    }

    public function indexAction()
    {
    	// $this->assets
     //         ->addCss('css/reloj.css');

    	// $this->assets
     //        ->addJs('js/jquery.js')
     //        ->addJs('js/jpegcam/htdocs/webcam.js')
     //        ->addJs('js/countdown/jquery.countdown.min.js')
     //        ->addJs('js/pruebas.js');

        // $this->view->setVar("preguntas", Preguntas16pf::find());
    }

     public function subirFotoAction()
	 {
	    	$jpeg_data = file_get_contents('php://input');
			$foto = md5(microtime()*rand(1.1,1.9))."_".$this->session->get("per_cod");
			$filename = "photos/".$foto.".jpg";

			$fot = new Fotos();
			$fot->per_cod = $this->session->get("per_cod");
			$fot->fot_rut = $filename;
			$fot->fot_pru = "VALANTI";
			$fot->fot_fec = new \Phalcon\Db\RawValue('default');

			if($fot->save()){
				$result = file_put_contents( $filename, $jpeg_data );
			}
			
	    	
	}

    public function guardarAction()
    {
    	if ($this->request->isPost() == true) {
	    	$val = new Valanti();
	    	$val->per_cod = $this->request->getPost("per_cod");
	    	$val->req_cod = $this->request->getPost("req_cod");
	    	$val->usu_cod = $this->request->getPost("per_cod");
	    	$val->val_fch = new \Phalcon\Db\RawValue('default');
	    	$val->val_web = new \Phalcon\Db\RawValue('default');

			$this->db->begin();
			
			if($val->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 1;
			$val_det->vde_res = $this->request->getPost("uno");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 2;
			$val_det->vde_res = $this->request->getPost("dos");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 3;
			$val_det->vde_res = $this->request->getPost("tres");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 4;
			$val_det->vde_res = $this->request->getPost("cuatro");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 5;
			$val_det->vde_res = $this->request->getPost("cinco");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 6;
			$val_det->vde_res = $this->request->getPost("seis");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 7;
			$val_det->vde_res = $this->request->getPost("siete");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 8;
			$val_det->vde_res = $this->request->getPost("ocho");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 9;
			$val_det->vde_res = $this->request->getPost("nueve");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 10;
			$val_det->vde_res = $this->request->getPost("diez");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre =11;
			$val_det->vde_res = $this->request->getPost("once");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 12;
			$val_det->vde_res = $this->request->getPost("doce");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 13;
			$val_det->vde_res = $this->request->getPost("trece");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 14;
			$val_det->vde_res = $this->request->getPost("catorce");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 15;
			$val_det->vde_res = $this->request->getPost("quince");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 16;
			$val_det->vde_res = $this->request->getPost("dieciseis");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 17;
			$val_det->vde_res = $this->request->getPost("diecisiete");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 18;
			$val_det->vde_res = $this->request->getPost("dieciocho");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 19;
			$val_det->vde_res = $this->request->getPost("diecinueve");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 20;
			$val_det->vde_res = $this->request->getPost("veinte");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 21;
			$val_det->vde_res = $this->request->getPost("veintiuno");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 22;
			$val_det->vde_res = $this->request->getPost("veintidos");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}
			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 23;
			$val_det->vde_res = $this->request->getPost("veintitres");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}
			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 24;
			$val_det->vde_res = $this->request->getPost("veinticuatro");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 25;
			$val_det->vde_res = $this->request->getPost("veinticinco");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 26;
			$val_det->vde_res = $this->request->getPost("veintiseis");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();

			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 27;
			$val_det->vde_res = $this->request->getPost("veintisiete");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();
			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 28;
			$val_det->vde_res = $this->request->getPost("veintiocho");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();
			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 29;
			$val_det->vde_res = $this->request->getPost("veintinueve");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}

			$val_det = new ValantiDet();
			$val_det->val_cod = $val->val_cod;
			$val_det->vde_pre = 30;
			$val_det->vde_res = $this->request->getPost("treinta");
			if($val_det->save() == false){
				$this->db->rollback();
				return;
			}


			$this->db->commit();

			 $this->dispatcher->forward(array(
		            "controller" => "verPruebas",
		            "action" => "index",
		            "params" => array("resultado" => 1)
        	  ));

		}else{
			 $this->response->redirect("verPruebas/");
		}

	
    }

}

