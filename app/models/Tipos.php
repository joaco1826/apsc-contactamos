<?php

class Tipos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $tip_cod;

    /**
     *
     * @var string
     */
    public $tip_tip;

    /**
     *
     * @var string
     */
    public $tip_des;

    /**
     *
     * @var string
     */
    public $tip_sor;
    /**
     *
     * @var string
     */
    public $tip_log;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'tip_cod' => 'tip_cod',
            'tip_tip' => 'tip_tip',
            'tip_des' => 'tip_des',
            'tip_sor' => 'tip_sor',
            'tip_log' => 'tip_log'
        );
    }

}
