<?php

class CarasPerDet extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cad_cod;

    /**
     *
     * @var integer
     */
    public $cas_cod;

    /**
     *
     * @var integer
     */
    public $car_cod;

    /**
     *
     * @var integer
     */
    public $car_rta;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'cad_cod' => 'cad_cod', 
            'cas_cod' => 'cas_cod', 
            'car_cod' => 'car_cod', 
            'car_rta' => 'car_rta'
        );
    }

}
