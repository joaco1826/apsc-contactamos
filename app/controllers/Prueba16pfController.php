<?php

class Prueba16pfController extends \Phalcon\Mvc\Controller
{

	    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
              ->addCss('css/reloj.css')
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jpegcam/htdocs/webcam.js')
            ->addJs('js/countdown/jquery.countdown.min.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/pruebas.js');
    }

    public function indexAction()
    {
    	// $this->assets
     //         ->addCss('css/reloj.css');

    	// $this->assets
     //        ->addJs('js/jquery.js')
     //        ->addJs('js/jpegcam/htdocs/webcam.js')
     //        ->addJs('js/countdown/jquery.countdown.min.js')
     //        ->addJs('js/pruebas.js');

        $this->view->setVar("preguntas", Preguntas16pf::find());
    	
    }

	 public function subirFotoAction()
	 {
	    	$jpeg_data = file_get_contents('php://input');
			$foto = md5(microtime()*rand(1.1,1.9))."_".$this->session->get("per_cod");
			$filename = "photos/".$foto.".jpg";

			$fot = new Fotos();
			$fot->per_cod = $this->session->get("per_cod");
			$fot->fot_rut = $filename;
			$fot->fot_pru = "16PF";
			$fot->fot_fec = new \Phalcon\Db\RawValue('default');

			if($fot->save()){
				$result = file_put_contents( $filename, $jpeg_data );
			}
			
	    	
	}

	public function guardarAction()
	{
	 	if ($this->request->isPost() == true) {
	    
	    	$per = new Personas16pf();
	    	$per->per_cod = $this->request->getPost("per_cod");
	    	$per->req_cod = $this->request->getPost("req_cod");
	    	$per->usu_cod = $this->request->getPost("per_cod");
	    	$per->p16_fch = new \Phalcon\Db\RawValue('default');
	    	$per->p16_web = new \Phalcon\Db\RawValue('default');


	    	$this->db->begin();

			if($per->save() == false){
				$this->db->rollback();
				return;
			}


			$preguntas = Preguntas16pf::find();

			

			foreach ($preguntas as  $reg) {
				$r16_cod = 0;
				$p16_cod = $reg->p16_cod;
				
				// if(isset($this->request->getPost($p16_cod)){
					$r16_cod = $this->request->getPost($p16_cod, null, 0); // (valor, sanizar, valor por defecto)
				// }

				$per_det = new Personasdet16pf();
				$per_det->pd_cod = $per->p16_cod;
				$per_det->p16_cod = $p16_cod;
				$per_det->r16_cod = $r16_cod;

				if($per_det->save() == false){
					$this->db->rollback();
					return;
				}

			}


			 $this->db->commit();
			 // echo "Prueba Enviada";
			  $this->dispatcher->forward(array(
		            "controller" => "verPruebas",
		            "action" => "index",
		            "params" => array("resultado" => 1)
        	  ));
		}else{
		     $this->response->redirect("verPruebas/");
		}	
	    	
	}


}

