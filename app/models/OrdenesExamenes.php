<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class OrdenesExamenes extends \Phalcon\Mvc\Model

{

	public  function ordenes($exa_tip, $ord_cod){
            $sql ="SELECT * FROM ordenes_examenes oe INNER JOIN examenes_medicos e ON e.exa_cod=oe.exa_cod WHERE e.exa_tip='$exa_tip' AND oe.ord_cod=$ord_cod ";

        // Base model
        $obj = new ExamenesAsignados();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function ordenesC($exa_tip){
            $sql ="SELECT * FROM  examenes_medicos WHERE exa_tip='$exa_tip' ";

        // Base model
        $obj = new ExamenesAsignados();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function examenesArealizar($ord_cod){
            $sql ="SELECT * FROM ordenes_examenes oe INNER JOIN examenes_medicos e ON e.exa_cod=oe.exa_cod WHERE oe.ord_cod=$ord_cod ";

        // Base model
        $obj = new OrdenesExamenes();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

}





?>