<?php
use Phalcon\Validation\Validator\PresenceOf,
        Phalcon\Validation\Validator\Email,
        Phalcon\Paginator\Adapter\Model as PaginatorModel;
class TextosController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/estados.js');
         
            
    }

    public function indexAction()
    {

        $this->view->setVar("textos", Textos::find(array(
            "tex_est='1'"
        )));

    }


    public function editarAction($tex_cod)
    {
        

        $this->view->setVar("texto", Textos::findFirst($tex_cod));
        
       
    }

    public function nuevoAction()
    {
        

        
       
    }

     public function guardarAction()
    {
        

        // $validation = new Phalcon\Validation();
       
        // $validation->add('est_nom', new PresenceOf(array(
        //     'message' => 'El campo Nombre Es Requerido',

        // )));

        // $validation->add('est_pro', new PresenceOf(array(
        //     'message' => 'El campo Tipo Es Requerido',

        // )));

        


        // $messages = $validation->validate($_POST);
        // if (count($messages)) {
        //     foreach ($messages as $message) {
        //         echo $message;
        //         return false;
        //     }
        // }



        // $estado = new Estados();
        // $estado->est_nom  = $this->request->getPost("est_nom");
        // $estado->est_pro  = $this->request->getPost("est_pro");
        // $estado->est_est = new \Phalcon\Db\RawValue('default');

        
        // if($estado->save()){
        //     echo "1";
        // }else{
        //      foreach ($estado->getMessages() as $message) {
        //         echo "Message: ", $message->getMessage();
        //         echo "Field: ", $message->getField();
        //         echo "Type: ", $message->getType();
        //     }
        // }
        

    }

     public function actualizarAction()
    {
        

        $validation = new Phalcon\Validation();
       
        $validation->add('tex_tex', new PresenceOf(array(
            'message' => 'El campo Texto Es Requerido',

        )));

        $validation->add('tex_tip', new PresenceOf(array(
            'message' => 'El campo Destinatario Es Requerido',

        )));


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $textos = Textos::findFirst($this->request->getPost("tex_cod"));
        $textos->tex_tex  = $this->request->getPost("tex_tex");
        $textos->tex_tip  = $this->request->getPost("tex_tip");

        
        if($textos->save()){
            echo "1";
        }else{
             foreach ($textos->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
        

    }

     public function eliminarAction()
    {
        
        $textos = Textos::findFirst($this->request->getPost("tex_cod"));
        $textos->est_est="0";
        if($textos->save()){
            echo "1";
        }else{
             foreach ($textos->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

}

