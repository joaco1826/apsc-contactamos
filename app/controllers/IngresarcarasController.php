<?php

class IngresarCarasController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js');


         $this->view->setVar("preguntas", CarasPre::find());
         
            
    }

     public function indexAction($per_cod)
    {
        $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $this->view->setVar("req_cod", $req_cod);
    }

    public function guardarAction()
    {
        if ($this->request->isPost() == true) {
            
            $per = new CarasPer();
            $per->per_cod    = $this->request->getPost("per_cod");
            $per->req_cod    = $this->request->getPost("req_cod");
            $per->usu_cod    = $this->request->getPost("per_cod");
            $per->cas_fecha  = new \Phalcon\Db\RawValue('default');
            $per->cas_web    = "int";


            $this->db->begin();

            if($per->save() == false){
                $this->db->rollback();
                return;
            }


            $preguntas = CarasPre::find();

            

            foreach ($preguntas as  $reg) {
                $car_rta = 0;
                $car_cod = $reg->car_cod;
                
                // if(isset($this->request->getPost($p16_cod)){
                    $car_rta = $this->request->getPost($car_cod, null, 0); // (valor, sanizar, valor por defecto)
                // }

                $per_det = new CarasPerDet();
                $per_det->cas_cod = $per->cas_cod;
                $per_det->car_cod = $car_cod;
                $per_det->car_rta = $car_rta;

                if($per_det->save() == false){
                    $this->db->rollback();
                    return;
                }

            }


             $this->db->commit();
             // echo "Prueba Enviada";
             // return;
              $this->dispatcher->forward(array(
                    "controller" => "IngresarCaras",
                    "action" => "index",
                    "params" => array("resultado" => $this->request->getPost("per_cod"))
              ));
        }else{
             $this->response->redirect("IngresarPruebas/");
        }   
            
    }


}
?>