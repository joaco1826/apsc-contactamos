<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;

class GestordocumentalController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        $this->assets
            ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
            ->addCss('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css', false)
            ->addCss('css/estilos.css');
        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', false)
            ->addJs('js/menu.js')
            ->addJs('js/obs.js')
            ->addJs('js/ordenes.js?id='.time());
    }

    public function indexAction()
    {
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_tip = 'C' AND emp_est='1' ORDER BY emp_raz"
        )));
        $this->view->setVar("cargos", Cargos::find(array(
            "car_est = '1' ORDER BY car_des"
        )));
        $est = new Estados();
        $this->view->setVar("estados", $est->listarCont());
    }

    public function newAction()
    {
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_tip = 'C' AND emp_est='1' ORDER BY emp_raz"
        )));
        $this->view->setVar("cargos", Cargos::find(array(
            "car_est = '1' ORDER BY car_des"
        )));
        $est = new Estados();
        $this->view->setVar("estados", $est->listarCont());
    }


    public function consultarcAction()
    {
        $emp_cod = $this->request->getPost("emp_cod");
        $txt_bus = $this->request->getPost("txt_bus");
        $oficina = $this->request->getPost("oficina");
        $estad = $this->request->getPost("estad");
        $ord_doc = $this->request->getPost("completo");
        $dotacion = $this->request->getPost("dotacion");
        $where = "WHERE p.per_est='1' AND (p.per_ide LIKE '%{$txt_bus}%' OR p.per_pno LIKE '%{$txt_bus}%' OR p.per_pap LIKE '%{$txt_bus}%' OR o.ord_car LIKE '%{$txt_bus}%') AND o.ord_est='2' AND oc.con_fre >= '2017-06-01'";
        $currentPage = (int)$this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $filas = $this->request->getPost("filas");
        if (empty($filas)) {
            $filas = 15;
        }
        $orden = new Ordenes();
        if (!empty($emp_cod)) {
            $where .= " AND o.emp_cod=$emp_cod";
        }
        if (!empty($oficina)) {
            $where .= " AND u.mun_cod=$oficina";
        }
        if (!empty($estad)) {
            $where .= " AND p.est_cod=$estad";
        }
        if (!empty($dotacion)) {
            $where .= " AND o.ord_dot='$dotacion'";
        }
        if (!empty($ord_doc) || $ord_doc != "") {
            $where .= " AND o.ord_doc='$ord_doc'";
        } else {
            $where .= " AND (o.ord_doc='$ord_doc' OR o.ord_doc IS NULL)";
        }

//        echo $where;
        $ordenes = $orden->listarGestor($where);
        $paginator = new PaginatorModel(
            array(
                "data" => $ordenes,
                "limit" => $filas,
                "page" => $currentPage
            )
        );
        // Get the paginated results
        $page = $paginator->getPaginate();
        if ($currentPage > 1) {
            $i = $filas * $currentPage - $filas + 1;
        } else {
            $i = 1;
        }
        ?>
        <table class="lista" style="max-width: 1200px;">
            <tr>
                <th>No.</th>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Usuario / Proyecto</th>
                <th>SOT/SIT</th>
                <th>F. Ingreso</th>
                <th>F. Firma</th>
                <th>Cargo</th>
                <th>Empresa</th>
                <th>Oficina</th>
                <th>Estado</th>
                <th>H.V</th>
                <th>Doc.</th>
                <th>Ver orden</th>
                <?
                if (!empty($ord_doc) || $ord_doc != "") {
                    echo '<th>Acción</th>';
                }
                ?>
            </tr>
            <?
            $i = 0;
            $estados = Estados::find(array(
                "est_pro='contratacion' AND est_est='1' ORDER BY est_nom"
            ));
            foreach ($page->items as $ord) {
                $nombre_apirante = $ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap;
                $i++;
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $ord->per_ide ?></td>
                    <td><?= ucwords(strtolower($ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap)) ?></td>
                    <td><?= $ord->emp_raz ?></td>
                    <td><?= $ord->ord_sot ?></td>
                    <td style="width: 70px"><?= $ord->ord_fec ?></td>
                    <td style="width: 70px"><?= $ord->fcon ?></td>
                    <td><?= $ord->ord_car ?></td>
                    <td><?= $ord->tip_des ?></td>
                    <td><?= $ord->mun_nom ?></td>
                    <td>
                        <select name="" id="est<?= $i ?>" style="width: 150px"
                                onchange="cambiarestado('<?= $ord->per_cod ?>', this.value, '<?= $ord->ord_cod ?>')">
                            <option value="">SIN CONTRATAR</option>
                            <?
                            foreach ($estados as $est) {
                                if ($ord->est_cod == $est->est_cod) { ?>
                                    <option selected value="<?= $est->est_cod ?>"><?= $est->est_nom ?></option>
                                <? } else { ?>
                                    <option value="<?= $est->est_cod ?>"><?= $est->est_nom ?></option>
                                <? }
                                ?>
                            <? }
                            ?>
                        </select>
                    </td>
                    <td><a target="_blank"
                           href="<?php echo $this->url->get('Verhojadevida/index') ?>/<?= $ord->per_cod ?>"><span
                                class="citar">Ver</span></a></td>
                    <td><a href="#" onclick="abrirModalDoc('<?=$ord->ord_cod?>');">Doc.</a></td>
                    <td><a target="_blank" href="<?=$this->url->get()?>Ordenes/view/<?=$ord->ord_cod?>">Ver</a></td>
                    <?
                    if (!empty($ord_doc) || $ord_doc != "") { ?>
                        <td><span class="citar" onclick="quitarCompletado(<?=$ord->ord_cod?>, this)">Quitar</span></td>
                    <? }
                    ?>
                </tr>
                <?
            }
            ?>
            <tr>
                <td colspan="14" align="right">
                    <?php echo "<span class='btn ml10' onclick='filtrarDoc(1)'>Primera</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrarDoc(" . $page->before . ")'>Anterior</span>"; ?>
                    <?php
                    $num = ($page->total_items + ($filas - 1)) / $filas;
                    $quinta = $num / 30 + $currentPage;
                    $segunda = $num / 1.1 + $currentPage;
                    for ($i = 1; $i <= $num; $i++) {
                        if ($i <= $quinta || $i >= $segunda) {
                            if ($currentPage == $i) {
                                echo "<span class='btn'>" . $i . "</span>";
                            } else {
                                echo "<span class='btn ml10' onclick='filtrarDoc(" . $i . ")'>" . $i . "</span>";
                            }
                        } else {
                            if ($num < 30) {
                                echo "<span class='btn ml10'  onclick='filtrarDoc(" . $i . ")'>" . $i . "</span>";
                            } else {
                                echo "";
                            }

                        }

                    }
                    ?>
                    <?php echo "<span class='btn ml10' onclick='filtrarDoc(" . $page->next . ")'>Siguiente</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrarDoc(" . $page->last . ")'>Última</span>"; ?>
                    <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span style="margin-left: 5px">filas</span>
                    <select name="filas" id="filas" style="width: 40px" onchange="filtrarDoc(1)">
                        <option <? if ($filas == 15) echo "selected"; ?> value="15">15</option>
                        <option <? if ($filas == 30) echo "selected"; ?> value="30">30</option>
                        <option <? if ($filas == 50) echo "selected"; ?> value="50">50</option>
                        <option <? if ($filas == 100) echo "selected"; ?> value="100">100</option>
                    </select>
                </td>
            </tr>
        </table>
    <? }


    public function eliminarAction()
    {
        $doc = DocumentosOrdenes::findFirst(["document_id=".$this->request->getPost("tdo_cod")." AND ord_cod=".$this->request->getPost("ord_cod")]);
        $ruta = $doc->ruta;
        if (!unlink("files/".$ruta)) {
            echo "Error eliminando archivo";
        }
        if ($doc->delete()) {
            echo "1";
        } else {
            foreach ($doc->getMessages() as $message) {
                echo $message->getMessage(), "<br/>";
            }
        }
    }

    public function eliminardriveAction()
    {
        $doc = DocumentosOrdenes::findFirst(["document_id=".$this->request->getPost("tdo_cod")." AND ord_cod=".$this->request->getPost("ord_cod")]);
        if ($doc->delete()) {
            echo "1";
        } else {
            foreach ($doc->getMessages() as $message) {
                echo $message->getMessage(), "<br/>";
            }
        }
    }

    public function quitarAction()
    {
        $id = $this->request->getPost("id", "int");
        $ord = Ordenes::findFirst($id);
        $ord->ord_doc = "";
        if ($ord->save()) {
            echo "1";
        } else {
            echo "hubo un error, contacte al administrador del sistema";
        }
    }

}
