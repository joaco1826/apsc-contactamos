jQuery(document).ready(function($) {
	$(".fecha").datepicker({
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		yearRange: '-76:+0',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
	})
	$("#btn_guardar").click(function(event) {
		

		
		$.ajax({
			url: $("#url").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize()+"&opcion="+$(this).attr('data-id'),
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Registro Guardado");
				if ($(this).attr('data-id') == 1) {
					$(".siguiente").show();
				} else {
					location.reload();
				}
				
			}else{
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$("#btn_guardar2").click(function(event) {
		
		if($("[name=che_exp]").is(":checked")) {
			exp = 1;
		}else {
			exp = 0;
		}
		
		$.ajax({
			url: $("#url").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize()+"&opcion="+$(this).attr('data-id')+"&exp="+exp,
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Registro Guardado");
				if ($(this).attr('data-id') == 1) {
					$(".siguiente").show();
				} else {
					location.reload();
				}
				
			}else{
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$(".guardar_h").click(function(event) {
		if($("#frm").valid()){
		var op = $(this).attr('data-id');
		var exp;
		if (op == 2) {
			if($("[name=che_exp]").is(":checked")) {
				exp = 1;
			}else {
				exp = 0;
			}
		}
		$.ajax({
			url: $("#url").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize()+"&opcion="+op+"&exp="+exp,
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				// alert("Registro Guardado");
				if (op == 1) {
					$(this).css('display', 'none');
					$(".siguiente").fadeIn(200);
					var strAncla=$("#segtabla");
					$('#central').stop(true,true).animate({
						//realizamos la animacion hacia el ancla
						scrollTop: 1500
					},800);
				} else {
					if(confirm("Registro Guardado. \n¿Desea postularse a una vacante?")) {
						location.href = $("#ver_ofertas").val();
					} else {
						location.reload();
					}
					
				}
				
			}else{
				if(datos==2) {
					alert("Usted se ha postulado a una vacante activa de Contactamos. Lo invitamos a adelantar el proceso de selección, realizando las siguientes pruebas.");
					window.location = $("#url_red").val()
				} else {
					alert(datos)
				}
				
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	} else {
		$('#central').stop(true,true).animate({
            scrollTop: $(".error").offset().top
        }, 1000);
	}
		
	});

	$("#btn_hij").click(function(event) {
		

		
		$.ajax({
			url: $("#url_hij").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			console.log(datos)
			if (datos == 1) {
				$("#hij_nom").val("");
				$("#hij_eda").val("");
				$("#hij_ocu").val("");
				$("#hij_par").val("");
				con_hij($("#per_cod").val());
			} else {
				alert(datos)
			}
			
			
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$("#btn_est").click(function(event) {
		

		
		$.ajax({
			url: $("#url_est").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			// console.log(datos)
			if (datos==1) {
				$("#est_ent").val("");
				$("#est_pro").val("");
				$("#est_fec").val("");
				con_est($("#per_cod").val());
			} else {
				alert(datos)
			}
			
			
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$("#btn_exp").click(function(event) {
		

		
		$.ajax({
			url: $("#url_exp").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			// console.log(datos)
			if (datos == 1) {
				$("#exp_emp").val("");
				$("#exp_car").val("");
				$("#exp_ini").val("");
				$("#exp_ffi").val("");
				$("#exp_mot").val("");
				$("#exp_des").val("");
				con_exp($("#per_cod").val());
			} else {
				alert(datos)
			}
			
			
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$("#btn_ref").click(function(event) {
		

		
		$.ajax({
			url: $("#url_ref").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			// console.log(datos)
			if (datos == 1) {
				$("#fam_nom").val("");
				$("#fam_par").val("");
				$("#fam_tel").val("");
				$("#fam_ocu").val("");
				con_fam($("#per_cod").val());
			} else {
				alert(datos)
			}
			
			
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	// $(".eliminar").click(function(event) {
			

	// 		$.ajax({
	// 			url: $("#url").val(),
	// 			type: 'POST',
	// 			dataType: 'html',
	// 			data: "cod="+$(this).attr('id'),
	// 		})
	// 		.done(function(datos) {
	// 			console.log(datos)
	// 			if(datos==1){
	// 				alert("Registro Eliminado");
	// 				location.reload()
	// 			}else{
	// 				alert(datos)
	// 			}

				
	// 		})
	// 		.fail(function() {
	// 			console.log("error");
	// 		})
	// 		.always(function() {
	// 			console.log("complete");
	// 		});
			
	// });
});

function con_hij(per_cod) {
	$.ajax({
			url: $("#url_chij").val(),
			type: 'POST',
			dataType: 'html',
			data: "per_cod="+per_cod,
		})
		.done(function(datos) {
			// console.log(datos)
			$("#res_hij").html(datos)
			
			
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function con_est(per_cod) {
	$.ajax({
			url: $("#url_cest").val(),
			type: 'POST',
			dataType: 'html',
			data: "per_cod="+per_cod,
		})
		.done(function(datos) {
			// console.log(datos)
			$("#res_est").html(datos)
			
			
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function con_exp(per_cod) {
	$.ajax({
			url: $("#url_cexp").val(),
			type: 'POST',
			dataType: 'html',
			data: "per_cod="+per_cod,
		})
		.done(function(datos) {
			// console.log(datos)
			$("#res_exp").html(datos)
			
			
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function con_fam(per_cod) {
	$.ajax({
			url: $("#url_cref").val(),
			type: 'POST',
			dataType: 'html',
			data: "per_cod="+per_cod,
		})
		.done(function(datos) {
			// console.log(datos)
			$("#res_ref").html(datos)
			
			
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function eliminar(cod, tipo){
	if(tipo=="hijos")
	{
		res = "res_hij"
	}

	if(tipo=="estudios")
	{
		res = "res_est"
	}

	if(tipo=="experiencias")
	{
		res = "res_exp"
	}

	if(tipo=="referencias")
	{
		res = "res_ref"
	}

	$.ajax({
		url: $("#url_eli").val(),
		type: 'POST',
		dataType: 'html',
		data: "cod="+cod+"&tipo="+tipo+"&per_cod="+$("#per_cod").val(),
	})
	.done(function(datos) {
		console.log(datos)
		$("#"+res).html(datos)

		
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

}