<?php

class IpvRes extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $ire_cod;

    /**
     *
     * @var integer
     */
    public $ipr_cod;

    /**
     *
     * @var string
     */
    public $ire_num;

    /**
     *
     * @var string
     */
    public $ire_des;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'ire_cod' => 'ire_cod', 
            'ipr_cod' => 'ipr_cod', 
            'ire_num' => 'ire_num', 
            'ire_des' => 'ire_des'
        );
    }

}
