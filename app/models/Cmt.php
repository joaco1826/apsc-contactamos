<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Cmt extends \Phalcon\Mvc\Model
{

    public  function resultado($per_cod){
        $hoy = date("Y-m-d");
        $fecha = strtotime ( '-720 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
         $sql = "SELECT cmd_pre, cmd_res, cmt.cmt_cod FROM cmt 
				JOIN cmt_det ON cmt.`cmt_cod`=cmt_det.`cmt_cod` 
				WHERE cmt.`per_cod`=$per_cod AND DATE(cmt.cmt_fec) BETWEEN '$fecha' AND '$hoy' LIMIT 75
				";

        // Base model
        $obj = new Cmt();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function resultadoU($per_cod, $req_cod){
        $hoy = date("Y-m-d");
        $fecha = strtotime ( '-90 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
         $sql = "SELECT * FROM cmt 
                WHERE per_cod=$per_cod AND DATE(cmt_fec) BETWEEN '$fecha' AND '$hoy' ORDER BY cmt_cod DESC LIMIT 1
                ";

        // Base model
        $obj = new Cmt();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public static function npruebas($fecha)
    {
        // A raw SQL statement

       $sql ="SELECT COUNT(*) as total
            FROM cmt 
            WHERE DATE(cmt_fec) = '$fecha'";

        // Base model
       $obj = new Cmt();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

}
