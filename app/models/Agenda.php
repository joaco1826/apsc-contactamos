<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Agenda extends \Phalcon\Mvc\Model
{

     public function initialize()
    {
        $this->belongsTo("per_cod", "personas", "per_cod"); //relacion Personas
        $this->belongsTo("usu_cod", "usuarios", "usu_cod"); //relacion Usuarios

    }

    public  function listar($fec_ini, $fec_fin){
        $sql = "SELECT p.per_cod, p.per_ide, p.per_pno, p.per_sno, p.per_pap, p.per_sap,p.per_obs,p.per_tel,p.per_cel,c.cit_cod, c.cit_fec, c.cit_hor,c.est_cod as estado,c.cit_asi,c.cit_obs,u.usu_nom, u.usu_ape, v.val_obs FROM agenda c INNER JOIN personas p ON p.per_cod=c.per_cod INNER JOIN usuarios u ON u.usu_cod=c.usu_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod WHERE c.cit_fec BETWEEN '$fec_ini' AND '$fec_fin' AND u.usu_est='1' ORDER BY c.cit_fec";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarCon($psi, $txt_bus, $fec_ini, $fec_fin){
        $sql = "SELECT p.per_cod, p.per_ide, p.per_pno, p.per_sno, p.per_pap, p.per_sap,p.per_obs,p.per_tel,p.per_cel,c.cit_cod, c.cit_fec, c.cit_hor,c.est_cod as estado,c.cit_asi,c.cit_obs,u.usu_nom, u.usu_ape, v.val_obs FROM agenda c INNER JOIN personas p ON p.per_cod=c.per_cod INNER JOIN usuarios u ON u.usu_cod=c.usu_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod WHERE c.cit_fec BETWEEN '$fec_ini' AND '$fec_fin' AND c.usu_cod='$psi' AND (p.per_pno LIKE '%$txt_bus%' OR p.per_pap LIKE '%$txt_bus%' OR p.per_ide LIKE '%$txt_bus%') AND u.usu_est='1' ORDER BY c.cit_fec";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarBus($txt_bus, $fec_ini, $fec_fin){
        $sql = "SELECT p.per_cod, p.per_ide, p.per_pno, p.per_sno, p.per_pap, p.per_sap,p.per_obs,p.per_tel,p.per_cel,p.est_cod,c.cit_cod, c.cit_fec,c.est_cod as estado, c.cit_hor,c.cit_asi,c.cit_obs,u.usu_nom, u.usu_ape, ca.car_des,r.req_cod, v.val_obs, m.mun_nom, us.usu_nom as nombre, us.usu_ape as apellido FROM agenda c INNER JOIN personas p ON p.per_cod=c.per_cod INNER JOIN usuarios u ON u.usu_cod=c.usu_cod INNER JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN muni m ON m.mun_cod=r.mun_cod INNER JOIN cargos ca ON ca.car_cod=r.car_cod  LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) LEFT JOIN usuarios us ON us.usu_cod=v.usu_cod WHERE DATE(c.cit_fec) BETWEEN '$fec_ini' AND '$fec_fin' AND (p.per_pno LIKE '%$txt_bus%' OR p.per_pap LIKE '%$txt_bus%' OR p.per_ide LIKE '%$txt_bus%') AND u.usu_est='1' ORDER BY c.cit_fec";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarBusE($txt_bus, $fec_ini, $fec_fin, $est_cod){
        $sql = "SELECT p.per_cod, p.per_ide, p.per_pno, p.per_sno, p.per_pap, p.per_sap,p.per_obs,p.per_tel,p.per_cel,p.est_cod,c.cit_cod, c.cit_fec,c.est_cod as estado, c.cit_hor,c.cit_asi,c.cit_obs,u.usu_nom, u.usu_ape, ca.car_des,r.req_cod, v.val_obs, m.mun_nom, us.usu_nom as nombre, us.usu_ape as apellido FROM agenda c INNER JOIN personas p ON p.per_cod=c.per_cod INNER JOIN usuarios u ON u.usu_cod=c.usu_cod INNER JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN muni m ON m.mun_cod=r.mun_cod INNER JOIN cargos ca ON ca.car_cod=r.car_cod  LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) LEFT JOIN usuarios us ON us.usu_cod=v.usu_cod WHERE DATE(c.cit_fec) BETWEEN '$fec_ini' AND '$fec_fin' AND (p.per_pno LIKE '%$txt_bus%' OR p.per_pap LIKE '%$txt_bus%' OR p.per_ide LIKE '%$txt_bus%') AND u.usu_est='1' AND p.est_cod=$est_cod ORDER BY c.cit_fec";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarUsu($txt_bus, $fec_ini, $fec_fin, $usu_cod){
        $sql = "SELECT p.per_cod, p.per_ide, p.per_pno, p.per_sno, p.per_pap, p.per_sap,p.per_obs,p.per_tel,p.per_cel,p.est_cod,c.cit_cod, c.cit_fec, c.cit_hor,c.cit_asi,c.cit_obs,u.usu_nom, u.usu_ape, ca.car_des,r.req_cod, v.val_obs, m.mun_nom, us.usu_nom as nombre, us.usu_ape as apellido FROM agenda c INNER JOIN personas p ON p.per_cod=c.per_cod INNER JOIN usuarios u ON u.usu_cod=c.usu_cod INNER JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN muni m ON m.mun_cod=r.mun_cod INNER JOIN cargos ca ON ca.car_cod=r.car_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) LEFT JOIN usuarios us ON us.usu_cod=v.usu_cod WHERE DATE(c.cit_fec) BETWEEN '$fec_ini' AND '$fec_fin' AND c.usu_cod=$usu_cod AND (p.per_pno LIKE '%$txt_bus%' OR p.per_pap LIKE '%$txt_bus%' OR p.per_ide LIKE '%$txt_bus%') AND u.usu_est='1' ORDER BY c.cit_fec";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarUsuE($txt_bus, $fec_ini, $fec_fin, $usu_cod, $est_cod){
        $sql = "SELECT p.per_cod, p.per_ide, p.per_pno, p.per_sno, p.per_pap, p.per_sap,p.per_obs,p.per_tel,p.per_cel,p.est_cod,c.cit_cod, c.cit_fec, c.cit_hor,c.cit_asi,c.cit_obs,u.usu_nom, u.usu_ape, ca.car_des,r.req_cod, v.val_obs, m.mun_nom, us.usu_nom as nombre, us.usu_ape as apellido FROM agenda c INNER JOIN personas p ON p.per_cod=c.per_cod INNER JOIN usuarios u ON u.usu_cod=c.usu_cod INNER JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN muni m ON m.mun_cod=r.mun_cod INNER JOIN cargos ca ON ca.car_cod=r.car_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) LEFT JOIN usuarios us ON us.usu_cod=v.usu_cod WHERE DATE(c.cit_fec) BETWEEN '$fec_ini' AND '$fec_fin' AND c.usu_cod=$usu_cod AND (p.per_pno LIKE '%$txt_bus%' OR p.per_pap LIKE '%$txt_bus%' OR p.per_ide LIKE '%$txt_bus%') AND u.usu_est='1' AND p.est_cod=$est_cod ORDER BY c.cit_fec";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarEva($txt_bus){
        $sql = "SELECT p.per_cod, p.per_ide, p.per_pno, p.per_sno, p.per_pap, p.per_sap,p.per_obs,p.per_tel,p.per_cel,c.cit_cod, c.cit_fec, c.cit_hor,c.est_cod,c.cit_asi,c.cit_obs,u.usu_nom, u.usu_ape, v.val_obs FROM agenda c INNER JOIN personas p ON p.per_cod=c.per_cod INNER JOIN usuarios u ON u.usu_cod=c.usu_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod WHERE p.per_pno LIKE '%$txt_bus%' OR p.per_pap LIKE '%$txt_bus%' OR p.per_ide LIKE '%$txt_bus%' AND u.usu_est='1' ORDER BY c.cit_fec";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function fechaCita($per_cod){
        $sql = "SELECT * FROM agenda WHERE per_cod=$per_cod ORDER BY cit_cod DESC LIMIT 1";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosB($fechai, $fechaf, $mun_cod){
        $sql = "SELECT * FROM agenda c INNER JOIN usuarios u ON c.cod_usu=u.usu_cod WHERE DATE(c.cit_fec) BETWEEN '$fechai' AND '$fechaf' AND u.mun_cod=$mun_cod AND u.usu_est='1' AND (u.tus_cod=29 OR u.tus_cod=35 OR u.tus_cod=40 OR u.tus_cod=34 OR u.tus_cod=46)";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorPsi($fechai, $fechaf, $usu_cod){
        $sql = "SELECT COUNT(c.cit_cod) as total, u.usu_nom, u.usu_ape FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(c.cit_fec) BETWEEN '$fechai' AND '$fechaf' AND c.cod_usu=$usu_cod AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function valoracionesPorPsi($fechai, $fechaf, $usu_cod){
        $sql = "SELECT COUNT(v.val_cod) as total, u.usu_nom, u.usu_ape FROM valoraciones v INNER JOIN usuarios u ON u.usu_cod=v.usu_cod WHERE DATE(v.val_fch) BETWEEN '$fechai' AND '$fechaf' AND v.usu_cod=$usu_cod AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function valoracionesPorPsiD($fecha, $usu_cod){
        $sql = "SELECT COUNT(v.val_cod) as total, u.usu_nom, u.usu_ape FROM valoraciones v INNER JOIN usuarios u ON u.usu_cod=v.usu_cod WHERE DATE(v.val_fch) = '$fecha' AND v.usu_cod=$usu_cod AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function ordenesPorPsi($fechai, $fechaf, $usu_cod){
        $sql = "SELECT COUNT(o.ord_cod) as total, u.usu_nom, u.usu_ape FROM ordenes o INNER JOIN usuarios u ON u.usu_cod=o.usu_cod WHERE DATE(o.ord_ftr) BETWEEN '$fechai' AND '$fechaf' AND o.usu_cod=$usu_cod AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function ordenesPorPsiD($fecha, $usu_cod){
        $sql = "SELECT COUNT(o.ord_cod) as total, u.usu_nom, u.usu_ape FROM ordenes o INNER JOIN usuarios u ON u.usu_cod=o.usu_cod WHERE DATE(o.ord_ftr) = '$fecha' AND o.usu_cod=$usu_cod AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function ordenesPorPsiC($fechai, $fechaf, $usu_cod){
        $sql = "SELECT COUNT(o.ord_cod) as tot, u.usu_nom, u.usu_ape FROM ordenes o INNER JOIN personas p ON p.per_cod=o.pos_cod INNER JOIN usuarios u ON u.usu_cod=o.usu_cod WHERE DATE(o.ord_ftr) BETWEEN '$fechai' AND '$fechaf' AND o.usu_cod=$usu_cod AND p.per_ciu <> '' AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function ordenesPorPsiCD($fecha, $usu_cod){
        $sql = "SELECT COUNT(o.ord_cod) as tot, u.usu_nom, u.usu_ape FROM ordenes o INNER JOIN personas p ON p.per_cod=o.pos_cod INNER JOIN usuarios u ON u.usu_cod=o.usu_cod WHERE DATE(o.ord_ftr) = '$fecha' AND o.usu_cod=$usu_cod AND p.per_ciu <> '' AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function contratadosPorPsi($fechai, $fechaf, $usu_cod){
        $sql = "SELECT COUNT(o.ord_cod) as total, u.usu_nom, u.usu_ape FROM ordenes o INNER JOIN usuarios u ON u.usu_cod=o.usu_cod INNER JOIN personas p ON p.per_cod=o.pos_cod AND p.est_cod='7' WHERE DATE(o.ord_ftr) BETWEEN '$fechai' AND '$fechaf' AND o.usu_cod=$usu_cod AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }
    public  function contratadosPorPsiD($fecha, $usu_cod){
        $sql = "SELECT COUNT(o.ord_cod) as total, u.usu_nom, u.usu_ape FROM ordenes o INNER JOIN usuarios u ON u.usu_cod=o.usu_cod INNER JOIN personas p ON p.per_cod=o.pos_cod AND p.est_cod='7' WHERE DATE(o.ord_ftr) = '$fecha' AND o.usu_cod=$usu_cod AND u.usu_est='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorturno($fechai, $fechaf){
        $sql = "SELECT COUNT(cit_hor) as total, cit_hor FROM agenda c WHERE DATE(cit_fec) BETWEEN '$fechai' AND '$fechaf' GROUP BY cit_hor";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorturnoD($fecha, $turno){
        $sql = "SELECT COUNT(cit_hor) as total, cit_hor FROM agenda c WHERE DATE(cit_fec) = '$fecha' AND cit_hor='$turno'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorturnoDC($fecha, $turno, $ciudad){
        $sql = "SELECT COUNT(c.cit_hor) as total, c.cit_hor FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(c.cit_fec) = '$fecha' AND c.cit_hor='$turno' AND u.mun_cod=$ciudad";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorturnoDCO($fecha, $turno){
        $sql = "SELECT COUNT(c.cit_hor) as total, c.cit_hor FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(c.cit_fec) = '$fecha' AND c.cit_hor='$turno' AND u.mun_cod NOT IN(126,149)";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorturnoDA($fecha, $turno){
        $sql = "SELECT COUNT(cit_hor) as total, cit_hor FROM agenda c WHERE DATE(cit_fec) = '$fecha' AND cit_hor='$turno' AND cit_asi='1'";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorturnoDCA($fecha, $turno, $ciudad){
        $sql = "SELECT COUNT(c.cit_hor) as total, c.cit_hor FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(c.cit_fec) = '$fecha' AND c.cit_hor='$turno' AND c.cit_asi='1' AND u.mun_cod=$ciudad";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorturnoDCOA($fecha, $turno){
        $sql = "SELECT COUNT(c.cit_hor) as total, c.cit_hor FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(c.cit_fec) = '$fecha' AND c.cit_hor='$turno' AND c.cit_asi='1' AND u.mun_cod NOT IN(126,149)";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorciudad($fechai, $fechaf, $mun_cod){
        $sql = "SELECT COUNT(c.cit_hor) as total, c.cit_hor FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(cit_fec) BETWEEN '$fechai' AND '$fechaf' AND u.mun_cod=$mun_cod GROUP BY c.cit_hor";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosOtras($fechai, $fechaf){
        $sql = "SELECT COUNT(c.cit_hor) as total, c.cit_hor FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(cit_fec) BETWEEN '$fechai' AND '$fechaf' AND u.mun_cod NOT IN(126,149) GROUP BY c.cit_hor";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorciudadA($fechai, $fechaf, $mun_cod){
        $sql = "SELECT COUNT(c.cit_hor) as total, c.cit_hor FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(cit_fec) BETWEEN '$fechai' AND '$fechaf' AND c.cit_asi='1' AND u.mun_cod=$mun_cod GROUP BY c.cit_hor";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosOtrasA($fechai, $fechaf){
        $sql = "SELECT COUNT(c.cit_hor) as total, c.cit_hor FROM agenda c INNER JOIN usuarios u ON u.usu_cod=c.cod_usu WHERE DATE(cit_fec) BETWEEN '$fechai' AND '$fechaf' AND c.cit_asi='1' AND u.mun_cod NOT IN(126,149) GROUP BY c.cit_hor";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function citadosPorturnoSala($fechai, $fechaf){
        $sql = "SELECT COUNT(cit_hor) as total, cit_hor FROM agenda c WHERE DATE(cit_fec) BETWEEN '$fechai' AND '$fechaf' AND cit_asi='1' GROUP BY cit_hor";
        $obj = new Agenda();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }


}
