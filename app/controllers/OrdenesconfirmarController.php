<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;
class OrdenesconfirmarController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
              ->addCss('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css', false)
             ->addCss('css/estilos.css');
        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', false)
            ->addJs('js/menu.js')
            ->addJs('js/obs.js')
            ->addJs('js/ordenes.js');
    }
    public function indexAction()
    {
    	$this->view->setVar("empresas", Empresas::find(array(
                "emp_tip = 'C' AND emp_est='1' ORDER BY emp_raz"
      )));
      $this->view->setVar("cargos", Cargos::find(array(
                "car_est = '1' ORDER BY car_des"
         )));
    }

    public function consultarAction()
    {
      $emp_cod = $this->request->getPost("emp_cod");
      $txt_bus = $this->request->getPost("txt_bus");
      $where = "WHERE p.per_est='1' AND (p.per_ide LIKE '%{$txt_bus}%' OR p.per_pno LIKE '%{$txt_bus}%' OR p.per_pap LIKE '%{$txt_bus}%' OR o.ord_car LIKE '%{$txt_bus}%') AND o.ord_est='1'";
       $currentPage = (int) $this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
         $filas = $this->request->getPost("filas");
        if(empty($filas)) {
            $filas = 15;
        }
        $orden = new Ordenes();
        if(!empty($emp_cod)) {
          $where .= " AND o.emp_cod=$emp_cod";
        }
        $ordenes = $orden->listar($where);
       $paginator   = new PaginatorModel(
        array(
            "data"  => $ordenes,
            "limit" => $filas,
            "page"  => $currentPage
            )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();
        if ($currentPage > 1) {
            $i = $filas * $currentPage - $filas + 1;
        } else {
            $i = 1;
        }
      ?>

      <table class="lista">
  <tr>
    <th>No.</th>
    <th>Cedula</th>
    <th>Nombre</th>
    <th>Usuario / Proyecto</th>
    <th>F. Ingreso</th>
    <th>Cargo</th>
    <th>Empresa</th>
    <th>Estado</th>
    <th>Observaciones</th>
    <th>Acción</th>
    <th>Confirmar</th>
  </tr>
  <?

    $i=0;
    $estados = Estados::find(array(
            "est_pro='contratacion' AND est_est='1' ORDER BY est_nom"
        ));
    foreach ($page->items as  $ord) {
      $nombre_apirante = $ord->per_pno." ".$ord->per_sno." ".$ord->per_pap." ".$ord->per_sap;
      $i++;
      ?>
      <tr>
        <td><?=$i?></td>
        <td><?=$ord->per_ide?></td>
        <td><?=ucwords(strtolower($ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap))?></td>
        <td><?=$ord->emp_raz?></td>
        <td><?=$ord->ord_fec?></td>
        <td><?=$ord->ord_car?></td>
        <td><?=$ord->tip_des?></td>

        <td>
                    <select name="" id="est<?=$i?>" style="width: 150px" onchange="cambiarestado('<?=$ord->per_cod?>', this.value, '<?=$ord->ord_cod?>')">
                    <option value="">SIN CONTRATAR</option>
                    <?
                    foreach ($estados as $est) {
                        if ($ord->est_cod == $est->est_cod) { ?>
                            <option selected value="<?=$est->est_cod?>"><?=$est->est_nom?></option>
                        <? } else { ?>
                            <option value="<?=$est->est_cod?>"><?=$est->est_nom?></option>
                        <? }
                     ?>

                    <? }
                    ?>
                    </select>
                </td>
                <td><?
                if ($ord->per_obs == "") {
                  if ($ord->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                    <span class="citar" onclick="abrirModalO('<?=$ord->per_cod?>','<?=$ord->per_pno." ".$ord->per_pap?>', '', '<?=$ord->val_obs?>')">Agregar</span>
                <? }else {
                  if ($ord->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                    <?=substr($ord->per_obs, 0, 100)?>...<br>
                    <span class="citar" onclick="abrirModalO('<?=$ord->per_cod?>','<?=$ord->per_pno." ".$ord->per_pap?>', '<?=$ord->per_obs?>', '<?=$ord->val_obs?>')">Editar</span>
                <? }
                ?></td>
                <td><a href="<?php echo $this->url->get('Ordenes/editar') ?>/<?php echo $ord->ord_cod; ?>">Editar</a> <a href="#" onclick="eliminar('<?=$ord->ord_cod?>');"><img src="<?php echo $this->url->get()?>img/cerrar.png" alt=""></a></td>
                <td> <span class="citar" onclick="confirmar('<?=$ord->ord_cod?>', this)">Confirmar</span></td>

      </tr>
      <?
    }
  ?>

              <tr>
                <td colspan="13" align="right">
                     <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                     <?php echo "<span class='btn ml10' onclick='filtrar(".$page->before.")'>Anterior</span>"; ?>
                    <?php
                    $num = ($page->total_items + ($filas - 1)) / $filas;
                    $quinta = $num / 10 + $currentPage ;
                    $segunda = $num / 2 + $currentPage ;
                    for ($i=1; $i <= $num; $i++) {

                            if ($i <= $quinta || $i >= $segunda) {
                                if ($currentPage == $i) {
                                    echo "<span class='btn'>".$i."</span>";
                                } else {
                                    echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                                }
                            } else {
                                if ($num < 30){
                                    echo "<span class='btn ml10'  onclick='filtrar(".$i.")'>".$i."</span>";
                                } else {
                                    echo ".";
                                }

                            }




                    }
                    ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->next.")'>Siguiente</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->last.")'>Última</span>"; ?>
                    <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span style="margin-left: 5px">filas</span>
                    <select name="filas" id="filas" style="width: 40px" onchange="filtrar(1)">
                        <option <?if($filas == 15) echo "selected";?> value="15">15</option>
                        <option <?if($filas == 30) echo "selected";?> value="30">30</option>
                        <option <?if($filas == 50) echo "selected";?> value="50">50</option>
                        <option <?if($filas == 100) echo "selected";?> value="100">100</option>
                    </select>
                </td>
            </tr>


</table>

    <? }
    public function confirmarAction() {
        date_default_timezone_get("America/Bogota");
        $ordenes = Ordenes::findFirst($this->request->getPost("ord_cod"));
        $ordenes->ord_est = '2';
        $ordenes->ord_fco = date("Y-m-d");
        if($ordenes->save()){
            echo "1";
        }else{
             foreach ($ordenes->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }


}
