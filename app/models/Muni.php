<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Muni extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $mun_cod;

    /**
     *
     * @var integer
     */
    public $dep_cod;

    /**
     *
     * @var string
     */
    public $mun_cdg;

    /**
     *
     * @var string
     */
    public $mun_nom;


     public function initialize()
    {
        $this->belongsTo("dep_cod", "Dpto", "dep_cod"); //relacion con dpto
        $this->hasMany("mun_cod", "Requisiciones", "mun_cod"); //relacion con requisiciones
     
    }

    public function listarMuniDpto(){
        $sql = " SELECT m.mun_cod, m.mun_nom, m.mun_cor, d.dep_nom FROM Dpto d JOIN Muni m ORDER BY m.mun_nom";
        return $this->getModelsManager()->executeQuery($sql);
       
    }

    public function obtenerMuniDpto($mun_cod){
        $sql = "SELECT m.mun_cod, m.mun_nom, m.mun_cor, d.dep_nom FROM Dpto d JOIN Muni m WHERE m.mun_cod='$mun_cod' ORDER BY m.mun_nom";
        return $this->getModelsManager()->executeQuery($sql);
       
    }

}
