<?php

class EvaluarEntrevistaController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/EvaluarPrueba.js');
         
            
    }

     public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $this->view->setVar("req_cod", $req_cod);
    }

     public function guardarAction()
    {
        if ($this->request->isPost() == true) {
            
            $ent = new Entrevistas();
            $ent->pos_cod = $this->request->getPost("per_cod");
            $ent->req_cod = $this->request->getPost("req_cod");
            $ent->usu_cod = $this->session->get("usu_cod");
            $ent->ent_fec = new \Phalcon\Db\RawValue('default');
            $ent->ent_rip = $this->request->getPost("pru_rip");
            $ent->ent_ahn = $this->request->getPost("pru_ahn");
            $ent->ent_ada = $this->request->getPost("pru_ada");
            $ent->ent_com = $this->request->getPost("pru_com");
            $ent->ent_lid = $this->request->getPost("pru_lid");
            $ent->ent_mot = $this->request->getPost("pru_mot");
            $ent->ent_cap = $this->request->getPost("pru_cap");
            $ent->ent_obs = $this->request->getPost("pru_obs");
            

            if($ent->save() == false){
                $this->db->rollback();
                return;
            }

            echo "1";
        }
    }
}