<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;

class ConfirmarController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        $this->assets
            ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
            ->addCss('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css', false)
            ->addCss('css/estilos.css');
        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', false)
            ->addJs('js/menu.js')
            ->addJs('js/confirmar.js');

    }

    public function indexAction()
    {

    }

    public function consultarAction()
    {
        $currentPage = (int)$this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $filas = $this->request->getPost("filas");
        $oficina = $this->request->getPost("oficina");
        if (empty($filas)) {
            $filas = 15;
        }
        $txt_bus = $this->request->getPost("txt_bus");
        $usu = "";
        if (!empty($oficina)) {
            $usu = " AND u.mun_cod=$oficina";
        }
        $where = "WHERE p.per_est='1' AND DATE(o.ord_ftr) > '2017-05-30' AND (p.per_ide LIKE '%{$txt_bus}%' OR p.per_pno LIKE '%{$txt_bus}%' OR p.per_pap LIKE '%{$txt_bus}%' OR o.ord_car LIKE '%{$txt_bus}%') {$usu}";
        ?>
        <table class="lista">
            <tr>
                <th>No.</th>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Cliente</th>
                <th>Fecha de Ingreso</th>
                <th>Cargo</th>
                <th>Firma de Contrato</th>
                <!-- <th>Documentos</th> -->
            </tr>
            <?
            $orden = new Ordenes();
            $asignados = $orden->listarConfirmacion($where);
            $paginator = new PaginatorModel(
                array(
                    "data" => $asignados,
                    "limit" => $filas,
                    "page" => $currentPage
                )
            );
            // Get the paginated results
            $page = $paginator->getPaginate();
            if ($currentPage > 1) {
                $i = $filas * $currentPage - $filas + 1;
            } else {
                $i = 1;
            }
            foreach ($page->items as $asi) {
                $nombre_apirante = $asi->per_pno . " " . $asi->per_sno . " " . $asi->per_pap . " " . $asi->per_sap;
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $asi->per_ide ?></td>
                    <td><?= $asi->per_pno . " " . $asi->per_sno . " " . $asi->per_pap . " " . $asi->per_sap ?></td>
                    <td><?= $asi->emp_raz ?></td>
                    <td><?= $asi->ord_fec ?></td>
                    <td><?= $asi->ord_car ?></td>
                    <td>
                        <?
                        // $ordenes = OrdenesConfirmar::findFirst("pos_cod=".$asi->per_cod);
                        if ($asi->cod_ord == "") {
                            ?>
                            <a href="#"
                               onclick="enviar('<?= $asi->per_cod ?>', '<?= $asi->codigo ?>', this);">Confirmar</a>
                        <? } else {
                            echo "<strong>Confirmado</strong>";
                        }
                        ?>
                    </td>
                    <!-- <td>

                      <a href="#" > Asignar</a>

                    </td> -->
                </tr>
                <?
                $i++;
            }
            ?>
            <tr>
                <td colspan="7" align="right">
                    <div>
                        <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                        <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->before . ")'>Anterior</span>"; ?>
                        <?php
                        $titem = $page->total_items;
                        if ($titem == 0) {
                            $titem = 1;
                        }
                        $num = ($titem + ($filas - 1)) / $filas;
                        $quinta = $num / 25 + $currentPage;
                        $segunda = $num / 2 + $currentPage;
                        for ($i = 1; $i <= $num; $i++) {
                            // if ($i % 32 == 0) echo "<br>";
                            //     if ($i <= $quinta || $i >= $segunda) {
                            //         if ($currentPage == $i) {
                            //             echo "<span class='btn'>".$i."</span>";
                            //         } else {
                            //             echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                            //         }
                            //     } else {
                            //         if ($num < 30){
                            //             echo "<span class='btn ml10'  onclick='filtrar(".$i.")'>".$i."</span>";
                            //         } else {
                            //             echo ".";
                            //         }
                            //     }
                            if ($i > ($currentPage - 12) and $i < ($currentPage + 12)) {
                                if ($currentPage == $i) {
                                    echo "<span class='btn'>" . $i . "</span>";
                                } else {
                                    echo "<span class='btn ml10' onclick='filtrar(" . $i . ")'>" . $i . "</span>";
                                }
                            }

                        }
                        ?>
                        <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->next . ")'>Siguiente</span>"; ?>
                        <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->last . ")'>Última</span>"; ?>
                        <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span
                                style="margin-left: 5px">filas</span>
                        <select name="filas" id="filas" style="width: 40px" onchange="filtrar(1)">
                            <option <? if ($filas == 15) echo "selected"; ?> value="15">15</option>
                            <option <? if ($filas == 30) echo "selected"; ?> value="30">30</option>
                            <option <? if ($filas == 50) echo "selected"; ?> value="50">50</option>
                            <option <? if ($filas == 100) echo "selected"; ?> value="100">100</option>
                        </select>
                    </div>
                </td>
            </tr>
        </table>
        <?
    }

    public function enviarAction()
    {
        date_default_timezone_get("America/Bogota");
        $hoy = date("Y-m-d H:i:s");
        $obj = new OrdenesConfirmar();
        $obj->pos_cod = $_POST['pos_cod'];
        $obj->ord_cod = $_POST['ord_cod'];
        $obj->con_fre = new \Phalcon\Db\RawValue('default');
        if ($obj->save()) {
            $personas = Personas::findFirst($_POST['pos_cod']);
            $personas->est_cod = 7;
            $personas->per_fecm = $hoy;
            if ($personas->save()) {
                echo "1";
            }

        } else {
            foreach ($obj->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }

    }
}

?>