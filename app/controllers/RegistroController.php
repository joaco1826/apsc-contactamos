<?php
use Phalcon\Validation\Validator\PresenceOf,
	Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\Email;
class RegistroController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
    	 $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos_login.css')
             ->addCss('css/estilos.css');

    	$this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/registro.js?id='.time())
            ->addJs('js/IniciarSesionAspirante.js');
        if ($this->request->getPost("req_cod")) {
            $this->session->set("req_cod", $this->request->getPost("req_cod"));
        }

        $this->view->setVar("fuentes", Tipos::find(array(
                "tip_tip = 'rec' ORDER BY tip_des"
        )));

        $this->view->setVar("cargos", Cargos::find(array(
                "car_est = '1' ORDER BY car_des"
         )));
        $obj = new Requisiciones();
        $this->view->setVar("vacantes", $obj->listarA());
    }

    public function internoAction()
    {
        $this->assets
            ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
            ->addCss('css/estilos_login.css')
            ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/registro.js?id='.time())
            ->addJs('js/IniciarSesionAspirante.js');
        if ($this->request->getPost("req_cod")) {
            $this->session->set("req_cod", $this->request->getPost("req_cod"));
        }

        $this->view->setVar("fuentes", Tipos::find(array(
            "tip_tip = 'rec' ORDER BY tip_des"
        )));

        $this->view->setVar("cargos", Cargos::find(array(
            "car_est = '1' ORDER BY car_des"
        )));
        $obj = new Requisiciones();
        $this->view->setVar("vacantes", $obj->listarA());
    }

     public function guardarAction()
    {


        /*if (isset($_POST['g-recaptcha-response'])) {
            require_once (__DIR__ . '/../library/recaptcha/recaptcha-php/src/autoload.php');

// Register API keys at https://www.google.com/recaptcha/admin
            $siteKey = '6LeN10YUAAAAAI5wNmq_Q5glhETi7bRg1U_x4NWy';
            $secret = '6LeN10YUAAAAALNj8aBcDpQNSNapibg5n2GGYshc';
            // $recaptcha = new \ReCaptcha\ReCaptcha($secret);

            $recaptcha = new \ReCaptcha\ReCaptcha($secret, new \ReCaptcha\RequestMethod\SocketPost());

            // Make the call to verify the response and also pass the user's IP address
            $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
            if ($resp->isSuccess()) {

                $validation = new Phalcon\Validation();

                $validation->add('per_pno', new PresenceOf(array(
                    'message' => 'El campo Nombre Es Requerido',

                )));

                // $validation->add('per_sno', new PresenceOf(array(
                //     'message' => 'El campo Segundo Nombre Es Requerido',

                // )));

                $validation->add('per_pap', new PresenceOf(array(
                    'message' => 'El campo Primer Apellido Es Requerido',

                )));

                // $validation->add('per_sap', new PresenceOf(array(
                //     'message' => 'El campo Segundo Apellido Es Requerido',

                // )));

                $validation->add('per_ide', new PresenceOf(array(
                    'message' => 'El campo No. Identificación Es Requerido',

                )));

                $validation->add('per_ema', new PresenceOf(array(
                    'message' => 'El campo Email Es Requerido',

                )));

                $validation->add('per_ema', new Email(array(
                    'message' => 'El campo Formato de Email no es valido',

                )));

                $validation->add('per_cel', new PresenceOf(array(
                    'message' => 'El campo Teléfono o celular es requerido',

                )));

                $validation->add('per_fre', new PresenceOf(array(
                    'message' => 'El campo fuente de reclutamiento es requerido',

                )));
                $validation->add('car_cod', new PresenceOf(array(
                    'message' => 'El campo cargo de interés es requerido',

                )));

                $validation->add('req_cod', new PresenceOf(array(
                    'message' => 'El campo vacante es requerido',

                )));

                // $validation->add('per_con', new PresenceOf(array(
                //     'message' => 'El campo Contraseña Es Requerido',

                // )));

                // $validation->add('per_con2', new PresenceOf(array(
                //     'message' => 'El campo Repita Contraseña Es Requerido',

                // )));

                // $validation->add('per_con', new Identical(array(
                //    'value'   => $this->request->getPost("per_con2"),
                //    'message' => 'No coinciden las contraseñas'
                // )));

                $validation->add('chk', new Identical(array(
                    'value'   => 1,
                    'message' => 'Debe aceptar los terminos'
                )));




                $messages = $validation->validate($_POST);
                if (count($messages)) {
                    foreach ($messages as $message) {
                        echo $message;
                        return false;
                    }
                }

                $registro = Personas::find(array(
                    "per_ide='".$this->request->getPost("per_ide")."'"
                ));

                if (count($registro) > 0) {
                    echo "Usted ya se encuentra registrado por favor inicie sesión. Le recordamos que su usuario y contraseña son su número de identificación.";
                    return false;
                }

                $persona = new Personas();
                $persona->per_ide  = $this->request->getPost("per_ide");
                $persona->per_pno  = $this->request->getPost("per_pno");
                $persona->per_sno  = $this->request->getPost("per_sno");
                $persona->per_pap  = $this->request->getPost("per_pap");
                $persona->per_sap  = $this->request->getPost("per_sap");
                $persona->per_ema  = $this->request->getPost("per_ema");
                $persona->per_con  = $this->request->getPost("per_ide");
                $persona->sec_nom  = $this->request->getPost("sec_nom");
                $persona->per_cel  = $this->request->getPost("per_cel");
                $persona->per_fre  = $this->request->getPost("per_fre");
                $persona->car_cod  = $this->request->getPost("car_cod");
                $persona->per_est  = new \Phalcon\Db\RawValue('default');
                $persona->per_exp  = new \Phalcon\Db\RawValue('default');
                $persona->per_fin  = new \Phalcon\Db\RawValue('default');


                if($persona->save()){
                    $pos = new Postulaciones();
                    $pos->per_cod = $persona->per_cod;
                    $pos->req_cod = $this->request->getPost("req_cod");
                    $pos->pos_fec = new \Phalcon\Db\RawValue('default');
                    $pos->pos_est = new \Phalcon\Db\RawValue('default');
                    if(!$pos->save()) {
                        echo "Hubo un error - Contacte al administrador del sistema";
                        return false;
                    }
                    echo "1";
                }else{
                    foreach ($persona->getMessages() as $message) {
                        echo "Message: ", $message->getMessage();
                        echo "Field: ", $message->getField();
                        echo "Type: ", $message->getType();
                    }
                }


            }else{
                // If it's not successfull, then one or more error codes will be returned.
                foreach ($resp->getErrorCodes() as $code) {
                    echo $code , '<br> ';
                }

            }
        } else {
            echo "No se envió la petición captcha";
        }*/

        $validation = new Phalcon\Validation();

        $validation->add('per_pno', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));

        // $validation->add('per_sno', new PresenceOf(array(
        //     'message' => 'El campo Segundo Nombre Es Requerido',

        // )));

        $validation->add('per_pap', new PresenceOf(array(
            'message' => 'El campo Primer Apellido Es Requerido',

        )));

        // $validation->add('per_sap', new PresenceOf(array(
        //     'message' => 'El campo Segundo Apellido Es Requerido',

        // )));

        $validation->add('per_ide', new PresenceOf(array(
            'message' => 'El campo No. Identificación Es Requerido',

        )));

        $validation->add('per_ema', new PresenceOf(array(
            'message' => 'El campo Email Es Requerido',

        )));

        $validation->add('per_ema', new Email(array(
            'message' => 'El campo Formato de Email no es valido',

        )));

        $validation->add('per_cel', new PresenceOf(array(
            'message' => 'El campo Teléfono o celular es requerido',

        )));

        $validation->add('per_fre', new PresenceOf(array(
            'message' => 'El campo fuente de reclutamiento es requerido',

        )));
        $validation->add('car_cod', new PresenceOf(array(
            'message' => 'El campo cargo de interés es requerido',

        )));

        $validation->add('req_cod', new PresenceOf(array(
            'message' => 'El campo vacante es requerido',

        )));

        // $validation->add('per_con', new PresenceOf(array(
        //     'message' => 'El campo Contraseña Es Requerido',

        // )));

        // $validation->add('per_con2', new PresenceOf(array(
        //     'message' => 'El campo Repita Contraseña Es Requerido',

        // )));

        // $validation->add('per_con', new Identical(array(
        //    'value'   => $this->request->getPost("per_con2"),
        //    'message' => 'No coinciden las contraseñas'
        // )));

        $validation->add('chk', new Identical(array(
            'value'   => 1,
            'message' => 'Debe aceptar los terminos'
        )));




        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }

        $registro = Personas::find(array(
            "per_ide='".$this->request->getPost("per_ide")."'"
        ));

        if (count($registro) > 0) {
            echo "Usted ya se encuentra registrado por favor inicie sesión. Le recordamos que su usuario y contraseña son su número de identificación.";
            return false;
        }

        $persona = new Personas();
        $persona->per_ide  = $this->request->getPost("per_ide");
        $persona->per_pno  = $this->request->getPost("per_pno");
        $persona->per_sno  = $this->request->getPost("per_sno");
        $persona->per_pap  = $this->request->getPost("per_pap");
        $persona->per_sap  = $this->request->getPost("per_sap");
        $persona->per_ema  = $this->request->getPost("per_ema");
        $persona->per_con  = $this->request->getPost("per_ide");
        $persona->sec_nom  = $this->request->getPost("sec_nom");
        $persona->per_cel  = $this->request->getPost("per_cel");
        $persona->per_fre  = $this->request->getPost("per_fre");
        $persona->car_cod  = $this->request->getPost("car_cod");
        $persona->per_est  = new \Phalcon\Db\RawValue('default');
        $persona->per_exp  = new \Phalcon\Db\RawValue('default');
        $persona->per_fin  = new \Phalcon\Db\RawValue('default');


        if($persona->save()){
            $pos = new Postulaciones();
            $pos->per_cod = $persona->per_cod;
            $pos->req_cod = $this->request->getPost("req_cod");
            $pos->pos_fec = new \Phalcon\Db\RawValue('default');
            $pos->pos_est = new \Phalcon\Db\RawValue('default');
            if(!$pos->save()) {
                echo "Hubo un error - Contacte al administrador del sistema";
                return false;
            }
            echo "1";
        }else{
            foreach ($persona->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }



    }

    public function guardarinternoAction()
    {

                $validation = new Phalcon\Validation();

                $validation->add('per_pno', new PresenceOf(array(
                    'message' => 'El campo Nombre Es Requerido',

                )));

                // $validation->add('per_sno', new PresenceOf(array(
                //     'message' => 'El campo Segundo Nombre Es Requerido',

                // )));

                $validation->add('per_pap', new PresenceOf(array(
                    'message' => 'El campo Primer Apellido Es Requerido',

                )));

                // $validation->add('per_sap', new PresenceOf(array(
                //     'message' => 'El campo Segundo Apellido Es Requerido',

                // )));

                $validation->add('per_ide', new PresenceOf(array(
                    'message' => 'El campo No. Identificación Es Requerido',

                )));

                $validation->add('per_ema', new PresenceOf(array(
                    'message' => 'El campo Email Es Requerido',

                )));

                $validation->add('per_ema', new Email(array(
                    'message' => 'El campo Formato de Email no es valido',

                )));

                $validation->add('per_cel', new PresenceOf(array(
                    'message' => 'El campo Teléfono o celular es requerido',

                )));

                $validation->add('per_fre', new PresenceOf(array(
                    'message' => 'El campo fuente de reclutamiento es requerido',

                )));
                $validation->add('car_cod', new PresenceOf(array(
                    'message' => 'El campo cargo de interés es requerido',

                )));

                $validation->add('req_cod', new PresenceOf(array(
                    'message' => 'El campo vacante es requerido',

                )));

                // $validation->add('per_con', new PresenceOf(array(
                //     'message' => 'El campo Contraseña Es Requerido',

                // )));

                // $validation->add('per_con2', new PresenceOf(array(
                //     'message' => 'El campo Repita Contraseña Es Requerido',

                // )));

                // $validation->add('per_con', new Identical(array(
                //    'value'   => $this->request->getPost("per_con2"),
                //    'message' => 'No coinciden las contraseñas'
                // )));

                $validation->add('chk', new Identical(array(
                    'value'   => 1,
                    'message' => 'Debe aceptar los terminos'
                )));




                $messages = $validation->validate($_POST);
                if (count($messages)) {
                    foreach ($messages as $message) {
                        echo $message;
                        return false;
                    }
                }

                $registro = Personas::find(array(
                    "per_ide='".$this->request->getPost("per_ide")."'"
                ));

                if (count($registro) > 0) {
                    echo "Usted ya se encuentra registrado por favor inicie sesión. Le recordamos que su usuario y contraseña son su número de identificación.";
                    return false;
                }

                $persona = new Personas();
                $persona->per_ide  = $this->request->getPost("per_ide");
                $persona->per_pno  = $this->request->getPost("per_pno");
                $persona->per_sno  = $this->request->getPost("per_sno");
                $persona->per_pap  = $this->request->getPost("per_pap");
                $persona->per_sap  = $this->request->getPost("per_sap");
                $persona->per_ema  = $this->request->getPost("per_ema");
                $persona->per_con  = $this->request->getPost("per_ide");
                $persona->sec_nom  = $this->request->getPost("sec_nom");
                $persona->per_cel  = $this->request->getPost("per_cel");
                $persona->per_fre  = $this->request->getPost("per_fre");
                $persona->car_cod  = $this->request->getPost("car_cod");
                $persona->per_est  = new \Phalcon\Db\RawValue('default');
                $persona->per_exp  = new \Phalcon\Db\RawValue('default');
                $persona->per_fin  = new \Phalcon\Db\RawValue('default');


                if($persona->save()){
                    $pos = new Postulaciones();
                    $pos->per_cod = $persona->per_cod;
                    $pos->req_cod = $this->request->getPost("req_cod");
                    $pos->pos_fec = new \Phalcon\Db\RawValue('default');
                    $pos->pos_est = new \Phalcon\Db\RawValue('default');
                    if(!$pos->save()) {
                        echo "Hubo un error - Contacte al administrador del sistema";
                        return false;
                    }
                    echo "1";
                }else{
                    foreach ($persona->getMessages() as $message) {
                        echo "Message: ", $message->getMessage();
                        echo "Field: ", $message->getField();
                        echo "Type: ", $message->getType();
                    }
                }





    }

    public function iniciarAction()
    {
    	$persona = Personas::find(array(
    		"per_ide='".$this->request->getPost("txt_usu")."' AND per_con='".$this->request->getPost("txt_con")."'"
    	));
    	if(count($persona)==1)
    	{
    		foreach ($persona as  $per)
    		{
	    		$this->session->set("per_cod", $per->per_cod);
	    		$this->session->set("per_nom", $per->per_pno." ".$per->per_sno." ".$per->per_pap." ".$per->per_sap);
	    		echo "1";
    		}

    	}else{
    		echo "Usuario y/o Contraseña incorrectos";
    	}


    }


        public function olvidarAction()
    {
    	$persona = Personas::find(array(
    		"per_ide='".$this->request->getPost("txt_usu")."'"
    	));
    	if(count($persona)==0)
    	{
    		echo "Su N° de identificación no aparece registrado en nuestra base de datos";

    	}else{
    		$obj = new General();
    		$per = Personas::findFirst("per_ide='".$this->request->getPost("txt_usu")."'");
    		$contra = $per->per_con;
    		$body = "Su contraseña es $contra";
    		$obj->enviarCorreo($body, $per->per_ema, "Recuperacion de contrasena");
    		echo "Se ha enviado la  contraseña a su correo electronico";
    	}


    }

}

