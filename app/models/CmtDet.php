<?php

class CmtDet extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cmd_cod;

    /**
     *
     * @var integer
     */
    public $cmt_cod;

    /**
     *
     * @var string
     */
    public $cmd_pre;

    /**
     *
     * @var string
     */
    public $cmd_res;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'cmd_cod' => 'cmd_cod', 
            'cmt_cod' => 'cmt_cod', 
            'cmd_pre' => 'cmd_pre', 
            'cmd_res' => 'cmd_res'
        );
    }

}
