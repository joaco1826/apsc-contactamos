jQuery(document).ready(function($) {
		$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_pag="+$("#txt_pag").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	$("#txt_bus").keypress(function(e) {
		if(e.which == 13) {
			$.ajax({
				url: $("#url_consultar").val(),
				type: 'POST',
				dataType: 'html',
				data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&filas="+$("#filas").val()+"&car_cod="+$("#car_cod").val()+"&car_int="+$("#car_int").val()+"&mun_cod="+$("#mun_cod").val(),
				 beforeSend: function() {
					$("#respuesta").html("Cargando...")
	  			 }
			})

			.done(function(datos) {
				// console.log(datos)
				$("#respuesta").html(datos)

				
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
	});
	$("#cit_fec").datepicker({
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
	})

});

function filtrar(pag, cod) {
	$("#txt_pag").val(pag);
	if (cod == 1) {
		$("#car_int").val("");
	} else {
		if (cod == 2) {
			$("#mun_cod").val("");
			$("#car_cod").val("");
		}
	}

	$.ajax({
		url: $("#url_consultar").val(),
		type: 'POST',
		dataType: 'html',
		data: "txt_pag="+$("#txt_pag").val()+"&txt_bus="+$("#txt_bus").val()+"&car_cod="+$("#car_cod").val()+"&car_int="+$("#car_int").val()+"&mun_cod="+$("#mun_cod").val()+"&mun_res="+$("#mun_res").val()+"&estado="+$("#estado").val()+"&filas="+$("#filas").val(),
		 beforeSend: function() {
			$("#respuesta").html("Cargando...")
			 }
	})

	.done(function(datos) {
		// console.log(datos)
		$("#respuesta").html(datos)

		
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function eliminar(id){
	if (confirm("Desea realmente eliminar este aspirante?")) {
	$.ajax({
			url: $("#url_eliminar").val(),
			type: 'POST',
			dataType: 'html',
			data: "id="+id,
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			console.log(datos)
			alert("Registro eliminado")
			$("#respuesta").html("")
			$("#txt_bus").focus();
			$("#txt_bus").val("");
			location.reload();

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	}
}

function abrirModal(cod, nombre, req_cod) {
	$(".negro").fadeIn(100);
	$("#modal_documentos").fadeIn(100);
	$("#IsName").html(nombre);
	$("#per_cod").val(cod);
	$("#req_cod").val(req_cod);
}

function abrirModalP(per_cod, req_cod) {
	$(".negro").css("display", "block")
	$("#modal_documentos_p").css("display", "block")
	$("#per_cod").val(per_cod)

	$.ajax({
		url: $("#url_pruebas").val(),
		type: 'POST',
		dataType: 'html',
		data: "per_cod="+per_cod+"&req_cod="+req_cod,
		 beforeSend: function() {
			 }
	})

	.done(function(datos) {
		$("#modal_documentos_p").html(datos);

		
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function cerrarModal(per_cod){
	$(".negro").css("display", "none")
	$("#modal_documentos").css("display", "none")
	$("#modal_documentos_p").css("display", "none")
	$("#modal_documentos_o").css("display", "none")
}

function guardar(usu_cod) {
	if (usu_cod == "") {
		alert("Seleccione a la psicóloga");
		return false;
	}
	$.ajax({
		url: $("#url_guardar").val(),
		type: 'POST',
		dataType: 'html',
		data: "usu_cod="+usu_cod+"&cit_fec="+$("#cit_fec").val()+"&cit_hor="+$("#cit_hor").val()+"&per_cod="+$("#per_cod").val()+"&req_cod="+$("#req_cod").val(),
		 beforeSend: function() {
			 }
	})

	.done(function(datos) {
		console.log(datos)
		if (datos == 2) {
			alert("La sessión ha caducado");
			location.reload();
		}
		if (datos == 1) {
			alert("Cita guardada exitosamente");
			// location.reload();
		} else {
			alert(datos);
			return false;
		}

		
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function cambiarestado(per_cod, est_cod) {
	$.ajax({
			url: $("#url_estado").val(),
			type: 'POST',
			dataType: 'html',
			data: "per_cod="+per_cod+"&est_cod="+est_cod,
			 beforeSend: function() {
				// $("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			console.log(datos)
			if (datos == 1) {
				// alert("Estado cambiado");
			} else {
				alert(datos);
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}
