<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Assessment extends \Phalcon\Mvc\Model
{

	 public function getSource()
    {
        return "ass";
    }

    public  function obtener($pos_cod){
        $sql ="SELECT *, cp.nombre as name FROM ass e INNER JOIN comp_ass ce ON ce.ass_cod=e.ass_cod INNER JOIN competencias cp ON cp.id=ce.com_cod WHERE e.pos_cod=$pos_cod ORDER BY e.ass_cod DESC";

        // Base model
        $obj = new Assessment();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

}
