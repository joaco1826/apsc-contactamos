<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email,
        Phalcon\Paginator\Adapter\Model as PaginatorModel;
class ConsultaraspiranteController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
              ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/obs.js')
            ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false)
            ->addJs('js/consultarPostulante.js');
    }

    public function indexAction()
    {

        $this->view->setVar("cargos", Cargos::find(array(
                "car_est = '1' ORDER BY car_des"
         )));

         $mun = new Muni();
         $this->view->setVar("municipios", $mun->listarMuniDpto());

         $req = new Requisiciones();
         $this->view->setVar("requisiciones", $req->listar());

          $this->view->setVar("estados", Estados::find(array(
                "est_est = '1'"
         )));

          $this->view->setVar("usuarios", Usuarios::find(array(
                "(tus_cod = '29' OR tus_cod='40' OR tus_cod='34') AND usu_est='1' ORDER BY usu_nom"
         )));

    }

     public function consultarAction()
    {

        $txt_bus = $this->request->getPost("txt_bus");
        $car_cod = $this->request->getPost("car_cod");
        $car_int = $this->request->getPost("car_int");
        $mun_cod = $this->request->getPost("mun_cod");
        $mun_res = $this->request->getPost("mun_res");
        $estado = $this->request->getPost("estado");
        $filas = $this->request->getPost("filas");
        if(empty($filas)) {
            $filas = 15;
        }
        $pers =  new Personas();
        if (!empty($estado)) {
            $where = "WHERE p.per_est = '1' AND (p.per_pno LIKE '%${txt_bus}%' OR p.per_pap LIKE '%${txt_bus}%' OR p.per_ide LIKE '%${txt_bus}%') AND p.est_cod='{$estado}'";
        } else {
            $where = "WHERE p.per_est = '1' AND (p.per_pno LIKE '%${txt_bus}%' OR p.per_pap LIKE '%${txt_bus}%' OR p.per_ide LIKE '%${txt_bus}%')";
        }

        if (!empty($mun_cod) and !empty($car_cod) and empty($mun_res)) {
            $where .= " AND r.mun_cod={$mun_cod} AND r.req_til='{$car_cod}'";
            $personas = $pers->listarBusCM($where);
        }elseif (!empty($mun_cod) and !empty($car_cod) and !empty($mun_res)) {
            $where .= " AND r.mun_cod={$mun_cod} AND r.req_til='{$car_cod}' AND p.per_ciu={$mun_res}";
            $personas = $pers->listarBusCM($where);
        }elseif (!empty($car_cod) and empty($mun_cod) and !empty($mun_res) ) {
            $where .= " AND r.req_til='{$car_cod}' AND p.per_ciu={$mun_res}";
            $personas = $pers->listarBusC($where);
        }elseif (!empty($car_cod) and empty($mun_cod) and empty($mun_res)) {
            $where .= " AND r.req_til='{$car_cod}'";
            $personas = $pers->listarBusC($where);
        }elseif (empty($car_cod) and !empty($mun_cod) and !empty($mun_res)) {
            $where .= " AND r.mun_cod={$mun_cod} AND p.per_ciu={$mun_res}";
            $personas = $pers->listarBusM($where);
        }elseif (empty($car_cod) and !empty($mun_cod) and empty($mun_res)) {
            $where .= " AND r.mun_cod={$mun_cod}";
            $personas = $pers->listarBusM($where);
        }elseif (!empty($car_int) and !empty($mun_res)) {
            $where .= " AND p.car_cod={$car_int} AND p.per_ciu={$mun_res}";
            $personas = $pers->listarInt($where);
        }elseif (!empty($car_int) and empty($mun_res)) {
            $where .= " AND p.car_cod={$car_int}";
            $personas = $pers->listarInt($where);
        }elseif (!empty($mun_res)) {
            $where .= " AND p.per_ciu={$mun_res}";
            $personas = $pers->listarInt($where);
        } else {
            $personas = $pers->listarBus($where);
        }



        $currentPage = (int) $this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
       $paginator   = new PaginatorModel(
        array(
            "data"  => $personas,
            "limit" => $filas,
            "page"  => $currentPage
            )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();
        ?>
        <table class="lista">
             <tr>
                <th>No.</th>
                <th>Ciudad</th>
                <th>No. Documento</th>
                <th>Nombres</th>
                <th>Vacante/<br>Cargo interés</th>
                <th>Cita</th>
                <th>Estado</th>
                <th>Pruebas</th>
                <th>Acciones</th>
                <th>Observaciones</th>
                <th>Fecha última modificación</th>
             </tr>
        <?

        $estado = new Estados();
        $no = 'agenda' . ',' . 'contratacion';
        $estados = $estado->listarNotIn('agenda');
        if ($currentPage > 1) {
            $i = $filas * $currentPage - $filas + 1;
        } else {
            $i = 1;
        }

        foreach ($page->items as  $per) {
            ?>
                <tr>
                    <td><?=$i?></td>
                    <td><?=$per->mun_nom?></td>
                    <td><?=$per->per_ide?></td>
                    <td><?=ucwords(strtolower($per->per_pno . " " . $per->per_sno . " " . $per->per_pap . " " . $per->per_sap))?></td>
                    <td><?=$per->req_til?></td>
                    <td>
                       <?
                        // $fecha = $agenda->fechaCita($per->per_cod)->cit_fec;
                        $fecha = Agenda::find(array(
                            "per_cod='$per->cod' ORDER BY cit_cod DESC LIMIT 1"
                        ));
                        if (count($fecha) > 0) {
                            $date = $fecha[0]->cit_fec;
                            if (strtotime($date) < strtotime(date("Y-m-d"))) { ?>
                                <span class="citar" onclick="abrirModal('<?=$per->cod?>','<?=$per->per_pno." ".$per->per_pap?>','<?=$per->req?>')">Citar</span>
                            <? } else {
                                echo 'Citado';
                             }

                        } else { ?>
                            <span class="citar" onclick="abrirModal('<?=$per->cod?>','<?=$per->per_pno." ".$per->per_pap?>','<?=$per->req?>')">Citar</span>
                        <? }

                        ?>

                    </td>
                    <td>
                        <select onchange="cambiarestado('<?=$per->cod?>', this.value)" style="width: 150px" id="estados_<?=$i?>">
                        <option value="">SIN PROCESO</option>
                        <?

                            foreach ($estados as $est) {
                                if ($est->est_cod == $per->est_cod) { ?>
                                    <option selected value="<?=$est->est_cod?>"><?=$est->est_nom?></option>
                                <? } else { ?>
                                    <option value="<?=$est->est_cod?>"><?=$est->est_nom?></option>
                                <? }?>

                            <? }
                        ?>
                        </select>
                    </td>
                    <td><a href="#" onclick="abrirModalP('<?=$per->cod?>', '<?=$per->req?>');">Ver Pruebas</a></td>
                     <td><a target="_blank" href="<?php echo $this->url->get('Verhojadevida/index') ?>/<?=$per->cod?>"><span class="citar">Ver</span></a><a target="_blank" href="<?php echo $this->url->get('editarAspirante/index') ?>/<?=$per->cod?>"><img src="<?php echo $this->url->get()?>img/icon3.png" alt=""></a><a href="#" onclick="eliminar('<?=$per->cod?>');"><img src="<?php echo $this->url->get()?>img/cerrar.png" alt=""></a></td>
                      <td><?
                    if ($per->per_obs == "") {
                        if ($per->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                        <span class="citar" onclick="abrirModalO('<?=$per->cod?>')">Agregar</span>
                    <? }else {
                        if ($per->val_obs != "") echo "<b>Valorado</b><br>";
                        ?>
                        <?=substr($per->per_obs, 0, 100)?>...<br>
                        <span class="citar" onclick="abrirModalO('<?=$per->cod?>')">Editar</span>
                    <? }
                    ?></td>
                    <td><?if ($per->per_fecm != "") { echo date("d-m-Y H:i:s", strtotime($per->per_fecm)); }?></td>
                </tr>
            <?
            $i++;
        }
        ?>
             <tr>
                <td colspan="11" align="right">
                     <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                     <?php echo "<span class='btn ml10' onclick='filtrar(".$page->before.")'>Anterior</span>"; ?>
                    <?php
                    $titem = $page->total_items;
                    if ($titem == 0) {
                        $titem = 1;
                    }
                    $num = ($titem + ($filas - 1)) / $filas;
                    $quinta = $num / 25 + $currentPage ;
                    $segunda = $num / 2 + $currentPage ;
                    for ($i=1; $i <= $num; $i++) {
                        // if ($i % 32 == 0) echo "<br>";

                        //     if ($i <= $quinta || $i >= $segunda) {
                        //         if ($currentPage == $i) {
                        //             echo "<span class='btn'>".$i."</span>";
                        //         } else {
                        //             echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                        //         }
                        //     } else {
                        //         if ($num < 30){
                        //             echo "<span class='btn ml10'  onclick='filtrar(".$i.")'>".$i."</span>";
                        //         } else {
                        //             echo ".";
                        //         }

                        //     }

                        if ($i > ($currentPage - 12) and $i < ($currentPage + 12)) {
                            if ($currentPage == $i) {
                                echo "<span class='btn'>".$i."</span>";
                            } else {
                                echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                            }
                        }




                    }
                    ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->next.")'>Siguiente</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->last.")'>Última</span>"; ?>
                    <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span style="margin-left: 5px">filas</span>
                    <select name="filas" id="filas" style="width: 40px" onchange="filtrar(1)">
                        <option <?if($filas == 15) echo "selected";?> value="15">15</option>
                        <option <?if($filas == 30) echo "selected";?> value="30">30</option>
                        <option <?if($filas == 50) echo "selected";?> value="50">50</option>
                        <option <?if($filas == 100) echo "selected";?> value="100">100</option>
                    </select>
                    <span style="margin-left: 10px">Núm. de Reg <?=$titem?></span>
                </td>
            </tr>
        </table>
        <?
    }

    public function eliminarAction()
    {
        $persona = Personas::findFirst($this->request->getPost("id"));
        $persona->per_est  = 0;


        if($persona->save()){
            echo "1";
        }else{
             foreach ($persona->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }

    }

    public function verpruebasAction()
    {
        $per_cod = $this->request->getPost("per_cod");
        $req_cod = $this->request->getPost("req_cod");
        $req = new Requisiciones();
        $prueba = $req->getPruebasReq($req_cod);
        ?>
        <table class="form" width="100%">
            <tr>
                <th>Seleccione una prueba a continuación</th>
            </tr>
            <tr>
                <td>
                    <?if($prueba[0]->car_16p == 1){?>
                    <table class="caja_prueba">
                        <tr>
                            <th><img src="img/chulo.png"> 16 PF</th>
                        </tr>
                        <tr>
                            <td><a target="_blank" data-url="VerPF/index" href="VerPF/index/<?=$per_cod?>" class="boton" target="_blank">Abrir Prueba</a></td>
                        </tr>
                    </table>
                    <?}?>
                    <?if($prueba[0]->car_pnl == 1){?>
                    <table class="caja_prueba">
                        <tr>
                            <th><img src="img/chulo.png"> PNL</th>
                        </tr>
                        <tr>
                            <td><a target="_blank" data-url="VerPNL/index" href="VerPNL/index/<?=$per_cod?>" class="boton">Abrir Prueba</a></td>
                        </tr>
                    </table>
                    <?}?>
                    <?if($prueba[0]->car_car == 1){?>
                    <table class="caja_prueba">
                        <tr>
                            <th><img src="img/chulo.png"> CARAS</th>
                        </tr>
                        <tr>
                            <td><a target="_blank" data-url="VerCaras/index" href="VerCaras/index/<?=$per_cod?>" class="boton" target="_blank">Abrir Prueba</a></td>
                        </tr>
                    </table>
                    <?}?>
                    <?if($prueba[0]->car_ipv == 1){?>
                    <table class="caja_prueba">
                        <tr>
                            <th><img src="img/chulo.png"> IPV</th>
                        </tr>
                        <tr>
                            <td><a target="_blank" data-url="VerIPV/index" href="VerIPV/index/<?=$per_cod?>" class="boton" target="_blank">Abrir Prueba</a></td>
                        </tr>
                    </table>
                    <?}?>
                    <?if($prueba[0]->car_val == 1){?>
                    <table class="caja_prueba">
                        <tr>
                            <th><img src="img/chulo.png"> VALANTI</th>
                        </tr>
                        <tr>
                            <td><a target="_blank" data-url="VerValanti/index" href="VerValanti/index/<?=$per_cod?>" class="boton" target="_blank">Abrir Prueba</a></td>
                        </tr>
                    </table>
                    <?}?>
                    <?if($prueba[0]->car_cmt == 1){?>
                    <table class="caja_prueba">
                        <tr>
                            <th><img src="img/chulo.png"> CMT</th>
                        </tr>
                        <tr>
                            <td><a target="_blank" data-url="ver_cmt/index" href="ver_cmt/index/<?=$per_cod?>" class="boton" target="_blank">Abrir Prueba</a></td>
                        </tr>
                    </table>
                    <?}?>
                    <table class="caja_prueba">
                        <tr>
                            <th><img src="img/chulo.png"> Ver Perfil</th>
                        </tr>
                        <tr>
                            <td><a target="_blank" data-url="verPerfil/index" href="verPerfil/index/<?=$per_cod . "-" . $req_cod?>" class="boton" target="_blank">Ver Perfil</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    <? }

     public function estadoAction()
     {
        $hoy = date("Y-m-d H:i:s");
        $personas = Personas::findFirst($this->request->getPost("per_cod"));
        $personas->est_cod = $this->request->getPost("est_cod");
        $personas->per_fecm = $hoy;
        if($personas->save()){
            if ($this->request->getPost("est_cod") == 8 || $this->request->getPost("est_cod") == 35 || $this->request->getPost("est_cod") == 36) {
                $body = "<h3>Estado de laboratorio</h3><br>";
                $body .= "<table border='1' width='900px'>
                <tr>
                  <td>Identificación</td>
                  <td>Nombres</td>
                  <td>Usuaria / Proyecto</td>
                  <td>Cargo</td>
                  <td>Salario</td>
                  <td>Empresa</td>
                  <td>Fecha Contratación</td>
                   <td>Psicóloga</td>
                   <td>Estado</td>
                </tr>";
                $per = Personas::findFirst($this->request->getPost("per_cod"));
                if (!$this->request->getPost("ord_cod")) {
                    echo "0";
                    return false;
                }
                $ord = Ordenes::findFirst($this->request->getPost("ord_cod"));
                $empresa = Empresas::findFirst($ord->emp_cod);
                $body .= "<tr>
                            <td>".$per->per_ide."</td>
                              <td>".$per->per_pno." ".$per->per_sno." ".$per->per_pap." ".$per->per_sap."</td>
                              <td>".$empresa->getEmpRaz()."</td>
                              <td>".$ord->ord_car."</td>
                              <td>".$ord->ord_sal."</td>
                              <td>".Tipos::findFirst($ord->tip_cod)->tip_des."</td>
                              <td>".$ord->ord_fec."</td>
                               <td>".Usuarios::findFirst($ord->usu_cod)->usu_nom." ".Usuarios::findFirst($ord->usu_cod)->usu_ape."</td>
                               <td>".Estados::findFirst($this->request->getPost("est_cod"))->est_nom."</td>
                        </tr>";
                $body .= "</table>";
                $correo = Usuarios::findFirst($ord->usu_cod)->usu_ema;
                $cor = new General();
                $res = $cor->enviarCorreoLA($body, "Estado de laboratorio ".$per->per_pno." ".$per->per_sno." ".$per->per_pap." ".$per->per_sap, $correo, $empresa->getEmpExa());
                if ($res == 1) {
                    echo "1";
                } else {
                    echo $cor;
                }
            } elseif ($this->request->getPost("est_cod") == 7 || $this->request->getPost("est_cod") == 45) {
                $orden = Ordenes::findFirst($this->request->getPost("ord_cod"));
                $orden->fecha_correo = date("Y-m-d");
                $orden->save();
                $empresa = Empresas::findFirst($orden->emp_cod);
                $tipo = Tipos::findFirst($orden->tip_cod);
                $user = Usuarios::findFirst($orden->usu_cod);
                $per = Personas::findFirst($this->request->getPost("per_cod"));
                $cor = new General();
                $obs = Textos::findFirst(6)->tex_tex;
                $nombre_aspirante = ucwords($per->per_pno . " " . $per->per_sno . " " . $per->per_pap . " " . $per->per_sap);
                $asunto = "Firma de Contrato " . $nombre_aspirante;
                $obs .= "<table border='1' width='900px'><tr><th>CEDULA</th>  <th>NOMBRE COMPLETO</th>  <th>TELEFONO</th>  <th>CORREO</th><th>CARGO</th><th>SALARIO</th><th>EMPRESA</th><th>FECHA DE INGRESO</th><th>TALLA<br>CAMISA</th><th>TALLA<br>PANTALÓN</th><th>TALLA<br>ZAPATOS</thZ</tr>
            <tr><td style='padding: 5px 10px'>" . $per->per_ide . "</td><td style='padding: 5px 10px'>" . strtoupper($per->per_pno) . " " . strtoupper($per->per_sno) . " " . strtoupper($per->per_pap) . " " . strtoupper($per->per_sap) . "</td><td style='padding: 5px 10px'>" . $per->per_tel . " " . $per->per_cel . "</td><td style='padding: 5px 10px'>" . strtoupper($per->per_ema) . "</td><td style='padding: 5px 10px'>" . strtoupper($orden->ord_car) . "</td><td style='padding: 5px 10px'>" . $orden->ord_sal . "</td><td style='padding: 5px 10px'>" . $empresa->getEmpRaz() . "</td><td style='padding: 5px 10px'>" . strtoupper($orden->ord_fec) . "</td><td style='padding: 5px 10px'>" . strtoupper($per->per_tca) . "</td><td style='padding: 5px 10px'>" . strtoupper($per->per_tpa) . "</td><td style='padding: 5px 10px'>" . strtoupper($per->per_tza) . "</td></tr></table>";
                $obs .= "<br><br><b>" . $tipo->tip_des . "</b>";
                $destinatario = $empresa->getEmpEma();
                if ($user->mun_cod == 149) {
                    $res = $cor->enviarCorreoConfirmarBogota($obs, $destinatario, $asunto);
                } else {
                    $res = $cor->enviarCorreoConfirmar($obs, $destinatario, $asunto);
                }
                if ($res == 1) {
                    echo "1";
                } else {
                    echo $res;
                }
            } else {
                echo "1";
            }


        }else{
             foreach ($personas->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

     public function consultoAction() {
        $per = Personas::findFirst($this->request->getPost("per_cod"));
        $val = Valoraciones::find(array("pos_cod=".$per->per_cod . " ORDER BY val_cod DESC LIMIT 1"));
        if (count($val) > 0) {
            $usu = Usuarios::findFirst($val[0]->usu_cod);
        }

        ?>
        <table class="form" width="100%">
            <tr>
                <th colspan="2">Observaciones</th>
            </tr>
            <tr>
                <td>
                    Nombre
                </td>
                <td>
                    <input type="hidden" value="<?=$per->per_cod?>" id="cod" name="cod">
                    <span id="IsNameO"><?=$per->per_pno . " " . $per->per_pap?></span>
                </td>
            </tr>
            <tr>
                <td>Observaciones</td>
                <td><textarea id="per_obs" name="per_obs" rows="10"><?=$per->per_obs?></textarea></td>
            </tr>
            <tr>
                <td>Valoración: </td>
                <td id="valoracion">

                    <?if(count($val) > 0){ echo $val[0]->val_obs . " - <b>" . $usu->usu_nom . " " . $usu->usu_ape . "</b>"; }?>
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" class="boton" onclick="observaciones();" value="Guardar"></td>
            </tr>
        </table>
        <?
     }

     public function observacionesAction()
     {
        $hoy = date("Y-m-d H:i:s");
        $personas = Personas::findFirst($this->request->getPost("per_cod"));
        $personas->per_obs = $this->request->getPost("per_obs");
        $personas->per_fecm = $hoy;
        if($personas->save()){
            echo "1";
        }else{
             foreach ($personas->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }




}

