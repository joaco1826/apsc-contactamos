<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Ordenes extends \Phalcon\Mvc\Model
{

    public function listar($where)
    {
        $sql = "SELECT * FROM ordenes o

					JOIN personas p on o.pos_cod=p.per_cod AND o.ord_cod=(SELECT MAX(ord_cod) FROM ordenes WHERE pos_cod=p.per_cod)

					JOIN empresas e on o.emp_cod=e.emp_cod
                    JOIN usuarios u on u.usu_cod=o.usu_cod
                    JOIN muni m on u.mun_cod=m.mun_cod

					JOIN tipos t on o.tip_cod=t.tip_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE per_cod=p.per_cod) {$where} ";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function listarAfiliaciones($where)
    {
        $sql = "SELECT p.per_cod, p.per_ide, p.per_pno, p.per_sno, p.per_pap, p.per_sap, p.est_cod, e.emp_raz, o.pos_cod, o.ord_fec, o.ord_car, v.reingreso, v.arl, v.eps, v.caja, v.pension, v.banco, t.tip_des, m.mun_nom, o.ord_cod as cod FROM ordenes o

					JOIN personas p on o.pos_cod=p.per_cod AND o.ord_cod=(SELECT MAX(ord_cod) FROM ordenes WHERE pos_cod=p.per_cod)
					AND (p.est_cod='7' OR p.est_cod='45') AND o.ord_fec > '2017-08-16'

					JOIN empresas e on o.emp_cod=e.emp_cod
                    JOIN usuarios u on u.usu_cod=o.usu_cod
                    JOIN muni m on u.mun_cod=m.mun_cod

					JOIN tipos t on o.tip_cod=t.tip_cod 
					LEFT JOIN afiliaciones v ON v.ord_cod=o.ord_cod
					{$where}  ";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function listarGestor($where)
    {
        $sql = "SELECT *, date(oc.con_fre) as fcon FROM ordenes o
                    JOIN ordenes_confirmar oc ON oc.ord_cod=o.ord_cod
					JOIN personas p on o.pos_cod=p.per_cod

					JOIN empresas e on o.emp_cod=e.emp_cod
                    JOIN usuarios u on u.usu_cod=o.usu_cod
                    JOIN muni m on u.mun_cod=m.mun_cod

					JOIN tipos t on o.tip_cod=t.tip_cod {$where} ORDER BY oc.con_fre DESC";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function listarAsignacionExamen($txt)
    {
        $sql = "SELECT * FROM examenes_asignados e

                JOIN empresas p on e.pro_cod=p.emp_cod

                JOIN personas per on e.pos_cod=per.per_cod

                WHERE per.per_ide LIKE '%{$txt}%' or per.per_pno LIKE '%{$txt}%' or per.per_pap LIKE '%{$txt}%'";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function listarConfirmacion($where)
    {
        $sql = "SELECT *, o.ord_cod as codigo, oc.ord_cod as cod_ord, DATE(oc.con_fre) as fecha FROM ordenes o
                INNER JOIN usuarios u ON u.usu_cod=o.usu_cod 

                INNER JOIN personas p on o.pos_cod=p.per_cod

                INNER JOIN empresas e on o.emp_cod=e.emp_cod
                INNER JOIN afiliaciones a ON a.ord_cod=o.ord_cod
                LEFT JOIN ordenes_confirmar oc ON oc.ord_cod=o.ord_cod {$where} AND (p.est_cod='7' OR p.est_cod='45') AND a.banco='SI' AND a.arl<>'' AND a.eps<>'' AND a.pension<>'' AND a.caja<>''  ORDER BY con_cod";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function consecutivo()
    {
        $sql = "SELECT MAX(ord_cod) as cons FROM ordenes";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function ordenesPorCliente($fechai, $fechaf, $emp_cod)
    {
        $sql = "SELECT COUNT(emp_cod) as total  FROM ordenes WHERE DATE(ord_ftr) BETWEEN '$fechai' AND '$fechaf' AND emp_cod=$emp_cod";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function ordenesVS($fechai, $fechaf, $mun_cod)
    {
        $sql = "SELECT o.*  FROM ordenes o INNER JOIN usuarios u ON u.usu_cod=o.usu_cod WHERE DATE(o.ord_ftr) BETWEEN '$fechai' AND '$fechaf' AND u.mun_cod=$mun_cod";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function ordenesVST($fechai, $fechaf, $mun_cod)
    {
        $sql = "SELECT COUNT(o.ord_cod) as total  FROM ordenes o INNER JOIN usuarios u ON u.usu_cod=o.usu_cod INNER JOIN ordenes_confirmar oc ON oc.pos_cod=o.pos_cod WHERE DATE(oc.con_fre) BETWEEN '$fechai' AND '$fechaf' AND u.mun_cod=$mun_cod";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function ordenesPorClienteC($fechai, $fechaf, $emp_cod)
    {
        $sql = "SELECT COUNT(o.emp_cod) as total  FROM ordenes o INNER JOIN personas p ON p.per_cod=o.pos_cod INNER JOIN ordenes_confirmar oc ON oc.pos_cod=o.pos_cod WHERE DATE(o.ord_ftr) BETWEEN '$fechai' AND '$fechaf' AND o.emp_cod=$emp_cod";
        // Base model
        $obj = new Ordenes();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));

    }

    public function listarDatBas($in)
    {
        $sql = "SELECT p.per_tid AS Tipo_de_documento, p.per_ide AS Numero_de_identificacion, p.per_fex AS Fecha_expedicion, p.per_cex as Ciudad_expedicion, p.per_pno as Primer_nombre, p.per_sno as Segundo_nombre, p.per_pap as Primer_apellido, p.per_sap as Segundo_apellido, p.per_ema as Email, p.per_fna as Fecha_nacimiento, p.per_tel as Telefono, p.per_cel as Celular, p.per_ciu AS Ciudad, p.per_dir as Direccion, p.per_bar as Barrio, p.per_ato AS Estrato, p.per_gsa AS Grupo_sanguineo, p.per_mcf as MCF, p.per_sex AS Sexo,p.per_civ AS Estado_civil,p.per_ura AS Estatura, p.per_eda as Edad, p.per_pes as Peso, p.per_tca as Talla_camisa, p.per_tpa as Talla_pantalon, p.per_tza as Talla_zapato, p.per_pro as Profesional, p.per_gso as Grupo_social, p.per_get as Grupo_etnico, p.per_her as Numero_hermanos, p.per_hij as Numero_hijos, p.per_pac as Numero_personas_a_cargo, p.per_niv AS Nivel_escolaridad FROM personas p
        INNER JOIN ordenes o ON p.per_cod=o.pos_cod AND o.ord_est='2' AND o.ord_cod=(SELECT MAX(ord_cod) FROM ordenes WHERE pos_cod=p.per_cod)
        /*LEFT JOIN muni me ON me.mun_cod=p.per_cex
        LEFT JOIN muni mn ON mn.mun_cod=p.per_ciu
        LEFT JOIN tipos ti ON ti.tip_cod=p.per_tid
        LEFT JOIN tipos tg ON tg.tip_cod=p.per_gsa
        LEFT JOIN tipos tx ON tx.tip_cod=p.per_sex
        LEFT JOIN tipos tv ON tv.tip_cod=p.per_civ
        LEFT JOIN tipos tu ON tu.tip_cod=p.per_ura
        LEFT JOIN tipos ts ON ts.tip_cod=p.per_ato
        LEFT JOIN tipos tn ON tn.tip_cod=p.per_niv*/
        WHERE p.per_est='1' AND p.per_cod IN ({$in}) AND p.est_cod NOT IN(7,8,9,10)";
        $obj = new Ordenes();
        // Execute the query
        $result = new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
        return $result->toArray();
    }

    public function listarEstudios($in)
    {
        $sql = "SELECT p.per_ide as Identificacion, e.est_ent as Entidad, REPLACE(e.est_pro, ',', '.') as Titulo, e.est_fec as Fecha FROM personas p
        INNER JOIN ordenes o ON p.per_cod=o.pos_cod AND o.ord_est='2' AND o.ord_cod=(SELECT MAX(ord_cod) FROM ordenes WHERE pos_cod=p.per_cod)
        INNER JOIN personas_estudios e ON e.per_cod=p.per_cod
        WHERE p.per_est='1' AND p.per_cod IN ({$in}) AND p.est_cod NOT IN(7,8,9,10)";
        $obj = new Ordenes();
        // Execute the query
        $result = new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
        return $result->toArray();
    }

    public function listarExperiencias($in)
    {
        $sql = "SELECT p.per_ide as Identificacion, e.exp_emp as Empresa, REPLACE(e.exp_car, ',', '.') as Cargo, e.exp_ini as Fecha_Inicio, e.exp_ffi as Fecha_FIn, REPLACE(REPLACE(REPLACE(e.exp_des,CHAR(10),''),CHAR(13),''), ',', '.') as Funciones, REPLACE(e.exp_mot, ',', '.') as Motivo_Retiro FROM personas p
        INNER JOIN ordenes o ON p.per_cod=o.pos_cod AND o.ord_est='2' AND o.ord_cod=(SELECT MAX(ord_cod) FROM ordenes WHERE pos_cod=p.per_cod)
        INNER JOIN personas_exp e ON e.per_cod=p.per_cod
        WHERE p.per_est='1' AND p.per_cod IN ({$in}) AND p.est_cod NOT IN(7,8,9,10)";
        $obj = new Ordenes();
        // Execute the query
        $result = new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
        return $result->toArray();
    }

    public function listarRefencias($in)
    {
        $sql = "SELECT p.per_ide as Identificacion, e.fam_nom as Nombre, e.fam_par as Parentesco, REPLACE(e.fam_tel, ',', '.') as Telefono, REPLACE(e.fam_ocu, ',', '.') as Ocupacion FROM personas p
        INNER JOIN ordenes o ON p.per_cod=o.pos_cod AND o.ord_est='2' AND o.ord_cod=(SELECT MAX(ord_cod) FROM ordenes WHERE pos_cod=p.per_cod)
        INNER JOIN personas_fam e ON e.per_cod=p.per_cod
        WHERE p.per_est='1' AND p.per_cod IN ({$in}) AND p.est_cod NOT IN(7,8,9,10)";
        $obj = new Ordenes();
        // Execute the query
        $result = new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
        return $result->toArray();
    }

    public function listarFamiliares($in)
    {
        $sql = "SELECT p.per_ide as Identificacion, e.hij_nom as Nombre, e.hij_par as Parentesco, REPLACE(e.hij_eda, ',', '.') as Edad, REPLACE(e.hij_ocu, ',', '.') as Ocupacion FROM personas p
        INNER JOIN ordenes o ON p.per_cod=o.pos_cod AND o.ord_est='2' AND o.ord_cod=(SELECT MAX(ord_cod) FROM ordenes WHERE pos_cod=p.per_cod)
        INNER JOIN personas_hijos e ON e.per_cod=p.per_cod
        WHERE p.per_est='1' AND p.per_cod IN ({$in}) AND p.est_cod NOT IN(7,8,9,10)";
        $obj = new Ordenes();
        // Execute the query
        $result = new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
        return $result->toArray();
    }

}

?>