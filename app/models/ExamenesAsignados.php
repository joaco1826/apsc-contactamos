<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class ExamenesAsignados extends \Phalcon\Mvc\Model
{

   public  function ExamenesAsignadosT($exa_tip, $ord_cod){
            $sql ="SELECT * FROM examenes_asignados ea INNER JOIN ordenes_examenes oe ON oe.ord_cod=ea.ord_cod INNER JOIN examenes_medicos e ON e.exa_cod=oe.exa_cod WHERE e.exa_tip='$exa_tip' AND ea.ord_cod=$ord_cod ";

        // Base model
        $obj = new ExamenesAsignados();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }
  

}
