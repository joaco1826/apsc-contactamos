<?php
use Phalcon\Validation\Validator\PresenceOf;

class ReportesController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        $this->assets
            ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
            ->addCss('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css', false)
            ->addCss('css/estilos.css');
        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', false)
            ->addJs('js/menu.js')
            ->addJs('js/confirmar.js');

    }

    public function inscritos_cargos_formAction()
    {

    }

    public function consolidado_formAction()
    {

    }

    public function consolidadoiAction()
    {

    }

    public function consolidadomAction()
    {

    }

    public function encuestaAction()
    {

    }

    public function inscritos_cargosAction($fechai, $fechaf)
    {
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
    }

    public function encuestasalidaAction($fechai, $fechaf, $empresa)
    {
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
        $this->view->setVar("empresa", $empresa);
    }

    public function consolidadoAction($fechai, $fechaf)
    {
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
        $permisos = Consempresas::find("usu_id=" . $this->session->get("usu_cod"));
        $cargos = "";
        foreach ($permisos as $key => $p) {
            if ($key == 0) {
                $cargos .= $p->car_id;
            } else {
                $cargos .= "," . $p->car_id;
            }
        }
        $this->view->setVar("cargos", $cargos);
    }

    public function operativoAction($perreq, $op){
        $perreq = explode("-", $perreq);
        $per_cod = $perreq[0];
        $req_cod = $perreq[1];
        $this->view->setVar("per", Personas::findFirst($per_cod));
        $this->view->setVar("req", Requisiciones::findFirst($req_cod));
        $this->view->setVar("op", $op);
    }

    public function consolidadoingresosAction($fechai, $fechaf)
    {
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
    }

    public function consolidadomaiAction($fechai, $fechaf)
    {
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
    }

}