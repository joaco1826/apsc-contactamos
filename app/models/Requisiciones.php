<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Requisiciones extends \Phalcon\Mvc\Model
{


     public function initialize()
    {
        $this->belongsTo("car_cod", "Cargos", "car_cod"); //relacion Cargos
        $this->belongsTo("mun_cod", "Muni", "mun_cod"); //relacion Municipios
        $this->belongsTo("emp_cod", "Empresas", "emp_cod"); //relacion Empresas
        $this->belongsTo("req_cod", "Postulaciones", "req_cod"); //relacion Empresas

    }

    public  function listar(){
        $sql = "SELECT r.req_cod, r.req_til, r.req_fec, r.req_des, r.req_sex, r.req_eda, r.req_sal, r.req_niv, r.req_exp, r.req_fci, r.req_fex, r.req_obs, m.mun_nom,  e.emp_raz, c.car_des
                FROM Requisiciones r JOIN Cargos c
                JOIN Muni m
                JOIN Empresas e ORDER BY r.req_til";
        return $this->getModelsManager()->executeQuery($sql);
    }

    public  function listarA(){
        $fecha = date("Y-m-d");
        $sql = "SELECT r.req_cod,r.req_til, r.req_fec, r.req_des, r.req_sex, r.req_eda, r.req_sal, r.req_niv, r.req_exp, r.req_fci, r.req_obs, m.mun_nom,  e.emp_raz, c.car_des
                FROM Requisiciones r JOIN Cargos c
                JOIN Muni m
                JOIN Empresas e WHERE r.req_fex >= '$fecha' ORDER BY r.req_til";
        return $this->getModelsManager()->executeQuery($sql);
    }

    public  function obtener($req_cod){
        $sql = "SELECT r.req_cod, r.req_fec, r.req_des, r.req_sex, r.req_eda, r.req_sal, r.req_niv, r.req_exp, r.req_fci, r.req_obs, m.mun_nom,  e.emp_raz, c.car_des
                FROM Requisiciones r JOIN Cargos c
                JOIN Muni m
                JOIN Empresas e WHERE r.req_cod=$req_cod ORDER BY c.car_des";
        return $this->getModelsManager()->executeQuery($sql);
    }


    public function getPruebasReq($req_cod){
        $sql = "SELECT r.req_sal,c.car_16p, c.car_val, c.car_cmt, c.car_car, c.car_ipv, c.car_pnl 
                FROM Requisiciones r
                JOIN Cargos c
                WHERE r.req_cod=$req_cod";
        // return $this->modelsManager->executeQuery($sql);
         return $this->getModelsManager()->executeQuery($sql);

    }

    public function PruebasCar($req_cod){
        $sql = "SELECT r.req_sal,c.car_16p, c.car_val, c.car_cmt, c.car_car, c.car_ipv, c.car_pnl 
                FROM Requisiciones r
                INNER JOIN Cargos c
                ON c.car_cod=r.car_cod
                WHERE r.req_cod=$req_cod";
        return $this->getModelsManager()->executeQuery($sql);

    }

     public  function listarConfirmacion($fechai, $fechaf){
         $sql = "SELECT * FROM personas per
                JOIN postulaciones pos on pos.per_cod=per.per_cod
                JOIN requisiciones r on pos.req_cod=r.req_cod
                JOIN
                 cargos c on r.car_cod=c.car_cod
                where pos.pos_fec between '$fechai' and '$fechaf' ";

        // Base model
        $obj = new Ordenes();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

}
