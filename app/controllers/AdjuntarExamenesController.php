<?php

use Phalcon\Validation\Validator\PresenceOf,
Phalcon\Paginator\Adapter\Model as PaginatorModel;



class AdjuntarExamenesController extends \Phalcon\Mvc\Controller

{
	 public function initialize()

    {

        $this->assets

             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)

              ->addCss('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css', false)

             ->addCss('css/estilos.css');



        $this->assets

            ->addJs('js/jquery.js')

            ->addJs('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', false)

            ->addJs('js/menu.js')

            ->addJs('js/adjuntar_examenes.js');

    }

	public function indexAction()
	{

	}

	    public function consultarAction() {
        $txt = $this->request->getPost("txt_bus");
        $currentPage = (int) $this->request->getPost("txt_pag");
        $filas = $this->request->getPost("filas");
        if(empty($filas)) {
            $filas = 15;
        }
        if (empty($currentPage)) {
            $currentPage = 1;
        }
       	$orden = new Ordenes();
		$asignados = $orden->listarAsignacionExamen($txt);
       
       // $citas = $cit->listar();
       $paginator   = new PaginatorModel(
        array(
            "data"  => $asignados,
            "limit" => $filas,
            "page"  => $currentPage
            )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();
        ?>
        <table class="lista">
             <tr>
                <th>No.</th>
                <th>No. Documento</th>
                <th>Nombres</th>
                <th>Proveedor</th>
                <th>Tipo exámen</th>
                <th>Fecha</th>
                <th>Psicóloga</th>
                <th></th>
                <th></th>
             </tr>
        <?
        if ($currentPage > 1) {
            $i = $filas * $currentPage - $filas + 1;
        } else {
            $i = 1;
        }
        foreach ($page->items as  $asi) {

            $nombre_apirante = $asi->per_pno." ".$asi->per_sno." ".$asi->per_pap." ".$asi->per_sap;
			
			?>
				<tr>
					<td><?=$i?></td>
					<td><?=$asi->per_ide?></td>
					<td><?=ucfirst(ucwords($asi->per_pno." ".$asi->per_sno." ".$asi->per_pap." ".$asi->per_sap))?></td>
					<td><?=$asi->emp_raz?></td>
					<td><?=$asi->asi_tip?></td>
					<td><?=$asi->asi_fec?></td>

					<td>
					
						<a href="#" onclick="abrirModal('<?=$asi->asi_cod?>', '<?=$asi->per_cod?>')">Adjuntar</a>

					</td>
					<td>
					<?
						$exa = ExamenesPersonas::findFirst("asi_cod=".$asi->asi_cod);
						if($exa){
					?>
						<a href="<?=$this->url->get()?>examenes/<?=$exa->exa_rut?>" target="_blank"> Ver</a>
						<? } ?>
					</td>
					
				</tr>
			<?
			$i++;
        }
        ?>
 
        <tr>
                <td colspan="9" align="right">
                     <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                     <?php echo "<span class='btn ml10' onclick='filtrar(".$page->before.")'>Anterior</span>"; ?>
                    <?php
                    $titem = $page->total_items;
                    if ($titem == 0) {
                        $titem = 1;
                    }
                    $num = ($titem + ($filas - 1)) / $filas;
                    $quinta = $num / 25 + $currentPage ;
                    $segunda = $num / 2 + $currentPage ;
                    for ($i=1; $i <= $num; $i++) { 
                        // if ($i % 32 == 0) echo "<br>";
                        
                        //     if ($i <= $quinta || $i >= $segunda) {
                        //         if ($currentPage == $i) {
                        //             echo "<span class='btn'>".$i."</span>";
                        //         } else {
                        //             echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                        //         }
                        //     } else {
                        //         if ($num < 30){
                        //             echo "<span class='btn ml10'  onclick='filtrar(".$i.")'>".$i."</span>";
                        //         } else {
                        //             echo ".";
                        //         }
                                
                        //     }

                        if ($i > ($currentPage - 12) and $i < ($currentPage + 12)) {
                            if ($currentPage == $i) {
                                echo "<span class='btn'>".$i."</span>";
                            } else {
                                echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                            }
                        } 
                        
                        
                        
                        
                    }
                    ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->next.")'>Siguiente</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->last.")'>Última</span>"; ?>
                    <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span style="margin-left: 5px">filas</span>
                    <select name="filas" id="filas" style="width: 40px" onchange="filtrar(1)">
                        <option <?if($filas == 15) echo "selected";?> value="15">15</option>
                        <option <?if($filas == 30) echo "selected";?> value="30">30</option>
                        <option <?if($filas == 50) echo "selected";?> value="50">50</option>
                        <option <?if($filas == 100) echo "selected";?> value="100">100</option>
                    </select>
                </td>
            </tr>
        
        </table>
        <?
    }

	public function subirAction()
	{
		$asi_cod = $_POST['asi_cod'];
		foreach (ExamenesPersonas::find("asi_cod=$asi_cod") as $obj) {
		    if ($obj->delete() == false) {
		        //echo "Sorry, we can't delete the robot right now: \n";
		        foreach ($obj->getMessages() as $message) {
		            //echo $message, "\n";
		        }
		    } else {
		        //echo "The robot was deleted successfully!";
		    }
		}

				if ($this->request->hasFiles() == true) {
		            // Print the real file names and sizes
		            foreach ($this->request->getUploadedFiles() as $file) {

		                //Print file details
		               // echo $file->getName(), " ", $file->getSize(), "\n";


		                //Move the file into the application
		                $ruta = $file->getName();
		                $file->moveTo('examenes/'.$file->getName());
		            }
		        }

		        $exa = new ExamenesPersonas();
		        $exa->pos_cod = $_POST['per_cod'];
		        $exa->exa_rut = $ruta;
		        $exa->exa_obs = $_POST['exa_obs'];
		        $exa->asi_cod = $_POST['asi_cod'];

		        if($exa->save()){
		        	?>
						<script>
							alert("Archivo subido con exito");
						</script>
		        	<?

		        	$this->response->redirect("adjuntar_examenes/index");
		        }else{
		        	?>
						<script>
							alert("error");
						</script>
		        	<?
		        	$this->response->redirect("adjuntar_examenes/index");
		        }
    
	}


	

}