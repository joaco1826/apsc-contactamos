<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class EditarAspiranteController extends \Phalcon\Mvc\Controller
{
	 public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jpegcam/htdocs/webcam.js')
            ->addJs('js/menu.js')
            ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false)
             ->addJs('js/HojaVida.js');


    }

     public function indexAction($per_cod)
    {
        $int = split("-", $per_cod);
        $per_cod = $int[0];
        $camara = $int[1];
        $this->view->setVar("camara", $camara);
         $this->view->setVar("per_cod", $per_cod);

        $this->view->setVar("identificaciones", Tipos::find(array(
                "tip_tip = 'identificacion'"
        )));

        $this->view->setVar("grupos", Tipos::find(array(
                "tip_tip = 'sangre'"
        )));

        $this->view->setVar("cargos", Cargos::find(array(
                "car_est = '1' ORDER BY car_des"
         )));

        $this->view->setVar("sexos", Tipos::find(array(
                "tip_tip = 'sexo'"
        )));

        $this->view->setVar("fuentes", Tipos::find(array(
                "tip_tip = 'rec'"
        )));

        $this->view->setVar("civiles", Tipos::find(array(
                "tip_tip = 'civil'"
        )));

        $this->view->setVar("estaturas", Tipos::find(array(
                "tip_tip = 'est'"
        )));

        $this->view->setVar("niveles", Tipos::find(array(
                "tip_tip = 'niv'"
        )));

        $this->view->setVar("estratos", Tipos::find(array(
                "tip_tip = 'eto'"
        )));

        $muni = new Muni();

        $this->view->setVar("municipios", $muni->listarMuniDpto());

        $this->view->setVar("per",Personas::findFirst($per_cod));

        $this->view->setVar("hijos", PersonasHijos::find(array(
                "per_cod = '".$per_cod."'"
        )));

        $this->view->setVar("estudios", PersonasEstudios::find(array(
                "per_cod = '".$per_cod."'"
        )));

        $this->view->setVar("experiencias", PersonasExp::find(array(
                "per_cod = '".$per_cod."'"
        )));

        $this->view->setVar("referencias", PersonasFam::find(array(
                "per_cod = '".$per_cod."'"
        )));

    }

    public function guardarAction()
    {


        $validation = new Phalcon\Validation();

        $validation->add('car_cod', new PresenceOf(array(
            'message' => 'El campo cargo de interés Es Requerido',

        )));

        $validation->add('per_ide', new PresenceOf(array(
            'message' => 'El campo No. Identificacion Es Requerido',

        )));

        $validation->add('per_pno', new PresenceOf(array(
            'message' => 'El campo  Nombre Es Requerido',

        )));



        $validation->add('per_pap', new PresenceOf(array(
            'message' => 'El campo   Apellido Es Requerido',

        )));


        $validation->add('per_ema', new PresenceOf(array(
            'message' => 'El campo  Email Es Requerido',

        )));

        // $validation->add('per_tel', new PresenceOf(array(
        //     'message' => 'El campo  Telefono Es Requerido',

        // )));


        $validation->add('per_cel', new PresenceOf(array(
            'message' => 'El campo  Celular Es Requerido',

        )));


        // $validation->add('per_mcf', new PresenceOf(array(
        //     'message' => 'El campo  Madre Cabeza de familia Es Requerido',

        // )));

        // $validation->add('per_tca', new PresenceOf(array(
        //     'message' => 'El campo  Talla Camisa Es Requerido',

        // )));

        // $validation->add('per_tpa', new PresenceOf(array(
        //     'message' => 'El campo  Talla Pantalón Es Requerido',

        // )));

        // $validation->add('per_tza', new PresenceOf(array(
        //     'message' => 'El campo  Talla Zapato Es Requerido',

        // )));



        // $validation->add('per_fre', new PresenceOf(array(
        //     'message' => 'El campo  Fuente de reclutamiento Es Requerido',

        // )));






        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }




        $per           = Personas::findFirst($this->request->getPost("per_cod"));
        $per->car_cod  = $this->request->getPost("car_cod");
        $per->per_tid  = $this->request->getPost("per_tid");
        $per->per_ide  = $this->request->getPost("per_ide");
        $per->per_con  = $this->request->getPost("per_con");
        $per->per_pno  = $this->request->getPost("per_pno");
        $per->per_sno  = $this->request->getPost("per_sno");
        $per->per_pap  = $this->request->getPost("per_pap");
        $per->per_sap  = $this->request->getPost("per_sap");
        $per->per_fex  = $this->request->getPost("per_fex");
        $per->per_cex  = $this->request->getPost("per_cex");
        $per->per_dir  = $this->request->getPost("per_dir");
        $per->per_bar  = $this->request->getPost("per_bar");
        $per->per_pai  = $this->request->getPost("per_pai");
        $per->per_ciu  = $this->request->getPost("per_ciu");
        $per->per_ema  = $this->request->getPost("per_ema");
        $per->per_tel  = $this->request->getPost("per_tel");
        $per->per_ato  = $this->request->getPost("per_ato");
        $per->per_cel  = $this->request->getPost("per_cel");
        $per->per_fna  = $this->request->getPost("per_fna");
        $per->per_cna  = $this->request->getPost("per_cna");
        $per->per_gsa  = $this->request->getPost("per_gsa");
        $per->per_pro  = $this->request->getPost("per_pro");
        $per->per_mcf  = $this->request->getPost("per_mcf");
        $per->per_sex  = $this->request->getPost("per_sex");
        $per->per_civ  = $this->request->getPost("per_civ");
        $per->per_niv  = $this->request->getPost("per_niv");
        $per->per_ura  = $this->request->getPost("per_ura");
        $per->per_eda  = $this->request->getPost("per_eda");
        $per->per_pes  = $this->request->getPost("per_pes");
        $per->per_tca  = $this->request->getPost("per_tca");
        $per->per_tpa  = $this->request->getPost("per_tpa");
        $per->per_tza  = $this->request->getPost("per_tza");
        $per->per_gso  = $this->request->getPost("per_gso");
        $per->per_get  = $this->request->getPost("per_get");
        $per->per_her  = $this->request->getPost("per_her");
        $per->per_hij  = $this->request->getPost("per_hij");
        $per->per_pac  = $this->request->getPost("per_pac");
        $per->per_asp  = $this->request->getPost("per_asp");
        $per->per_tur  = $this->request->getPost("per_tur");
        $per->per_din  = $this->request->getPost("per_din");
        // $per->per_fre  = $this->request->getPost("per_fre");
        $per->per_nlm  = $this->request->getPost("per_nlm");
        $per->per_dis  = $this->request->getPost("per_dis");
        $per->per_lco  = $this->request->getPost("per_lco");
        $per->per_cat  = $this->request->getPost("per_cat");
        $per->per_cju  = $this->request->getPost("per_cju");
        if ($this->request->getPost("exp") == 0) {
            $experiencias = PersonasExp::find(array(
                "per_cod='".$this->request->getPost("per_cod")."'"
            ));
            if (count($experiencias) == 0) {
                echo "Debe por lo menos colocar una experiencia laboral, si no tiene experiencia por favor seleccione la opción sin experiencia";
                return;
            }
        } else {
            $per->per_exp  = $this->request->getPost("exp");
        }

        if($per->save()){

                echo "1";

        }else{
             foreach ($per->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }


    }

    public function subirFotoAction($per_cod)
     {
            $jpeg_data = file_get_contents('php://input');
            $foto = md5(microtime()*rand(1.1,1.9))."_".$per_cod;
            $filename = "files/".$foto.".jpg";

            $personas = Personas::findFirst($per_cod);
            $personas->per_fot = $foto.".jpg";

            if($personas->save()){
                $result = file_put_contents( $filename, $jpeg_data );
            }


    }
}