<?php
use Phalcon\Validation\Validator\PresenceOf,
	Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\Email;
class EncuestaController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
    	 $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos_login.css')
             ->addCss('css/estilos.css');

    	$this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/encuestac.js');

    }

    public function copiaAction()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos_login.css')
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/encuestac.js');

    }

    public function consultarAction()
    {
        $per = new Personas();
        $personas = $per->consultarCedula($this->request->getPost("ced", "int"))->toArray();
        if (count($personas) == 0) {
            $personas = $per->consultarCedulaC($this->request->getPost("ced", "int"))->toArray();
        }
        echo json_encode($personas);
    }

   public function guardarAction()
     {
        $validation = new Phalcon\Validation();

        $validation->add('per_ide', new PresenceOf(array(
            'message' => 'El campo No de identificación se encuentra vacio',

        )));

        $validation->add('cargo', new PresenceOf(array(
            'message' => 'El campo Cargo Es Requerido',

        )));


        $validation->add('tiempo', new PresenceOf(array(
            'message' => 'El campo  Tiempo en la empresa Es Requerido',

        )));

        $validation->add('proyecto', new PresenceOf(array(
            'message' => 'El campo  Proyecto Es Requerido',

        )));

        if ($this->request->getPost("enc_ret") == "si") {
            $validation->add('enc_mot', new PresenceOf(array(
                'message' => 'Razon principal por su retiro Es Requerido',

            )));
        }

         $validation->add('enc_cal', new PresenceOf(array(
            'message' => 'El campo  como se sintió en la empresa mientras duró Es Requerido',

        )));

        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }
        if ($this->request->getPost("per_cod") == "") {
            $personas = Personas::findFirst(array("per_ide='".$this->request->getPost("per_ide")."'"));
            if (!$personas) {
                $per = new Personas();
                $per->per_ide = $this->request->getPost("per_ide");
                $per->per_pno = $this->request->getPost("per_nom");
                $per->per_con = $this->request->getPost("per_ide");
                $per->est_cod = 27;
                $per->per_est = 1;

            } else {
                $hoy = date("Y-m-d H:i:s");
                $per = Personas::findFirst($personas->per_cod);
                $per->est_cod = 27;
                $per->per_fecm = $hoy;
            }

            if($per->save()){
                $per_cod = $per->per_cod;
            }else{
                 foreach ($personas->getMessages() as $message) {
                    echo "Message: ", $message->getMessage();
                    echo "Field: ", $message->getField();
                    echo "Type: ", $message->getType();
                }
            }

        } else {
            $per_cod = $this->request->getPost("per_cod");
        }
        $encuesta = new Encuesta();
        $encuesta->enc_est  = new \Phalcon\Db\RawValue('default');
        $encuesta->per_cod  = $per_cod;
        $encuesta->enc_tie  = $this->request->getPost("tiempo");
        $encuesta->enc_ret  = $this->request->getPost("enc_ret");
        $encuesta->enc_mot  = $this->request->getPost("enc_mot");
        $encuesta->enc_cal  = $this->request->getPost("enc_cal");
        $encuesta->enc_otr  = $this->request->getPost("enc_otr");
        $encuesta->enc_obs  = $this->request->getPost("enc_obs");
        $encuesta->enc_raz  = $this->request->getPost("razon");
        $encuesta->enc_ciu  = $this->request->getPost("ciudad");
        $encuesta->enc_fec = new \Phalcon\Db\RawValue('default');
        if($encuesta->save()){
            echo "1";
        }else{
             foreach ($encuesta->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

     public function guardaroAction()
     {
        $validation = new Phalcon\Validation();

        $validation->add('per_ide', new PresenceOf(array(
            'message' => 'El campo No de identificación se encuentra vacio',

        )));

        $validation->add('cargo', new PresenceOf(array(
            'message' => 'El campo Cargo Es Requerido',

        )));


        $validation->add('tiempo', new PresenceOf(array(
            'message' => 'El campo  Tiempo en la empresa Es Requerido',

        )));

        $validation->add('proyecto', new PresenceOf(array(
            'message' => 'El campo  Proyecto Es Requerido',

        )));

        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }
        if ($this->request->getPost("per_cod") == "") {
            $personas = Personas::findFirst(array("per_ide='".$this->request->getPost("per_ide")."'"));
            if (!$personas) {
                $per = new Personas();
                $per->per_ide = $this->request->getPost("per_ide");
                $per->per_pno = $this->request->getPost("per_nom");
                $per->per_con = $this->request->getPost("per_ide");
                $per->est_cod = 27;
                $per->per_est = 1;

            } else {
                $hoy = date("Y-m-d H:i:s");
                $per = Personas::findFirst($personas->per_cod);
                $per->est_cod = 27;
                $per->per_fecm = $hoy;
            }

            if($per->save()){
                $per_cod = $per->per_cod;
            }else{
                 foreach ($personas->getMessages() as $message) {
                    echo "Message: ", $message->getMessage();
                    echo "Field: ", $message->getField();
                    echo "Type: ", $message->getType();
                }
            }

        } else {
            $per_cod = $this->request->getPost("per_cod");
        }
        $causa = "";
        if (count($this->request->getPost("causa")) > 0) {
            foreach ($this->request->getPost("causa") as $key => $c) {
                $causa .= $c ." - ";
            }
        }
        
        $mejoras = "";
        if (count($this->request->getPost("mejoras")) > 0) {
            foreach ($this->request->getPost("mejoras") as $key => $c) {
                $mejoras .= $c ." - ";
            }
        }

        $encuesta = new Logytech();
        $encuesta->per_cod  = $per_cod;
        $encuesta->tiempo  = $this->request->getPost("tiempo");
        $encuesta->motivo  = $this->request->getPost("motivoretiro");
        $encuesta->causa  = $causa;
        $encuesta->jefe  = $this->request->getPost("jefe");
        $encuesta->relacion  = $this->request->getPost("relacion");
        $encuesta->trabajar  = $this->request->getPost("trabajar");
        $encuesta->ejercer  = $this->request->getPost("ejercer");
        $encuesta->cumplio  = $this->request->getPost("cumplio");
        $encuesta->condiciones  = $this->request->getPost("condiciones");
        $encuesta->mejoras  = $mejoras;
        $encuesta->fortalezas  = $this->request->getPost("fortalezas");
        $encuesta->terminos  = $this->request->getPost("terminos");
        $encuesta->mejorar  = $this->request->getPost("mejorar");
        $encuesta->fondo  = $this->request->getPost("fondo");
        $encuesta->dfondo  = $this->request->getPost("dfondo");
        $encuesta->razon  = $this->request->getPost("razon");
        $encuesta->ciudad  = $this->request->getPost("ciudad");
        $encuesta->date_create = new \Phalcon\Db\RawValue('default');
        if($encuesta->save()){
            echo "1";
        }else{
             foreach ($encuesta->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

}

