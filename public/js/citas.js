jQuery(document).ready(function($) {
	$.ajax({
			url: $("#url_index").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_pag="+$("#txt_pag").val()+"&txt_bus="+$("#txt_bus").val()+"&psi="+$("#psi").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	$("#txt_bus").keypress(function(e) {
		if(e.which == 13){
		$.ajax({
			url: $("#url_index").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	}
		
	});


	$("#psi").change(function(event) {
		
		$.ajax({
			url: $("#url_index").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&psi="+$(this).val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$(".datepickier").change(function(event) {
		
		$.ajax({
			url: $("#url_index").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&psi="+$("#psi").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$("#cit_fec").datepicker({
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
	})
	$(".datepickier").datepicker({
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
	})
});

function filtrar(pag) {
	$("#txt_pag").val(pag);
	$.ajax({
		url: $("#url_index").val(),
		type: 'POST',
		dataType: 'html',
		data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&psi="+$("#psi").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val(),
		 beforeSend: function() {
			$("#respuesta").html("Cargando...")
			 }
	})

	.done(function(datos) {
		console.log(datos)
		$("#respuesta").html(datos)

		
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function buscar() {
	$.ajax({
			url: $("#url_index").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&psi="+$(this).val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function cambiarestado(cit_cod, est_cod) {
	$.ajax({
			url: $("#url_estado").val(),
			type: 'POST',
			dataType: 'html',
			data: "cit_cod="+cit_cod+"&est_cod="+est_cod,
			 beforeSend: function() {
				// $("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			if (datos == 1) {
				// alert("Estado cambiado");
			} else {
				alert(datos);
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function eliminarCita(cit_cod) {
	$.ajax({
			url: $("#url_eliminar").val(),
			type: 'POST',
			dataType: 'html',
			data: "cit_cod="+cit_cod,
			 beforeSend: function() {
				// $("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			if (datos == 1) {
				alert("Cita eliminada exitosamente");
				location.reload();
			} else {
				alert(datos);
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function abrirModal(cod, nombre, fec, hor) {
	$(".negro").fadeIn(100);
	$("#modal_documentos").fadeIn(100);
	$("#IsName").html(nombre);
	$("#cit_cod").val(cod);
	$("#cit_fec").val(fec);
	$("#cit_hor").val(hor);
}

function cerrarModal(per_cod){
	$(".negro").css("display", "none")
	$("#modal_documentos").css("display", "none")
	$("#modal_documentos_o").css("display", "none")
}

function editarCita() {
	$.ajax({
		url: $("#url_editar").val(),
		type: 'POST',
		dataType: 'html',
		data: "cit_fec="+$("#cit_fec").val()+"&cit_hor="+$("#cit_hor").val()+"&cit_cod="+$("#cit_cod").val(),
		 beforeSend: function() {
			 }
	})

	.done(function(datos) {
		// console.log(datos)
		if (datos == 1) {
			alert("Cita editada exitosamente");
			// location.reload();
		} else {
			alert(datos);
			return false;
		}

		
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function chekear(obj, cit_cod) {
	var cit_asi;
	if ($(obj).is(':checked')) {
			cit_asi = 1;
		} else {
			cit_asi = 0;
		}
		$.ajax({
			url: $("#url_asistio").val(),
			type: 'POST',
			dataType: 'html',
			data: "cit_cod="+cit_cod+"&cit_asi="+cit_asi,
			 beforeSend: function() {
				// $("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			// if (datos == 1) {
			// 	alert("chekeado");
			// }

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}