<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Preguntas16pf extends \Phalcon\Mvc\Model
{


    public function initialize()
    {
        $this->setSource('16pf_pre');
    }

	  public static function obtenerRes($per_cod)
    {
        // A raw SQL statement
         $hoy = date("Y-m-d");
        $fecha = strtotime ( '-720 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
        $sql = "SELECT pd.`p16_cod` AS pregunta, pd.`r16_cod` AS respuesta_asp, pr.`r16_res` AS respuesta 
		FROM 16pf_per pa 
		JOIN 16pf_per_det pd ON pa.`p16_cod`=pd.`pd_cod`
		LEFT JOIN 16pf_res pr ON pd.`r16_cod`=pr.`r16_cod`
		WHERE pa.p16_cod=(SELECT MAX(p16_cod) FROM 16pf_per WHERE per_cod=$per_cod) AND DATE(pa.p16_fch) BETWEEN '$fecha' AND '$hoy' ";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function obtenerVaremo($varemo)
    {
        // A raw SQL statement
        $sql = "SELECT
				  16t_nom as deciseis_nom
				FROM 16pf_t 
				WHERE 16t_cod=$varemo
				LIMIT 1";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

    public static function decatipoA($varemo, $A)
    {
        // A raw SQL statement
        $sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom, 16pf_a.16a_val, 16pf_a.16a_res as dieciseis_res FROM 16pf_t,16pf_a WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_a.16t_cod AND 16a_val=$A LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoB($varemo, $B)
    {
        // A raw SQL statement
        $sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_b.16b_val, 16pf_b.16b_res as dieciseis_res FROM 16pf_t,16pf_b WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_b.16t_cod AND 16b_val=$B LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoC($varemo, $C)
    {
        // A raw SQL statement
        $sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_c.16c_val, 16pf_c.16c_res as dieciseis_res FROM 16pf_t,16pf_c WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_c.16t_cod AND 16c_val=$C LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

    public static function decatipoE($varemo, $E)
    {
        // A raw SQL statement
        $sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_e.16e_val, 16pf_e.16e_res as dieciseis_res FROM 16pf_t,16pf_e WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_e.16t_cod AND 16e_val=$E LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoF($varemo, $F)
    {
        // A raw SQL statement
        $sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_f.16f_val, 16pf_f.16f_res as dieciseis_res FROM 16pf_t,16pf_f WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_f.16t_cod AND 16f_val=$F LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoG($varemo, $G)
    {
        // A raw SQL statement
       $sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_g.16g_val, 16pf_g.16g_res as dieciseis_res FROM 16pf_t,16pf_g WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_g.16t_cod AND 16g_val=$G LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }


    public static function decatipoH($varemo, $H)
    {
        // A raw SQL statement
      $sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_h.16h_val, 16pf_h.16h_res as dieciseis_res FROM 16pf_t,16pf_h WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_h.16t_cod AND 16h_val=$H LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }


     public static function decatipoI($varemo, $I)
    {
        // A raw SQL statement
     $sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_i.16i_val, 16pf_i.16i_res as dieciseis_res FROM 16pf_t,16pf_i WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_i.16t_cod AND 16i_val=$I LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoL($varemo, $L)
    {
        // A raw SQL statement
    	$sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_l.16l_val, 16pf_l.16l_res as dieciseis_res FROM 16pf_t,16pf_l WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_l.16t_cod AND 16l_val=$L LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoM($varemo, $M)
    {
        // A raw SQL statement
    	$sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_m.16m_val, 16pf_m.16m_res as dieciseis_res FROM 16pf_t,16pf_m WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_m.16t_cod AND 16m_val=$M LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoN($varemo, $N)
    {
        // A raw SQL statement
    	$sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_n.16n_val, 16pf_n.16n_res as dieciseis_res FROM 16pf_t,16pf_n WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_n.16t_cod AND 16n_val=$N LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoO($varemo, $O)
    {
        // A raw SQL statement
    	$sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_o.16o_val, 16pf_o.16o_res as dieciseis_res FROM 16pf_t,16pf_o WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_o.16t_cod AND 16o_val=$O LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoQ1($varemo, $Q1)
    {
        // A raw SQL statement
    	$sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_q1.16q1_val, 16pf_q1.16q1_res as dieciseis_res FROM 16pf_t,16pf_q1 WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_q1.16t_cod AND 16q1_val=$Q1 LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoQ2($varemo, $Q2)
    {
        // A raw SQL statement
    	$sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_q2.16q2_val, 16pf_q2.16q2_res as dieciseis_res FROM 16pf_t,16pf_q2 WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_q2.16t_cod AND 16q2_val=$Q2 LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoQ3($varemo, $Q3)
    {
        // A raw SQL statement
    	$sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_q3.16q3_val, 16pf_q3.16q3_res as dieciseis_res FROM 16pf_t,16pf_q3 WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_q3.16t_cod AND 16q3_val=$Q3 LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

     public static function decatipoQ4($varemo, $Q4)
    {
        // A raw SQL statement
    	$sql = "SELECT 16pf_t.16t_cod, 16pf_t.16t_nom as dieciseis_nom, 16pf_q4.16q4_val, 16pf_q4.16q4_res as dieciseis_res FROM 16pf_t,16pf_q4 WHERE 16pf_t.16t_cod=$varemo AND  16pf_t.16t_cod = 16pf_q4.16t_cod AND 16q4_val=$Q4 LIMIT 1;";

        // Base model
        $pf = new Preguntas16pf();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }



}
