<?php

class Proveedores extends \Phalcon\Mvc\Model
{

   

    /**
     *
     * @var integer
     */
    protected $pro_cod;

    /**
     *
     * @var integer
     */
    protected $emp_cod;

    /**
     *
     * @var string
     */
    protected $pro_pre;

    /**
     *
     * @var integer
     */
    protected $pro_con;

    /**
     * Method to set the value of field pro_cod
     *
     * @param integer $pro_cod
     * @return $this
     */
    public function setProCod($pro_cod)
    {
        $this->pro_cod = $pro_cod;

        return $this;
    }

    /**
     * Method to set the value of field emp_cod
     *
     * @param integer $emp_cod
     * @return $this
     */
    public function setEmpCod($emp_cod)
    {
        $this->emp_cod = $emp_cod;

        return $this;
    }

    /**
     * Method to set the value of field pro_pre
     *
     * @param string $pro_pre
     * @return $this
     */
    public function setProPre($pro_pre)
    {
        $this->pro_pre = $pro_pre;

        return $this;
    }

    /**
     * Method to set the value of field pro_con
     *
     * @param integer $pro_con
     * @return $this
     */
    public function setProCon($pro_con)
    {
        $this->pro_con = $pro_con;

        return $this;
    }

    /**
     * Returns the value of field pro_cod
     *
     * @return integer
     */
    public function getProCod()
    {
        return $this->pro_cod;
    }

    /**
     * Returns the value of field emp_cod
     *
     * @return integer
     */
    public function getEmpCod()
    {
        return $this->emp_cod;
    }

    /**
     * Returns the value of field pro_pre
     *
     * @return string
     */
    public function getProPre()
    {
        return $this->pro_pre;
    }

    /**
     * Returns the value of field pro_con
     *
     * @return integer
     */
    public function getProCon()
    {
        return $this->pro_con;
    }

     public function initialize()
    {
        $this->belongsTo("emp_cod", "Empresas", "emp_cod");
       
    }

 

}
