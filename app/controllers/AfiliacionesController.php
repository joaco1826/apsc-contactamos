<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;

class AfiliacionesController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        $this->assets
            ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
            ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false)
            ->addCss('css/estilos.css');
        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false)
            ->addJs('js/menu.js')
            ->addJs('js/obs.js')
            ->addJs('js/afiliaciones.js');
    }

    public function indexAction()
    {
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_tip = 'C' AND emp_est='1' ORDER BY emp_raz"
        )));
        $this->view->setVar("cargos", Cargos::find(array(
            "car_est = '1' ORDER BY car_des"
        )));
        $est = new Estados();
        $this->view->setVar("estados", $est->listarCont());
    }

    public function consultarAction()
    {
        $emp_cod = $this->request->getPost("emp_cod");
        $txt_bus = $this->request->getPost("txt_bus");
        $oficina = $this->request->getPost("oficina");
        $estad = $this->request->getPost("estad");
        $where = "WHERE p.per_est='1' AND (p.per_ide LIKE '%{$txt_bus}%' OR p.per_pno LIKE '%{$txt_bus}%' OR p.per_pap LIKE '%{$txt_bus}%' OR o.ord_car LIKE '%{$txt_bus}%') AND o.ord_est='2'";
        $currentPage = (int)$this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $filas = $this->request->getPost("filas");
        if (empty($filas)) {
            $filas = 15;
        }
        $orden = new Ordenes();
        if (!empty($emp_cod)) {
            $where .= " AND o.emp_cod=$emp_cod";
        }
        if (!empty($oficina)) {
            $where .= " AND u.mun_cod=$oficina";
        }
        if (!empty($estad)) {
            $where .= " AND p.est_cod=$estad";
        }
        // echo $where . "-" . $estad;
        $ordenes = $orden->listarAfiliaciones($where);
        $paginator = new PaginatorModel(
            array(
                "data" => $ordenes,
                "limit" => $filas,
                "page" => $currentPage
            )
        );
        // Get the paginated results
        $page = $paginator->getPaginate();
        if ($currentPage > 1) {
            $i = $filas * $currentPage - $filas + 1;
        } else {
            $i = 1;
        }
        ?>
        <table class="lista">
            <tr>
                <th>No.</th>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Usuario / Proyecto</th>
                <th>F. Ingreso</th>
                <th>Cargo</th>
                <th>Empresa</th>
                <th>Oficina</th>
                <th>Estado</th>
                <th>Ingreso/<br>Reingreso</th>
                <th>C. Banco</th>
                <th>ARL</th>
                <th>EPS</th>
                <th>Pensión</th>
                <th>C. Compensación</th>
            </tr>
            <?
            $i = 0;
            $estados = Estados::find(array(
                "est_pro='contratacion' AND est_est='1' ORDER BY est_nom"
            ));
            foreach ($page->items as $ord) {
                $nombre_apirante = $ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap;
                $ordenes = Ordenes::count("pos_cod=".$ord->pos_cod);
                $i++;
                    ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $ord->per_ide ?></td>
                        <td><?= $ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap ?></td>
                        <td><?= $ord->emp_raz ?></td>
                        <td style="width: 90px;"><?= $ord->ord_fec ?></td>
                        <td><?= $ord->ord_car ?></td>
                        <td><?= $ord->tip_des ?></td>
                        <td><?= $ord->mun_nom ?></td>
                        <td>
                            <select name="" id="est<?= $i ?>" style="width: 150px"
                                    onchange="cambiarestado('<?= $ord->per_cod ?>', this.value, '<?= $ord->cod ?>')">
                                <option value="">SIN CONTRATAR</option>
                                <?
                                foreach ($estados as $est) {
                                    if ($ord->est_cod == $est->est_cod) { ?>
                                        <option selected value="<?= $est->est_cod ?>"><?= $est->est_nom ?></option>
                                    <? } else { ?>
                                        <option value="<?= $est->est_cod ?>"><?= $est->est_nom ?></option>
                                    <? }
                                    ?>
                                <? }
                                ?>
                            </select>
                        </td>
                        <td>
                            <select name="reingreso_<?= $ord->cod ?>" id="reingreso_<?= $ord->cod ?>"
                                    onchange="fechas('reingreso', this.value, <?= $ord->cod ?>)">
                                <option <? if ($ord->reingreso == "0") echo "selected"; ?> value="0">NUEVO INGRESO
                                </option>
                                <option <? if ($ord->reingreso == "1" || $ordenes > 1) echo "selected"; ?> value="1">REINGRESO</option>
                            </select>
                        </td>
                        <td>
                            <select style="width: 50px" name="banco_<?= $ord->cod ?>" id="banco_<?= $ord->cod ?>"
                                    onchange="fechas('banco', this.value, <?= $ord->cod ?>)">
                                <option <? if ($ord->banco == "NO") echo "selected"; ?> value="NO">NO</option>
                                <option <? if ($ord->banco == "SI") echo "selected"; ?> value="SI">SI</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" style="width: 62px;"
                                   onchange="fechas('arl', this.value, <?= $ord->cod ?>)" value="<?= $ord->arl ?>"
                                   class="fecha-A_<?= $ord->cod ?>">
                        </td>
                        <td>
                            <input type="text" style="width: 62px;"
                                   onchange="fechas('eps', this.value, <?= $ord->cod ?>)" value="<?= $ord->eps ?>"
                                   class="fecha-A_<?= $ord->cod ?>">
                        </td>
                        <td>
                            <input type="text" style="width: 62px;"
                                   onchange="fechas('pension', this.value, <?= $ord->cod ?>)"
                                   value="<?= $ord->pension ?>" class="fecha-A_<?= $ord->cod ?>">
                        </td>
                        <td>
                            <input type="text" style="width: 62px;"
                                   onchange="fechas('caja', this.value, <?= $ord->cod ?>)" value="<?= $ord->caja ?>"
                                   class="fecha-A_<?= $ord->cod ?>">
                        </td>
                    </tr>
                    <?
                }
            ?>
            <tr>
                <td colspan="15" align="right">
                    <div>
                        <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                        <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->before . ")'>Anterior</span>"; ?>
                        <?php
                        $titem = $page->total_items;
                        if ($titem == 0) {
                            $titem = 1;
                        }
                        $num = ($titem + ($filas - 1)) / $filas;
                        $quinta = $num / 25 + $currentPage;
                        $segunda = $num / 2 + $currentPage;
                        for ($i = 1; $i <= $num; $i++) {
                            // if ($i % 32 == 0) echo "<br>";
                            //     if ($i <= $quinta || $i >= $segunda) {
                            //         if ($currentPage == $i) {
                            //             echo "<span class='btn'>".$i."</span>";
                            //         } else {
                            //             echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                            //         }
                            //     } else {
                            //         if ($num < 30){
                            //             echo "<span class='btn ml10'  onclick='filtrar(".$i.")'>".$i."</span>";
                            //         } else {
                            //             echo ".";
                            //         }
                            //     }
                            if ($i > ($currentPage - 12) and $i < ($currentPage + 12)) {
                                if ($currentPage == $i) {
                                    echo "<span class='btn'>" . $i . "</span>";
                                } else {
                                    echo "<span class='btn ml10' onclick='filtrar(" . $i . ")'>" . $i . "</span>";
                                }
                            }

                        }
                        ?>
                        <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->next . ")'>Siguiente</span>"; ?>
                        <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->last . ")'>Última</span>"; ?>
                        <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span
                                style="margin-left: 5px">filas</span>
                        <select name="filas" id="filas" style="width: 40px" onchange="filtrar(1)">
                            <option <? if ($filas == 15) echo "selected"; ?> value="15">15</option>
                            <option <? if ($filas == 30) echo "selected"; ?> value="30">30</option>
                            <option <? if ($filas == 50) echo "selected"; ?> value="50">50</option>
                            <option <? if ($filas == 100) echo "selected"; ?> value="100">100</option>
                        </select>
                    </div>
                </td>
            </tr>
        </table>
        <div class="fecha-A" style="visibility: hidden">
        <script>
                <?
                    foreach ($page->items as $ord) { ?>
                        $(".fecha-A_<?=$ord->cod?>").datepicker({
                            dateFormat: 'yy-mm-dd',
                            changeMonth: true,
                            changeYear: true,
                            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
                            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá']
                        })
                   <? }
                ?>

        </script>
        </div>
    <? }

    public function fechasAction() {
        $ord_cod = $this->request->getPost("ord_cod");
        $valor = $this->request->getPost("valor");
        $campo = $this->request->getPost("campo");
        $obj = Afiliaciones::findFirst("ord_cod=".$ord_cod);
        if ($obj) {
            $obj->$campo = $valor;
            $obj->save();
        } else {
            $afi = new Afiliaciones();
            $afi->ord_cod = $ord_cod;
            $afi->save();
            $obj = Afiliaciones::findFirst("ord_cod=".$ord_cod);
            $obj->$campo = $valor;
            $obj->save();
        }
    }

}
