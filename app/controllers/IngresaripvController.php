<?php

class IngresarIPVController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js');


         $this->view->setVar("preguntas", IpvPre::find());
         
            
    }

     public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $this->view->setVar("req_cod", $req_cod);
    }

     public function guardarAction()
    {
        if ($this->request->isPost() == true) {
            
            $per = new IpvPer();
            $per->per_cod    = $this->request->getPost("per_cod");
            $per->req_cod    = $this->request->getPost("req_cod");
            $per->usu_cod    = $this->request->getPost("per_cod");
            $per->ias_fch    = new \Phalcon\Db\RawValue('default');
            $per->ias_web    = "int";


            $this->db->begin();

            if($per->save() == false){
                $this->db->rollback();
                return;
            }


            $preguntas = IpvPre::find();

            foreach ($preguntas as  $reg) {
                $ire_num="";
                $ipr_cod = $reg->ipr_cod;
                
                // if(isset($this->request->getPost($p16_cod)){
                    $ire_num = $this->request->getPost($ipr_cod, null, ""); // (valor, sanizar, valor por defecto)
                // }

                $per_det = new IpvPerDet();
                $per_det->ias_cod = $per->ias_cod;
                $per_det->ipr_cod = $ipr_cod;
                $per_det->ire_num = $ire_num;

                if($per_det->save() == false){
                    $this->db->rollback();
                    return;
                }

            }


             $this->db->commit();
             // echo "Prueba Enviada";
             // return;
              $this->dispatcher->forward(array(
                    "controller" => "IngresarIPV",
                    "action" => "index",
                   "params" => array("resultado" => $this->request->getPost("per_cod"))
              ));
        }else{
             $this->response->redirect("verpruebas/");
        }   
            
    }

}
?>