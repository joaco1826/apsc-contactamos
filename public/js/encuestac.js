jQuery(document).ready(function($) {
	$("#per_ide").focusout(function(event) {
		
		$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'json',
			data: "ced="+$(this).val(),
			 beforeSend: function() {
				// $("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			console.log(datos)
			var f1, f2, aF1, aF2, numMeses;
			f1 = $("#fecha").val();
			f2 = datos[0].ord_fec;

			aF1 = f1.split("-");
			aF2 = f2.split("-");

			numMeses =  parseInt(aF2[0])*12 + parseInt(aF2[1]) - (parseInt(aF1[0])*12 + parseInt(aF1[1]));
			if (aF2[2]<aF1[2]){
				numMeses = numMeses - 1;
			}
			numMeses = numMeses * -1;
			if (datos[0].emp_cod == 157 || datos[0].emp_cod == 169) {
				$("#blo7").css('display', 'block');
				$("#blo8").css('display', 'block');
				$("#blo15").css('display', 'block');
				$("#blo2").css('display', 'none');
			} else {
				$("#sel1").css('display', 'block');
			}
			$("#per_nom").val(datos[0].per_pno + " " + datos[0].per_sno + " " + datos[0].per_pap + " " + datos[0].per_sap);
			$("#cargo").val(datos[0].ord_car);
			$("#tiempo").val(numMeses);
			$("#proyecto").val(datos[0].emp_raz);
			$("#razon").val(datos[0].tip_des);
			$("#ciudad").val(datos[0].mun_nom);
			$("#emp_cod").val(datos[0].emp_cod);
			$("#per_cod").val(datos[0].per_cod);
			
		})
		.fail(function(datos) {
			console.log("error " + datos);
		})
		.always(function() {
			console.log("complete");
		});
		
	});
});
var x = 0;
function continuar1() {
	if ($("#enc_ret").val() == "") {
		alert("Selecione si fue retirado voluntariamente o no");
		return false;
	}
	if ($("#razon").val() == "") {
		alert("La razón social es requerida");
		return false;
	}
	if ($("#ciudad").val() == "") {
		alert("La ciudad es requerida");
		return false;
	}
	$("#blo1").css('display', 'none');
	$("#blo2").css('display', 'none');
	if ($("#enc_ret").val() == "si") {
		$("#blo3").css('display', 'block');
		$("#blo4").css('display', 'block');
		x = 1;
	} else {
		$("#blo5").css('display', 'block');
		$("#blo6").css('display', 'block');
		x = 2;
	}
}

function continuar3() {
	if ($("#motivoretiro").val() == "") {
		alert("Selecione motivo de retiro");
		return false;
	}
	$("#blo1").css('display', 'none');
	$("#blo7").css('display', 'none');
	$("#blo8").css('display', 'none');
	$("#blo15").css('display', 'none');
	$("#blo9").css('display', 'block');
	$("#blo10").css('display', 'block');
	$("#blo16").css('display', 'block');
}

function continuar4() {
	if ($("#jefe").val() == "") {
		alert("Por favor digite el nombre de su jefe directo");
		return false;
	}
	if ($("#relacion").val() == "") {
		alert("Seleccione la relación con su jefe directo");
		return false;
	}
	if ($("#trabajar").val() == "") {
		alert("Seleccione si trabajaría nuevamente con esta empresa");
		return false;
	}
	if ($("#ejercer").val() == "") {
		alert("Seleccione si estaba ejerciendo las funciones por las cuales fue contratado");
		return false;
	}
	if ($("#cumplio").val() == "") {
		alert("Seleccione si cumplió sus expectativas la empresa");
		return false;
	}
	if ($("#condiciones").val() == "") {
		alert("Seleccione como califica las condiciones laborales de la compañía");
		return false;
	}
	$("#blo16").css('display', 'none');
	$("#blo9").css('display', 'none');
	$("#blo10").css('display', 'none');
	$("#blo11").css('display', 'block');
	$("#blo12").css('display', 'block');
	$("#blo17").css('display', 'block');
}

function continuar5() {
	$.valor = 0;
	$(".mejoras").each(function(index, el) {
		if ($(this).is(":checked")) {
			$.valor++;
		}
	});
	if ($.valor < 3) {
		alert("Debe seleccionar al menos 3 recomendaciones");
		return false;
	}
	if ($("#fortalezas").val() == "") {
		alert("Digite que fortalezas destaca de la empresa cliente");
		return false;
	}
	$("#blo17").css('display', 'none');
	$("#blo11").css('display', 'none');
	$("#blo12").css('display', 'none');
	$("#blo13").css('display', 'block');
	$("#blo14").css('display', 'block');
	$("#blo19").css('display', 'block');
}

function continuar2() {
	if ($("[name*=enc_mot]:radio:checked").val() == "Otro") {
		if ($("#enc_otr").val() == "") {
			alert("Al seleccionar la opción otro, por favor coloque cual. ");
			return false
		}
	}
	$("#blo3").css('display', 'none');
	$("#blo4").css('display', 'none');
	$("#blo5").css('display', 'block');
	$("#blo6").css('display', 'block');
}

function atras1() {
	$("#blo3").css('display', 'none');
	$("#blo4").css('display', 'none');
	$("#blo2").css('display', 'block');
	$("#blo1").css('display', 'block');
}

function atras2() {
	if (x == 1) {
		$("#blo1").css('display', 'none');
		$("#blo2").css('display', 'none');
		$("#blo3").css('display', 'block');
		$("#blo4").css('display', 'block');
		$("#blo5").css('display', 'none');
		$("#blo6").css('display', 'none');
	} else {
		$("#blo5").css('display', 'none');
		$("#blo6").css('display', 'none');
		$("#blo3").css('display', 'none');
		$("#blo4").css('display', 'none');
		$("#blo2").css('display', 'block');
		$("#blo1").css('display', 'block');
	}
	
}

function atras3() {
	$("#blo1").css('display', 'block');
	$("#blo7").css('display', 'block');
	$("#blo8").css('display', 'block');
	$("#blo15").css('display', 'block');
	$("#blo9").css('display', 'none');
	$("#blo10").css('display', 'none');
	$("#blo16").css('display', 'none');
}

function atras4() {
	$("#blo16").css('display', 'block');
	$("#blo9").css('display', 'block');
	$("#blo10").css('display', 'block');
	$("#blo11").css('display', 'none');
	$("#blo12").css('display', 'none');
	$("#blo17").css('display', 'none');
}

function atras5() {
	$("#blo17").css('display', 'block');
	$("#blo11").css('display', 'block');
	$("#blo12").css('display', 'block');
	$("#blo13").css('display', 'none');
	$("#blo14").css('display', 'none');
	$("#blo19").css('display', 'none');
}

function enviar() {
	$("#Eenviar").attr('onclick', 'esperando()');
	$.ajax({
			url: $("#url_guardar").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm_encuesta").serialize(),
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Gracias por su tiempo.");
				location.reload()
			}else{
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function cfondo() {
	if ($("#fondo").val() == "NO") {
		$("#blo18").css('display', 'block');
	} else {
		$("#blo18").css('display', 'none');
	}
}

function enviar2() {
	if ($("#fondo").val() == "") {
		alert("Seleccione si estuvo afiliado a Fondo de Empleados");
		return false;
	}
	if ($("#fondo").val() == "NO") {
		if ($("#dfondo").val() == "") {
			alert("Seleccione la razón por las cuales no se afilió al fondo");
			return false;
		}
	}
	$("#Eenviar2").attr('onclick', 'esperando()');
	$.ajax({
			url: $("#url_guardar2").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm_encuesta").serialize(),
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Gracias por su tiempo.");
				location.reload()
			}else{
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}