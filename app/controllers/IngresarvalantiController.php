<?php

class IngresarValantiController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
            ->addJs('js/validate.js')
             ->addJs('js/msj_validate.js')
            ->addJs('js/IngresarValanti.js');
          


         $this->view->setVar("preguntas", PreguntasPnl::find());
         
            
    }

     public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $this->view->setVar("req_cod", $req_cod);
    }

    public function guardarAction()
    {
        if ($this->request->isPost() == true) {
            $val = new Valanti();
            $val->per_cod = $this->request->getPost("per_cod");
            $val->req_cod = $this->request->getPost("req_cod");
            $val->usu_cod = $this->request->getPost("per_cod");
            $val->val_fch = new \Phalcon\Db\RawValue('default');
            $val->val_web = "int";

            $this->db->begin();
            
            if($val->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 1;
            $val_det->vde_res = $this->request->getPost("uno");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 2;
            $val_det->vde_res = $this->request->getPost("dos");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 3;
            $val_det->vde_res = $this->request->getPost("tres");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 4;
            $val_det->vde_res = $this->request->getPost("cuatro");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 5;
            $val_det->vde_res = $this->request->getPost("cinco");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 6;
            $val_det->vde_res = $this->request->getPost("seis");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 7;
            $val_det->vde_res = $this->request->getPost("siete");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 8;
            $val_det->vde_res = $this->request->getPost("ocho");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 9;
            $val_det->vde_res = $this->request->getPost("nueve");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 10;
            $val_det->vde_res = $this->request->getPost("diez");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre =11;
            $val_det->vde_res = $this->request->getPost("once");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 12;
            $val_det->vde_res = $this->request->getPost("doce");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 13;
            $val_det->vde_res = $this->request->getPost("trece");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 14;
            $val_det->vde_res = $this->request->getPost("catorce");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 15;
            $val_det->vde_res = $this->request->getPost("quince");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 16;
            $val_det->vde_res = $this->request->getPost("dieciseis");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 17;
            $val_det->vde_res = $this->request->getPost("diecisiete");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 18;
            $val_det->vde_res = $this->request->getPost("dieciocho");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 19;
            $val_det->vde_res = $this->request->getPost("diecinueve");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 20;
            $val_det->vde_res = $this->request->getPost("veinte");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 21;
            $val_det->vde_res = $this->request->getPost("veintiuno");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 22;
            $val_det->vde_res = $this->request->getPost("veintidos");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }
            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 23;
            $val_det->vde_res = $this->request->getPost("veintitres");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }
            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 24;
            $val_det->vde_res = $this->request->getPost("veinticuatro");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 25;
            $val_det->vde_res = $this->request->getPost("veinticinco");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 26;
            $val_det->vde_res = $this->request->getPost("veintiseis");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();

            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 27;
            $val_det->vde_res = $this->request->getPost("veintisiete");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();
            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 28;
            $val_det->vde_res = $this->request->getPost("veintiocho");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();
            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 29;
            $val_det->vde_res = $this->request->getPost("veintinueve");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }

            $val_det = new ValantiDet();
            $val_det->val_cod = $val->val_cod;
            $val_det->vde_pre = 30;
            $val_det->vde_res = $this->request->getPost("treinta");
            if($val_det->save() == false){
                $this->db->rollback();
                return;
            }


            $this->db->commit();

            $this->dispatcher->forward(array(
                    "controller" => "IngresarValanti",
                    "action" => "index",
                    "params" => array("resultado" => $this->request->getPost("per_cod"))
              ));

        }else{
             $this->response->redirect("IngresarPruebas/");
        }

    
    }
}