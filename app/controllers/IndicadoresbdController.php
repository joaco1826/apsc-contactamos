<?php

use Phalcon\Validation\Validator\PresenceOf;



class IndicadoresbdController extends \Phalcon\Mvc\Controller

{
	 public function initialize()
   {

        $this->assets

             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)

              ->addCss('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css', false)

             ->addCss('css/estilos.css');



        $this->assets

            ->addJs('js/jquery.js')

            ->addJs('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', false)

            ->addJs('js/menu.js');

    }


    public function indexAction(){
        $this->view->setVar("departamentos", Dpto::find(array("dep_cod <> '' ORDER BY dep_nom")));
    }

    public function gestionAction(){

    }

    public function salaAction(){

    }

    public function contratacionAction(){

    }

    public function reclutamientoAction(){

    }

    public function consultarAction(){
        $indicador = $this->request->getPost("indicador");
        $fechai = $this->request->getPost("fechai");
        $fechaf = $this->request->getPost("fechaf");
        $personas = new Personas();
        if ($indicador == 1) {
            $reg = Personas::find(array("per_fin BETWEEN '".$fechai."' AND '".$fechaf."'"));
            echo '<table class="lista" style="width: 200px;">';
            echo "<tr><td>No. Registros</td><td>".count($reg)."</td></tr>";
        } elseif ($indicador == 2) {
            $registros = $personas->registrosF($fechai, $fechaf);
            echo '<table class="lista" style="width: 400px;">';
            $num=0;
            foreach ($registros as $key => $reg) {
                $num+=$reg->total;
                echo "<tr><td>".$reg->per_fre."</td><td>".$reg->total."</td></tr>";
            }
            echo "<tr><td>Total</td><td>".$num."</td></tr>";
        } elseif ($indicador == 3) {

        } elseif ($indicador == 4) {
             $registros = $personas->registrosC($fechai, $fechaf);
            echo '<table class="lista" style="width: 400px;">';
            $num=0;
            foreach ($registros as $key => $reg) {
                $num+=$reg->total;
                echo "<tr><td>".$reg->mun_nom."</td><td>".$reg->total."</td></tr>";
            }
             echo "<tr><td>Total</td><td>".$num."</td></tr>";
        } elseif ($indicador == 5) {
             $registros = $personas->registrosS($fechai, $fechaf);
            echo '<table class="lista" style="width: 400px;">';
            foreach ($registros as $key => $reg) {
                echo "<tr><td>".$reg->tip_des."</td><td>".$reg->total."</td></tr>";
            }

        } elseif ($indicador == 6) {
            $registros = $personas->registrosE($fechai, $fechaf);
            echo '<table class="lista" style="width: 400px;">';
            foreach ($registros as $key => $reg) {
                echo "<tr><td>".$reg->est_nom."</td><td>".$reg->total."</td></tr>";
            }
        } elseif ($indicador == 7) {
            $reg = Postulaciones::find(array("pos_fec BETWEEN '".$fechai."' AND '".$fechaf."' GROUP BY per_cod"));
            echo '<table class="lista" style="width: 200px;">';
            echo "<tr><td>No. Postulados</td><td>".count($reg)."</td></tr>";
        }
        echo '</table>';
    }


    public function excelAction($fechai, $fechaf, $dep_cod){
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
        $this->view->setVar("dpto", $dep_cod);
    }

     public function excelgAction($fechai, $fechaf){
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
    }

     public function excelsAction($fechai, $fechaf){
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
    }

    public function excelcAction($fechai, $fechaf){
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
    }

    public function excelrAction($fechai, $fechaf){
        $this->view->setVar("fechai", $fechai);
        $this->view->setVar("fechaf", $fechaf);
    }


}