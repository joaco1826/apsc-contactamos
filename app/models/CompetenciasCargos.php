<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class CompetenciasCargos extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
        $this->setSource('competencias_cargos');
    }

    public function listar($id) {
        $sql = "SELECT *, c.id as cod FROM competencias c LEFT JOIN competencias_cargos cc ON c.id=cc.competencia_id AND cc.cargo_id=$id WHERE c.status='activo' ORDER BY c.nombre";
        $obj = new CompetenciasCargos();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function listarE($id, $empresa_id) {
        $sql = "SELECT *, c.id as cod FROM competencias c LEFT JOIN competencias_cargos_empresas cc ON c.id=cc.competencia_id AND cc.cargo_id=$id AND cc.empresa_id=$empresa_id WHERE c.status='activo' ORDER BY c.nombre";
        $obj = new CompetenciasCargos();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function listarT($id) {
        $sql = "SELECT c.id, c.nombre, c.texto, cc.cargo_id, cc.valor FROM competencias c INNER JOIN competencias_cargos cc ON c.id=cc.competencia_id AND cc.cargo_id=$id WHERE c.status='activo' ORDER BY c.nombre";
        $obj = new CompetenciasCargos();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function listarTE($id, $empresa_id) {
        $sql = "SELECT c.id, c.nombre, c.texto, cc.empresa_id, cc.cargo_id, cc.valor FROM competencias c INNER JOIN competencias_cargos_empresas cc ON c.id=cc.competencia_id AND cc.cargo_id=$id AND cc.empresa_id=$empresa_id WHERE c.status='activo' ORDER BY c.nombre";
        $obj = new CompetenciasCargos();
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }
}