<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;
class DocumentosController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        $this->assets
            ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
            ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/estados.js');


    }

    public function indexAction()
    {
        $this->view->setVar("documentos", TipoDocumento::find(array(
            "tdo_est='1'"
        )));

    }


    public function editarAction($tdo_cod)
    {


        $this->view->setVar("doc", TipoDocumento::findFirst($tdo_cod));


    }

    public function nuevoAction()
    {




    }

    public function guardarAction()
    {


        $validation = new Phalcon\Validation();

        $validation->add('tdo_des', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $docu = new TipoDocumento();
        $docu->tdo_des  = $this->request->getPost("tdo_des");
        $docu->tdo_est = new \Phalcon\Db\RawValue('default');
        $docu->tdo_create = new \Phalcon\Db\RawValue('default');
        $docu->tdo_update = new \Phalcon\Db\RawValue('default');


        if($docu->save()){
            echo "1";
        }else{
            foreach ($docu->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }


    }

    public function actualizarAction()
    {


        $validation = new Phalcon\Validation();

        $validation->add('tdo_des', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));

        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }

        $docu = TipoDocumento::findFirst($this->request->getPost("tdo_cod"));
        $docu->tdo_des  = $this->request->getPost("tdo_des");
        $docu->tdo_update = new \Phalcon\Db\RawValue('default');


        if($docu->save()){
            echo "1";
        }else{
            foreach ($docu->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }


    }

    public function eliminarAction()
    {

        $docu = TipoDocumento::findFirst($this->request->getPost("est_cod"));
        $docu->tdo_est="0";
        if($docu->save()){
            echo "1";
        }else{
            foreach ($docu->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

}

