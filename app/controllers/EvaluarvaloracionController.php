<?php

class EvaluarValoracionController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
            ->addJs('js/EvaluarPrueba.js');




    }

     public function indexAction($per_cod)
    {
        $int = explode("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
        $persona = Personas::findFirst($per_cod);
        $this->view->setVar("persona", $persona);
        $this->view->setVar("req_cod", $req_cod);
        $this->view->setVar("valoracion", Valoraciones::count("pos_cod=".$per_cod));
        $req = Requisiciones::findFirst($req_cod);
        $obj = new CompetenciasCargos();
        $competencias = $obj->listarTE($req->car_cod, $req->emp_cod);
        if (count($competencias) == 0) {
            $competencias = $obj->listarT($req->car_cod);
            if (count($competencias) == 0) {
                $competencias = Competencias::find("status='activo'");
            }
        }
        $this->view->setVar("competencias", $competencias);
        $this->view->setVar("req", Requisiciones::findFirst($req_cod));
    }

    public function copiaAction($per_cod)
    {
        $int = explode("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
        $persona = Personas::findFirst($per_cod);
        $this->view->setVar("persona", $persona);
        $this->view->setVar("req_cod", $req_cod);
        $req = Requisiciones::findFirst($req_cod);
        $obj = new CompetenciasCargos();
        $competencias = $obj->listarTE($req->car_cod, $req->emp_cod);
        if (count($competencias) == 0) {
            $competencias = $obj->listarT($req->car_cod);
            if (count($competencias) == 0) {
                $competencias = Competencias::find("status='activo'");
            }
        }
        $this->view->setVar("competencias", $competencias);
        $this->view->setVar("req", $req);
    }

     public function guardarAction()
    {
        if (!$this->session->has("usu_cod")) {
            echo "2";
            return false;
        }
        if ($this->request->isPost() == true) {

            $val = new Valoraciones();
            $val->pos_cod = $this->request->getPost("per_cod");
            $val->req_cod = $this->request->getPost("req_cod");
            $val->usu_cod = $this->session->get("usu_cod");
            $val->val_fch = new \Phalcon\Db\RawValue('default');
            $val->val_obs = $this->request->getPost("val_obs");



            if($val->save() == false){
                $this->db->rollback();
                return;
            } else {
              $ent = new Entrevistas();
              $ent->pos_cod = $this->request->getPost("per_cod");
              $ent->req_cod = $this->request->getPost("req_cod");
              $ent->usu_cod = $this->session->get("usu_cod");
              $ent->ent_fec = new \Phalcon\Db\RawValue('default');
              $ent->ent_rip = $this->request->getPost("pru_rip");
              $ent->ent_ahn = $this->request->getPost("pru_ahn");
              $ent->ent_ada = $this->request->getPost("pru_ada");
              $ent->ent_com = $this->request->getPost("pru_com");
              $ent->ent_lid = $this->request->getPost("pru_lid");
              $ent->ent_mot = $this->request->getPost("pru_mot");
              $ent->ent_cap = $this->request->getPost("pru_cap");
              $ent->ent_obs = $this->request->getPost("ent_obs");


              if($ent->save() == false){
                  $this->db->rollback();
                  return;
              } else {
                $pru = new Pruebas();
                $pru->pos_cod = $this->request->getPost("per_cod");
                $pru->req_cod = $this->request->getPost("req_cod");
                $pru->usu_cod = $this->session->get("usu_cod");
                $pru->pru_fec = new \Phalcon\Db\RawValue('default');
                $pru->pru_rip = $this->request->getPost("pru_rip");
                $pru->pru_rip = $this->request->getPost("pru_rip");
                $pru->pru_ahn = $this->request->getPost("pru_ahn");
                $pru->pru_ada = $this->request->getPost("pru_ada");
                $pru->pru_com = $this->request->getPost("pru_com");
                $pru->pru_lid = $this->request->getPost("pru_lid");
                $pru->pru_mot = $this->request->getPost("pru_mot");
                $pru->pru_cap = $this->request->getPost("pru_cap");
                $pru->pru_obs = $this->request->getPost("pru_obs");


                if($pru->save() == false){
                    $this->db->rollback();
                    return;
                }
              }
            }

            echo "1";
        }
    }

    public function guardarcAction()
    {
        if (!$this->session->has("usu_cod")) {
            echo "2";
            return false;
        }

        $req = Requisiciones::findFirst($this->request->getPost("req_cod"));
        $obj = new CompetenciasCargos();
        $competencias = $obj->listarTE($req->car_cod, $req->emp_cod);
        if (count($competencias) == 0) {
            $competencias = $obj->listarT($req->car_cod);
            if (count($competencias) == 0) {
                $competencias = Competencias::find("status='activo'");
            }
        }
        if ($this->request->isPost() == true) {
            $val = new Valoraciones();
            $val->pos_cod = $this->request->getPost("per_cod");
            $val->req_cod = $this->request->getPost("req_cod");
            $val->usu_cod = $this->session->get("usu_cod");
            $val->val_fch = new \Phalcon\Db\RawValue('default');
            $val->val_obs = $this->request->getPost("val_obs");
            if ($val->save() == false) {
                $this->db->rollback();
                return;
            } else {
                $ent = new Entrevistas();
                $ent->pos_cod = $this->request->getPost("per_cod");
                $ent->req_cod = $this->request->getPost("req_cod");
                $ent->usu_cod = $this->session->get("usu_cod");
                $ent->ent_fec = new \Phalcon\Db\RawValue('default');
                $ent->ent_obs = $this->request->getPost("ent_obs");
                if ($ent->save() == false) {
                    $this->db->rollback();
                    return;
                } else {
                    $id = $ent->ent_cod;
                    foreach ($competencias as $c) {
                        $val = $this->request->getPost("radio_".$c->id, null, 0);
                        if ($val > 0) {
                            $obj = new CompEntrevista();
                            $obj->ent_cod = $id;
                            $obj->com_cod = $c->id;
                            $obj->result = $val;
                            $obj->created_at = new \Phalcon\Db\RawValue('default');
                            $obj->updated_at = new \Phalcon\Db\RawValue('default');
                            $obj->save();
                        }

                    }
                    $pru = new Pruebas();
                    $pru->pos_cod = $this->request->getPost("per_cod");
                    $pru->req_cod = $this->request->getPost("req_cod");
                    $pru->usu_cod = $this->session->get("usu_cod");
                    $pru->pru_fec = new \Phalcon\Db\RawValue('default');
                    $pru->pru_obs = $this->request->getPost("pru_obs");
                    if ($pru->save() == false) {
                        $this->db->rollback();
                        return;
                    } else {
                        $id = $pru->pru_cod;
                        foreach ($competencias as $c) {
                            $val = $this->request->getPost("radio_".$c->id, null, 0);
                            if ($val > 0) {
                                $obj = new CompPruebas();
                                $obj->pru_cod = $id;
                                $obj->com_cod = $c->id;
                                $obj->result = $val;
                                $obj->created_at = new \Phalcon\Db\RawValue('default');
                                $obj->updated_at = new \Phalcon\Db\RawValue('default');
                                $obj->save();
                            }


                        }
                    }
                }
            }
            echo "1";
        }
    }
}