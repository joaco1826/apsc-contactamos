<?php

class IngresarPFController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
            ->addJs('js/IngresarPruebas.js');

         $this->view->setVar("preguntas", Preguntas16pf::find());
         
            
    }

    public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $this->view->setVar("req_cod", $req_cod);
    }

    public function guardarAction()
	{
	 	if ($this->request->isPost() == true) {
	    
	    	$per = new Personas16pf();
	    	$per->per_cod = $this->request->getPost("per_cod");
	    	$per->req_cod = $this->request->getPost("req_cod");
	    	$per->usu_cod = $this->request->getPost("usu_cod");
	    	$per->p16_fch = new \Phalcon\Db\RawValue('default');
	    	$per->p16_web = "int";


	    	$this->db->begin();

			if($per->save() == false){
				$this->db->rollback();
				return;
			}


			$preguntas = Preguntas16pf::find();

			

			foreach ($preguntas as  $reg) {
				$r16_cod = 0;
				$p16_cod = $reg->p16_cod;
				
				// if(isset($this->request->getPost($p16_cod)){
					$r16_cod = $this->request->getPost($p16_cod, null, 0); // (valor, sanizar, valor por defecto)
				// }

				$per_det = new Personasdet16pf();
				$per_det->pd_cod = $per->p16_cod;
				$per_det->p16_cod = $p16_cod;
				$per_det->r16_cod = $r16_cod;

				if($per_det->save() == false){
					$this->db->rollback();
					return;
				}

			}


			 $this->db->commit();
			 // echo "Prueba Enviada";
			  $this->dispatcher->forward(array(
		            "controller" => "IngresarPF",
		            "action" => "index",
		            "params" => array("resultado" => $this->request->getPost("per_cod"))
        	  ));
		}else{
		     $this->response->redirect("IngresarPruebas/");
		}	
	    	
	}
}