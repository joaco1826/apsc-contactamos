<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class EmpresasController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
       $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
             ->addJs('js/jquery.js')
             ->addJs('js/menu.js')
             ->addJs('js/jnalert.js')
             ->addJs('js/empresas.js');
    }

    public function indexAction()
    {


    	 $this->view->setVar("empresas", Empresas::find(array(
		    	"emp_est = '1' AND emp_tip = 'C' ORDER BY emp_raz"
		 )));

    }

    public function nuevoAction()
    {
    	// $this->assets
     //        ->addJs('js/jquery.js')
     //        ->addJs('js/empresas.js');


    }

    public function editarAction($emp_cod)
    {


 		$this->view->setVar("empresa", Empresas::findFirst($emp_cod));
 		$this->view->setVar("cargos", Cargos::find("car_est='1' ORDER BY car_des"));
        $this->view->setVar("emp_cod", $emp_cod);


    }

     public function guardarAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('emp_nit', new PresenceOf(array(
		    'message' => 'El campo Nit Es Requerido',

		)));

		$validation->add('emp_raz', new PresenceOf(array(
		    'message' => 'el campo  Razon social es requerido'
		)));

		$validation->add('emp_con', new PresenceOf(array(
		    'message' => 'el campo  Contacto es requerido'
		)));

		$validation->add('emp_tel', new PresenceOf(array(
		    'message' => 'el campo  Telefono es requerido'
		)));

		$validation->add('emp_ema', new PresenceOf(array(
		    'message' => 'el campo  Email es requerido'
		)));
        $validation->add('emp_exa', new PresenceOf(array(
            'message' => 'el campo  Correos Laboratorio es requerido'
        )));
        // $validation->add('emp_ing', new PresenceOf(array(
        //     'message' => 'el campo  Correos Ordenes de Ingreso es requerido'
        // )));

		// $validation->add('emp_ema', new Email(array(
		//     'message' => 'el formato de  Email no es valido'
		// )));


		$validation->add('emp_pas', new PresenceOf(array(
		    'message' => 'el campo Contraseña es requerido'
		)));




		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}




    	$empresa = new Empresas();
    	$empresa->setEmpNit($this->request->getPost("emp_nit"));
    	$empresa->setEmpRaz($this->request->getPost("emp_raz"));
    	$empresa->setEmpCon($this->request->getPost("emp_con"));
    	$empresa->setEmpTel($this->request->getPost("emp_tel"));
    	$empresa->setEmpEma($this->request->getPost("emp_ema"));
        $empresa->setEmpDir($this->request->getPost("emp_dir"));
        $empresa->setEmpPas($this->request->getPost("emp_pas"));
        $empresa->setEmpExa($this->request->getPost("emp_exa"));
    	$empresa->setEmpIng($this->request->getPost("emp_ing"));
    	$empresa->setEmpEst(new \Phalcon\Db\RawValue('default'));
    	$empresa->setEmpTip("C");//cliente


    	if($empresa->save()){
    		echo "1";
    	}else{
    		 foreach ($empresa->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

     public function actualizarAction()
    {

		$validation = new Phalcon\Validation();

		$validation->add('emp_nit', new PresenceOf(array(
		    'message' => 'El campo Nit Es Requerido',

		)));

		$validation->add('emp_raz', new PresenceOf(array(
		    'message' => 'el campo  Razon social es requerido'
		)));

		$validation->add('emp_con', new PresenceOf(array(
		    'message' => 'el campo  Contacto es requerido'
		)));

		$validation->add('emp_tel', new PresenceOf(array(
		    'message' => 'el campo  Telefono es requerido'
		)));

		$validation->add('emp_ema', new PresenceOf(array(
		    'message' => 'el campo  Email es requerido'
		)));
        $validation->add('emp_exa', new PresenceOf(array(
            'message' => 'el campo  Correos Laboratorio es requerido'
        )));
        // $validation->add('emp_ing', new PresenceOf(array(
        //     'message' => 'el campo  Correos Ordenes de Ingreso es requerido'
        // )));

		// $validation->add('emp_ema', new Email(array(
		//     'message' => 'el formato de  Email no es valido'
		// )));


		$validation->add('emp_pas', new PresenceOf(array(
		    'message' => 'el campo Contraseña es requerido'
		)));




		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



		$empresa = Empresas::findFirst($this->request->getPost("emp_cod"));
    	// $empresa = new Empresas();

    	$empresa->setEmpNit($this->request->getPost("emp_nit"));
    	$empresa->setEmpRaz($this->request->getPost("emp_raz"));
    	$empresa->setEmpCon($this->request->getPost("emp_con"));
    	$empresa->setEmpTel($this->request->getPost("emp_tel"));
    	$empresa->setEmpEma($this->request->getPost("emp_ema"));
        $empresa->setEmpDir($this->request->getPost("emp_dir"));
    	$empresa->setEmpPas($this->request->getPost("emp_pas"));
        $empresa->setEmpIng($this->request->getPost("emp_ing"));
        $empresa->setEmpExa($this->request->getPost("emp_exa"));
    	$empresa->setEmpEst(new \Phalcon\Db\RawValue('default'));
    	$empresa->setEmpTip("C");//cliente


    	if($empresa->save()){
    		echo "1";
    	}else{
    		 foreach ($empresa->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    }

    public function eliminarAction()
    {

    	$empresa = Empresas::findFirst($this->request->getPost("cod"));
    	$empresa->setEmpEst("0");
    	if($empresa->save()){
    		echo "1";
    	}else{
    		 foreach ($empresa->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    }

    public function listarAction() {
        $empresa_id = $this->request->getPost("empresa_id", "int");
        $cargo_id = $this->request->getPost("cargo_id", "int");
        $obj = new CompetenciasCargos();
        $competencias = $obj->listarE($cargo_id, $empresa_id);
        $sw = false;
        foreach ($competencias as $com) {
            if ($com->empresa_id <> "") {
                $sw = true;
            }
        }
        if (!$sw) {
            $competencias = $obj->listar($cargo_id);
        }
        ?>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2">
                    <input type="hidden" name="cargo_id" value="<?=$cargo_id?>">
                    <input type="hidden" name="empresa_id" value="<?=$empresa_id?>">
                    <h3>Competencias del cargo <?=Cargos::findFirst($cargo_id)->car_des?> para la empresa <?=Empresas::findFirst($empresa_id)->getEmpRaz()?></h3><br>
                </td>
            </tr>

            <?

            foreach ($competencias as $com) { ?>
                <tr>
                    <td>
                        <?
                        if ($com->cargo_id <> "") {
                            $ck = "checked";
                        } else {
                            $ck = "";
                        }
                        ?>
                        <input type="checkbox" <?=$ck?> id="com_<?=$com->cod?>" value="<?=$com->cod?>" name="competencias[]">
                        <label for="com_<?=$com->cod?>"> <?=$com->nombre?> </label>
                    </td>
                    <td align="right">
                        <select style="width: 50px" name="radio_<?=$com->cod?>">
                            <option value="">Seleccione</option>
                            <?
                            for ($i=5; $i<=100; $i+=5){
                                if ($i == $com->valor) {
                                    echo '<option selected value="'.$i.'">'.$i.'</option>';
                                } else {
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            <? }
            ?>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class="boton" style="cursor: pointer" onclick="competencias()">Guardar</div>
                </td>
            </tr>

        </table>
    <? }

    public function guardarcompAction()
    {
        $empresa_id = $this->request->getPost("empresa_id", "int");
        $cargo_id = $this->request->getPost("cargo_id", "int");
        $comp_cargos = CompetenciasCargosEmpresas::find(["cargo_id=".$cargo_id." AND empresa_id=".$empresa_id]);
        $comp_cargos->delete();
        $competencias = $this->request->getPost("competencias");
        foreach ($competencias as $i => $com) {
            $comp_cargos = new CompetenciasCargosEmpresas();
            $comp_cargos->cargo_id = $cargo_id;
            $comp_cargos->empresa_id = $empresa_id;
            $comp_cargos->competencia_id = $com;
            $comp_cargos->valor = $this->request->getPost("radio_".$com);
            $comp_cargos->created_at  = new \Phalcon\Db\RawValue('default');
            $comp_cargos->updated_at  = new \Phalcon\Db\RawValue('default');
            $comp_cargos->status  = new \Phalcon\Db\RawValue('default');
            $comp_cargos->save();
        }
        echo "1";
    }

}

