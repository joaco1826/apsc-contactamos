<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class RequisicionesController extends \Phalcon\Mvc\Controller
{

	public function initialize()
    {
    	$this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
              ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false)
             ->addCss('css/estilos.css');

    	 $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false)
            ->addJs('js/requisiciones.js');

         $req = new Requisiciones();
         $this->view->setVar("requisiciones", $req->listar());
    }

    public function indexAction()
    {


         $req = new Requisiciones();
         $this->view->setVar("requisiciones", $req->listar());
    }

     public function nuevoAction()
    {


         $this->view->setVar("cargos", Cargos::find(array(
		    	"car_est = '1'"
		 )));

         $mun = new Muni();
		 $this->view->setVar("municipios", $mun->listarMuniDpto());

		 $this->view->setVar("empresas", Empresas::find(array(
		    	"emp_est = '1' AND emp_tip = 'C'"
		 )));

        $this->view->setVar("razones", Tipos::find(array(
            "tip_tip = 'bolsa'"
        )));
    }

     public function editarAction($req_cod)
    {


         $this->view->setVar("cargos", Cargos::find(array(
		    	"car_est = '1' ORDER BY car_des"
		 )));


		 $mun = new Muni();
		 $this->view->setVar("municipios", $mun->listarMuniDpto());


		 $this->view->setVar("empresas", Empresas::find(array(
		    	"emp_est = '1' AND emp_tip = 'C'"
		 )));

        $this->view->setVar("razones", Tipos::find(array(
            "tip_tip = 'bolsa'"
        )));

        $this->view->setVar("requisicion", Requisiciones::findFirst($req_cod));


    }

     public function guardarAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('req_til', new PresenceOf(array(
		    'message' => 'El campo Titulo Es Requerido',

		)));

        $validation->add('car_cod', new PresenceOf(array(
            'message' => 'El campo Cargo Es Requerido',

        )));

		$validation->add('mun_cod', new PresenceOf(array(
		    'message' => 'el campo  Ciudad es requerido'
		)));

		$validation->add('emp_cod', new PresenceOf(array(
		    'message' => 'el campo  Empresa es requerido'
		)));

		// $validation->add('req_sex', new PresenceOf(array(
		//     'message' => 'el campo  Sexo es requerido'
		// )));

		// $validation->add('req_eda', new PresenceOf(array(
		//     'message' => 'el campo  Edad es requerido'
		// )));

		// $validation->add('req_sal', new PresenceOf(array(
		//     'message' => 'el campo  Salario es requerido'
		// )));

		$validation->add('req_niv', new PresenceOf(array(
		    'message' => 'el campo  Nivel Académico es requerido'
		)));

		$validation->add('req_exp', new PresenceOf(array(
		    'message' => 'el campo  Experiencia es requerido'
		)));

		$validation->add('req_des', new PresenceOf(array(
		    'message' => 'el campo  Descripción es requerido'
		)));

		$validation->add('req_fci', new PresenceOf(array(
		    'message' => 'el Fecha Inicio es requerido'
		)));

		// $validation->add('req_obs', new PresenceOf(array(
		//     'message' => 'el Observacion es requerido'
		// )));

		$validation->add('req_fex', new PresenceOf(array(
		    'message' => 'el campo Fecha de Expiracion es requerido'
		)));

        $validation->add('tip_cod', new PresenceOf(array(
            'message' => 'el campo Razon social es requerido'
        )));

		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}




    	$req = new requisiciones();
        $req->req_til = $this->request->getPost("req_til");
    	$req->car_cod = $this->request->getPost("car_cod");
    	$req->mun_cod = $this->request->getPost("mun_cod");
    	$req->emp_cod = $this->request->getPost("emp_cod");
    	$req->req_fec = date("Y-m-d");
    	$req->emp_cod = $this->request->getPost("emp_cod");
    	$req->req_sex = $this->request->getPost("req_sex");
    	$req->req_eda = $this->request->getPost("req_eda");
    	$req->req_sal = $this->request->getPost("req_sal");
    	$req->req_niv = $this->request->getPost("req_niv");
    	$req->req_exp = $this->request->getPost("req_exp");
    	$req->req_des = $this->request->getPost("req_des");
    	$req->req_fci = $this->request->getPost("req_fci");
    	$req->req_obs = $this->request->getPost("req_obs");
    	$req->req_fex = $this->request->getPost("req_fex");
    	$req->req_tip = "L";//local - indica que la requisicion la hicieron en la intranet y no la hizo la empresa cliente

    	$req->req_est = new \Phalcon\Db\RawValue('default');


    	if($req->save()){
    		echo "1";
    	}else{
    		 foreach ($req->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

     public function actualizarAction()
    {


		$validation = new Phalcon\Validation();

        $validation->add('req_til', new PresenceOf(array(
            'message' => 'El campo Titulo Es Requerido',

        )));

		$validation->add('car_cod', new PresenceOf(array(
		    'message' => 'El campo Cargo Es Requerido',

		)));

		$validation->add('mun_cod', new PresenceOf(array(
		    'message' => 'el campo  Municipio es requerido'
		)));

		$validation->add('emp_cod', new PresenceOf(array(
		    'message' => 'el campo  Empresa es requerido'
		)));

		// $validation->add('req_sex', new PresenceOf(array(
		//     'message' => 'el campo  Sexo es requerido'
		// )));

		// $validation->add('req_eda', new PresenceOf(array(
		//     'message' => 'el campo  Edad es requerido'
		// )));

		// $validation->add('req_sal', new PresenceOf(array(
		//     'message' => 'el campo  Salario es requerido'
		// )));

		$validation->add('req_niv', new PresenceOf(array(
		    'message' => 'el campo  Nivel es requerido'
		)));

		$validation->add('req_exp', new PresenceOf(array(
		    'message' => 'el campo  Experiencia es requerido'
		)));

		$validation->add('req_des', new PresenceOf(array(
		    'message' => 'el campo  Descripción es requerido'
		)));

		$validation->add('req_fci', new PresenceOf(array(
		    'message' => 'el Fecha Inicio es requerido'
		)));

		// $validation->add('req_obs', new PresenceOf(array(
		//     'message' => 'el Observacion es requerido'
		// )));

		$validation->add('req_fex', new PresenceOf(array(
		    'message' => 'el campo Fecha de Expiracion es requerido'
		)));

        $validation->add('tip_cod', new PresenceOf(array(
            'message' => 'el campo Razon social es requerido'
        )));

		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}




    	$req = Requisiciones::findFirst($this->request->getPost("req_cod"));
    	$req->req_til = $this->request->getPost("req_til");
    	$req->car_cod = $this->request->getPost("car_cod");
    	$req->mun_cod = $this->request->getPost("mun_cod");
    	$req->emp_cod = $this->request->getPost("emp_cod");
    	$req->req_fec = date("Y-m-d");
    	$req->emp_cod = $this->request->getPost("emp_cod");
    	$req->req_sex = $this->request->getPost("req_sex");
    	$req->req_eda = $this->request->getPost("req_eda");
    	$req->req_sal = $this->request->getPost("req_sal");
    	$req->req_niv = $this->request->getPost("req_niv");
    	$req->req_exp = $this->request->getPost("req_exp");
    	$req->req_des = $this->request->getPost("req_des");
    	$req->req_fci = $this->request->getPost("req_fci");
    	$req->req_obs = $this->request->getPost("req_obs");
    	$req->req_fex = $this->request->getPost("req_fex");
    	$req->req_tip = "L";//local - indica que la requisicion la hicieron en la intranet y no la hizo la empresa cliente

    	$req->req_est = new \Phalcon\Db\RawValue('default');


    	if($req->save()){
    		echo "1";
    	}else{
    		 foreach ($req->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

     public function eliminarAction()
    {

    	$req = Requisiciones::findFirst($this->request->getPost("cod"));
    	if($req->delete()){
    		echo "1";
    	}else{
    		 foreach ($req->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    }

}

