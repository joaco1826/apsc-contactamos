jQuery(document).ready(function($) {
	$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val()+"&psi="+$("#psi").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)


		})
		.fail(function() {
			// console.log("error");
		})
		.always(function() {
			// console.log("complete");
		});

	$("#txt_bus").keypress(function(e) {
		if(e.which == 13){
		$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val()+"&psi="+$("#psi").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	}

	});

	$(".datepickier").datepicker({
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
	})

	$(".datep").datepicker({
		dateFormat:'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá']
	})

	$(".datepickier").change(function(event) {

		$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val()+"&psi="+$("#psi").val()+"&est_cod="+$("#est_cod").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	});


});



function buscar() {
	$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val()+"&psi="+$("#psi").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function filtrar(pag) {
	$("#txt_pag").val(pag);
	$.ajax({
		url: $("#url_consultar").val(),
		type: 'POST',
		dataType: 'html',
		data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&fec_ini="+$("#fec_ini").val()+"&fec_fin="+$("#fec_fin").val()+"&psi="+$("#psi").val()+"&est_cod="+$("#est_cod").val(),
		 beforeSend: function() {
			$("#respuesta").html("Cargando...")
			 }
	})

	.done(function(datos) {
		// console.log(datos)
		$("#respuesta").html(datos)


	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}


function abrirModal(per_cod, req_cod){
	$(".negro").css("display", "block")
	$("#modal_documentos").css("display", "block")
	$("#per_cod").val(per_cod)
	$(".caja_prueba a").each(function(i){

	      href = $(this).attr('data-url')+"/"+per_cod+"-"+req_cod;
	      $(this).attr('href', href);



	});
}

function abrirModalC() {
	$(".negro").fadeIn(100);
	$("#modal_documentos_c").fadeIn(100);
	var postu = "";
    $(".postu").each(function() {
        if ($(this).is(':checked')) {
            postu += $(this).val();
        }
    });
    $("#postulantes").val(postu);
}

function cerrarModal(per_cod){
	$(".negro").css("display", "none")
	$("#modal_documentos").css("display", "none")
	$("#modal_documentos_p").css("display", "none")
	$("#modal_documentos_c").css("display", "none")
	$("#modal_documentos_o").css("display", "none")
    $("#modal_documentos_i").css("display", "none")


}

function abrirModalI(per_cod, req_cod, nombre){
    $(".negro").css("display", "block")
    $("#modal_documentos").css("display", "none")
    $("#modal_documentos_p").css("display", "none")
    $("#modal_documentos_c").css("display", "none")
    $("#modal_documentos_o").css("display", "none")
    $("#modal_documentos_i").css("display", "block")
	$("#perreq").val(per_cod + "-" + req_cod)
	$("#pnom").html(nombre)

}

function abrirModalP(per_cod, req_cod) {
	$(".negro").css("display", "block")
	$("#modal_documentos_p").css("display", "block")
	$("#per_cod").val(per_cod)
	$.ajax({
		url: $("#url_pruebas").val(),
		type: 'POST',
		dataType: 'html',
		data: "per_cod="+per_cod+"&req_cod="+req_cod,
		 beforeSend: function() {
			 }
	})

	.done(function(datos) {
		$("#modal_documentos_p").html(datos);


	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function guardarOrd(tipo) {
	var ord;
	if($("[name=ord_dot]").is(":checked")) {
		ord = "SI";
	} else {
		ord = "NO";
	}
	$.ajax({
			url: $("#url_ord").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frmE").serialize()+"&emp_cod="+$("#emp_cod").val()+"&tip_cod="+$("#tip_cod").val()+"&ord_fec="+$("#ord_fec").val()+"&ord_car="+$("#ord_car").val()+"&postulantes="+$("#postulantes").val()+"&ord_sal="+$("#ord_sal").val()+"&ord_arl="+$("#ord_arl").val()+"&ord_tna="+$("#ord_tna").val()+"&ord_cto="+$("#ord_cto").val()+"&ord_sot="+$("#ord_sot").val()+"&ord_fec_sot="+$("#ord_fec_sot").val()+"&ord_dot="+ord+"&tipo="+tipo+"&ord_obs="+$("#ord_obs").val(),
			 beforeSend: function() {
			 	$("#guaOrd").val("ENVIANDO...");
			 	$("#guaOrd").attr('disabled', true);
  			 }
		})

		.done(function(datos) {
			console.log(datos);
         if($.trim(datos)=="1"){
         	alert("Orden de contratación generada");
         	location.reload();
         } else {
         	if ($.trim(datos) == "2") {
         		alert("La session ha caducado.");
         		location.reload();
         	} else {
         		alert(datos);
         	}

         }

         $("#guaOrd").attr('disabled', false);
         $("#guaOrd").val("GUARDAR");
		location.reload();


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function cambiarestado(per_cod, est_cod) {
	$.ajax({
			url: $("#url_estado").val(),
			type: 'POST',
			dataType: 'html',
			data: "per_cod="+per_cod+"&est_cod="+est_cod,
			 beforeSend: function() {
				// $("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			if (datos == 1) {
				// alert("Estado cambiado");
			} else {
				alert(datos);
			}


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function descargar_informe() {
	var op;
	if ($("[name=rol]").is(":checked") && $("[name=sint]").is(":checked")) {
		op = "todo";
	} else if ($("[name=rol]").is(":checked")) {
		op = "rol";
	} else if ($("[name=sint]").is(":checked")) {
		op = "sint";
	} else {
		op = "nada";
	}
	window.open("/contactamos/reportes/operativo/"+$("#perreq").val()+"/"+op)
}

