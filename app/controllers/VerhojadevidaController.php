<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class VerhojadevidaController extends \Phalcon\Mvc\Controller
{
	 public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/hojadevida.css');
         
            
    }

     public function indexAction($per_cod)
    {
         $this->view->setVar("per_cod", $per_cod);

        $this->view->setVar("identificaciones", Tipos::find(array(
                "tip_tip = 'identificacion'"
        )));

        $this->view->setVar("grupos", Tipos::find(array(
                "tip_tip = 'sangre'"
        )));

        $this->view->setVar("cargos", Cargos::find(array(
                "car_est = '1' ORDER BY car_des"
         )));

        $this->view->setVar("sexos", Tipos::find(array(
                "tip_tip = 'sexo'"
        )));

        $this->view->setVar("civiles", Tipos::find(array(
                "tip_tip = 'civil'"
        )));

        $this->view->setVar("estaturas", Tipos::find(array(
                "tip_tip = 'est'"
        )));

        $this->view->setVar("estratos", Tipos::find(array(
                "tip_tip = 'eto'"
        )));

        $muni = new Muni();

        $this->view->setVar("municipios", $muni->listarMuniDpto());

        $this->view->setVar("per",Personas::findFirst($per_cod));

        $this->view->setVar("hijos", PersonasHijos::find(array(
                "per_cod = '".$per_cod."'"
        )));

        $this->view->setVar("estudios", PersonasEstudios::find(array(
                "per_cod = '".$per_cod."'"
        )));

        $this->view->setVar("experiencias", PersonasExp::find(array(
                "per_cod = '".$per_cod."'"
        )));

        $this->view->setVar("referencias", PersonasFam::find(array(
                "per_cod = '".$per_cod."'"
        )));

    }

    public function imprimirAction($per_cod) {
        $this->view->setVar("per",Personas::findFirst($per_cod));
        $ord = Ordenes::findFirst(["pos_cod=".$per_cod." ORDER BY ord_cod DESC"]);
        $this->view->setVar("ord", $ord);
        $this->view->setVar("emp", Empresas::findFirst($ord->emp_cod));
    }
}