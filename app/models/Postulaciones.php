<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Postulaciones extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $pos_cod;

    /**
     *
     * @var integer
     */
    public $per_cod;

    /**
     *
     * @var integer
     */
    public $req_cod;

    /**
     *
     * @var string
     */
    public $pos_fec;

    /**
     *
     * @var string
     */
    public $pos_est;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'pos_cod' => 'pos_cod', 
            'per_cod' => 'per_cod', 
            'req_cod' => 'req_cod', 
            'pos_fec' => 'pos_fec', 
            'pos_est' => 'pos_est'
        );
    }

    public  function postulado($per_cod, $req_cod){
        $sql = "SELECT *
                FROM postulaciones WHERE per_cod=$per_cod and req_cod=$req_cod";
        $obj = new Postulaciones();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function todasP($txt_bus){
        $sql = "SELECT *
                FROM personas p INNER JOIN postulaciones po ON p.per_cod=po.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN cargos c ON c.car_cod=r.car_cod WHERE p.per_est = '1' AND p.per_pno LIKE '$txt_bus%' OR p.per_pap LIKE '$txt_bus%' OR p.per_ide LIKE '$txt_bus%'";
        $obj = new Postulaciones();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function postuladoV($per_cod){
        $sql = "SELECT *
                FROM postulaciones WHERE per_cod=$per_cod";
        $obj = new Postulaciones();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

     public  function cargoPostulado($req_cod){
        $sql = "SELECT c.car_des
                FROM requisiciones r INNER JOIN cargos c ON r.car_cod=c.car_cod WHERE r.req_cod=$req_cod";
        $obj = new Postulaciones();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

}
