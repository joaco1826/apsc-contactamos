<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Entrevistas extends \Phalcon\Mvc\Model
{

	public  function obtener($pos_cod){
            $sql ="SELECT * FROM entrevistas WHERE pos_cod=$pos_cod ORDER BY ent_cod DESC LIMIT 1";

        // Base model
        $obj = new Entrevistas();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function obtenerE($pos_cod){
        $sql ="SELECT *, cp.nombre as name FROM entrevistas e INNER JOIN comp_entrevista ce ON ce.ent_cod=e.ent_cod AND e.ent_cod=(SELECT MAX(ent_cod) FROM entrevistas WHERE pos_cod=$pos_cod) INNER JOIN competencias cp ON cp.id=ce.com_cod WHERE e.pos_cod=$pos_cod ORDER BY e.ent_cod DESC";

        // Base model
        $obj = new Entrevistas();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }
}
