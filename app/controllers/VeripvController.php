<?php

class VerIPVController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/EvaluarPrueba.js');
    }

     public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
           $ipv = IpvPer::findFirst(array("per_cod=$per_cod ORDER BY ias_cod DESC"))->ias_cod;
          $this->view->setVar("ipv", $ipv);

    }

    public function eliminarAction()
    {
           $ipv = IpvPer::findFirst($this->request->getPost("cod"));
           if ($ipv) {
             if ($ipv->delete() == false) {
                echo "Lo sentimos, hubo un error: \n";

                foreach ($robot->getMessages() as $message) {
                    echo $message, "\n";
                }
            } else {
                echo "1";
            }
           }

    }
}