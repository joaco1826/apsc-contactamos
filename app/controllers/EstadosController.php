<?php
use Phalcon\Validation\Validator\PresenceOf,
        Phalcon\Validation\Validator\Email,
        Phalcon\Paginator\Adapter\Model as PaginatorModel;
class EstadosController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/estados.js');
         
            
    }

    public function indexAction()
    {
        $this->view->setVar("estados_sel", Estados::find(array(
            "est_pro='seleccion' and est_est='1'"
        )));

        $this->view->setVar("estados", Estados::find(array(
            "est_est='1'"
        )));

    }


    public function editarAction($est_cod)
    {
        

        $this->view->setVar("estado", Estados::findFirst($est_cod));
        
       
    }

    public function nuevoAction()
    {
        

        
       
    }

     public function guardarAction()
    {
        

        $validation = new Phalcon\Validation();
       
        $validation->add('est_nom', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));

        $validation->add('est_pro', new PresenceOf(array(
            'message' => 'El campo Tipo Es Requerido',

        )));

        


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $estado = new Estados();
        $estado->est_nom  = $this->request->getPost("est_nom");
        $estado->est_pro  = $this->request->getPost("est_pro");
        $estado->est_est = new \Phalcon\Db\RawValue('default');

        
        if($estado->save()){
            echo "1";
        }else{
             foreach ($estado->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
        

    }

     public function actualizarAction()
    {
        

        $validation = new Phalcon\Validation();
       
        $validation->add('est_nom', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));

        $validation->add('est_pro', new PresenceOf(array(
            'message' => 'El campo Tipo Es Requerido',

        )));


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $estado = Estados::findFirst($this->request->getPost("est_cod"));
        $estado->est_nom  = $this->request->getPost("est_nom");
        $estado->est_pro  = $this->request->getPost("est_pro");

        
        if($estado->save()){
            echo "1";
        }else{
             foreach ($estado->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
        

    }

     public function eliminarAction()
    {
        
        $estado = Estados::findFirst($this->request->getPost("est_cod"));
        $estado->est_est="0";
        if($estado->save()){
            echo "1";
        }else{
             foreach ($estado->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

}

