<?php

class Personasdet16pf extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $d16_cod;

    /**
     *
     * @var integer
     */
    public $pd_cod;

    /**
     *
     * @var integer
     */
    public $p16_cod;

    /**
     *
     * @var integer
     */
    public $r16_cod;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource('16pf_per_det');
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'd16_cod' => 'd16_cod', 
            'pd_cod' => 'pd_cod', 
            'p16_cod' => 'p16_cod', 
            'r16_cod' => 'r16_cod'
        );
    }

}
