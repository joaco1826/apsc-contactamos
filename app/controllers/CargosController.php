<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class CargosController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/cargos.js');
    }

    public function indexAction()
    {
    	// $this->assets
    	// 	 ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
     //         ->addCss('css/estilos.css');

    	$this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/cargos.js');

    	 $this->view->setVar("cargos", Cargos::find(array(
		    	"car_est = '1' ORDER BY car_des"
		 )));
    }

     public function nuevoAction()
    {


         $this->view->setVar("cargos_tipos", CargosTipos::find());
         $this->view->setVar("sectores", Sectores::find(array(
		    	"sec_est = '1'"
		 )));
        $this->view->setVar("competencias", Competencias::find(array(
            "status = 'activo'"
        )));


    }

     public function editarAction($car_cod)
    {


 		$this->view->setVar("cargos", Cargos::findFirst($car_cod));

 		$this->view->setVar("cargos_tipos", CargosTipos::find());

        $this->view->setVar("sectores", Sectores::find(array(
		    	"sec_est = '1'"
		 )));

        $com = new CompetenciasCargos();
        $this->view->setVar("competencias", $com->listar($car_cod));


    }

     public function guardarAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('car_des', new PresenceOf(array(
		    'message' => 'El campo Cargo Es Requerido',

		)));

		$validation->add('tic_cod', new PresenceOf(array(
		    'message' => 'El campo Tipo de cargo Es Requerido',

		)));

		$validation->add('sec_cod', new PresenceOf(array(
		    'message' => 'El campo Sector Es Requerido',

		)));


		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$cargo = new Cargos();
    	$cargo->car_des  = $this->request->getPost("car_des");
		$cargo->car_16p  = ($this->request->getPost("car_16p")!="") ? "1" : "0";
		$cargo->car_val  = ($this->request->getPost("car_val")!="") ? "1" : "0";
		$cargo->car_cmt  = ($this->request->getPost("car_cmt")!="") ? "1" : "0";
		$cargo->car_car  = ($this->request->getPost("car_car")!="") ? "1" : "0";
		$cargo->car_ipv  = ($this->request->getPost("car_ipv")!="") ? "1" : "0";
		$cargo->car_pnl  = ($this->request->getPost("car_pnl")!="") ? "1" : "0";
		$cargo->tic_cod  = $this->request->getPost("tic_cod");
		$cargo->sec_cod  = $this->request->getPost("sec_cod");
    	$cargo->car_est  = new \Phalcon\Db\RawValue('default');


    	if($cargo->save()){
    	    $cargo_id = $cargo->car_cod;
    	    $competencias = $this->request->getPost("competencias");
    	    foreach ($competencias as $i => $com) {
                $comp_cargos = new CompetenciasCargos();
                $comp_cargos->cargo_id = $cargo_id;
                $comp_cargos->competencia_id = $com;
                $comp_cargos->valor = $this->request->getPost("radio_".$com);
                $comp_cargos->created_at  = new \Phalcon\Db\RawValue('default');
                $comp_cargos->updated_at  = new \Phalcon\Db\RawValue('default');
                $comp_cargos->status  = new \Phalcon\Db\RawValue('default');
                $comp_cargos->save();
            }
    		echo "1";
    	}else{
    		 foreach ($cargo->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

     public function actualizarAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('car_des', new PresenceOf(array(
		    'message' => 'El campo Cargo Es Requerido',

		)));

		$validation->add('tic_cod', new PresenceOf(array(
		    'message' => 'El campo Tipo de cargo Es Requerido',

		)));

		$validation->add('sec_cod', new PresenceOf(array(
		    'message' => 'El campo Sector Es Requerido',

		)));


		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$cargo           = Cargos::findFirst($this->request->getPost("car_cod"));
    	$cargo->car_des  = $this->request->getPost("car_des");
		$cargo->car_16p  = ($this->request->getPost("car_16p")!="") ? "1" : "0";
		$cargo->car_val  = ($this->request->getPost("car_val")!="") ? "1" : "0";
		$cargo->car_cmt  = ($this->request->getPost("car_cmt")!="") ? "1" : "0";
		$cargo->car_car  = ($this->request->getPost("car_car")!="") ? "1" : "0";
		$cargo->car_ipv  = ($this->request->getPost("car_ipv")!="") ? "1" : "0";
		$cargo->car_pnl  = ($this->request->getPost("car_pnl")!="") ? "1" : "0";
		$cargo->tic_cod  = $this->request->getPost("tic_cod");
		$cargo->sec_cod  = $this->request->getPost("sec_cod");
    	$cargo->car_est  = new \Phalcon\Db\RawValue('default');


    	if($cargo->save()){
            $cargo_id = $cargo->car_cod;
            $comp_cargos = CompetenciasCargos::find("cargo_id=".$cargo_id);
            $comp_cargos->delete();
            $competencias = $this->request->getPost("competencias");
            foreach ($competencias as $i => $com) {
                $comp_cargos = new CompetenciasCargos();
                $comp_cargos->cargo_id = $cargo_id;
                $comp_cargos->competencia_id = $com;
                $comp_cargos->valor = $this->request->getPost("radio_".$com);
                $comp_cargos->created_at  = new \Phalcon\Db\RawValue('default');
                $comp_cargos->updated_at  = new \Phalcon\Db\RawValue('default');
                $comp_cargos->status  = new \Phalcon\Db\RawValue('default');
                $comp_cargos->save();
            }
            echo "1";
    	}else{
    		 foreach ($cargo->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

     public function eliminarAction()
    {

    	$cargo = Cargos::findFirst($this->request->getPost("cod"));
    	$cargo->car_est="0";
    	if($cargo->save()){
    		echo "1";
    	}else{
    		 foreach ($cargo->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    }

}

