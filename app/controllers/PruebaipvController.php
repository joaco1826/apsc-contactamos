<?php

class PruebaIpvController extends \Phalcon\Mvc\Controller
{

	 public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
              ->addCss('css/reloj.css')
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jpegcam/htdocs/webcam.js')
            ->addJs('js/countdown/jquery.countdown.min.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/pruebas.js');
    }

    public function indexAction()
    {
    	// $this->assets
     //         ->addCss('css/reloj.css');

    	// $this->assets
     //        ->addJs('js/jquery.js')
     //        ->addJs('js/jpegcam/htdocs/webcam.js')
     //        ->addJs('js/countdown/jquery.countdown.min.js')
     //        ->addJs('js/pruebas.js');

        $this->view->setVar("preguntas", IpvPre::find());
    }

      public function subirFotoAction()
	 {
	    	$jpeg_data = file_get_contents('php://input');
			$foto = md5(microtime()*rand(1.1,1.9))."_".$this->session->get("per_cod");
			$filename = "photos/".$foto.".jpg";

			$fot = new Fotos();
			$fot->per_cod = $this->session->get("per_cod");
			$fot->fot_rut = $filename;
			$fot->fot_pru = "IPV";
			$fot->fot_fec = new \Phalcon\Db\RawValue('default');

			if($fot->save()){
				$result = file_put_contents( $filename, $jpeg_data );
			}
	 }

	 public function guardarAction()
	{
	 	if ($this->request->isPost() == true) {
	    	
	    	$per = new IpvPer();
	    	$per->per_cod    = $this->request->getPost("per_cod");
	    	$per->req_cod    = $this->request->getPost("req_cod");
	    	$per->usu_cod    = $this->request->getPost("per_cod");
	    	$per->ias_fch    = new \Phalcon\Db\RawValue('default');
	    	$per->ias_web    = new \Phalcon\Db\RawValue('default');


	    	$this->db->begin();

			if($per->save() == false){
				$this->db->rollback();
				return;
			}


			$preguntas = IpvPre::find();

			foreach ($preguntas as  $reg) {
				$ire_num="";
				$ipr_cod = $reg->ipr_cod;
				
				// if(isset($this->request->getPost($p16_cod)){
					$ire_num = $this->request->getPost($ipr_cod, null, 0); // (valor, sanizar, valor por defecto)
				// }

				$per_det = new IpvPerDet();
				$per_det->ias_cod = $per->ias_cod;
				$per_det->ipr_cod = $ipr_cod;
				$per_det->ire_num = $ire_num;

				if($per_det->save() == false){
					$this->db->rollback();
					return;
				}

			}


			 $this->db->commit();
			 // echo "Prueba Enviada";
			 // return;
			  $this->dispatcher->forward(array(
		            "controller" => "verPruebas",
		            "action" => "index",
		            "params" => array("resultado" => 1)
        	  ));
		}else{
		     $this->response->redirect("verPruebas/");
		}	
	    	
	}



}

