<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Valanti extends \Phalcon\Mvc\Model
{

 public static function resultado($id)
    {
      	$hoy = date("Y-m-d");
        $fecha = strtotime ( '-720 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
        $sql = "SELECT CASE 
				WHEN  vde_res=1 THEN 2
				WHEN vde_res=2 THEN 1
				WHEN vde_res=3 THEN 0
				WHEN vde_res=0 THEN 3
				END AS res2,  vde_pre AS pregunta, vde_res AS res1  FROM valanti v 
				JOIN valanti_det vd ON v.`val_cod`=vd.`val_cod`
				WHERE v.val_cod=(SELECT MAX(val_cod) FROM valanti WHERE per_cod='$id') AND DATE(v.val_fch) BETWEEN '$fecha' AND '$hoy' LIMIT 30";

        $valanti = new Valanti();

        return new Resultset(null, $valanti, $valanti->getReadConnection()->query($sql));
    }

    public static function npruebas($fecha)
    {
        // A raw SQL statement

       $sql ="SELECT COUNT(*) as total
            FROM valanti 
            WHERE DATE(val_fch) = '$fecha'";

        // Base model
       $valanti = new Valanti();

        return new Resultset(null, $valanti, $valanti->getReadConnection()->query($sql));
    }


}
