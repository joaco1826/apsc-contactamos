<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class Pruebas extends \Phalcon\Mvc\Model
{
    public  function obtener($pos_cod){
        $sql ="SELECT *, cp.nombre as name FROM pruebas e INNER JOIN comp_pruebas ce ON ce.pru_cod=e.pru_cod INNER JOIN competencias cp ON cp.id=ce.com_cod WHERE e.pos_cod=$pos_cod ORDER BY e.pru_cod DESC";

        // Base model
        $obj = new Pruebas();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

}
