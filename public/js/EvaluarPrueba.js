$(document).ready(function(){
	$("#btn_guardar").click(function(event) {
		
		$.ajax({
			url: $("#url").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Registro Guardado");
				location.reload()
			}else{
				if (datos==2) {
					alert("Su sessión ha expirado, por favor vuelva a iniciar");
					location.href= $("#url_base").val();
				}
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});
	  //RELACIONES INTERPERSONALES
	  $("input[name='pru_rip']").change(function() {
          // alert("cambio")
		  var titulo  = "<div class='nombre2'><p>REL. INTERPERSONALES</p></div>";
		  $("#titulo_ri").html(titulo)
		  
            if ($("input[name='pru_rip']:checked").val() == 'A'){
              // alert("A");
			   $("#desc_ri").html("Es consciente de  los demás y del entorno, así como de la influencia que ejerce sobre ambos. Refleja reconocimiento de los sentimientos de los demás. Entiende la óptica de sus compañeros, usando sus propias categorías y conceptos, la naturaleza de sus problemas emocionales y personales, así como la forma en que su acción y decisiones pueden afectar positiva o negativamente a quienes le rodean. Entiende de manera profunda los sentimientos y el estado emocional de quienes le rodean, y es consciente de hasta dónde se puede actuar sobre ello. Se preocupa por el estado de salud de sus compañeros y líderes. Investiga activamente problemas que puedan estar afectando a sus colaboradores o a otras personas en la organización. Apoya activamente a las personas que se añaden al grupo en su integración Recuerda los acontecimientos personales importantes. Resuelve problemas de los compañeros en el plano personal. Se interesa periódicamente por la situación emocional de sus compañeros. Se interesa por situaciones familiares, enfermedades, problemas personales y profesionales de los compañeros y líderes. Participa en celebraciones de relevancia de los compañeros. Comunica de forma inmediata las buenas noticias.<br><br>");
			}
			if ($("input[name='pru_rip']:checked").val() == 'B'){
              // alert("B");
			   $("#desc_ri").html("Logra hacer consciencia de los demás y del entorno, así como de la influencia que ejerce sobre ambos. Reconoce en general los sentimientos de los demás. Se esfuerza con éxito por entiender la óptica de sus compañeros, usando sus propias categorías y conceptos, la naturaleza de sus problemas emocionales y personales, así como la forma en que su acción y decisiones pueden afectar positiva o negativamente a quienes le rodean. En general entiende los sentimientos y  l estado emocional de quienes le rodean, y es consciente de hasta dónde se puede actuar sobre ello. Se preocupa con frecuencia regular por el estado de salud de sus compañeros y líderes. Se interesa activamente por los problemas que puedan estar afectando a sus colaboradores o a otras personas en la organización. Apoya a las personas que se añaden al grupo en su integración Se interesa en recordar los acontecimientos personales importantes. Se esfuerza con éxito por resolver problemas de los compañeros en el plano personal. Se interesa periódicamente por la situación emocional de sus compañeros. Se interesa periódicamente por situaciones familiares, enfermedades, problemas personales y profesionales de los compañeros y líderes. Participa con frecuencia en celebraciones de relevancia de los compañeros. Comunica de forma inmediata las buenas noticias.<br><br>");
			}
			if ($("input[name='pru_rip']:checked").val() == 'MR'){
              // alert("B");
			   $("#desc_ri").html("Con esfuerzo logra hacer consciencia de los demás y del entorno, así como de la influencia que ejerce sobre ambos. Tras mucho esfuerzo reconoce los sentimientos de los demás.Con trabajo entiende la óptica de sus compañeros, usando aspectos de sus categorías y conceptos, la naturaleza de sus problemas emocionales y personales, así como con esfuerzo calibra la forma en que su acción y decisiones pueden afectar positiva o negativamente a quienes le rodean. Con dificultad entiende los sentimientos y el estado emocional de quienes le rodean, y es con esfuerzo consciente de hasta dónde se puede actuar sobre ello. Se preocupa con alguna frecuencia por el estado de salud de sus compañeros y líderes. Eventualmente se interesa por los problemas que puedan estar afectando a sus colaboradores o a otras personas en la organización. En ocasiones apoya a las personas que se añaden al grupo en su integración Se interesa en contados acontecimientos personales importantes. Con algunas dificultades logra resolver problemas con sus compañeros en el plano personal. Se interesa eventualmente por la situación emocional de sus compañeros. Se interesa con poca frecuencia por situaciones familiares, enfermedades, problemas personales y profesionales de los compañeros y líderes. Participa en contadas celebraciones de relevancia de los compañeros. Comunica las buenas noticias.<br><br>");
			}
			if ($("input[name='pru_rip']:checked").val() == 'D'){
              // alert("B");
			   $("#desc_ri").html("No logra hacer consciencia de los demás y del entorno, así como de la influencia que ejerce sobre ambos. No le es posible reconocer los sentimientos de los demás. Le cuesta entender la óptica de sus compañeros, usar aspectos de sus categorías y conceptos, la naturaleza de sus problemas emocionales y personales, así como se le imposibilita calibrar la forma en que su acción y decisiones pueden afectar positiva o negativamente a quienes le rodean. No logra entender los sentimientos y el estado emocional de quienes le rodean, y no logra ser consciente de  asta dónde se puede actuar sobre ello. No se preocupa por el estado de salud de sus compañeros y lí eres. No se interesa por los problemas que puedan estar afectando a sus colaboradores o a otras per onas en la organización. No apoya a las personas que se añaden al grupo en su integración No se interesa en acontecimientos personales importantes. No logra resolver problemas con sus compañeros en el plano personal.  No se interesa por la situación emocional de sus compañeros. No Se interesa por situaciones familiares, enfermedades, problemas personales y profesionales de los compañeros y líderes. No participa en celebraciones de relevancia de los compañeros.<br><br>");
			}
			
			
	  });
	  
	  //ACTITUD HACIA LAS NORMAS
	  
	   $("input[name='pru_ahn']").change(function() {
          // alert("cambio")
		  var titulo  = "<div class='nombre2'><p>ACTITUD HACIA LAS NORMAS</p></div>";
		  $("#titulo_an").html(titulo)
		  
            if ($("input[name='pru_ahn']:checked").val() == 'A'){
              // alert("A");
			   $("#desc_an").html("Actúa fácilmente conforme a las normas y a los estándares éticos establecidos y se responsabiliza de las propias decisiones erróneas. No acepta por ningún motivo o circunstancia beneficios inmerecidos o inequidades con respecto a otros de igual derecho. Asume siempre la responsabilidad de las consecuencias negativas de la propia actuación. Muestra mucha coherencia entre lo que dice y lo que hace. No se apropia nunca de éxitos ajenos.<br><br>");
			}
			if ($("input[name='pru_ahn']:checked").val() == 'B'){
              // alert("B");
			   $("#desc_an").html("Actúa conforme a las normas y a los estándares éticos establecidos y se responsabiliza de las propias decisiones erróneas. No acepta beneficios inmerecidos o inequidades con respecto a otros de igual derecho. Asume responsabilidad de las consecuencias negativas de la propia actuación. Muestra coherencia entre lo que dice y lo que hace. No se apropia de éxitos ajenos.<br><br>");
			}
			if ($("input[name='pru_ahn']:checked").val() == 'MR'){
              // alert("B");
			   $("#desc_an").html("Le cuesta actuar conforme a las normas y a los estándares éticos establecidos y tiende a responsabilizar a otros de las propias decisiones erróneas. Puede aceptar beneficios inmerecidos o inequidades con respecto a otros de igual derecho. Le es difícil asumir responsabilidad de las consecuencias negativas de la propia actuación. Muestra poco coherencia entre lo que dice y lo que hace. Se apropia de éxitos ajenos.<br><br>");
			}
			if ($("input[name='pru_ahn']:checked").val() == 'D'){
              // alert("B");
			   $("#desc_an").html("No actua conforme a las normas y a los estándares éticos establecidos y responsabiliza a otros de las propias decisiones erróneas. Acepta beneficios inmerecidos o inequidades con respecto a otros de igual derecho. No se Responsabiliza de las consecuencias negativas de la propia actuación. No Muestra coherencia entre lo que dice y lo que hace. Se apropia de éxitos ajenos.<br><br>");
			}
			
			
	  });
	  
	   //ADAPTABILIDAD
	  
	   $("input[name='pru_ada']").change(function() {
          // alert("cambio")
		  var titulo  = "<div class='nombre2'><p>ADAPTABILIDAD</p></div>";
		  $("#titulo_ad").html(titulo)
		  
            if ($("input[name='pru_ada']:checked").val() == 'A'){
              // alert("A");
			   $("#desc_ad").html("Modifica su conducta rápidamente para alcanzar determinados objetivos cuando surgen dificultades, nuevos datos o cambios en el entorno Es versátil en la emisión de conductas adaptativas sin cambiar su sistema de valores, expectativas y/o creencias, actuando en el momento adecuado para su implantación o a otros cambios en el entorno. Modifica objetivos de un colaborador cuando se ve imposible que éste los alcance. Cambia la estrategia de relación con un alterno cuando han cambiado los interlocutores en el seno de su organización. Cambia la forma de relación con el líder cuando se han percibido cambios en su actitud en los últimos encuentros. Cambia la actitud ante un alterno cuando la calidad de la información del interlocutor ha cambiado sustancialmente en la actividad. Anula un accionar y adopta otro cuando la situación lo requiere.<br><br>");
			}
			if ($("input[name='pru_ada']:checked").val() == 'B'){
              // alert("B");
			   $("#desc_ad").html("Modifica su conducta para alcanzar determinados objetivos cuando surgen dificultades, nuevos datos o cambios en el entorno Emite conductas adaptativas sin cambiar su sistema de valores, expectativas y/o creencias, actuando en el momento adecuado para su implantación o a otros cambios en el entorno. Figura como modificar objetivos de un colaborador cuando se ve imposible que éste los alcance. Se esfuerza en el cambio de estrategia en relación con un alterno cuando han cambiado los interlocutores en el seno de su organización. Modifica aspectos en la forma de relación con el líder cuando se han percibido cambios en su actitud en los últimos encuentros. Con esfuerzo cambia su actitud ante un alterno cuando la calidad de la información del interlocutor ha cambiado sustancialmente en la actividad. Tras breve tiempo logra cambiar un accionar y adopta otro cuando la situación lo requiere.<br><br>");
			}
			if ($("input[name='pru_ada']:checked").val() == 'MR'){
              // alert("B");
			   $("#desc_ad").html("Tras mucho esfuerzo puede modificar su conducta para alcanzar determinados objetivos cuando surgen dificultades, nuevos datos o cambios en el entorno Con trabajo logra emitir conductas adaptativas sin cambiar su sistema de valores, expectativas y/o creencias, actuando para su implantación o a otros cambios en el entorno. Con esfuerzo modifica objetivos de un colaborador cuando se ve imposible que éste los alcance. Con dificultades logra un cambio de estrategia en relación con un alterno cuando han cambiado los interlocutores en el seno de su organización. Con mucho trabajo cambia la forma de relación con el líder cuando se han percibido cambios en su actitud en los últimos encuentros. eventualmente puede cambiar su actitud ante un alterno cuando la calidad de la información del interlocutor ha cambiado sustancialmente en la actividad. Con mucho trabajo logra cambiar un accionar y adopta otro cuando la situación lo requiere.<br><br>");
			}
			if ($("input[name='pru_ada']:checked").val() == 'D'){
              // alert("B");
			   $("#desc_ad").html("No le es posible modificar su conducta para alcanzar determinados objetivos cuando surgen dificultades, nuevos datos o cambios en el entorno No logra emitir conductas adaptativas  y puede cambiar su sistema de valores, expectativas y/o creencias, actuando para su implantación o a otros cambios en el entorno. No se interesa en modificar objetivos de un colaborador cuando se ve imposible que éste los alcance. No logra un cambio de estrategia en relación con un alterno cuando han cambiado los interlocutores en el seno de su organización. Le cuesta cambiar la forma de relación con el líder cuando se han percibido cambios en su actitud en los últimos encuentros. No le es posible cambiar su actitud ante un alterno cuando la calidad de la información del interlocutor ha cambiado sustancialmente en la actividad.No logra cambiar un accionar y adoptar otro cuando la situación lo requiere.<br><br>");
			}
			
			
	  });
	  
	  //COMUNICACIÓN
	   $("input[name='pru_com']").change(function() {
          // alert("cambio")
		  var titulo  = "<div class='nombre2'><p>COMUNICACIÓN</p></div>";
		  $("#titulo_c").html(titulo)
		  
            if ($("input[name='pru_com']:checked").val() == 'A'){
              // alert("A");
			   $("#desc_c").html("Canaliza clara y comprensiblemente ideas y opiniones hacia los demás a través del discurso hablado y escrito Es capaz de expresar pensamientos o contenidos internos de manera comprensible para el interlocutor, con toda la potencia de la palabra hablada, utilizada de forma proporcional al objetivo y a la audiencia que recibe el mensaje, utilizando las imágenes verbales y los recursos lingüísticos adecuados.Estructura bien los mensajes. Capta la atención del interlocutor. Precisa el mensaje oral y no permite frases hechas y/o sobreentendidas. Influye exitosamente en la mejora de la escucha del interlocutor, tanto personalmente como en los compañeros Habla con precisión Identifica con claridad y acierto los contenidos de la propia comunicación. Expresa ideas con orden. Da y recibe Feedback. Es conciso y directo. Utiliza expresiones brillantes y descriptivas.<br><br>");
			}
			if ($("input[name='pru_com']:checked").val() == 'B'){
              // alert("B");
			   $("#desc_c").html("Con esfuerzo logra canalizar clara y comprensiblemente ideas y opiniones hacia los demás a través del discurso hablado y escrito Es en general capaz de expresar pensamientos o contenidos internos de manera comprensible para el interlocutor, con potencia de la palabra hablada, utilizada de forma proporcional al objetivo y a la audiencia que recibe el mensaje, utilizando las imágenes verbales y los recursos lingüísticos adecuados. Se esfuerza con éxito en estructurar bien los mensajes. Con esfuerzo capta la atención del interlocutor. Trabaja en la precisión del mensaje oral y no permite frases hechas y/o sobreentendidas. Influye en la mejora de la escucha del interlocutor, tanto personalmente como en los compañeros Habla en general con un buen grado de prec sión Identifica los contenidos de la propia comunicación. Expresa ideas con estructura adecuada Da y recibe Feedback. Es en general conciso y directo. Utiliza expresiones entendibles y descriptivas.<br><br>");
			}
			if ($("input[name='pru_com']:checked").val() == 'MR'){
              // alert("B");
			   $("#desc_c").html("Con mucho esfuerzo logra canalizar ideas y opiniones hacia los demás a través del discurso hablado y escrito Con mucho trabajo y tiempo logra expresar pensamientos o contenidos internos de manera comprensible para el interlocutor. Con trabajo logra utilizar la palabra de forma proporcional al objetivo y a la audiencia que recibe el mensaje, utilizando las imágenes verbales y los recursos lingüísticos mínimos necesarios. Con mucho trabajo logra estructurar bien los mensajes. Con dificultad logra captar la atención del interlocutor. Lecuesta la precisión del mensaje oral, cayendo en ocasiones en frases hechas y/o sobreentendidas. Le cuesta hablar con precisión Con dificultad logra identificar los contenidos de la propia comunicación. Expresa ideas con estructura simple Le cuesta ser conciso y directo. Utiliza expresiones simples y poco elaboradas.<br><br>");
			}
			if ($("input[name='pru_com']:checked").val() == 'D'){
              // alert("B");
			   $("#desc_c").html("No le es posible logra canalizar ideas y opiniones hacia los demás a través del discurso hablado y escrito No logra expresar pensamientos o contenidos internos de manera comprensible para el interlocutor. No logra utilizar la palabra de forma proporcional al objetivo y a la audiencia que recibe el mensaje. No logra estructurar bien los mensajes. No logra captar la atención del interlocutor. No muestra precisión del mensaje oral, cayendo en frases hechas y/o sobreentendidas. No logra identificar los contenidos de la propia comunicación. Utiliza expresiones simples y sin estructura.<br><br>");
			}
			
			
	  });
	  
	   //LIDERAZGO
	   $("input[name='pru_lid']").change(function() {
          // alert("cambio")
		  var titulo  = "<div class='nombre2'><p>LIDERAZGO</p></div>";
		  $("#titulo_l").html(titulo)
		  
            if ($("input[name='pru_lid']:checked").val() == 'A'){
              // alert("A");
			   $("#desc_l").html("Informa a las personas sobre todos los detalles y aspectos relevantes que afecten a su trabajo.  Atiende las demandas de información y/o ayuda de los colaboradores. Aclara dudas. Recibe información referente a las personas. Se preocupa por los temas personales de sus colaboradores. Reconoce el éxito de los colaboradores. Analiza a cada colaborador para utilizar con él las técnicas de comunicación adecuadas. Adapta a cada colaborador y a sus necesidades el propio estilo de mando y el nivel de exigencia. Mantiene con cada colaborador una relación personal cercana y de confianza mutua, donde puedan expresarse todos los problemas y dificultades sin recelos. Sabe modular los niveles de exigencia respecto a las posibilidades reales de cada cual, imponiendo con firmeza objetivos ambiciosos pero realistas. Marca objetivos (reuniones con colaboradores y subordinados). Establece y diseña los puestos de trabajo de sus colaboradores. Fija políticas de actuación a los colaboradores. Establece corrientes de comunicación (transmitir ideas, ser comunicador del grupo). Corrige actuaciones de desviaciones de objetivos. Delegar funciones. Resuelve incidencias organizativas y/o de relaciones interpersonales. Marca objetivos cualitativos. Corrige y mecaniza conductas o actuaciones orientadas a los objetivos.<br><br>");
			}
			if ($("input[name='pru_lid']:checked").val() == 'B'){
              // alert("B");
			   $("#desc_l").html("Informa a las personas sobre todos los detalles y aspectos relevantes que afecten a su trabajo.  Atiende las demandas de información y/o ayuda de los colaboradores. Aclara dudas. Recibe información referente a las personas. Se preocupa por los temas personales de sus colaboradores. Reconoce el éxito de los colaboradores. Analiza a cada colaborador para utilizar con él las técnicas de comunicación adecuadas. Adapta a cada colaborador y a sus necesidades el propio estilo de mando y el nivel de exigencia. Mantiene con cada colaborador una relación personal cercana y de confianza mutua, donde puedan expresarse todos los problemas y dificultades sin recelos. Sabe modular los niveles de exigencia respecto a las posibilidades reales de cada cual, imponiendo con firmeza objetivos ambiciosos pero realistas. Marca objetivos (reuniones con colaboradores y subordinados). Establece y diseña los puestos de trabajo de sus colaboradores. Fija políticas de actuación a los colaboradores. Establece corrientes de comunicación (transmitir ideas, ser comunicador del grupo). Corrige actuaciones de desviaciones de objetivos. Delegar funciones. Resuelve incidencias organizativas y/o de relaciones interpersonales. Marca objetivos cualitativos. Corrige y mecaniza conductas o actuaciones orientadas a los objetivos.<br><br>");
			}
			if ($("input[name='pru_lid']:checked").val() == 'MR'){
              // alert("B");
			   $("#desc_l").html("Poco informa a las personas sobre todos los detalles y aspectos relevantes que afecten a su trabajo. Difícilmente atiende las demandas de información y/o ayuda de los colaboradores. Poco aclara dudas o recibe información referente a las personas. Difícilmente se preocupa por los temas personales de sus colaboradores. Poco reconoce el éxito de los colaboradores. Le es difícil analizar a cada colaborador para utilizar con él las técnicas de comunicación adecuadas. Se le dificulta adapta a cada colaborador y a sus necesidades el propio estilo de mando y el nivel de exigencia. Poco mantiene con cada colaborador una relación personal cercana y de confianza mutua, donde puedan expresarse todos los problemas y dificultades sin recelos. Poco modula los niveles de exigencia respecto a las posibilidades reales de cada cual, sin imponer con mucha firmeza objetivos ambiciosos pero realistas. Poco marca objetivos (reuniones con colaboradores y subordinados). Difícilmente establece o diseña los puestos de trabajo de sus colaboradores. Poco fija políticas de actuación a los colaboradores. Poco establece corrientes de comunicación (transmitir ideas, ser comunicador del grupo). Poco corrige actuaciones de desviaciones de objetivos. Difícilmente delega funciones ni resuelve incidencias organizativas y/o de relaciones interpersonales. Se le dificulta marcar objetivos cualitativos. Poco corrige o mecaniza conductas o actuaciones orientadas a los objetivos.<br><br>");
			}
			if ($("input[name='pru_lid']:checked").val() == 'D'){
              // alert("B");
			   $("#desc_l").html("No informa a las personas sobre todos los detalles y aspectos relevantes que afecten a su trabajo.  No atiende las demandas de información y/o ayuda de los colaboradores. No aclara dudas ni recibe información referente a las personas. No se preocupa por los temas personales de sus colaboradores. No reconoce el éxito de los colaboradores. No analiza a cada colaborador para utilizar con él las técnicas de comunicación adecuadas. No adapta a cada colaborador y a sus necesidades el propio estilo de mando y el nivel de exigencia. No mantiene con cada colaborador una relación personal cercana y de confianza mutua, donde puedan expresarse todos los problemas y dificultades sin recelos. No modula los niveles de exigencia respecto a las posibilidades reales de cada cual, sin imponer con firmeza objetivos ambiciosos pero realistas. No marca objetivos (reuniones con colaboradores y subordinados). No establece ni diseña los puestos de trabajo de sus colaboradores. No fija políticas de actuación a los colaboradores. No establece corrientes de comunicación (transmitir ideas, ser comunicador del grupo). No corrige actuaciones de desviaciones de objetivos. No delega funciones ni resuelve incidencias organizativas y/o de relaciones interpersonales. No marca objetivos cualitativos. No corrige ni mecaniza conductas o actuaciones orientadas a los objetivos.<br><br>");
			}
			
			
	  });
	  
	   //MOTIVACION
	   $("input[name='pru_mot']").change(function() {
          // alert("cambio")
		  var titulo  = "<div class='nombre2'><p>MOTIVACIÓN</p></div>";
		  $("#titulo_ml").html(titulo)
		  
            if ($("input[name='pru_mot']:checked").val() == 'A'){
              // alert("A");
			   $("#desc_ml").html("Fija  metas de forma ambiciosa y determinada, por encima de los estándares y de las expectativas, mostrando insatisfacción con el desempeño promedio Es ambicioso en cuanto a la consecución de resultados positivos, aún más allá de las exigencias institucionales. Muestra un impulso alto para conseguir retos y desafíos personales, aplicando de forma autodirigida la originalidad de planteamientos novedosos para alcanzar la meta. Se fija objetivos superiores a los establecidos, de forma realista y ambiciosa. Se interesa en concursos, premios, competiciones deportivas, etc. Es frecuente voluntario a tareas. Trabaja hasta alcanzar las metas o retos propuestos.<br><br>");
			}
			if ($("input[name='pru_mot']:checked").val() == 'B'){
              // alert("B");
			   $("#desc_ml").html("Se esfuerza con éxito  en fijar  metas de forma ambiciosa y determinada, acorde con los estándares y de las expectativas, mostrando insatisfacción con el desempeño inferior Se enfuerza con éxito en la consecución de resultados positivos, acorde con lo demandado por las exigencias institucionales. Muestra interes en conseguir retos y desafíos personales, aplicando de forma autodirigida la originalidad de planteamientos novedosos para alcanzar la meta. Se fija objetivos acorde a los establecidos, de forma realista y ambiciosa. Se interesa en concursos, premios, competiciones deportivas, etc Es frecuente voluntario a tareas. Trabaja hasta alcanzar las metas o retos propuestos.<br><br>");
			}
			if ($("input[name='pru_mot']:checked").val() == 'MR'){
              // alert("B");
			   $("#desc_ml").html("Con esfuerzo logra fijar  metas acordes con los estándares y de las expectativas, mostrando satisfacción con el desempeño mínimo requerido Logra con mucho esfuerzo la consecución de resultados positivos, acorde con lo demandado por las exigencias institucionales. Busca cumplir con los retos y desafíos personales. Los objetivos fijados son los estblecidos por el programa para él. Participa en concursos, premios, competiciones deportivas, etc que son delegados por el programa. cumple con el mínimo requerido para hasta alcanzar las metas o retos propuestos.<br><br>");
			}
			if ($("input[name='pru_mot']:checked").val() == 'D'){
              // alert("B");
			   $("#desc_ml").html("No logra fijar  metas acordes con los estándares y de las expectativas, mostrando satisfacción con el desempeño mínimo requerido No logra la consecución de resultados positivos con lo demandado por las exigencias institucionales. Los objetivos fijados son los estblecidos por el programa para él. No participa en concursos, premios, competiciones deportivas, etc que son delegados por el programa. No cumple con el mínimo requerido para hasta alcanzar las metas o retos propuestos.<br><br>");
			}
			
			
	  });
	  
	   //CAPACIDAD DE APRENDIZAJE
	   $("input[name='pru_cap']").change(function() {
          // alert("cambio")
		  var titulo  = "<div class='nombre2'><p>CAPACIDAD DE APRENDIZAJE</p></div>";
		  $("#titulo_ca").html(titulo)
		  
            if ($("input[name='pru_cap']:checked").val() == 'A'){
              // alert("A");
			   $("#desc_ca").html("Aprovecha fácilmente las soluciones aportadas por otros a los problemas cuando observa determinadas conductas en los interlocutores. Lleva con agilidad determinadas teorías a casos concretos y reales, modifica la propia conducta después de cometer errores. Actúa diligentemente tras estudiar y analizar las diferentes circulares y notas internas sobre normativa, enviadas por el departamento de Organización. Asimila con mucha facilidad nueva información y aplica correctamente e Imita la conducta de otras personas para mejorar la propia, lleva con facilidad a la práctica correctamente instrucciones complejas.<br><br>");
			}
			if ($("input[name='pru_cap']:checked").val() == 'B'){
              // alert("B");
			   $("#desc_ca").html("Aprovecha las soluciones aportadas por otros a los problemas cuando observa determinadas conductas en los interlocutores. Lleva determinadas teorías a casos concretos y reales, modifica la propia conducta después de cometer errores. Actúa tras estudiar y analizar las diferentes circulares y notas internas sobre normativa, enviadas por el departamento de Organización. Asimila nueva información y aplica correctamente e Imita la conducta de otras personas para mejorar la propia, lleva a la práctica correctamente instrucciones complejas.<br><br>");
			}
			if ($("input[name='pru_cap']:checked").val() == 'MR'){
              // alert("B");
			   $("#desc_ca").html("Se le dificulta aprovechar las soluciones aportadas por otros a los problemas cuando observa determinadas conductas en los interlocutores. Le es difícil llevar determinadas teorías a casos concretos y reales, le cuesta modificar la propia conducta después de cometer errores. Actúa poco tras estudiar y analizar las diferentes circulares y notas internas sobre normativa, enviadas por el departamento de Organización. Tiene dificultad para asimilar nueva información y aplicarla correctamente e Imitar la conducta de otras personas para mejorar la propia, le cuesta llevar a la práctica correctamente instrucciones complejas.<br><br>");
			}
			if ($("input[name='pru_cap']:checked").val() == 'D'){
              // alert("B");
			   $("#desc_ca").html("Le es imposible aprovechar las soluciones aportadas por otros a los problemas cuando observa determinadas conductas en los interlocutores. No puede aplicar determinadas teorías a casos concretos y reales, ni modificar la propia conducta después de cometer errores. No actua tras estudiar y analizar las diferentes circulares y notas internas sobre normativa, enviadas por el departamento de Organización. No puede asimilar nueva información y aplicarla correctamente e Imitar la conducta de otras personas para mejorar la propia, ni llevar a la práctica correctamente instrucciones complejas.<br><br>");
			}
			
			
	  });
	  
	});

function eliminarPrueba(cod) {
	if(confirm("Desea realmente eliminar la prueba?")){
	$.ajax({
			url: $("#url_eliminar").val(),
			type: 'POST',
			dataType: 'html',
			data: "cod="+cod,
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Registro Eliminado");
				location.reload()
			}else{
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	}
}