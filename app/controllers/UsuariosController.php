<?php
use Phalcon\Validation\Validator\PresenceOf,
        Phalcon\Validation\Validator\Email;
class UsuariosController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/usuarios.js');
    }

    public function indexAction()
    {
    	
            
    	 $this->view->setVar("usuarios", Usuarios::find(array(
		    	"usu_est = '1'"
		 )));

		 
    }

    public function nuevoAction()
    {
        
         $this->view->setVar("tipoUsuarios", TipoUsuarios::find(array(
                "tus_est = '1'"
         )));
         $muni = new Muni();
        $this->view->setVar("municipios", $muni->listarMuniDpto());
    }

    public function editarAction($usu_cod)
    {
       

         $this->view->setVar("tipoUsuarios", TipoUsuarios::find(array(
                "tus_est = '1'"
         )));
         $muni = new Muni();
        $this->view->setVar("municipios", $muni->listarMuniDpto());

          $this->view->setVar("usuario", Usuarios::findFirst($usu_cod));
    }

     public function guardarAction()
    {
        

        $validation = new Phalcon\Validation();
       
        $validation->add('usu_id', new PresenceOf(array(
            'message' => 'El campo No. Identificación Es Requerido',

        )));

        $validation->add('usu_nom', new PresenceOf(array(
            'message' => 'El campo Nombres Es Requerido',

        )));   

        $validation->add('usu_ape', new PresenceOf(array(
            'message' => 'El campo Apellidos Es Requerido',

        )));   

        $validation->add('usu_ema', new PresenceOf(array(
            'message' => 'El campo Email Es Requerido',

        ))); 

        $validation->add('usu_ema', new Email(array(
            'message' => 'el formato de  Email no es valido'
        )));  

        $validation->add('usu_con', new PresenceOf(array(
            'message' => 'El campo Contraseña Es Requerido',

        )));   

        $validation->add('tus_cod', new PresenceOf(array(
            'message' => 'El campo Tipo Usuario Es Requerido',

        )));

        $validation->add('mun_cod', new PresenceOf(array(
            'message' => 'El campo Ciudad Es Requerido',

        )));   


        


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $usuario = new Usuarios();
        $usuario->usu_id  = $this->request->getPost("usu_id");
        $usuario->usu_nom  = $this->request->getPost("usu_nom");
        $usuario->usu_ape  = $this->request->getPost("usu_ape");
        $usuario->usu_ema  = $this->request->getPost("usu_ema");
        $usuario->usu_con  = $this->request->getPost("usu_con");
        $usuario->tus_cod  = $this->request->getPost("tus_cod");
        $usuario->mun_cod  = $this->request->getPost("mun_cod");
        $usuario->usuario  = $this->session->get("usu_cod");
        $usuario->usu_fec  = date("Y-m-d H:i:s");
        $usuario->usu_est  = new \Phalcon\Db\RawValue('default');

        
        if($usuario->save()){
            echo "1";
        }else{
             foreach ($usuario->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
        

    }

     public function actualizarAction()
    {
        date_default_timezone_set("America/Bogota");
        setlocale(LC_ALL,"es_ES@euro","es_ES","esp");

        $validation = new Phalcon\Validation();
       
        $validation->add('usu_id', new PresenceOf(array(
            'message' => 'El campo No. Identificación Es Requerido',

        )));

        $validation->add('usu_nom', new PresenceOf(array(
            'message' => 'El campo Nombres Es Requerido',

        )));   

        $validation->add('usu_ape', new PresenceOf(array(
            'message' => 'El campo Apellidos Es Requerido',

        )));   

        $validation->add('usu_ema', new PresenceOf(array(
            'message' => 'El campo Email Es Requerido',

        ))); 

        $validation->add('usu_ema', new Email(array(
            'message' => 'el formato de  Email no es valido'
        )));  

        $validation->add('usu_con', new PresenceOf(array(
            'message' => 'El campo Contraseña Es Requerido',

        )));   

        $validation->add('tus_cod', new PresenceOf(array(
            'message' => 'El campo Tipo Usuario Es Requerido',

        )));   

         $validation->add('mun_cod', new PresenceOf(array(
            'message' => 'El campo Ciudad Es Requerido',

        )));  


        


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $usuario = Usuarios::findFirst($this->request->getPost("usu_cod"));
        $usuario->usu_id   = $this->request->getPost("usu_id");
        $usuario->usu_nom  = $this->request->getPost("usu_nom");
        $usuario->usu_ape  = $this->request->getPost("usu_ape");
        $usuario->usu_ema  = $this->request->getPost("usu_ema");
        $usuario->usu_con  = $this->request->getPost("usu_con");
        $usuario->tus_cod  = $this->request->getPost("tus_cod");
        $usuario->mun_cod  = $this->request->getPost("mun_cod");
        $usuario->usuario  = $this->session->get("usu_cod");
        $usuario->usu_fec  = date("Y-m-d H:i:s");
        $usuario->usu_est  = new \Phalcon\Db\RawValue('default');

        
        if($usuario->save()){
            echo "1";
        }else{
             foreach ($usuario->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
        

    }

      public function eliminarAction()
    {
        
        $usu = Usuarios::findFirst($this->request->getPost("cod"));
        $usu->usu_est="0";
        $usu->usu_fec  = date("Y-m-d H:i:s");
        if($usu->save()){
            echo "1";
        }else{
             foreach ($usu->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

}

