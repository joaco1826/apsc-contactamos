<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class PnlPer extends \Phalcon\Mvc\Model
{

   public static function resultado($per_cod, $req_cod)
    {
        // A raw SQL statement
        $hoy = date("Y-m-d");
        $fecha = strtotime ( '-720 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
       $sql ="SELECT *
            FROM pnl_per 
            WHERE per_cod='$per_cod' AND DATE(pna_fch) BETWEEN '$fecha' AND '$hoy' ORDER BY pna_cod DESC LIMIT 1";

        // Base model
        $ipv = new PnlPer();

        // Execute the query
        return new Resultset(null, $ipv, $ipv->getReadConnection()->query($sql));
    }

    public static function npruebas($fecha)
    {
        // A raw SQL statement

       $sql ="SELECT COUNT(*) as total
            FROM pnl_per 
            WHERE DATE(pna_fch) = '$fecha'";

        // Base model
        $ipv = new PnlPer();

        // Execute the query
        return new Resultset(null, $ipv, $ipv->getReadConnection()->query($sql));
    }

}