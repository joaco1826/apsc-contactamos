<?php

class PruebaCmtController extends \Phalcon\Mvc\Controller
{

	  public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
              ->addCss('css/reloj.css')
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jpegcam/htdocs/webcam.js')
            ->addJs('js/countdown/jquery.countdown.min.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/pruebas.js');
    }

    public function indexAction()
    {
    	// $this->assets
     //         ->addCss('css/reloj.css');

    	// $this->assets
     //        ->addJs('js/jquery.js')
     //        ->addJs('js/jpegcam/htdocs/webcam.js')
     //        ->addJs('js/countdown/jquery.countdown.min.js')
     //        ->addJs('js/pruebas.js');
    }

     public function subirFotoAction()
	 {
	    	$jpeg_data = file_get_contents('php://input');
			$foto = md5(microtime()*rand(1.1,1.9))."_".$this->session->get("per_cod");
			$filename = "photos/".$foto.".jpg";

			$fot = new Fotos();
			$fot->per_cod = $this->session->get("per_cod");
			$fot->fot_rut = $filename;
			$fot->fot_pru = "CMT";
			$fot->fot_fec = new \Phalcon\Db\RawValue('default');

			if($fot->save()){
				$result = file_put_contents( $filename, $jpeg_data );
			}
	    	
	}

    public function guardarAction()
    {
    	if ($this->request->isPost() == true) {
	    	$cmt = new Cmt();

	    	


	    	$cmt->per_cod = $this->request->getPost("per_cod");
	    	$cmt->req_cod = $this->request->getPost("req_cod");
	    	$cmt->usu_cod = $this->request->getPost("per_cod");
	    	$cmt->cmt_fec = new \Phalcon\Db\RawValue('default');
	    	$cmt->cmt_est = new \Phalcon\Db\RawValue('default');

	    	$this->db->begin();

	    	if($cmt->save() == false){
				$this->db->rollback();
				return;
			}



			//PRIMERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 1;
			$cmt_det->cmd_res = $this->request->getPost("primero_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//PRIMERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 1;
			$cmt_det->cmd_res = $this->request->getPost("primero_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//PRIMERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 1;
			$cmt_det->cmd_res = $this->request->getPost("primero_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//PRIMERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 1;
			$cmt_det->cmd_res = $this->request->getPost("primero_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//PRIMERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 1;
			$cmt_det->cmd_res = $this->request->getPost("primero_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEGUNDA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 2;
			$cmt_det->cmd_res = $this->request->getPost("segundo_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEGUNDA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 2;
			$cmt_det->cmd_res = $this->request->getPost("segundo_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEGUNDA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 2;
			$cmt_det->cmd_res = $this->request->getPost("segundo_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEGUNDA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 2;
			$cmt_det->cmd_res = $this->request->getPost("segundo_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEGUNDA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 2;
			$cmt_det->cmd_res = $this->request->getPost("segundo_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//TERCERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 3;
			$cmt_det->cmd_res = $this->request->getPost("tercero_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//TERCERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 3;
			$cmt_det->cmd_res = $this->request->getPost("tercero_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//TERCERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 3;
			$cmt_det->cmd_res = $this->request->getPost("tercero_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//TERCERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 3;
			$cmt_det->cmd_res = $this->request->getPost("tercero_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//TERCERA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 3;
			$cmt_det->cmd_res = $this->request->getPost("tercero_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//CUARTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 4;
			$cmt_det->cmd_res = $this->request->getPost("cuarto_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//CUARTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 4;
			$cmt_det->cmd_res = $this->request->getPost("cuarto_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//CUARTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 4;
			$cmt_det->cmd_res = $this->request->getPost("cuarto_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//CUARTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 4;
			$cmt_det->cmd_res = $this->request->getPost("cuarto_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//CUARTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 4;
			$cmt_det->cmd_res = $this->request->getPost("cuarto_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//QUINTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 5;
			$cmt_det->cmd_res = $this->request->getPost("quinto_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//QUINTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 5;
			$cmt_det->cmd_res = $this->request->getPost("quinto_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//QUINTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 5;
			$cmt_det->cmd_res = $this->request->getPost("quinto_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//QUINTA PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 5;
			$cmt_det->cmd_res = $this->request->getPost("quinto_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEXTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 6;
			$cmt_det->cmd_res = $this->request->getPost("sexto_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEXTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 6;
			$cmt_det->cmd_res = $this->request->getPost("sexto_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEXTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 6;
			$cmt_det->cmd_res = $this->request->getPost("sexto_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEXTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 6;
			$cmt_det->cmd_res = $this->request->getPost("sexto_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEXTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 6;
			$cmt_det->cmd_res = $this->request->getPost("sexto_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEPTIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 7;
			$cmt_det->cmd_res = $this->request->getPost("septimo_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEPTIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 7;
			$cmt_det->cmd_res = $this->request->getPost("septimo_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEPTIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 7;
			$cmt_det->cmd_res = $this->request->getPost("septimo_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEPTIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 7;
			$cmt_det->cmd_res = $this->request->getPost("septimo_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//SEPTIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 7;
			$cmt_det->cmd_res = $this->request->getPost("septimo_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//OCTAVO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 8;
			$cmt_det->cmd_res = $this->request->getPost("octavo_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//OCTAVO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 8;
			$cmt_det->cmd_res = $this->request->getPost("octavo_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//OCTAVO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 8;
			$cmt_det->cmd_res = $this->request->getPost("octavo_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//OCTAVO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 8;
			$cmt_det->cmd_res = $this->request->getPost("octavo_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//OCTAVO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 8;
			$cmt_det->cmd_res = $this->request->getPost("octavo_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//NOVENO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 9;
			$cmt_det->cmd_res = $this->request->getPost("noveno_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//NOVENO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 9;
			$cmt_det->cmd_res = $this->request->getPost("noveno_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//NOVENO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 9;
			$cmt_det->cmd_res = $this->request->getPost("noveno_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//NOVENO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 9;
			$cmt_det->cmd_res = $this->request->getPost("noveno_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//NOVENO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 9;
			$cmt_det->cmd_res = $this->request->getPost("noveno_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}


			//DECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 10;
			$cmt_det->cmd_res = $this->request->getPost("decimo_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 10;
			$cmt_det->cmd_res = $this->request->getPost("decimo_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 10;
			$cmt_det->cmd_res = $this->request->getPost("decimo_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 10;
			$cmt_det->cmd_res = $this->request->getPost("decimo_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 10;
			$cmt_det->cmd_res = $this->request->getPost("decimo_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//UNDECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 11;
			$cmt_det->cmd_res = $this->request->getPost("undecimo_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//UNDECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 11;
			$cmt_det->cmd_res = $this->request->getPost("undecimo_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//UNDECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 11;
			$cmt_det->cmd_res = $this->request->getPost("undecimo_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//UNDECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 11;
			$cmt_det->cmd_res = $this->request->getPost("undecimo_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//UNDECIMO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 11;
			$cmt_det->cmd_res = $this->request->getPost("undecimo_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOSEGUNDO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 12;
			$cmt_det->cmd_res = $this->request->getPost("decimosegundo_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOSEGUNDO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 12;
			$cmt_det->cmd_res = $this->request->getPost("decimosegundo_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOSEGUNDO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 12;
			$cmt_det->cmd_res = $this->request->getPost("decimosegundo_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOSEGUNDO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 12;
			$cmt_det->cmd_res = $this->request->getPost("decimosegundo_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOSEGUNDO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 12;
			$cmt_det->cmd_res = $this->request->getPost("decimosegundo_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOSEGUNDO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 12;
			$cmt_det->cmd_res = $this->request->getPost("decimosegundo_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOTERCERO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 13;
			$cmt_det->cmd_res = $this->request->getPost("decimotercero_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOTERCERO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 13;
			$cmt_det->cmd_res = $this->request->getPost("decimotercero_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOTERCERO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 13;
			$cmt_det->cmd_res = $this->request->getPost("decimotercero_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOTERCERO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 13;
			$cmt_det->cmd_res = $this->request->getPost("decimotercero_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOTERCERO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 13;
			$cmt_det->cmd_res = $this->request->getPost("decimotercero_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOCUARTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 14;
			$cmt_det->cmd_res = $this->request->getPost("decimocuarto_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOQUINTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 15;
			$cmt_det->cmd_res = $this->request->getPost("decimoquinto_uno");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOQUINTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 15;
			$cmt_det->cmd_res = $this->request->getPost("decimoquinto_dos");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOQUINTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 15;
			$cmt_det->cmd_res = $this->request->getPost("decimoquinto_tres");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOQUINTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 15;
			$cmt_det->cmd_res = $this->request->getPost("decimoquinto_cuatro");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			//DECIMOQUINTO PREGUNTA
			$cmt_det = new CmtDet();
			$cmt_det->cmt_cod = $cmt->cmt_cod;
			$cmt_det->cmd_pre = 15;
			$cmt_det->cmd_res = $this->request->getPost("decimoquinto_cinco");
			if($cmt_det->save() == false){
				$this->db->rollback();
				return;
			}

			$this->db->commit();

			  $this->dispatcher->forward(array(
		            "controller" => "verPruebas",
		            "action" => "index",
		            "params" => array("resultado" => 1)
        	  ));


	    }else{
	    	 $this->response->redirect("verPruebas/");
	    }
    }

}

