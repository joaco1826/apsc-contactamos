<?php

class VerDocumentosController extends \Phalcon\Mvc\Controller
{

	 public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
            ->addJs('js/VerDocumentos.js');


    }

    public function indexAction()
    {
    	  $tipoDocumentos =  TipoDocumento::find();
    	  $this->view->setVar("tipoDocumentos", $tipoDocumentos);
    }

     public function consultarAction()
    {
        $txt_bus = $this->request->getPost("txt_bus");
        $personas =  Personas::find(array(
               "per_est = '1' AND per_pno LIKE '$txt_bus%' OR per_pap LIKE '$txt_bus%' OR per_ide LIKE '$txt_bus%'"
        ));
        ?>
        <table class="lista">
             <tr>
                <th>No. Documento</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Profesión</th>
                <th>Accion</th>
             </tr>
        <?
        foreach ($personas as  $per) {

            ?>
                <tr>
                    <td><?=$per->per_ide?></td>
                    <td><?=$per->per_pno?></td>
                    <td><?=$per->per_pap?></td>
                    <td><?=$per->per_pro?></td>
                    <td><a href="#" onclick="abrirModal('<?=$per->per_cod?>');">Subir Doc.</a></td>
                </tr>
            <?
        }
        ?>
        </table>
        <?
    }

     public function obtenerAction()
    {
    	  $per_cod = $this->request->getPost("per_cod");
    	  $tipoDocumentos =  TipoDocumento::find();
    	  ?>
    	  <table class="form" width="100%">
				<tr>
					<th colspan="3">DOCUMENTOS</th>

				</tr>

				<tr>
					<td width="50%"><strong>Tipo de Documento</strong></td>
					<td><strong>Nombre</strong></td>
					<td><strong>Accion</strong></td>
				</tr>

				<?php foreach ($tipoDocumentos as  $tip): ?>
						<?php
							  $tdo_cod = $tip->tdo_cod;
							  $personasDocumentos =  PersonasDocumentos::findFirst(array("tdo_cod = '$tdo_cod' AND per_cod = '$per_cod'"));
							  if(!$personasDocumentos){
							  	$doc_rut = "Sin adjuntar";
							  }else{
							  	$doc_rut = '<a href="'.$this->url->get().'files/'.$personasDocumentos->doc_rut.'" target="_blank">'.$personasDocumentos->doc_rut.'</a>';
							  }
						 ?>
					<tr>
						<td style="border-bottom: 1px solid #CCC;"><?php echo $tip->tdo_des ?></td>
						<td style="border-bottom: 1px solid #CCC;"><?=$doc_rut;?></td>
						<td>
							<input type="file" name="archivo_<?=$tdo_cod?>">
						</td>
					</tr>
				<?php endforeach ?>





				<tr>
					<td colspan="3"><input type="submit" value="Guardar y Continuar" class="boton w100" id="btn_guardar"></td>
				</tr>
		  </table>
		  <?
    }

    public function obtenerdocAction()
    {
        $ord_cod = $this->request->getPost("ord_cod");
        $per_cod = Ordenes::findFirst($ord_cod)->pos_cod;
        $per = Personas::findFirst($per_cod);
        $tipoDocumentos =  TipoDocumento::find("tdo_est='1'");
        ?>

        <table class="form" width="100%">
            <tr>
                <th colspan="3">
                    <input type="hidden" name="cedula" value="<?=$per->per_ide?>">
                    C.C. <?=$per->per_ide . " " . $per->per_pno . " " . $per->per_pap?> - DOCUMENTOS</th>

            </tr>

            <tr>
                <td width="50%"><strong>Tipo de Documento</strong></td>
                <td><strong>Nombre</strong></td>
                <td><strong>Accion</strong></td>
            </tr>

            <?php foreach ($tipoDocumentos as  $tip): ?>
                <?php
                $tdo_cod = $tip->tdo_cod;
                $personasDocumentos =  DocumentosOrdenes::findFirst(array("document_id = '$tdo_cod' AND ord_cod = '$ord_cod'"));
                if(!$personasDocumentos){
                    $doc_rut = "Sin adjuntar";
                    $inp = ' <input type="text" name="otro'.$tdo_cod.'" style="width: 300px">';
                }else{
                    if ($personasDocumentos->drive == "SI") {
                        $doc_rut = '<a href="'.$personasDocumentos->ruta.'" target="_blank">'.$personasDocumentos->ruta.'</a> <span onclick="eliminarDocDrive('.$tdo_cod.', '.$ord_cod.', this)" class="citar">Eliminar</span>';
                    } else {
                        $doc_rut = '<a href="'.$this->url->get().'files/'.$personasDocumentos->ruta.'" target="_blank">'.$personasDocumentos->ruta.'</a> <span onclick="eliminarDoc('.$tdo_cod.', '.$ord_cod.', this)" class="citar">Eliminar</span>';
                    }

                    $inp = " - " . $personasDocumentos->otro;
                }
                ?>
                <tr>
                    <td style="border-bottom: 1px solid #CCC;">
                        <?php
                            echo $tip->tdo_des;
                            if ($tip->tdo_des == "Otro") {
                                echo $inp;
                            }
                        ?>
                    </td>
                    <td style="border-bottom: 1px solid #CCC;"><?=$doc_rut;?></td>
                    <td>
                        <input type="file" name="archivo_<?=$tdo_cod?>">
                    </td>
                </tr>
            <?php endforeach ?>





            <tr>
                <td><input type="button" onclick="SubirDoc('')" value="Guardar y Continuar" class="boton w100" id="btn_guardar"></td>
                <td colspan="2"><input type="button" onclick="SubirDoc('completado')" value="Completado" class="boton w100"></td>
            </tr>
        </table>
        <?
    }


     public function uploadAction()
    {
        $per_cod = $this->request->getPost("per_cod");

        if ($this->request->hasFiles() == true) {
        	$this->db->begin();
        	$sw = true;
            foreach ($this->request->getUploadedFiles() as  $file) {
                // echo $file->getName(), " ", $file->getSize(), " ", $file->getKey(), "<br>";
                $file->moveTo('files/'.$file->getName());
                $vector = explode("_", $file->getKey());
                $tdo_cod = $vector[1];//$id del documento

 				 $personaDocumento =  PersonasDocumentos::findFirst(array("tdo_cod = '$tdo_cod' AND per_cod = '$per_cod'"));
				 if(!$personaDocumento){
				    $personaDocumento = new PersonasDocumentos();
				 }
                // $personaDocumento = new PersonasDocumentos();
                $personaDocumento->tdo_cod = $tdo_cod;
                $personaDocumento->per_cod = $per_cod;
                $personaDocumento->doc_rut = $file->getName();
                if(!$personaDocumento->save())
                {
                	$sw = false;
                }

            }

            if($sw)
            {
            	 $this->db->commit();
            	 $this->dispatcher->forward(array(
            	 	"controller" => "VerDocumentos",
				    "action" => "index",
				    "params" => array("resultado" => "1")
				 ));
            }else{
            	 $this->db->rollback();
            	 $this->dispatcher->forward(array(
            	 	"controller" => "VerDocumentos",
				    "action" => "index",
				    "params" => array("resultado" => "0")
				 ));
            }
        }
    }

    public function uploaddocAction()
    {
        $ord_cod = $this->request->getPost("ord_cod");
        $ord_doc = $this->request->getPost("ord_doc");
        $swich = false;
        if ($ord_doc == "completado") {
            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as  $file) {
                    if ($file->getName() != "") {
                        $swich = true;
                    }
                }
            }
        } else {
            $swich = true;
        }
        if (!$swich) {
            echo "No puede completar sin haber montado por lo menos un documento";
            return false;
        }
        $ordenes = Ordenes::findFirst($ord_cod);
        $ordenes->ord_doc = $ord_doc;
        $ordenes->ord_fdo = new \Phalcon\Db\RawValue('default');
        $ordenes->save();
        if ($this->request->hasFiles() == true) {
            $sw = true;
            foreach ($this->request->getUploadedFiles() as  $file) {
                if ($file->getName() != ""){
                    $nombre = "documento-".date("Y-m-d")."-".substr(md5(uniqid(rand())),0,5) . "." . $file->getExtension();
                    $file->moveTo('files/'.$nombre);
                    $vector = explode("_", $file->getKey());
                    $tdo_cod = $vector[1];//$id del documento

                    $docu =  DocumentosOrdenes::findFirst(array("document_id = $tdo_cod AND ord_cod = $ord_cod"));
                    if(!$docu){
                        $docu = new DocumentosOrdenes();
                        $docu->create_at = new \Phalcon\Db\RawValue('default');
                        $docu->updated_at = new \Phalcon\Db\RawValue('default');
                    } else {
                        $docu->updated_at = new \Phalcon\Db\RawValue('default');
                    }
                    $docu->document_id = $tdo_cod;
                    $docu->ord_cod = $ord_cod;
                    $docu->ruta = $nombre;
                    if ($tdo_cod == 28 || $tdo_cod == 32 || $tdo_cod == 33 || $tdo_cod == 34) {
                        $docu->otro = $this->request->getPost("otro" . $tdo_cod);
                    }

                    if(!$docu->save())
                    {
                        $sw = false;
                        echo "-".$tdo_cod ."-" . $ord_cod . "-". $nombre;
                    }
                }

            }

            if($sw)
            {
                echo "1";
            }else{
                echo "Hubo un error";
            }
        }
    }

    public function getOAuthCredentialsFile()
    {
        // oauth2 creds
        $oauth_creds = __DIR__ . '/../../public/client_secret.json';
        if (file_exists($oauth_creds)) {
            return $oauth_creds;
        }
        return false;
    }

    public function uploaddocdriveAction()
    {
        $ord_cod = $this->request->getPost("ord_cod");
        $ord_doc = $this->request->getPost("ord_doc");
        $ordenes = Ordenes::findFirst($ord_cod);
        $ordenes->ord_doc = $ord_doc;
        $ordenes->ord_fdo = new \Phalcon\Db\RawValue('default');
        $ordenes->save();
        if ($this->request->hasFiles() == true) {
            require_once (__DIR__ . '/../library/vendor/autoload.php');
            $client = new Google_Client();
            $client->setApplicationName('Google Drive API PHP Quickstart');
            $client->addScope("https://www.googleapis.com/auth/drive");
            $client->setAuthConfig(__DIR__ . '/../views/index/credentials.json');
            $client->setAccessType('offline');

            // Load previously authorized credentials from a file.
            $credentialsPath = __DIR__ . '/../views/index/token.json';
            if (file_exists($credentialsPath)) {
                $accessToken = json_decode(file_get_contents($credentialsPath), true);
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = "4/AAC2VzZCUaVpnfNrHNFFuRaCLeWhFwPCqPi67waPMm5B-mOBeGGg-Rw";

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

                // Store the credentials to disk.
                if (!file_exists(dirname($credentialsPath))) {
                    mkdir(dirname($credentialsPath), 0700, true);
                }
                file_put_contents($credentialsPath, json_encode($accessToken));
                printf("Credentials saved to %s\n", $credentialsPath);
            }
            $client->setAccessToken($accessToken);

            // Refresh the token if it's expired.
            if ($client->isAccessTokenExpired()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
            }
            $service = new Google_Service_Drive($client);



            $sw = true;
            foreach ($this->request->getUploadedFiles() as  $file) {
                if ($file->getName() != "" && $client->getAccessToken()){
                    $nombre = $this->request->getPost("cedula") . "-" . $ord_cod ."-" .date("Y-m-d")."-".substr(md5(uniqid(rand())),0,5) . "." . $file->getExtension();
                    $folderId = '1VoVCPWV-IDtCHLf6XNc1us719idUxfqp';
                    $fileMetadata = new Google_Service_Drive_DriveFile(array(
                        'name' => $nombre, 'parents' => array($folderId)));
                    $content = file_get_contents($file->getTempName());
                    $fil = $service->files->create($fileMetadata, array(
                        'data' => $content,
                        'uploadType' => 'multipart',
                        'fields' => 'id'));
                    $vector = explode("_", $file->getKey());
                    $tdo_cod = $vector[1];//$id del documento

                    $docu =  DocumentosOrdenes::findFirst(array("document_id = $tdo_cod AND ord_cod = $ord_cod"));
                    if(!$docu){
                        $docu = new DocumentosOrdenes();
                        $docu->create_at = new \Phalcon\Db\RawValue('default');
                        $docu->updated_at = new \Phalcon\Db\RawValue('default');
                    } else {
                        $docu->updated_at = new \Phalcon\Db\RawValue('default');
                    }
                    $docu->document_id = $tdo_cod;
                    $docu->ord_cod = $ord_cod;
                    $docu->drive = "SI";
                    $docu->ruta = "https://drive.google.com/open?id=" . $fil->id;
                    if ($tdo_cod == 28) {
                        $docu->otro = $this->request->getPost("otro");
                    }

                    if(!$docu->save())
                    {
                        $sw = false;
                        echo "-".$tdo_cod ."-" . $ord_cod . "-". $nombre;
                    }
                }

            }

            if($sw)
            {
                echo "1";
            }else{
                echo "Hubo un error";
            }
        }
    }

}

