<?php

class EvaluarAssController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/EvaluarPrueba.js');
    }

     public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $this->view->setVar("req_cod", $req_cod);
    }

     public function guardarAction()
    {
        if ($this->request->isPost() == true) {
            
            $ass = new Assessment();
            $ass->pos_cod = $this->request->getPost("per_cod");
            $ass->req_cod = $this->request->getPost("req_cod");
            $ass->usu_cod = $this->session->get("usu_cod");
            $ass->ass_fec = new \Phalcon\Db\RawValue('default');
            $ass->ass_rip = $this->request->getPost("pru_rip");
            $ass->ass_ahn = $this->request->getPost("pru_ahn");
            $ass->ass_ada = $this->request->getPost("pru_ada");
            $ass->ass_com = $this->request->getPost("pru_com");
            $ass->ass_lid = $this->request->getPost("pru_lid");
            $ass->ass_mot = $this->request->getPost("pru_mot");
            $ass->ass_cap = $this->request->getPost("pru_cap");
            $ass->ass_obs = $this->request->getPost("pru_obs");
            

            if($ass->save() == false){
                $this->db->rollback();
                return;
            }

            echo "1";
        }
    }
}