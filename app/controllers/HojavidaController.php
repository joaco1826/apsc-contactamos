<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class HojavidaController extends \Phalcon\Mvc\Controller
{
	  public function initialize()
	  {
		 $this->assets
	             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
	              ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false)
	             ->addCss('css/estilos.css');

	        $this->assets
	             ->addJs('js/jquery.js')
	             ->addJs('js/jpegcam/htdocs/webcam.js')
	             ->addJs('js/validate.js')
	             ->addJs('js/msj_validate.js')
	             ->addJs('js/menu.js')
	             ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false)
	             ->addJs('js/HojaVida.js');
      }


    public function datbasAction($personas){
	      $personas = explode("SS", $personas);
	      $fin = count($personas);
	      $cadena = "";
	      foreach ($personas as $i => $p) {
	          if ($i == ($fin - 1)) {
                  $cadena .= $p;
              } else {
	              if ($i > 0) {
                      $cadena .= $p . ",";
                  }

              }

          }
          $this->view->setVar("in", $cadena);
    }
    public function estudiosAction($personas){
        $personas = explode("SS", $personas);
        $fin = count($personas);
        $cadena = "";
        foreach ($personas as $i => $p) {
            if ($i == ($fin - 1)) {
                $cadena .= $p;
            } else {
                if ($i > 0) {
                    $cadena .= $p . ",";
                }

            }

        }
        $this->view->setVar("in", $cadena);
    }
    public function explabAction($personas){
        $personas = explode("SS", $personas);
        $fin = count($personas);
        $cadena = "";
        foreach ($personas as $i => $p) {
            if ($i == ($fin - 1)) {
                $cadena .= $p;
            } else {
                if ($i > 0) {
                    $cadena .= $p . ",";
                }

            }

        }
        $this->view->setVar("in", $cadena);
    }
    public function refperAction($personas){
        $personas = explode("SS", $personas);
        $fin = count($personas);
        $cadena = "";
        foreach ($personas as $i => $p) {
            if ($i == ($fin - 1)) {
                $cadena .= $p;
            } else {
                if ($i > 0) {
                    $cadena .= $p . ",";
                }

            }

        }
        $this->view->setVar("in", $cadena);
    }

    public function nucfamAction($personas){
        $personas = explode("SS", $personas);
        $fin = count($personas);
        $cadena = "";
        foreach ($personas as $i => $p) {
            if ($i == ($fin - 1)) {
                $cadena .= $p;
            } else {
                if ($i > 0) {
                    $cadena .= $p . ",";
                }

            }

        }
        $this->view->setVar("in", $cadena);
    }

    public function indexAction($camara)
    {

    	 $this->view->setVar("camara", $camara);
    	//  $this->assets
     //   		 ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false);


    	// $this->assets
     //        ->addJs('js/jquery.js')
     //        ->addJs('js/HojaVida.js')
     //        ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false);


    	$this->view->setVar("identificaciones", Tipos::find(array(
		    	"tip_tip = 'identificacion'"
		)));

		$this->view->setVar("grupos", Tipos::find(array(
		    	"tip_tip = 'sangre'"
		)));

		$this->view->setVar("cargos", Cargos::find(array(
		    	"car_est = '1' ORDER BY car_des"
		 )));

		$this->view->setVar("sexos", Tipos::find(array(
		    	"tip_tip = 'sexo'"
		)));

		$this->view->setVar("fuentes", Tipos::find(array(
		    	"tip_tip = 'rec'"
		)));

		$this->view->setVar("civiles", Tipos::find(array(
		    	"tip_tip = 'civil'"
		)));

		$this->view->setVar("estaturas", Tipos::find(array(
		    	"tip_tip = 'est'"
		)));

		$this->view->setVar("niveles", Tipos::find(array(
		    	"tip_tip = 'niv'"
		)));

		$this->view->setVar("estratos", Tipos::find(array(
		    	"tip_tip = 'eto'"
		)));

		$muni = new Muni();
        $this->view->setVar("municipios", $muni->listarMuniDpto());

		$this->view->setVar("per",Personas::findFirst($this->session->get("per_cod")));

		$this->view->setVar("hijos", PersonasHijos::find(array(
		    	"per_cod = '".$this->session->get("per_cod")."'"
		)));

		$this->view->setVar("estudios", PersonasEstudios::find(array(
		    	"per_cod = '".$this->session->get("per_cod")."'"
		)));

		$this->view->setVar("experiencias", PersonasExp::find(array(
		    	"per_cod = '".$this->session->get("per_cod")."'"
		)));

		$this->view->setVar("referencias", PersonasFam::find(array(
		    	"per_cod = '".$this->session->get("per_cod")."'"
		)));

		// echo "holaaa";

    }

    public function uploadFotoAction()
    {
    	if ($this->request->isPost() == true) {
		    	 if ($this->request->hasFiles() == true)
		        {

		            foreach ($this->request->getUploadedFiles() as $file) {


		                // echo $file->getName(), " ", $file->getSize(), "\n";

		                $nombre_foto = $file->getName() . "_" . substr(md5(uniqid(rand())),0,10);
		                $file->moveTo('files/' . $nombre_foto);
		            }
		            $per = Personas::findFirst($this->request->getPost("per_cod"));

		            unlink('files/'.$per->per_fot); //borrar archivo anterior

		            $per->per_fot = $nombre_foto;
		            if($per->save()){
		            	$msg =  "Su foto se ha guardado con exito";
		        	}else{
		        		$msg =  "Problemas al guardar su foto";
		        	}

		        }else{

		        	$msg =  "Debe Seleccionar una foto";
		        }

		     //    $ambito = $this->request->getPost("ambito");

		     //    if($ambito=="intranet")
		     //    {
		     //    	$controlador = "Editaraspirante";
		     //    	$action = "index";
		     //    }else{
		     //    	$controlador = "Hojavida";
		     //    	$action = "index";
		     //    }

		     //    $this->dispatcher->forward(array(
			    //         "controller" => $controlador,
			    //         "action" => $action,
			    //         "params" => array($msg)
			   	// ));

		         $ambito = $this->request->getPost("ambito");
		         if($ambito!="web"){
			   	 	$this->response->redirect("editarAspirante/index/".$this->request->getPost("per_cod"));
			   	 }else{
			   	 	$this->response->redirect("Hojavida/index/");
			   	 }

		}else{
			 $this->response->redirect("Hojavida/");
		}
    }

      public function guardarHijoAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('hij_nom', new PresenceOf(array(
		    'message' => 'El campo Nombre Es Requerido',

		)));

		$validation->add('hij_eda', new PresenceOf(array(
		    'message' => 'El campo Edad Es Requerido',

		)));

		$validation->add('hij_ocu', new PresenceOf(array(
		    'message' => 'El campo Ocupación Es Requerido',

		)));

		$validation->add('hij_par', new PresenceOf(array(
		    'message' => 'El campo Parentesco Es Requerido',

		)));




		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$hij = new PersonasHijos();
    	$hij->hij_nom  = $this->request->getPost("hij_nom");
    	$hij->hij_eda  = $this->request->getPost("hij_eda");
    	$hij->hij_ocu  = $this->request->getPost("hij_ocu");
    	$hij->hij_par  = $this->request->getPost("hij_par");
    	$hij->per_cod  = $this->request->getPost("per_cod");


    	if($hij->save()){
    		echo "1";

    	}else{
    		 foreach ($hij->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

     public function guardarEstudioAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('est_ent', new PresenceOf(array(
		    'message' => 'El campo Entidad Es Requerido',

		)));

		$validation->add('est_pro', new PresenceOf(array(
		    'message' => 'El campo Profesion Es Requerido',

		)));

		$validation->add('est_fec', new PresenceOf(array(
		    'message' => 'El campo Fecha Es Requerido',

		)));




		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$est = new PersonasEstudios();
    	$est->est_ent  = $this->request->getPost("est_ent");
    	$est->est_pro  = $this->request->getPost("est_pro");
    	$est->est_fec  = $this->request->getPost("est_fec");
    	$est->per_cod  = $this->request->getPost("per_cod");


    	if($est->save()){
    		echo "1";
    	}else{
    		 foreach ($est->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

     public function guardarExperienciaAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('exp_emp', new PresenceOf(array(
		    'message' => 'El campo Entidad Es Requerido',

		)));

		$validation->add('exp_car', new PresenceOf(array(
		    'message' => 'El campo Cargo Es Requerido',

		)));

		$validation->add('exp_ini', new PresenceOf(array(
		    'message' => 'El campo Fecha Desde Es Requerido',

		)));

		$validation->add('exp_ffi', new PresenceOf(array(
		    'message' => 'El campo Fecha Hasta Es Requerido',

		)));

		$validation->add('exp_mot', new PresenceOf(array(
		    'message' => 'El campo Motivo de Retiro Es Requerido',

		)));

		$validation->add('exp_des', new PresenceOf(array(
		    'message' => 'El campo Descripción de Funciones Es Requerido',

		)));




		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$exp = new PersonasExp();
    	$exp->exp_emp  = $this->request->getPost("exp_emp");
    	$exp->exp_car  = $this->request->getPost("exp_car");
    	$exp->exp_ini  = $this->request->getPost("exp_ini");
    	$exp->exp_ffi  = $this->request->getPost("exp_ffi");
    	$exp->exp_mot  = $this->request->getPost("exp_mot");
    	$exp->exp_des  = $this->request->getPost("exp_des");
    	$exp->per_cod  = $this->request->getPost("per_cod");


    	if($exp->save()){
    		echo "1";
    	}else{
    		 foreach ($exp->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

    public function guardarReferenciaAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('fam_nom', new PresenceOf(array(
		    'message' => 'El campo Nombre Es Requerido',

		)));

		$validation->add('fam_par', new PresenceOf(array(
		    'message' => 'El campo Parentesco Es Requerido',

		)));

		$validation->add('fam_tel', new PresenceOf(array(
		    'message' => 'El campo Telefono Es Requerido',

		)));

		$validation->add('fam_ocu', new PresenceOf(array(
		    'message' => 'El campo Ocupación Es Requerido',

		)));






		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$fam = new PersonasFam();
    	$fam->fam_nom  = $this->request->getPost("fam_nom");
    	$fam->fam_par  = $this->request->getPost("fam_par");
    	$fam->fam_tel  = $this->request->getPost("fam_tel");
    	$fam->fam_ocu  = $this->request->getPost("fam_ocu");
    	$fam->per_cod  = $this->request->getPost("per_cod");


    	if($fam->save()){
    		echo "1";
    	}else{
    		 foreach ($fam->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

    public function guardarAction()
    {


		$validation = new Phalcon\Validation();

		$validation->add('car_cod', new PresenceOf(array(
		    'message' => 'El campo cargo de interés Es Requerido',

		)));

		$validation->add('per_tid', new PresenceOf(array(
		    'message' => 'El campo Tipo de identificacion Es Requerido',

		)));

		$validation->add('per_ide', new PresenceOf(array(
		    'message' => 'El campo No. Identificacion Es Requerido',

		)));

		$validation->add('per_pno', new PresenceOf(array(
		    'message' => 'El campo  Nombre Es Requerido',

		)));



		$validation->add('per_pap', new PresenceOf(array(
		    'message' => 'El campo   Apellido Es Requerido',

		)));



		$validation->add('per_fex', new PresenceOf(array(
		    'message' => 'El campo  Fecha Expedicion Es Requerido',

		)));

		$validation->add('per_cex', new PresenceOf(array(
		    'message' => 'El campo  Lugar Expedicion Es Requerido',

		)));

		$validation->add('per_dir', new PresenceOf(array(
		    'message' => 'El campo  Direccion Es Requerido',

		)));

		$validation->add('per_bar', new PresenceOf(array(
		    'message' => 'El campo  Barrio Es Requerido',

		)));

		$validation->add('per_ciu', new PresenceOf(array(
		    'message' => 'El campo  Lugar de Residencia Es Requerido',

		)));

		if ($this->request->getPost("per_ciu") == 149) {
            $validation->add('per_loc', new PresenceOf(array(
                'message' => 'El campo  Localidad Es Requerido',

            )));
        }

		$validation->add('per_ema', new PresenceOf(array(
		    'message' => 'El campo  Email Es Requerido',

		)));

		// $validation->add('per_tel', new PresenceOf(array(
		//     'message' => 'El campo  Telefono Es Requerido',

		// )));

		$validation->add('per_ato', new PresenceOf(array(
		    'message' => 'El campo  Estrato Es Requerido',

		)));

		$validation->add('per_cel', new PresenceOf(array(
		    'message' => 'El campo  Celular Es Requerido',

		)));

		$validation->add('per_fna', new PresenceOf(array(
		    'message' => 'El campo  Fecha de Nacimiento Es Requerido',

		)));

		$validation->add('per_cna', new PresenceOf(array(
		    'message' => 'El campo  Lugar de Nacimiento Es Requerido',

		)));


		$validation->add('per_gsa', new PresenceOf(array(
		    'message' => 'El campo  Grupo Sanguineo Es Requerido',

		)));

		$validation->add('per_pro', new PresenceOf(array(
		    'message' => 'El campo  Profesion Es Requerido',

		)));

		// $validation->add('per_mcf', new PresenceOf(array(
		//     'message' => 'El campo  Madre Cabeza de familia Es Requerido',

		// )));

		$validation->add('per_sex', new PresenceOf(array(
		    'message' => 'El campo  Sexo Es Requerido',

		)));

		$validation->add('per_civ', new PresenceOf(array(
		    'message' => 'El campo  Estado Civil Es Requerido',

		)));

		$validation->add('per_ura', new PresenceOf(array(
		    'message' => 'El campo  Estatura Es Requerido',

		)));

		$validation->add('per_eda', new PresenceOf(array(
		    'message' => 'El campo  Edad Es Requerido',

		)));

		$validation->add('per_pes', new PresenceOf(array(
		    'message' => 'El campo  Peso Es Requerido',

		)));

		$validation->add('per_tca', new PresenceOf(array(
		    'message' => 'El campo  Talla Camisa Es Requerido',

		)));

		$validation->add('per_tpa', new PresenceOf(array(
		    'message' => 'El campo  Talla Pantalón Es Requerido',

		)));

		$validation->add('per_tza', new PresenceOf(array(
		    'message' => 'El campo  Talla Zapato Es Requerido',

		)));

		$validation->add('per_hij', new PresenceOf(array(
		    'message' => 'El campo  No. de Hijos Es Requerido',

		)));

		$validation->add('per_pac', new PresenceOf(array(
		    'message' => 'El campo  Personas a Cargo Es Requerido',

		)));

		$validation->add('per_asp', new PresenceOf(array(
		    'message' => 'El campo  Aspiración Salarial Es Requerido',

		)));

		$validation->add('per_tur', new PresenceOf(array(
		    'message' => 'El campo  Disponibilidad para turnos Es Requerido',

		)));
		$validation->add('per_din', new PresenceOf(array(
		    'message' => 'El campo  Disponibilidad inmediata Es Requerido',

		)));
		$validation->add('per_con', new PresenceOf(array(
		    'message' => 'El campo  Contraseña Es Requerido',

		)));
		// $validation->add('per_fre', new PresenceOf(array(
		//     'message' => 'El campo  Fuente de reclutamiento Es Requerido',

		// )));






		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}

		if ($this->request->getPost("opcion") == "2") {
			$ref = PersonasFam::find(array(
    			"per_cod='".$this->request->getPost("per_cod")."'"
	    	));

	    	if (count($ref) < 2) {
	    		echo "Debe ingresar por lo menos dos referencias";
	    		return false;
	    	}

	    	$hij = PersonasHijos::find(array(
    			"per_cod='".$this->request->getPost("per_cod")."'"
	    	));

	    	if (count($hij) < 1) {
	    		echo "Debe ingresar por lo menos un miembro de su familia";
	    		return false;
	    	}

	    	if ($this->request->getPost("per_niv") <> 130) {
	    		$estud = PersonasEstudios::find(array(
	    			"per_cod='".$this->request->getPost("per_cod")."'"
		    	));

		    	if (count($estud) < 1) {
		    		echo "Debe ingresar por lo menos un estudio";
		    		return false;
		    	}
	    	}

	    	$body = Textos::findFirst(array("tex_tip='Registro'"))->tex_tex;
            $cor = new General();
           $cor->enviarCorreo($body, $this->request->getPost("per_ema"), "Registro de hoja de vida");


		}

        $per           = Personas::findFirst($this->request->getPost("per_cod"));
    	$per->car_cod  = $this->request->getPost("car_cod");
    	$per->per_tid  = $this->request->getPost("per_tid");
    	$per->per_ide  = $this->request->getPost("per_ide");
    	$per->per_pno  = strtoupper($this->request->getPost("per_pno"));
    	$per->per_sno  = strtoupper($this->request->getPost("per_sno"));
    	$per->per_pap  = strtoupper($this->request->getPost("per_pap"));
    	$per->per_sap  = strtoupper($this->request->getPost("per_sap"));
    	$per->per_fex  = $this->request->getPost("per_fex");
    	$per->per_cex  = $this->request->getPost("per_cex");
    	$per->per_dir  = strtoupper($this->request->getPost("per_dir"));
    	$per->per_bar  = strtoupper($this->request->getPost("per_bar"));
    	$per->per_pai  = strtoupper($this->request->getPost("per_pai"));
    	$per->per_ciu  = $this->request->getPost("per_ciu");
    	$per->per_loc  = $this->request->getPost("per_loc");
    	$per->per_ema  = strtoupper($this->request->getPost("per_ema"));
    	$per->per_tel  = $this->request->getPost("per_tel");
    	$per->per_ato  = $this->request->getPost("per_ato");
    	$per->per_cel  = $this->request->getPost("per_cel");
    	$per->per_fna  = $this->request->getPost("per_fna");
    	$per->per_cna  = $this->request->getPost("per_cna");
    	$per->per_gsa  = $this->request->getPost("per_gsa");
    	$per->per_pro  = strtoupper($this->request->getPost("per_pro"));
    	$per->per_mcf  = strtoupper($this->request->getPost("per_mcf"));
    	$per->per_sex  = $this->request->getPost("per_sex");
    	$per->per_civ  = $this->request->getPost("per_civ");
    	$per->per_niv  = strtoupper($this->request->getPost("per_niv"));
    	$per->per_ura  = strtoupper($this->request->getPost("per_ura"));
    	$per->per_eda  = strtoupper($this->request->getPost("per_eda"));
    	$per->per_pes  = strtoupper($this->request->getPost("per_pes"));
    	$per->per_tca  = strtoupper($this->request->getPost("per_tca"));
    	$per->per_tpa  = strtoupper($this->request->getPost("per_tpa"));
    	$per->per_tza  = strtoupper($this->request->getPost("per_tza"));
    	$per->per_gso  = strtoupper($this->request->getPost("per_gso"));
    	$per->per_get  = strtoupper($this->request->getPost("per_get"));
    	$per->per_her  = strtoupper($this->request->getPost("per_her"));
    	$per->per_hij  = strtoupper($this->request->getPost("per_hij"));
    	$per->per_pac  = strtoupper($this->request->getPost("per_pac"));
    	$per->per_asp  = strtoupper($this->request->getPost("per_asp"));
    	$per->per_tur  = strtoupper($this->request->getPost("per_tur"));
    	$per->per_din  = strtoupper($this->request->getPost("per_din"));
    	// $per->per_fre  = $this->request->getPost("per_fre");
    	$per->per_nlm  = strtoupper($this->request->getPost("per_nlm"));
    	$per->per_dis  = strtoupper($this->request->getPost("per_dis"));
    	$per->per_lco  = strtoupper($this->request->getPost("per_lco"));
    	$per->per_cat  = strtoupper($this->request->getPost("per_cat"));
    	$per->per_cju  = strtoupper($this->request->getPost("per_cju"));
    	if ($this->request->getPost("opcion") == "2") {
	    	if ($this->request->getPost("exp") == 0) {
	    		$experiencias = PersonasExp::find(array(
	    			"per_cod='".$this->session->get("per_cod")."'"
		    	));
		    	if (count($experiencias) == 0) {
		    		echo "Debe por lo menos colocar una experiencia laboral, si no tiene experiencia por favor seleccione la opción sin experiencia";
		    		return;
		    	}
	    	} else {
	    		$per->per_exp  = $this->request->getPost("exp");
	    	}
	    }

    	if($per->save()){
    		if ($this->request->getPost("opcion") == "2") {
    			if ($this->session->get("req_cod")) {
    				$postulaciones = Postulaciones::find(array(
		    			"req_cod='".$this->session->get("req_cod")."' AND per_cod='".$this->session->get("per_cod")."'"
			    	));
			    	if(count($postulaciones)>=1)
			    	{
			    		echo "2";
			    		return;
			    	}
			    	$pos = new Postulaciones();
			    	$pos->per_cod = $this->session->get("per_cod");
			    	$pos->req_cod = $this->session->get("req_cod");
			    	$pos->pos_fec = new \Phalcon\Db\RawValue('default');
			    	$pos->pos_est = new \Phalcon\Db\RawValue('default');
			    	if($pos->save())
			    	{
			    		echo "2";
			    	}else{
			    		echo "Ocurrio un problemas al postularse";
			    	}
    			} else {
    				echo "1";
    			}
    		} else {
    			echo "1";
    		}

    	}else{
    		 foreach ($per->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}


    }

    public function consultarHijoAction() {
    	$this->tablaHijo($this->request->getPost("per_cod"));
    }
    public function consultarEstudioAction() {
    	$this->tablaEstudio($this->request->getPost("per_cod"));
    }
    public function consultarExperienciaAction() {
    	$this->tablaExperiencia($this->request->getPost("per_cod"));
    }
    public function consultarReferenciaAction() {
    	$this->tablaReferencia($this->request->getPost("per_cod"));
    }

    public function tablaHijo($per_cod){
    	$hijos = PersonasHijos::find(array(
    			"per_cod='".$per_cod."'"
    	));
    		?>
    		<table border="0" class="lista">
    			<th>Nombre</th>
    			<th>Ocupación</th>
    			<th>Parentesco</th>
    			<th>Edad</th>
    			<th>Acción</th>
	    		<?php
	    		foreach ($hijos as  $hij) {
	    		?>
					<tr>
						<td><?=$hij->hij_nom?></td>
						<td><?=$hij->hij_ocu?></td>
						<td><?=$hij->hij_par?></td>
						<td><?=$hij->hij_eda?></td>
						<td><a href="#" onclick="eliminar('<?=$hij->hij_cod?>', 'hijos')">Eliminar</a></td>
					</tr>

	    		<?php } ?>

    		</table>
    		<?php

    }

     public function tablaEstudio($per_cod)
    {
    	$registros = PersonasEstudios::find(array(
    			"per_cod='".$per_cod."'"
    	));
    		?>
    		<table border="0" class="lista">
    			<th>Entidad</th>
    			<th>Profesión</th>
    			<th>Fecha Finalización</th>
    			<th>Acción</th>
	    		<?php
	    		foreach ($registros as  $reg) {
	    		?>
					<tr>
						<td><?=$reg->est_ent?></td>
						<td><?=$reg->est_pro?></td>
						<td><?=$reg->est_fec?></td>
						<td><a href="#" onclick="eliminar('<?=$reg->est_cod?>', 'estudios')">Eliminar</a></td>
					</tr>

	    		<?php } ?>

    		</table>
    		<?php

    }

     public function tablaExperiencia($per_cod)
    {
    	$registros = PersonasExp::find(array(
    			"per_cod='".$per_cod."'"
    	));
    		?>
    		<table border="0" class="lista">
    			<th>Entidad</th>
    			<th>Cargo</th>
    			<th>Fecha Desde</th>
    			<th>Fecha Hasta</th>
    			<th>Motivo de Retiro</th>
    			<th>Funciones</th>
    			<th>Acción</th>
	    		<?php
	    		foreach ($registros as  $reg) {
	    		?>
					<tr>
						<td><?=$reg->exp_emp?></td>
						<td><?=$reg->exp_car?></td>
						<td><?=$reg->exp_ini?></td>
						<td><?=$reg->exp_ffi?></td>
						<td><?=$reg->exp_mot?></td>
						<td><?=$reg->exp_des?></td>
						<td><a href="#" onclick="eliminar('<?=$reg->exp_cod?>', 'experiencias')">Eliminar</a></td>
					</tr>

	    		<?php } ?>

    		</table>
    		<?php

    }

     public function tablaReferencia($per_cod)
    {
    	$registros = PersonasFam::find(array(
    			"per_cod='".$per_cod."'"
    	));
    		?>
    		<table border="0" class="lista">
    			<th>Nombre</th>
    			<th>Relación</th>
    			<th>Teléfono</th>
    			<th>Ocupación</th>
    			<th>Acción</th>
	    		<?php
	    		foreach ($registros as  $reg) {
	    		?>
					<tr>
						<td><?=$reg->fam_nom?></td>
						<td><?=$reg->fam_par?></td>
						<td><?=$reg->fam_tel?></td>
						<td><?=$reg->fam_ocu?></td>
						<td><a href="#" onclick="eliminar('<?=$reg->fam_cod?>', 'referencias')">Eliminar</a></td>
					</tr>

	    		<?php } ?>

    		</table>
    		<?php

    }

    public function eliminarAction()
    {
    	$cod = $this->request->getPost("cod");
    	$tipo = $this->request->getPost("tipo");
    	if($tipo =="hijos")
    	{
    		$obj = PersonasHijos::findFirst($cod);

		    if ($obj->delete() == false) {
		        foreach ($obj->getMessages() as $message) {
		            echo $message, "\n";
		        }
		    } else {
		        echo $this->tablaHijo($this->request->getPost("per_cod"));
		        return;
		    }
		}

		if($tipo =="estudios")
    	{
    		$obj = PersonasEstudios::findFirst($cod);

		    if ($obj->delete() == false) {
		        foreach ($obj->getMessages() as $message) {
		            echo $message, "\n";
		        }
		    } else {
		        echo $this->tablaEstudio($this->request->getPost("per_cod"));
		        return;
		    }
		}

		if($tipo =="experiencias")
    	{
    		$obj = PersonasExp::findFirst($cod);

		    if ($obj->delete() == false) {
		        foreach ($obj->getMessages() as $message) {
		            echo $message, "\n";
		        }
		    } else {
		        echo $this->tablaExperiencia($this->request->getPost("per_cod"));
		        return;
		    }
		}

		if($tipo =="referencias")
    	{
    		$obj = PersonasFam::findFirst($cod);

		    if ($obj->delete() == false) {
		        foreach ($obj->getMessages() as $message) {
		            echo $message, "\n";
		        }
		    } else {
		        echo $this->tablaReferencia($this->request->getPost("per_cod"));
		        return;
		    }
		}

		echo "hola";

    }

    public function subirFotoAction()
	 {
	    	$jpeg_data = file_get_contents('php://input');
			$foto = md5(microtime()*rand(1.1,1.9))."_".$this->session->get("per_cod");
			$filename = "files/".$foto.".jpg";

			$personas = Personas::findFirst($this->session->get("per_cod"));
			$personas->per_fot = $foto.".jpg";

			if($personas->save()){
				$result = file_put_contents( $filename, $jpeg_data );
			}


	}

}

