<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;

class OrdenesController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        $this->assets
            ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
            ->addCss('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css', false)
            ->addCss('css/estilos.css');
        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', false)
            ->addJs('js/menu.js')
            ->addJs('js/obs.js')
            ->addJs('js/ordenes.js');
    }

    public function indexAction()
    {
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_tip = 'C' AND emp_est='1' ORDER BY emp_raz"
        )));
        $this->view->setVar("cargos", Cargos::find(array(
            "car_est = '1' ORDER BY car_des"
        )));
        $est = new Estados();
        $this->view->setVar("estados", $est->listarCont());
    }

    public function copiaAction()
    {
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_tip = 'C' AND emp_est='1' ORDER BY emp_raz"
        )));
        $this->view->setVar("cargos", Cargos::find(array(
            "car_est = '1' ORDER BY car_des"
        )));
        $est = new Estados();
        $this->view->setVar("estados", $est->listarCont());
    }

    public function editarAction($ord_cod)
    {
        $this->view->setVar("orden", Ordenes::findFirst($ord_cod));
        $this->view->setVar("examenes", ExamenesMedicos::find(array(
            "exa_tip = 'examenes'"
        )));
        $this->view->setVar("personas", Personas::find(array(
            "per_est = '1'"
        )));
        $this->view->setVar("bolsas", Tipos::find(array(
            "tip_tip = 'bolsa'"
        )));
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_tip = 'C' AND emp_est='1'"
        )));
        $per_cod = Ordenes::findFirst($ord_cod)->pos_cod;
        $this->view->setVar("persona", Personas::findFirst($per_cod));
    }

    public function viewAction($ord_cod)
    {
        $this->view->setVar("orden", Ordenes::findFirst($ord_cod));
        $this->view->setVar("examenes", ExamenesMedicos::find(array(
            "exa_tip = 'examenes'"
        )));
        $this->view->setVar("personas", Personas::find(array(
            "per_est = '1'"
        )));
        $this->view->setVar("bolsas", Tipos::find(array(
            "tip_tip = 'bolsa'"
        )));
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_tip = 'C' AND emp_est='1'"
        )));
        $per_cod = Ordenes::findFirst($ord_cod)->pos_cod;
        $this->view->setVar("persona", Personas::findFirst($per_cod));
    }

    public function asignar_examenesAction()
    {
        $validation = new Phalcon\Validation();
        $validation->add('pro_cod', new PresenceOf(array(
            'message' => 'El campo Proveedor  Es Requerido',
        )));
        $validation->add('asi_fec', new PresenceOf(array(
            'message' => 'El campo Fecha  Es Requerido',
        )));
        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }
        if ($_POST["tipo"] == "examenes") {
            foreach ($_POST['examenes'] as $examenes) {
                $orden_examen = new OrdenesExamenes();
                $orden_examen->ord_cod = $_POST['ord_cod'];
                $orden_examen->exa_cod = $examenes;
                $orden_examen->save();
            }
        }
        $exa = new ExamenesAsignados();
        $exa->ord_cod = $_POST['ord_cod'];
        $exa->pos_cod = $_POST['per_cod'];
        $exa->pro_cod = $_POST['pro_cod'];
        $exa->asi_fec = $_POST['asi_fec'];
        $exa->asi_fre = new \Phalcon\Db\RawValue('default');
        $exa->asi_obs = $_POST['asi_obs'];
        $exa->asi_tip = $_POST['tipo'];
        if ($exa->save()) {
            echo "1";
            $per = Personas::findFirst($_POST['per_cod']);
            $nombre_aspirante = $per->per_pno . " " . $per->per_sno . " " . $per->per_pap . " " . $per->per_sap;
            $cedula_asp = $per->per_ide;
            $emp = Empresas::findFirst($_POST['pro_cod']);
            $direccion = $emp->getEmpDir();
            $nombre_empresa = $emp->getEmpRaz();
            $cor = new General();
            $fecha = $_POST['asi_fec'];
            $texto_asp = Textos::findFirst(5)->tex_tex;
            $texto_pro = Textos::findFirst(2)->tex_tex;
            $observacion = $_POST['asi_obs'];
            //correo aspirante
            $body = "<p>Hola $nombre_aspirante</p>";
            $body .= "<p>$texto_asp - direccion:$direccion - $nombre_empresa,  antes de la fecha $fecha</p>";
            $body .= "<p>Observaciones: $observacion<p>";
            $destinatario = $per->per_ema;
            $asunto = "Orden de Examenes Medicos " . $_POST["ord_cod"];
            $cor->enviarCorreo($body, $destinatario, $asunto);
            //correo proveedor
            $body = "<p>Hola $nombre_empresa</p><br>";
            $body .= "<p>$texto_pro: $nombre_aspirante identificado con numero de documento: $cedula_asp</p><br>";
            $ord = new OrdenesExamenes();
            $examenesA = $ord->examenesArealizar($_POST["ord_cod"]);
            foreach ($examenesA as $key => $exa) {
                $body .= $exa->exa_nom . "<br>";
            }
            $destinatario = $emp->getEmpEma();
            $cor->enviarCorreo($body, $destinatario, $asunto);
        } else {
            echo "0";
        }
    }

    public function cargar_examenesAction()
    {
        ?>
        <script>
            jQuery(document).ready(function ($) {
                $(".fecha").datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });
        </script>
        <?
        $ord = Ordenes::findFirst($_POST['ord_cod']);
        $ord_cod = $ord->ord_cod;
        $exam = new OrdenesExamenes();
        if ($_POST["exa_tip"] == "estudios") {
            $examenes = $exam->ordenes($_POST['exa_tip'], $ord_cod);
        } else {
            $examenes = $exam->ordenesC($_POST['exa_tip']);
        }
        ?>
        <table border="1" style="border-collapse:collapse;" width="100%;" class="form">
            <tr>
                <th>ASIGNAR EXAMENES MEDICOS</th>
            </tr>
            <tr>
                <td>
                    <?php $i = 0;
                    foreach ($examenes as $key => $exa): $i++; ?>
                        <?
                        echo $i . " <input type='checkbox' name='examenes[]' value='" . $exa->exa_cod . "'> " . ExamenesMedicos::findFirst($exa->exa_cod)->exa_nom . "<br>";
                        ?>
                    <?php endforeach ?>
                </td>
            </tr>
            <tr>
                <td>
                    <strong><?= $_POST['nombre_aspirante'] ?></strong>
                </td>
            </tr>
            <tr>
                <td>Proveedor:
                    <select name="pro_cod" id="pro_cod">
                        <option value="">Seleccione...</option>
                        <? $proveedores = Empresas::find("emp_tip='P' AND emp_est=1"); ?>
                        <?php foreach ($proveedores as $key => $pro): ?>
                            <option value="<?= $pro->getEmpCod() ?>"><?= $pro->getEmpRaz() ?></option>
                        <?php endforeach ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Fecha Limite :
                    <input type="text" class="fecha" name="asi_fec" id="asi_fec">
                </td>
            </tr>
            <tr>
                <td>
                    Observaciones Aspirante :
                    <textarea name="" id="" cols="30" rows="10"><?= Textos::findFirst(1)->tex_tex; ?></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    Observaciones Adicionales Aspirante :
                    <textarea name="asi_obs" id="asi_obs" cols="30" rows="10"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <?
                    if ($_POST["exa_tip"] == "estudios") {
                        ?>
                        Observaciones Proveedor Estudios:
                        <textarea name="" id="" cols="30" rows="10"><?= Textos::findFirst(3)->tex_tex; ?></textarea>
                    <? } else { ?>
                        Observaciones Proveedor Exámenes:
                        <textarea name="" id="" cols="30" rows="10"><?= Textos::findFirst(2)->tex_tex; ?></textarea>
                    <? }
                    ?>
                </td>
            </tr>
        </table>
        <input type="button" value="Guardar y Finalizar" class="boton w100" id="btn_asignar_examenes"
               onclick="asignar_examenes('<?= $_POST["exa_tip"] ?>')">
        <?

    }

    public function nuevoAction()
    {
        $this->view->setVar("examenes", ExamenesMedicos::find(array(
            "exa_tip = 'estudios'"
        )));
        $this->view->setVar("personas", Personas::find(array(
            "per_est = '1' AND est_cod='22' ORDER BY per_pno, per_sno, per_pap, per_sap"
        )));
        $this->view->setVar("bolsas", Tipos::find(array(
            "tip_tip = 'bolsa'"
        )));
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_tip = 'C' AND emp_est='1' ORDER BY emp_raz"
        )));

    }

    public function consultarAction()
    {
        $emp_cod = $this->request->getPost("emp_cod");
        $txt_bus = $this->request->getPost("txt_bus");
        $oficina = $this->request->getPost("oficina");
        $estad = $this->request->getPost("estad");
        $dotacion = $this->request->getPost("dotacion");
        $where = "WHERE p.per_est='1' AND (p.per_ide LIKE '%{$txt_bus}%' OR p.per_pno LIKE '%{$txt_bus}%' OR p.per_pap LIKE '%{$txt_bus}%' OR o.ord_car LIKE '%{$txt_bus}%') AND p.est_cod NOT IN(7,8,9,10,27,6,28,23,5,4,12,11,21,40,42,13,14,22,20,19,17,16,18,29,30,15,44,37,39,10,34,45) AND o.ord_est='2'";
        $currentPage = (int)$this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $filas = $this->request->getPost("filas");
        if (empty($filas)) {
            $filas = 15;
        }
        $orden = new Ordenes();
        if (!empty($emp_cod)) {
            $where .= " AND o.emp_cod=$emp_cod";
        }
        if (!empty($oficina)) {
            $where .= " AND u.mun_cod=$oficina";
        }
        if (!empty($estad)) {
            $where .= " AND p.est_cod=$estad";
        }
        if (!empty($dotacion)) {
            $where .= " AND o.ord_dot='$dotacion'";
        }
        // echo $where . "-" . $estad;
        $ordenes = $orden->listar($where);
        $paginator = new PaginatorModel(
            array(
                "data" => $ordenes,
                "limit" => $filas,
                "page" => $currentPage
            )
        );
        // Get the paginated results
        $page = $paginator->getPaginate();
        if ($currentPage > 1) {
            $i = $filas * $currentPage - $filas + 1;
        } else {
            $i = 1;
        }
        ?>
        <table class="lista" cellspacing="0">
            <tr>
                <th>No.</th>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>H.V</th>
                <th>Usuario / Proyecto</th>
                <th>F. Ingreso</th>
                <th>Cargo</th>
                <th>Empresa</th>
                <th>Oficina</th>
                <th>Exámenes</th>
                <th>Estudios</th>
                <th>Estado</th>
                <th>H.V</th>
                <th>Observaciones</th>
                <th>Acción</th>
            </tr>
            <?
            $i = 0;
            $estados = Estados::find(array(
                "est_pro='contratacion' AND est_est='1' ORDER BY est_nom"
            ));
            foreach ($page->items as $ord) {
                $nombre_apirante = $ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap;
                $i++;
                if ($ord->sorftime == "SI") {
                    $bg = "#7bcd7b";
                    $sw = true;
                } else {
                    $bg = "#ffffff";
                    $sw = false;
                }
                ?>
                <tr style="background: <?=$bg?>">
                    <td><?= $i ?></td>
                    <td><?= $ord->per_ide ?></td>
                    <td><?= ucwords(strtolower($ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap)) ?></td>
                    <td><? if($ord->per_ciu <> "") echo "SI"; else echo "NO"; ?></td>
                    <td><?= $ord->emp_raz ?></td>
                    <td><?= $ord->ord_fec ?></td>
                    <td><?= $ord->ord_car ?></td>
                    <td><?= $ord->tip_des ?></td>
                    <td><?= $ord->mun_nom ?></td>
                    <td>
                        <select style="width: 80px" name="exac" id="exac"
                                onchange="ediExa(this.value, <?= $ord->ord_cod ?>)">
                            <option value="">Seleccione</option>
                            <option <? if ($ord->examenes == "APTO") {
                                echo "selected";
                            } ?> value="APTO">APTO
                            </option>
                            <option <? if ($ord->examenes == "CLAUSULA") {
                                echo "selected";
                            } ?> value="CLAUSULA">CLAUSULA
                            </option>
                            <option <? if ($ord->examenes == "SIN TURNO") {
                                echo "selected";
                            } ?> value="SIN TURNO">SIN TURNO
                            </option>
                            <option <? if ($ord->examenes == "N/A") {
                                echo "selected";
                            } ?> value="N/A">N/A
                            </option>
                        </select>
                    </td>
                    <td>
                        <select style="width: 80px" name="estd" id="estd"
                                onchange="ediEst(this.value, <?= $ord->ord_cod ?>)">
                            <option value="">Seleccione</option>
                            <option <? if ($ord->estudios == "APTO") {
                                echo "selected";
                            } ?> value="APTO">APTO
                            </option>
                            <option <? if ($ord->estudios == "SIN TURNO") {
                                echo "selected";
                            } ?> value="SIN TURNO">SIN TURNO
                            </option>
                            <option <? if ($ord->estudios == "N/A") {
                                echo "selected";
                            } ?> value="N/A">N/A
                            </option>
                        </select>
                    </td>
                    <td>
                        <select name="" id="est<?= $i ?>" style="width: 150px"
                                onchange="cambiarestado('<?= $ord->per_cod ?>', this.value, '<?= $ord->ord_cod ?>')">
                            <option value="">SIN CONTRATAR</option>
                            <?
                            foreach ($estados as $est) {
                                if ($ord->est_cod == $est->est_cod) { ?>
                                    <option selected value="<?= $est->est_cod ?>"><?= $est->est_nom ?></option>
                                <? } else { ?>
                                    <option value="<?= $est->est_cod ?>"><?= $est->est_nom ?></option>
                                <? }
                                ?>
                            <? }
                            ?>
                        </select>
                    </td>
                    <td><a target="_blank"
                           href="<?php echo $this->url->get('Verhojadevida/index') ?>/<?= $ord->per_cod ?>"><span
                                    class="citar">Ver</span></a>
                        <a target="_blank"
                           href="<?php echo $this->url->get('Verhojadevida/imprimir') ?>/<?= $ord->per_cod ?>"><img
                                    src="<?= $this->url->get() ?>img/printer.png" alt=""></a></td>
                    <td><?
                        if ($ord->per_obs == "") {
                            if ($ord->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                            <span class="citar" onclick="abrirModalO('<?= $ord->per_cod ?>')">Agregar</span>
                        <? } else {
                            if ($ord->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                            <?= substr($ord->per_obs, 0, 100) ?>...<br>
                            <span class="citar" onclick="abrirModalO('<?= $ord->per_cod ?>')">Editar</span>
                        <? }
                        ?></td>
                    <td><a href="#" onclick="editarO(<?php echo $ord->ord_cod; ?>)">Editar</a> <a href="#"
                                                                                                  onclick="eliminar('<?= $ord->ord_cod ?>');"><img
                                    src="<?php echo $this->url->get() ?>img/cerrar.png" alt=""></a>

                        <br><input type="checkbox" <? if ($sw) echo "checked"; ?> data-id="<?=$ord->ord_cod?>" name="contr[]" class="postu" onclick="desPerso(this)" value="SS<?=$ord->per_cod?>">
                    </td>
                </tr>
                <?
            }
            ?>
            <tr>
                <td colspan="14" align="right">
                    <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->before . ")'>Anterior</span>"; ?>
                    <?php
                    $num = ($page->total_items + ($filas - 1)) / $filas;
                    $quinta = $num / 10 + $currentPage;
                    $segunda = $num / 2 + $currentPage;
                    for ($i = 1; $i <= $num; $i++) {
                        if ($i <= $quinta || $i >= $segunda) {
                            if ($currentPage == $i) {
                                echo "<span class='btn'>" . $i . "</span>";
                            } else {
                                echo "<span class='btn ml10' onclick='filtrar(" . $i . ")'>" . $i . "</span>";
                            }
                        } else {
                            if ($num < 30) {
                                echo "<span class='btn ml10'  onclick='filtrar(" . $i . ")'>" . $i . "</span>";
                            } else {
                                echo ".";
                            }

                        }

                    }
                    ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->next . ")'>Siguiente</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->last . ")'>Última</span>"; ?>
                    <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span style="margin-left: 5px">filas</span>
                    <select name="filas" id="filas" style="width: 40px" onchange="filtrar(1)">
                        <option <? if ($filas == 15) echo "selected"; ?> value="15">15</option>
                        <option <? if ($filas == 30) echo "selected"; ?> value="30">30</option>
                        <option <? if ($filas == 50) echo "selected"; ?> value="50">50</option>
                        <option <? if ($filas == 100) echo "selected"; ?> value="100">100</option>
                    </select>
                </td>
            </tr>
        </table>
    <? }

    public function consultarcAction()
    {
        $emp_cod = $this->request->getPost("emp_cod");
        $txt_bus = $this->request->getPost("txt_bus");
        $oficina = $this->request->getPost("oficina");
        $estad = $this->request->getPost("estad");
        $dotacion = $this->request->getPost("dotacion");
        $where = "WHERE p.per_est='1' AND (p.per_ide LIKE '%{$txt_bus}%' OR p.per_pno LIKE '%{$txt_bus}%' OR p.per_pap LIKE '%{$txt_bus}%' OR o.ord_car LIKE '%{$txt_bus}%') AND p.est_cod NOT IN(7,8,9,10) AND o.ord_est='2'";
        $currentPage = (int)$this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $filas = $this->request->getPost("filas");
        if (empty($filas)) {
            $filas = 15;
        }
        $orden = new Ordenes();
        if (!empty($emp_cod)) {
            $where .= " AND o.emp_cod=$emp_cod";
        }
        if (!empty($oficina)) {
            $where .= " AND u.mun_cod=$oficina";
        }
        if (!empty($estad)) {
            $where .= " AND p.est_cod=$estad";
        }
        if (!empty($dotacion)) {
            $where .= " AND o.ord_dot='$dotacion'";
        }
        // echo $where . "-" . $estad;
        $ordenes = $orden->listar($where);
        $paginator = new PaginatorModel(
            array(
                "data" => $ordenes,
                "limit" => $filas,
                "page" => $currentPage
            )
        );
        // Get the paginated results
        $page = $paginator->getPaginate();
        if ($currentPage > 1) {
            $i = $filas * $currentPage - $filas + 1;
        } else {
            $i = 1;
        }
        ?>
        <table class="lista">
            <tr>
                <th>No.</th>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Usuario / Proyecto</th>
                <th>F. Ingreso</th>
                <th>Cargo</th>
                <th>Empresa</th>
                <th>Oficina</th>
                <th>Exámenes</th>
                <th>Estudios</th>
                <th>Estado</th>
                <th>H.V</th>
                <th>Doc.</th>
                <th>Observaciones</th>
                <th>Acción</th>
            </tr>
            <?
            $i = 0;
            $estados = Estados::find(array(
                "est_pro='contratacion' AND est_est='1' ORDER BY est_nom"
            ));
            foreach ($page->items as $ord) {
                $nombre_apirante = $ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap;
                $i++;

                ?>
                <tr style="background: <?=$bg?>">
                    <td><?= $i ?></td>
                    <td><?= $ord->per_ide ?></td>
                    <td><?= ucwords(strtolower($ord->per_pno . " " . $ord->per_sno . " " . $ord->per_pap . " " . $ord->per_sap)) ?></td>
                    <td><?= $ord->emp_raz ?></td>
                    <td><?= $ord->ord_fec ?></td>
                    <td><?= $ord->ord_car ?></td>
                    <td><?= $ord->tip_des ?></td>
                    <td><?= $ord->mun_nom ?></td>
                    <td>
                        <select style="width: 80px" name="exac" id="exac"
                                onchange="ediExa(this.value, <?= $ord->ord_cod ?>)">
                            <option value="">Seleccione</option>
                            <option <? if ($ord->examenes == "APTO") {
                                echo "selected";
                            } ?> value="APTO">APTO
                            </option>
                            <option <? if ($ord->examenes == "CLAUSULA") {
                                echo "selected";
                            } ?> value="CLAUSULA">CLAUSULA
                            </option>
                            <option <? if ($ord->examenes == "SIN TURNO") {
                                echo "selected";
                            } ?> value="SIN TURNO">SIN TURNO
                            </option>
                            <option <? if ($ord->examenes == "N/A") {
                                echo "selected";
                            } ?> value="N/A">N/A
                            </option>
                        </select>
                    </td>
                    <td>
                        <select style="width: 80px" name="estd" id="estd"
                                onchange="ediEst(this.value, <?= $ord->ord_cod ?>)">
                            <option value="">Seleccione</option>
                            <option <? if ($ord->estudios == "APTO") {
                                echo "selected";
                            } ?> value="APTO">APTO
                            </option>
                            <option <? if ($ord->estudios == "SIN TURNO") {
                                echo "selected";
                            } ?> value="SIN TURNO">SIN TURNO
                            </option>
                            <option <? if ($ord->estudios == "N/A") {
                                echo "selected";
                            } ?> value="N/A">N/A
                            </option>
                        </select>
                    </td>
                    <td>
                        <select name="" id="est<?= $i ?>" style="width: 150px"
                                onchange="cambiarestado('<?= $ord->per_cod ?>', this.value, '<?= $ord->ord_cod ?>')">
                            <option value="">SIN CONTRATAR</option>
                            <?
                            foreach ($estados as $est) {
                                if ($ord->est_cod == $est->est_cod) { ?>
                                    <option selected value="<?= $est->est_cod ?>"><?= $est->est_nom ?></option>
                                <? } else { ?>
                                    <option value="<?= $est->est_cod ?>"><?= $est->est_nom ?></option>
                                <? }
                                ?>
                            <? }
                            ?>
                        </select>
                    </td>
                    <td><a target="_blank"
                           href="<?php echo $this->url->get('Verhojadevida/index') ?>/<?= $ord->per_cod ?>"><span
                                    class="citar">Ver</span></a>
                        <a target="_blank"
                           href="<?php echo $this->url->get('Verhojadevida/imprimir') ?>/<?= $ord->per_cod ?>"><img
                                    src="<?= $this->url->get() ?>img/printer.png" alt=""></a></td>
                    <td><a href="#" onclick="abrirModalDoc('<?=$ord->ord_cod?>');">Doc.</a></td>
                    <td><?
                        if ($ord->per_obs == "") {
                            if ($ord->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                            <span class="citar" onclick="abrirModalO('<?= $ord->per_cod ?>')">Agregar</span>
                        <? } else {
                            if ($ord->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                            <?= substr($ord->per_obs, 0, 100) ?>...<br>
                            <span class="citar" onclick="abrirModalO('<?= $ord->per_cod ?>')">Editar</span>
                        <? }
                        ?></td>
                    <td><a href="#" onclick="editarO(<?php echo $ord->ord_cod; ?>)">Editar</a> <a href="#"
                                                                                                  onclick="eliminar('<?= $ord->ord_cod ?>');"><img
                                    src="<?php echo $this->url->get() ?>img/cerrar.png" alt=""></a></td>
                </tr>
                <?
            }
            ?>
            <tr>
                <td colspan="14" align="right">
                    <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->before . ")'>Anterior</span>"; ?>
                    <?php
                    $num = ($page->total_items + ($filas - 1)) / $filas;
                    $quinta = $num / 10 + $currentPage;
                    $segunda = $num / 2 + $currentPage;
                    for ($i = 1; $i <= $num; $i++) {
                        if ($i <= $quinta || $i >= $segunda) {
                            if ($currentPage == $i) {
                                echo "<span class='btn'>" . $i . "</span>";
                            } else {
                                echo "<span class='btn ml10' onclick='filtrar(" . $i . ")'>" . $i . "</span>";
                            }
                        } else {
                            if ($num < 30) {
                                echo "<span class='btn ml10'  onclick='filtrar(" . $i . ")'>" . $i . "</span>";
                            } else {
                                echo ".";
                            }

                        }

                    }
                    ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->next . ")'>Siguiente</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(" . $page->last . ")'>Última</span>"; ?>
                    <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span style="margin-left: 5px">filas</span>
                    <select name="filas" id="filas" style="width: 40px" onchange="filtrar(1)">
                        <option <? if ($filas == 15) echo "selected"; ?> value="15">15</option>
                        <option <? if ($filas == 30) echo "selected"; ?> value="30">30</option>
                        <option <? if ($filas == 50) echo "selected"; ?> value="50">50</option>
                        <option <? if ($filas == 100) echo "selected"; ?> value="100">100</option>
                    </select>
                </td>
            </tr>
        </table>
    <? }

    public function guardarAction()
    {
        date_default_timezone_get("America/Bogota");
        $validation = new Phalcon\Validation();
        $validation->add('emp_cod', new PresenceOf(array(
            'message' => 'El campo Empresa Cliente Es Requerido',
        )));
        $validation->add('ord_fec', new PresenceOf(array(
            'message' => 'El campo Fecha de Ingreso Es Requerido',
        )));
        $validation->add('ord_car', new PresenceOf(array(
            'message' => 'El campo Cargo Es Requerido',
        )));
        $validation->add('ord_sal', new PresenceOf(array(
            'message' => 'El campo Salario Es Requerido',
        )));
        //  $validation->add('ord_cto', new PresenceOf(array(
        //     'message' => 'El campo Centro de Costo Es Requerido',
        // )));
        $validation->add('ord_arl', new PresenceOf(array(
            'message' => 'El campo % ARL Es Requerido',
        )));
        $validation->add('ord_sot', new PresenceOf(array(
            'message' => 'El campo consecutivo SOT/SIT Es Requerido',
        )));
        $validation->add('ord_fec_sot', new PresenceOf(array(
            'message' => 'El campo fecha de ingreso SOT/SIT Es Requerido',
        )));
        // $validation->add('ord_tna', new PresenceOf(array(
        //     'message' => 'El campo Tipo de Nomina Es Requerido',
        // )));
        // $validation->add('ord_tel', new PresenceOf(array(
        //     'message' => 'El campo Telefono Es Requerido',
        // )));
        $validation->add('tip_cod', new PresenceOf(array(
            'message' => 'El campo Empresa Es Requerido',
        )));
        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }
        $ord = new Ordenes();
        $cad = "";
        $ord_ftr = date("Y-m-d");
        $body = "<h3>Notificación de orden de contratación</h3><br>";
        $body .= "<table border='1' width='900px'>
        <tr>
          <td>Identificación</td>
          <td>Nombres</td>
          <td>Usuaria / Proyecto</td>
          <td>Cargo</td>
          <td>Salario</td>
          <td>C.Costo</td>
          <td>Empresa</td>
          <td>Fecha Contratación</td>
           <td>Psicóloga</td>";
        if ($this->request->getPost("ord_dot") == "SI") {
            $body .= "<td>Talla camisa</td><td>Talla pantalón</td><td>Talla zapatos</td>";
        }
        if (!empty($_POST['examenes'])) {
            $body .= "<td>Estudios</td>";
        }
        $body .= "</tr>";
        $cor = new General();
        $texto = Textos::findFirst(5);
        $ord = new Ordenes();
        $conse = $ord->consecutivo();
        $empresa = Empresas::findFirst($this->request->getPost("emp_cod"));
        $post = $this->request->getPost("postulant");
        for ($i = 0; $i < count($post); $i++) {
            $ordenes = new Ordenes();
            $ordenes->pos_cod = $post[$i];
            $ordenes->emp_cod = $this->request->getPost("emp_cod");
            $ordenes->ord_ftr = $ord_ftr;
            $ordenes->ord_fec = $this->request->getPost("ord_fec");
            $ordenes->ord_car = $this->request->getPost("ord_car");
            $ordenes->ord_sal = $this->request->getPost("ord_sal");
            $ordenes->ord_arl = $this->request->getPost("ord_arl");
            $ordenes->ord_tna = $this->request->getPost("ord_tna");
            $ordenes->ord_cto = $this->request->getPost("ord_cto");
            $ordenes->tip_cod = $this->request->getPost("tip_cod");
            $ordenes->emp_cod = $this->request->getPost("emp_cod");
            $ordenes->ord_sot = $this->request->getPost("ord_sot");
            $ordenes->usu_cod = $this->session->get("usu_cod");
            $ordenes->ord_est = "2";
            $ordenes->ord_dir = "SI";
            $ordenes->ord_obs = $this->request->getPost("ord_obs");
            $ordenes->ord_ftr = new \Phalcon\Db\RawValue('default');
            $ordenes->ord_dot = $this->request->getPost("ord_dot");
            $ordenes->ord_fec_sot = $this->request->getPost("ord_fec_sot");
            $ordenes->save();
            $personas = Personas::findFirst($post[$i]);
            $personas->est_cod = 24;
            $personas->save();
            $cor->enviarCorreo($texto->tex_tex, $personas->per_ema, "Notificacion de seleccion Contactamos");
            
            $body .= "<tr>
          <td>" . $personas->per_ide . "</td>
          <td>" . $personas->per_pno . " " . $personas->per_sno . " " . $personas->per_pap . " " . $personas->per_sap . "</td>
          <td>" . $empresa->getEmpRaz() . "</td>
          <td>" . $this->request->getPost("ord_car") . "</td>
          <td>" . $this->request->getPost("ord_sal") . "</td>
          <td>" . $this->request->getPost("ord_cto") . "</td>
          <td>" . Tipos::findFirst($this->request->getPost("tip_cod"))->tip_des . "</td>
          <td>" . $this->request->getPost("ord_fec") . "</td>
           <td>" . Usuarios::findFirst($this->session->get("usu_cod"))->usu_nom . " " . Usuarios::findFirst($this->session->get("usu_cod"))->usu_ape . "</td>";
            $estudios = "";
            if ($this->request->getPost("ord_dot") == "SI") {
                $body .= "<td>" . $personas->per_tca . "</td>
          <td>" . $personas->per_tpa . "</td>
          <td>" . $personas->per_tza . "</td>";

            }
            $msg = 1;
            if (!empty($_POST['examenes'])) {
                foreach ($_POST['examenes'] as $examenes) {
                    $orden_examen = new OrdenesExamenes();
                    $orden_examen->ord_cod = $ordenes->ord_cod;
                    $orden_examen->exa_cod = $examenes;
                    $orden_examen->save();
                    $estudios .= ExamenesMedicos::findFirst($examenes)->exa_nom;
                    $estudios .= "<br>";
                }
            }
            if (!empty($_POST['examenes'])) {
                $body .= "<td>" . $estudios . "</td>";
            }
            $body .= "<tr>";
        }
        $body .= "</table>";
        $nume = $conse[0]->cons + 1;
        $usu = Usuarios::findFirst($this->session->get("usu_cod"));
        $correo = $usu->usu_ema;
        if ($this->request->getPost("ord_dot") == "SI" || !empty($_POST['examenes'])) {
            if ($usu->mun_cod == 149) {
                $cor->enviarCorreoB($body, "Orden de contratacion {$nume}", $correo, $empresa->getEmpIng());
            } else {
                $cor->enviarCorreoO($body, "Orden de contratacion {$nume}", $correo, $empresa->getEmpIng());
            }
        } else {
            $cor->enviarCorreoC($body, $correo, $empresa->getEmpIng(), "Orden de contratacion {$nume}");
        }
        echo "1";

    }

    public function guardarForAction()
    {
        if (!$this->session->has("usu_cod")) {
            echo "2";
            return false;
        }
        date_default_timezone_get("America/Bogota");
        $validation = new Phalcon\Validation();
        $validation->add('emp_cod', new PresenceOf(array(
            'message' => 'El campo Usuario/Proyecto Es Requerido',
        )));
        // $validation->add('pos_cod', new PresenceOf(array(
        //     'message' => 'El campo Postulante Es Requerido',
        // )));
        $validation->add('ord_fec', new PresenceOf(array(
            'message' => 'El campo Fecha de Ingreso Es Requerido',
        )));
        $validation->add('ord_car', new PresenceOf(array(
            'message' => 'El campo Cargo Es Requerido',
        )));
        $validation->add('ord_sot', new PresenceOf(array(
            'message' => 'El campo consecutivo sot/sit es requerido',
        )));
        $validation->add('ord_fec_sot', new PresenceOf(array(
            'message' => 'El campo fecha de ingreso SOT/SIT Es Requerido',
        )));
        // $validation->add('ord_sal', new PresenceOf(array(
        //     'message' => 'El campo Salario Es Requerido',
        // )));
        //  $validation->add('ord_cto', new PresenceOf(array(
        //     'message' => 'El campo Centro de Costo Es Requerido',
        // )));
        // $validation->add('ord_arl', new PresenceOf(array(
        //     'message' => 'El campo % ARL Es Requerido',
        // )));
        // $validation->add('ord_tna', new PresenceOf(array(
        //     'message' => 'El campo Tipo de Nomina Es Requerido',
        // )));
        // $validation->add('ord_tel', new PresenceOf(array(
        //     'message' => 'El campo Telefono Es Requerido',
        // )));
        $validation->add('tip_cod', new PresenceOf(array(
            'message' => 'El campo Empresa Es Requerido',
        )));
        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }
        $postulantes = $this->request->getPost("postulantes");
        $post = explode("SS", $postulantes);
        // echo var_dump($post);
        // return;
        $cad = "";
        $ord_ftr = date("Y-m-d");
        $body = "<h3>Notificación de orden de contratación</h3><br>";
        $body .= "<table border='1' width='900px'>
        <tr>
          <td>Identificación</td>
          <td>Nombres</td>
          <td>Usuaria / Proyecto</td>
          <td>Cargo</td>
          <td>Salario</td>
          <td>C.Costo</td>
          <td>Empresa</td>
          <td>Fecha Contratación</td>
           <td>Psicóloga</td>";
        if ($this->request->getPost("ord_dot") == "SI") {
            $body .= "<td>Talla camisa</td><td>Talla pantalón</td><td>Talla zapatos</td>";
        }
        if (!empty($_POST['examenes'])) {
            $body .= "<td>Estudios</td>";
        }
        $body .= "</tr>";
        $cor = new General();
        $texto = Textos::findFirst(5);
        $ord = new Ordenes();
        $conse = $ord->consecutivo();
        $empresa = Empresas::findFirst($this->request->getPost("emp_cod"));
        for ($i = 0; $i < count($post); $i++) {
            if ($i > 0) {
                $ordenes = new Ordenes();
                $ordenes->pos_cod = $post[$i];
                $ordenes->emp_cod = $this->request->getPost("emp_cod");
                $ordenes->ord_ftr = $ord_ftr;
                $ordenes->ord_fec = $this->request->getPost("ord_fec");
                $ordenes->ord_car = $this->request->getPost("ord_car");
                $ordenes->ord_sal = $this->request->getPost("ord_sal");
                $ordenes->ord_arl = $this->request->getPost("ord_arl");
                $ordenes->ord_tna = $this->request->getPost("ord_tna");
                $ordenes->ord_cto = $this->request->getPost("ord_cto");
                $ordenes->tip_cod = $this->request->getPost("tip_cod");
                $ordenes->emp_cod = $this->request->getPost("emp_cod");
                $ordenes->ord_sot = $this->request->getPost("ord_sot");
                $ordenes->usu_cod = $this->session->get("usu_cod");
                $ordenes->ord_est = "1";
                $ordenes->ord_dir = "NO";
                $ordenes->ord_obs = $this->request->getPost("ord_obs");
                $ordenes->ord_ftr = new \Phalcon\Db\RawValue('default');
                $ordenes->ord_dot = $this->request->getPost("ord_dot");
                $ordenes->ord_fec_sot = $this->request->getPost("ord_fec_sot");
                $personas = Personas::findFirst($post[$i]);
                $cor->enviarCorreo($texto->tex_tex, $personas->per_ema, "Notificacion de seleccion Contactamos");
                $personas->est_cod = 24;
                $body .= "<tr>
              <td>" . $personas->per_ide . "</td>
              <td>" . $personas->per_pno . " " . $personas->per_sno . " " . $personas->per_pap . " " . $personas->per_sap . "</td>
              <td>" . $empresa->getEmpRaz() . "</td>
              <td>" . $this->request->getPost("ord_car") . "</td>
              <td>" . $this->request->getPost("ord_sal") . "</td>
              <td>" . $this->request->getPost("ord_cto") . "</td>
              <td>" . Tipos::findFirst($this->request->getPost("tip_cod"))->tip_des . "</td>
              <td>" . $this->request->getPost("ord_fec") . "</td>
               <td>" . Usuarios::findFirst($this->session->get("usu_cod"))->usu_nom . " " . Usuarios::findFirst($this->session->get("usu_cod"))->usu_ape . "</td>";
                $estudios = "";
                if ($this->request->getPost("ord_dot") == "SI") {
                    $body .= "<td>" . $personas->per_tca . "</td>
              <td>" . $personas->per_tpa . "</td>
              <td>" . $personas->per_tza . "</td>";

                }
                if ($ordenes->save() AND $personas->save()) {
                    $msg = 1;
                } else {
                    $msg = "Problemas al guardar";
                }
                if (!empty($_POST['examenes'])) {
                    foreach ($_POST['examenes'] as $examenes) {
                        $orden_examen = new OrdenesExamenes();
                        $orden_examen->ord_cod = $ordenes->ord_cod;
                        $orden_examen->exa_cod = $examenes;
                        $orden_examen->save();
                        $estudios .= ExamenesMedicos::findFirst($examenes)->exa_nom;
                        $estudios .= "<br>";
                    }
                }
                if (!empty($_POST['examenes'])) {
                    $body .= "<td>" . $estudios . "</td>";
                }
                $body .= "<tr>";

            }
        }
        $body .= "</table>";
        $nume = $conse[0]->cons + 1;
        $usu = Usuarios::findFirst($this->session->get("usu_cod"));
        $correo = $usu->usu_ema;
        if ($this->request->getPost("ord_dot") == "SI" || !empty($_POST['examenes'])) {
            if ($usu->mun_cod == 149) {
                $cor->enviarCorreoB($body, "Orden de contratacion {$nume}", $correo, $empresa->getEmpIng());
            } else {
                $cor->enviarCorreoO($body, "Orden de contratacion {$nume}", $correo, $empresa->getEmpIng());
            }
        } else {
            $cor->enviarCorreoC($body, $correo, $empresa->getEmpIng(), "Orden de contratacion {$nume}");
        }
        echo "1";

    }

    public function actualizarAction()
    {
        $validation = new Phalcon\Validation();
        $validation->add('emp_cod', new PresenceOf(array(
            'message' => 'El campo Usuario / Proyecto Es Requerido',
        )));
        $validation->add('pos_cod', new PresenceOf(array(
            'message' => 'El campo Postulante Es Requerido',
        )));
        $validation->add('ord_fec', new PresenceOf(array(
            'message' => 'El campo Fecha de Ingreso Es Requerido',
        )));
        $validation->add('ord_car', new PresenceOf(array(
            'message' => 'El campo Cargo Es Requerido',
        )));
        $validation->add('ord_sal', new PresenceOf(array(
            'message' => 'El campo salario es requerido'
        )));
        $validation->add('ord_cto', new PresenceOf(array(
            'message' => 'El campo Centro de Costo Es Requerido',
        )));
        $validation->add('ord_arl', new PresenceOf(array(
            'message' => 'El campo %ARL Es Requerido',
        )));
        $validation->add('ord_tna', new PresenceOf(array(
            'message' => 'El campo Tipo de Nomina Es Requerido',
        )));
        $validation->add('tip_cod', new PresenceOf(array(
            'message' => 'El campo Empresa Es Requerido',
        )));
        $validation->add('ord_sot', new PresenceOf(array(
            'message' => 'El campo consecutivo sot/sit es requerido',
        )));
        $validation->add('ord_fec_sot', new PresenceOf(array(
            'message' => 'El campo fecha de ingreso SOT/SIT Es Requerido',
        )));
        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }
        $orden = Ordenes::findFirst($this->request->getPost("ord_cod"));
        $orden->emp_cod = $this->request->getPost("emp_cod");
        $orden->pos_cod = $this->request->getPost("pos_cod");
        $orden->ord_fec = $this->request->getPost("ord_fec");
        $orden->ord_car = $this->request->getPost("ord_car");
        $orden->ord_sal = $this->request->getPost("ord_sal");
        $orden->ord_cto = $this->request->getPost("ord_cto");
        $orden->ord_arl = $this->request->getPost("ord_arl");
        $orden->ord_tna = $this->request->getPost("ord_tna");
        $orden->tip_cod = $this->request->getPost("tip_cod");
        $orden->ord_obs = $this->request->getPost("ord_obs");
        $orden->ord_sot = $this->request->getPost("ord_sot");
        $orden->ord_fec_sot = $this->request->getPost("ord_fec_sot");
        if ($orden->save()) {
            echo "1";
        } else {
            foreach ($orden->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }

    }

    public function eliminarAction()
    {
        $ordenes = Ordenes::findFirst($this->request->getPost("id"));
        $ordenes->per_est = 0;
        if ($ordenes != false) {
            if ($ordenes->delete() == false) {
                echo "Lo sentimos, no se pudo eliminar la orden: \n";
                foreach ($ordenes->getMessages() as $message) {
                    echo $message, "\n";
                }
            } else {
                echo "1";
            }
        }

    }

    public function editarexAction()
    {
        $ord = Ordenes::findFirst($this->request->getPost("ord_cod"));
        $ord->examenes = $this->request->getPost("examenes");
        if ($ord->save()) {
            echo "1";
        } else {
            foreach ($ord->getMessages() as $message) {
                echo $message->getMessage(), "<br/>";
            }
        }
    }

    public function descargarAction()
    {
        $ordenes = Ordenes::findFirst($this->request->getPost("id"));
        $ordenes->sorftime = $this->request->getPost("sorftime");
        if ($ordenes->save()) {
            echo "1";
        } else {
            foreach ($ordenes->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

    public function editaresAction()
    {
        $ordenes = Ordenes::findFirst($this->request->getPost("ord_cod"));
        $ordenes->estudios = $this->request->getPost("estudios");
        if ($ordenes->save()) {
            echo "1";
        } else {
            foreach ($ordenes->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

}
