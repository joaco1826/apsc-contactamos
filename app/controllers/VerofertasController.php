<?php

class VerofertasController extends \Phalcon\Mvc\Controller
{

     public function initialize()
      {
         $this->assets
                 ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
                 ->addCss('css/estilos.css');

      }

    public function indexAction()
    {
    	$this->assets
            ->addJs('js/jquery.js')
             ->addJs('js/menu.js')
            ->addJs('js/verOfertas.js');

        $req = new Requisiciones();
        $this->view->setVar("ofertas", $req->listarA());
    }

    public function postularAction()
    {
    	$postulaciones = Postulaciones::find(array(
    		"per_cod='".$this->session->get("per_cod")."'"
    	));

        $postulado = Postulaciones::find(array(
            "req_cod='".$this->request->getPost("req_cod")."' AND per_cod='".$this->session->get("per_cod")."'"
        ));
        $validar = PersonasFam::find(array(
            "per_cod='".$this->session->get("per_cod")."'"
        ));
        if (count($validar) > 0) {
            $postulaciones->delete();
            if(count($postulado)>=1)
            {
                $this->session->set("req_cod", $this->request->getPost("req_cod"));
                echo "2";
                return;
            }
            $pos = new Postulaciones();
            $pos->per_cod = $this->session->get("per_cod");
            $pos->req_cod = $this->request->getPost("req_cod");
            $pos->pos_fec = new \Phalcon\Db\RawValue('default');
            $pos->pos_est = new \Phalcon\Db\RawValue('default');
            if($pos->save())
            {
                $this->session->set("req_cod", $this->request->getPost("req_cod"));
                echo "1";
            }else{
                echo "Ocurrio un problemas al postularse";
            }
        } else {
            echo "Debe diligenciar su hoja de vida completamente para poder realizar la postulación";
        }

    }

    public function postuladoAction()
    {

            $this->session->set("req_cod", $this->request->getPost("req_cod"));
            echo "1";

    }

    public function despostularAction()
     {
        $pos = Postulaciones::findFirst($this->request->getPost("pos_cod"));
        if($pos->delete()){
            echo "1";
        }else{
             foreach ($pos->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

     public function verofertaAction()
    {

        $of = new Requisiciones();
        $ofertas = $of->obtener($this->request->getPost("req_cod"));
        foreach ($ofertas as $key => $oft) {
            if ($oft->req_til == "") {
                $cargo = $oft->car_des;
            } else {
                $cargo = $oft->req_til;
            }
            echo '<table class="form" width="100%">
                            <tr>
                                <th>DETALLE DE LA OFERTA</th>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>Cargo</td>
                                            <td id="td_cargo">'.$cargo.'</td>
                                        </tr>
                                        <tr>
                                            <td>Ciudad</td>
                                            <td id="td_ciudad">'.$oft->mun_nom.'</td>
                                        </tr>
                                        <tr>
                                            <td>Sexo</td>
                                            <td id="td_sexo">'.$oft->req_sex.'</td>
                                        </tr>
                                        <tr>
                                            <td>Edad</td>
                                            <td id="td_edad">'.$oft->req_eda.'</td>
                                        </tr>
                                        <tr>
                                            <td>Salario</td>
                                            <td id="td_salario">'.$oft->req_sal.'</td>
                                        </tr>
                                        <tr>
                                            <td>Nivel Académico</td>
                                            <td id="td_nivel">'.$oft->req_niv.'</td>
                                        </tr>
                                        <tr>
                                            <td>Experiencia Requerida</td>
                                            <td id="td_experiencia">'.$oft->req_exp.'</td>
                                        </tr>
                                        <tr>
                                            <td>Descripción del Cargo</td>
                                            <td id="td_descripcion">'.$oft->req_des.'</td>
                                        </tr>
                                        <tr>
                                            <td>Fecha de Inicio</td>
                                            <td id="td_inicio">'.$oft->req_fci.'</td>
                                        </tr>
                                        <tr>
                                            <td>Observaciones</td>
                                            <td id="td_obs">'.$oft->req_obs.'</td>
                                        </tr>

                                    </table>

                                </td>
                            </tr>
                        </table>';
        }


    }

}

