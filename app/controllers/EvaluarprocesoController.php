<?php
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
class EvaluarProcesoController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
            ->addJs('js/obs.js')
            ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false)
            ->addJs('js/EvaluarProceso.js');



    }

    public function indexAction()
    {
        $this->view->setVar("empresas", Empresas::find(array(
            "emp_est = '1' AND emp_tip = 'C' ORDER BY emp_raz"
        )));

        $this->view->setVar("tipos", Tipos::find(array(
            "tip_tip = 'bolsa'"
        )));

        $this->view->setVar("examenes", ExamenesMedicos::find(array(
            "exa_tip = 'estudios'"
        )));

        $this->view->setVar("usuarios", Usuarios::find(array(
            "usu_nom <> '' ORDER BY usu_nom"
        )));
        $estado = new Estados();
        $this->view->setVar("estados", $estados = $estado->listarNotIn('agenda'));



    }

      public function consultarAction()
    {
        $txt_bus = $this->request->getPost("txt_bus");
        $cit = new Agenda();
        $fec_ini = $this->request->getPost("fec_ini");
        $fec_fin = $this->request->getPost("fec_fin");
        $usu_cod = $this->request->getPost("psi");
        $est_cod = $this->request->getPost("est_cod");
        if ($usu_cod != "" AND $est_cod == "") {
            $citas = $cit->listarUsu($txt_bus, $fec_ini, $fec_fin, $usu_cod);
        } elseif ($usu_cod != "" AND $est_cod != "") {
            $citas = $cit->listarUsuE($txt_bus, $fec_ini, $fec_fin, $usu_cod, $est_cod);
        } elseif ($usu_cod == "" AND $est_cod != "") {
            $citas = $cit->listarBusE($txt_bus, $fec_ini, $fec_fin, $est_cod);
        } else {
            $citas = $cit->listarBus($txt_bus, $fec_ini, $fec_fin);
        }

        $currentPage = (int) $this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
       $paginator   = new PaginatorModel(
        array(
            "data"  => $citas,
            "limit" => 10,
            "page"  => $currentPage
            )
        );
       $page = $paginator->getPaginate();
       $estado = new Estados();
        $estados = $estado->listarNotIn('agenda');
        ?>
        <table class="lista">
             <tr>
                <th>No. Documento</th>
                <th>Nombres y Apellidos</th>
                <th>Cargo</th>
                <th>Ciudad</th>
                <th>Estado</th>
                <th>En sala</th>
                <th>HV</th>
                <th>Pruebas</th>
                <th>Accion</th>
                <th>Informe</th>
                <th>Observaciones</th>
                <th>Contratar</th>
             </tr>
        <?
        foreach ($page->items as  $per) {

            ?>
                <tr>
                    <td><?=$per->per_ide?></td>
                    <td><?=ucwords(strtolower($per->per_pno . " " . $per->per_sno . " " . $per->per_pap . " " . $per->per_sap))?></td>
                    <td><?=$per->car_des?></td>
                    <td><?=$per->mun_nom?></td>
                    <td>
                        <select style="width: 100%" name="" id="est<?=$i?>" onchange="cambiarestado('<?=$per->per_cod?>', this.value)">
                        <option value="">SIN ESTADO</option>
                        <?
                        foreach ($estados as $est) {
                            if ($est->est_cod == $per->est_cod) { ?>
                                <option selected value="<?=$est->est_cod?>"><?=$est->est_nom?></option>
                            <? } else { ?>
                                <option value="<?=$est->est_cod?>"><?=$est->est_nom?></option>
                            <? }
                         ?>

                        <? }
                        ?>
                        </select>
                    </td>
                    <td align="center"><input type="checkbox" <?if($per->cit_asi == 1) { echo "checked"; } ?> name="che<?=$i?>"></td>
                    <td><a target="_blank" href="<?php echo $this->url->get('editarAspirante/index') ?>/<?=$per->per_cod?>"><img src="<?php echo $this->url->get()?>img/icon3.png" alt=""></a></td>
                    <td><a href="#" onclick="abrirModalP('<?=$per->per_cod?>', '<?=$per->req_cod?>');">Ver Pruebas</a></td>
                    <td><a href="#" onclick="abrirModal('<?=$per->per_cod?>', '<?=$per->req_cod?>');">Evaluar Proceso</a></td>
                    <td><a href="#" onclick="abrirModalI(<?=$per->per_cod?>, <?=$per->req_cod?>, '<?=$per->per_pno." ".$per->per_pap?>')"><img
                                    src="img/icon.png" alt=""></a></td>
                     <td><?
                    if ($per->per_obs == "") {
                        if ($per->val_obs != "") echo "<b>Valorado - ".$per->nombre." ".$per->apellido."</b><br>"; ?>
                        <span class="citar" onclick="abrirModalO('<?=$per->per_cod?>','<?=$per->per_pno." ".$per->per_pap?>', '', '<?=$per->val_obs?>')">Agregar</span>
                    <? }else {
                        if ($per->val_obs != "") echo "<b>Valorado - ".$per->nombre." ".$per->apellido."</b><br>"; ?>
                        <?=substr($per->per_obs, 0, 100)?>...<br>
                        <span class="citar" onclick="abrirModalO('<?=$per->per_cod?>','<?=$per->per_pno." ".$per->per_pap?>', '<?=$per->per_obs?>', '<?=$per->val_obs?>')">Editar</span>
                    <? }
                    ?></td>
                    <td align="center"><input type="checkbox" name="contr[]" class="postu" value="SS<?=$per->per_cod?>"></td>
                </tr>
            <?
        }
        ?>
            <tr>
                <td colspan="11" align="right">
                     <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                     <?php echo "<span class='btn ml10' onclick='filtrar(".$page->before.")'>Anterior</span>"; ?>
                    <?php
                    $num = ($page->total_items + 9) / 10;
                    for ($i=1; $i <= $num; $i++) {
                        if ($currentPage == $i) {
                            echo "<span class='btn'>".$i."</span>";
                        } else {
                            echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                        }

                    }
                    ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->next.")'>Siguiente</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->last.")'>Última</span>"; ?>
                    <?php echo "Página ", $page->current, " de ", $page->total_pages; ?>
                </td>
            </tr>
        </table>
        <?
    }
}