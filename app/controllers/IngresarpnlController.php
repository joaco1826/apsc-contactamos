<?php

class IngresarPNLController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
            ->addJs('js/IngresarPNL.js');


         $this->view->setVar("preguntas", PreguntasPnl::find());
         
            
    }

     public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $this->view->setVar("req_cod", $req_cod);
    }

    public function guardarAction()
    {
        if ($this->request->isPost() == true) {
            
            $per = new PersonasPnl();
            $per->per_cod = $this->request->getPost("per_cod");
            $per->req_cod = $this->request->getPost("req_cod");
            $per->usu_cod = $this->request->getPost("per_cod");
            $per->pna_fch = new \Phalcon\Db\RawValue('default');
            $per->pna_web = "int";


            $this->db->begin();

            if($per->save() == false){
                $this->db->rollback();
                return;
            }


            $preguntas = PreguntasPnl::find();

            

            foreach ($preguntas as  $reg) {
                $rpn_cod = 0;
                $pnl_cod = $reg->pnl_cod;
                
                // if(isset($this->request->getPost($p16_cod)){
                    $rpn_cod = $this->request->getPost($pnl_cod, null, 0); // (valor, sanizar, valor por defecto)
                // }

                $per_det = new PersonasPnlDet();
                $per_det->pna_cod = $per->pna_cod;
                $per_det->pnl_cod = $pnl_cod;
                $per_det->rpn_cod = $rpn_cod;

                if($per_det->save() == false){
                    $this->db->rollback();
                    return;
                }

            }


             $this->db->commit();
             // echo "Prueba Enviada";
             // return;
              $this->dispatcher->forward(array(
                    "controller" => "IngresarPNL",
                    "action" => "index",
                    "params" => array("resultado" => $this->request->getPost("per_cod"))
              ));
        }else{
             $this->response->redirect("IngresarPruebas/");
        }   
            
    }
}
