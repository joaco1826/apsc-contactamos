<?php

class PlantillaController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
    	$this->assets
    		 ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

    	$this->assets
            ->addJs('//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', false)
            ->addJs('js/jnalert.js');
    }

}

