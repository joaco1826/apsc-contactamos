jQuery(document).ready(function($) {
	$("#txt_bus").keyup(function(event) {

		$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			console.log(datos)
			$("#respuesta").html(datos)


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	});


});


function abrirModal(ord_cod){
	$(".negro").css("display", "block")
	$("#modal_documentos").css("display", "block")
	$("#ord_cod").val(ord_cod)
	$.ajax({
			url: $("#url_obtener").val(),
			type: 'POST',
			dataType: 'html',
			data: "ord_cod="+ord_cod,
			 beforeSend: function() {
				$("#modal_documentos").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			console.log(datos)
			$("#modal_documentos").html(datos)


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}

function cerrarModal(per_cod){
	$(".negro").css("display", "none")
	$("#modal_documentos").css("display", "none")


}
