<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Paginator\Adapter\Model as PaginatorModel;
class CompetenciasController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        $this->assets
            ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
            ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/competencias.js');


    }

    public function indexAction()
    {

        $this->view->setVar("competencias", Competencias::find(array(
            "status='activo'"
        )));

    }


    public function editarAction($id)
    {


        $this->view->setVar("com", Competencias::findFirst($id));


    }

    public function nuevoAction()
    {




    }

    public function guardarAction()
    {


        $validation = new Phalcon\Validation();

        $validation->add('nombre', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));

        $validation->add('texto', new PresenceOf(array(
            'message' => 'El campo Texto Es Requerido',

        )));




        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $competencia = new Competencias();
        $competencia->nombre  = $this->request->getPost("nombre");
        $ecompetencia->texto  = $this->request->getPost("texto");
        $competencia->status = new \Phalcon\Db\RawValue('default');
        $competencia->created_at = new \Phalcon\Db\RawValue('default');
        $competencia->updated_at = new \Phalcon\Db\RawValue('default');


        if($competencia->save()){
            echo "1";
        }else{
            foreach ($competencia->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }


    }

    public function actualizarAction()
    {


        $validation = new Phalcon\Validation();

        $validation->add('nombre', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));

        $validation->add('texto', new PresenceOf(array(
            'message' => 'El campo Texto Es Requerido',

        )));


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $competencia = Competencias::findFirst($this->request->getPost("id"));
        $competencia->nombre  = $this->request->getPost("nombre");
        $competencia->texto  = $this->request->getPost("texto");
        $competencia->updated_at = new \Phalcon\Db\RawValue('default');

        if($competencia->save()){
            echo "1";
        }else{
            foreach ($competencia->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }


    }

    public function eliminarAction()
    {

        $competencia = Competencias::findFirst($this->request->getPost("id"));
        $competencia->status="inactivo";
        if($competencia->save()){
            echo "1";
        }else{
            foreach ($competencia->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

}

