jQuery(document).ready(function($) {
	$("#per_ide").focusout(function(event) {
		
		$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'json',
			data: "ced="+$(this).val(),
			 beforeSend: function() {
				// $("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			console.log(datos)
			var f1, f2, aF1, aF2, numMeses;
			f1 = $("#fecha").val();
			f2 = datos[0].ord_fec;

			aF1 = f1.split("-");
			aF2 = f2.split("-");

			numMeses =  parseInt(aF2[0])*12 + parseInt(aF2[1]) - (parseInt(aF1[0])*12 + parseInt(aF1[1]));
			if (aF2[2]<aF1[2]){
			numMeses = numMeses - 1;
			}
			numMeses = numMeses * -1;
			$("#per_nom").val(datos[0].per_pno + " " + datos[0].per_sno + " " + datos[0].per_pap + " " + datos[0].per_sap);
			$("#cargo").val(datos[0].ord_car);
			$("#tiempo").val(numMeses);
			$("#proyecto").val(datos[0].emp_raz);
			$("#razon").val(datos[0].tip_des);
			$("#ciudad").val(datos[0].mun_nom);
			$("#emp_cod").val(datos[0].emp_cod);
			$("#per_cod").val(datos[0].per_cod);
			
		})
		.fail(function(datos) {
			console.log("error " + datos);
		})
		.always(function() {
			console.log("complete");
		});
		
	});
});
var x = 0;
function continuar1() {
	if ($("#enc_ret").val() == "") {
		alert("Selecione si fue retirado voluntariamente o no");
		return false;
	}
	if ($("#razon").val() == "") {
		alert("La razón social es requerida");
		return false;
	}
	if ($("#ciudad").val() == "") {
		alert("La ciudad es requerida");
		return false;
	}
	$("#blo1").css('display', 'none');
	$("#blo2").css('display', 'none');
	if ($("#enc_ret").val() == "si") {
		$("#blo3").css('display', 'block');
		$("#blo4").css('display', 'block');
		x = 1;
	} else {
		$("#blo5").css('display', 'block');
		$("#blo6").css('display', 'block');
		x = 2;
	}
}

function continuar2() {
	if ($("[name=enc_mot]").val() == "Otro") {
		if ($("#enc_otr").val().length < 5) {
			alert("Al seleccionar la opción otro, por favor coloque cual (5 caracteres por lo menos). ");
			return false
		}
	}
	$("#blo3").css('display', 'none');
	$("#blo4").css('display', 'none');
	$("#blo5").css('display', 'block');
	$("#blo6").css('display', 'block');
}

function atras1() {
	$("#blo3").css('display', 'none');
	$("#blo4").css('display', 'none');
	$("#blo2").css('display', 'block');
	$("#blo1").css('display', 'block');
}

function atras2() {
	if (x == 1) {
		$("#blo1").css('display', 'none');
		$("#blo2").css('display', 'none');
		$("#blo3").css('display', 'block');
		$("#blo4").css('display', 'block');
		$("#blo5").css('display', 'none');
		$("#blo6").css('display', 'none');
	} else {
		$("#blo5").css('display', 'none');
		$("#blo6").css('display', 'none');
		$("#blo3").css('display', 'none');
		$("#blo4").css('display', 'none');
		$("#blo2").css('display', 'block');
		$("#blo1").css('display', 'block');
	}
	
}

function enviar() {
	$("#Eenviar").attr('onclick', 'esperando()');
	$.ajax({
			url: $("#url_guardar").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm_encuesta").serialize(),
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Gracias por su tiempo.");
				location.reload()
			}else{
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
}