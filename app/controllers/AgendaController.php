<?php
use Phalcon\Validation\Validator\PresenceOf,
        Phalcon\Validation\Validator\Email,
        Phalcon\Paginator\Adapter\Model as PaginatorModel;
class AgendaController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/obs.js')
            ->addJs('//code.jquery.com/ui/1.11.3/jquery-ui.js', false)
            ->addJs('js/citas.js');


    }

    public function indexAction()
    {
        $this->view->setVar("usuarios", Usuarios::find(array(
            "usu_est='1'"
        )));


    }

    public function consultarAction() {
        $psi = $this->request->getPost("psi");
        $txt = $this->request->getPost("txt_bus");
        $fec_ini = $this->request->getPost("fec_ini");
        $fec_fin = $this->request->getPost("fec_fin");
        $currentPage = (int) $this->request->getPost("txt_pag");
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $cit = new Agenda();
        if (empty($psi) and empty($txt)) {
             $citas = $cit->listar($fec_ini, $fec_fin);
        } elseif (empty($psi) and !empty($txt)) {
            $citas = $cit->listarBus($txt, $fec_ini, $fec_fin);
        } else {
            $citas = $cit->listarCon($psi, $txt, $fec_ini, $fec_fin);
        }

       // $citas = $cit->listar();
       $paginator   = new PaginatorModel(
        array(
            "data"  => $citas,
            "limit" => 10,
            "page"  => $currentPage
            )
        );

        // Get the paginated results
        $page = $paginator->getPaginate();
        $estados = Estados::find(array(
            "est_pro='agenda'"
        ));
        ?>
        <table class="lista">
             <tr>
                <th>No.</th>
                <th>No. Documento</th>
                <th>Nombres</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Teléfono / Celular</th>
                <th>Psicóloga</th>
                <th>Estado</th>
                <th>Acción Cita</th>
                <th>En sala</th>
                <th>Observaciones</th>
             </tr>
        <?
        $i=1;
        foreach ($page->items as  $per) {

            ?>
                <tr>
                    <td><?=$i?></td>
                    <td><?=$per->per_ide?></td>
                    <td><?=ucwords(strtolower($per->per_pno . " " . $per->per_sno . " " . $per->per_pap . " " . $per->per_sap))?></td>
                    <td><?=date("d-m-Y", strtotime($per->cit_fec))?></td>
                    <td><?=$per->cit_hor?></td>
                    <td><?=$per->per_tel . " / " . $per->per_cel?></td>
                    <td><?=$per->usu_nom . " " . $per->usu_ape?></td>
                    <td>
                        <select name="" id="est<?=$i?>" onchange="cambiarestado('<?=$per->cit_cod?>', this.value)">
                        <?
                        foreach ($estados as $est) {
                            if ($per->estado == $est->est_cod) { ?>
                                <option selected value="<?=$est->est_cod?>"><?=$est->est_nom?></option>
                            <? } else { ?>
                                <option value="<?=$est->est_cod?>"><?=$est->est_nom?></option>
                            <? }
                         ?>

                        <? }
                        ?>
                        </select>
                    </td>
                    <td style="width: 120px"><a href="<?php echo $this->url->get('editarAspirante/index') ?>/<?php echo $per->per_cod; ?>"><img style="margin-right: 10px" src="<?php echo $this->url->get()?>img/icon3.png" alt=""></a><span class="citar" onclick="abrirModal('<?=$per->cit_cod?>','<?=$per->per_pno." ".$per->per_pap?>', '<?=$per->cit_fec ?>', '<?=$per->cit_hor ?>')">Editar</span><br><span class="citar" onclick="eliminarCita('<?=$per->cit_cod?>')"> Eliminar</span></td>
                    <td align="center"><input type="checkbox" <?if($per->cit_asi == 1) { echo "checked"; } ?> name="che<?=$i?>" onclick="chekear(this, '<?=$per->cit_cod?>')"></td>
                    <td><?
                    if ($per->per_obs == "") {
                         if ($per->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                        <span class="citar" onclick="abrirModalO('<?=$per->per_cod?>','<?=$per->per_pno." ".$per->per_pap?>', '', '<?=$per->val_obs?>')">Agregar</span>
                    <? }else {
                         if ($per->val_obs != "") echo "<b>Valorado</b><br>"; ?>
                        <?=substr($per->per_obs, 0, 100)?>...<br>
                        <span class="citar" onclick="abrirModalO('<?=$per->per_cod?>','<?=$per->per_pno." ".$per->per_pap?>', '<?=$per->per_obs?>', '<?=$per->val_obs?>')">Editar</span>

                    <? }
                    ?></td>
                </tr>


            <?
            $i++;
        }
        ?>

        <tr>
                <td colspan="13" align="right">
                     <?php echo "<span class='btn ml10' onclick='filtrar(1)'>Primera</span>"; ?>
                     <?php echo "<span class='btn ml10' onclick='filtrar(".$page->before.")'>Anterior</span>"; ?>
                    <?php
                    $num = ($page->total_items + 9) / 10;
                    for ($i=1; $i <= $num; $i++) {
                         if ($currentPage == $i) {
                                    echo "<span class='btn'>".$i."</span>";
                                } else {
                                    echo "<span class='btn ml10' onclick='filtrar(".$i.")'>".$i."</span>";
                                }
                    }

                    ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->next.")'>Siguiente</span>"; ?>
                    <?php echo "<span class='btn ml10' onclick='filtrar(".$page->last.")'>Última</span>"; ?>
                    <?php echo "Página ", $page->current, " de ", $page->total_pages; ?><span style="margin-left: 5px">filas</span>

                </td>
            </tr>

        </table>
        <?
    }

    public function guardarAction()
     {
        $validation = new Phalcon\Validation();

        $validation->add('per_cod', new PresenceOf(array(
            'message' => 'El codigo del aspirante se encuentra vacio',

        )));

        $validation->add('cit_fec', new PresenceOf(array(
            'message' => 'El campo Fecha Es Requerido',

        )));

        $validation->add('cit_hor', new PresenceOf(array(
            'message' => 'El campo  Hora Es Requerido',

        )));

        $validation->add('req_cod', new PresenceOf(array(
                'message' => 'El campo  Vacante Requerido',

            )));

        $citas = Agenda::count([
            "per_cod=".$this->request->getPost("per_cod"). " AND cit_fec >='".date("Y-m-d")."'"
        ]);

        if ($citas > 0) {
            echo "La persona ya tiene una cita asignada, deberá editar la cita ya creada.";
            return false;
        }

        $postulado = Postulaciones::find(array("per_cod=" . $this->request->getPost("per_cod")));
        if (count($postulado) > 0) {
            foreach ($postulado as $key => $po) {
                $obj = Postulaciones::findFirst($po->pos_cod);
                if ($obj->delete() == false) {
                    echo "Hubo un error - Contacte al administrador del sistema";
                    return false;
                }
            }
        }

        $pos = new Postulaciones();
        $pos->per_cod = $this->request->getPost("per_cod");
        $pos->req_cod = $this->request->getPost("req_cod");
        $pos->pos_fec = new \Phalcon\Db\RawValue('default');
        $pos->pos_est = new \Phalcon\Db\RawValue('default');
        if(!$pos->save()) {
            echo "Hubo un error - Contacte al administrador del sistema";
            return false;
        }

        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }

        if (!$this->session->has("usu_cod")) {
            echo "2";
            return false;
        }
        $agenda = new Agenda();
        $agenda->cit_est  = new \Phalcon\Db\RawValue('default');
        $agenda->cod_usu  = $this->session->get("usu_cod");
        if($agenda->save($_POST)){
            echo "1";
        }else{
             foreach ($agenda->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

     public function estadoAction()
     {
        $agenda = Agenda::findFirst($this->request->getPost("cit_cod"));
        $agenda->est_cod = $this->request->getPost("est_cod");
        if($agenda->save()){
            echo "1";
        }else{
             foreach ($agenda->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

     public function asistioAction()
     {
        $agenda = Agenda::findFirst($this->request->getPost("cit_cod"));
        $agenda->cit_asi = $this->request->getPost("cit_asi");
        if($agenda->save()){
            echo "1";
        }else{
             foreach ($agenda->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

     public function observacionesAction()
     {
        $agenda = Agenda::findFirst($this->request->getPost("per_cod"));
        $agenda->cit_obs = $this->request->getPost("per_obs");
        if($agenda->save()){
            echo "1";
        }else{
             foreach ($agenda->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

     public function editarAction()
     {
        $agenda = Agenda::findFirst($this->request->getPost("cit_cod"));
        $agenda->cit_fec = $this->request->getPost("cit_fec");
        $agenda->cit_hor = $this->request->getPost("cit_hor");
        if($agenda->save()){
            echo "1";
        }else{
             foreach ($agenda->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

     public function eliminarAction()
     {
        $agenda = Agenda::findFirst($this->request->getPost("cit_cod"));
        if($agenda->delete()){
            echo "1";
        }else{
             foreach ($agenda->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
     }

}

