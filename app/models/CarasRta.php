<?php

class CarasRta extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cre_cod;

    /**
     *
     * @var integer
     */
    public $car_cod;

    /**
     *
     * @var string
     */
    public $cre_rta;

    /**
     *
     * @var string
     */
    public $cre_ruta;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'cre_cod' => 'cre_cod', 
            'car_cod' => 'car_cod', 
            'cre_rta' => 'cre_rta', 
            'cre_ruta' => 'cre_ruta'
        );
    }

}
