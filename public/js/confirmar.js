jQuery(document).ready(function($) {
	$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_bus="+$("#txt_bus").val()+"&oficina="+$("#oficina").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			// console.log(datos)
			$("#respuesta").html(datos)


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	$("#txt_bus").keypress(function(e) {
		if(e.which == 13){
			$.ajax({
				url: $("#url_consultar").val(),
				type: 'POST',
				dataType: 'html',
				data: "txt_bus="+$("#txt_bus").val()+"&filas="+$("#filas").val()+"&txt_pag="+$("#txt_pag").val()+"&oficina="+$("#oficina").val(),
				 beforeSend: function() {
					$("#respuesta").html("Cargando...")
	  			 }
			})

			.done(function(datos) {
				// console.log(datos)
				$("#respuesta").html(datos)


			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}

	});
	$("#btn_guardar").click(function(event) {
		$.ajax({
			url: $("#url_base").val()+"confirmar/enviar",
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			//alert(datos)
			console.log(datos)
				alert("Se ha confirmado la contratacion");
				location.reload()


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	});

});

function abrirModal(per_cod, ord_cod){
	$(".negro").css("display", "block")
	$("#modal_documentos").css("display", "block")

	$("#per_cod").val(per_cod);
	$("#ord_cod").val(ord_cod)


}

function cerrarModal(per_cod){
	$(".negro").css("display", "none")
	$("#modal_documentos").css("display", "none")


}

function filtrar(pag) {
	$("#txt_pag").val(pag);
	$.ajax({
		url: $("#url_consultar").val(),
		type: 'POST',
		dataType: 'html',
		data: "txt_bus="+$("#txt_bus").val()+"&filas="+$("#filas").val()+"&txt_pag="+$("#txt_pag").val()+"&oficina="+$("#oficina").val(),
		 beforeSend: function() {
			$("#respuesta").html("Cargando...")
			 }
	})

	.done(function(datos) {
		console.log(datos)
		$("#respuesta").html(datos)


	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

function enviar(pos_cod, ord_cod, obj) {
    $.ajax({
        url: $("#url_base").val()+"confirmar/enviar",
        type: 'POST',
        dataType: 'html',
        data: "pos_cod="+pos_cod+"&ord_cod="+ord_cod,
    })
	.done(function(datos) {
		//alert(datos)
		console.log(datos)
		$(obj).parent().html('<strong>Confirmado</strong>');
		$(obj).remove();


	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}