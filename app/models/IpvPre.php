<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class IpvPre extends \Phalcon\Mvc\Model
{

   public static function obtenerResultado($pos_cod)
    {
        // A raw SQL statement
        $hoy = date("Y-m-d");
        $fecha = strtotime ( '-720 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
       $sql ="SELECT *
            FROM ipv_per JOIN ipv_per_det ON ipv_per.`ias_cod`=ipv_per_det.`ias_cod`
            WHERE ipv_per.ias_cod=(SELECT MAX(ias_cod) FROM ipv_per WHERE per_cod='$pos_cod') AND DATE(ipv_per.ias_fch) BETWEEN '$fecha' AND '$hoy'";

        // Base model
        $ipv = new IpvPre();

        // Execute the query
        return new Resultset(null, $ipv, $ipv->getReadConnection()->query($sql));
    }

    public static function npruebas($fecha)
    {
        // A raw SQL statement

       $sql ="SELECT COUNT(*) as total
            FROM ipv_per 
            WHERE DATE(ias_fch) = '$fecha'";

        // Base model
        $ipv = new IpvPre();

        // Execute the query
        return new Resultset(null, $ipv, $ipv->getReadConnection()->query($sql));
    }

}
