<?php

class PersonasHijos extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $hij_cod;

    /**
     *
     * @var string
     */
    public $hij_nom;

    /**
     *
     * @var string
     */
    public $hij_eda;

     /**
     *
     * @var string
     */
    public $hij_ocu;

     /**
     *
     * @var string
     */
    public $hij_par;

    /**
     *
     * @var integer
     */
    public $per_cod;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'hij_cod' => 'hij_cod', 
            'hij_nom' => 'hij_nom', 
            'hij_eda' => 'hij_eda', 
            'hij_ocu' => 'hij_ocu', 
            'hij_par' => 'hij_par',
            'per_cod' => 'per_cod'
        );
    }

}
