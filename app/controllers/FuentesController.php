<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class FuentesController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/sectores.js');
    }

    public function indexAction()
    {
       
            
            
    	 $this->view->setVar("fuentes", Tipos::find(array(
		    	"tip_tip = 'rec' ORDER BY tip_des"
		 )));
    }

     public function nuevoAction()
    {
    	
       
    }

     public function editarAction($tip_cod)
    {
    	

 		$this->view->setVar("fuente", Tipos::findFirst($tip_cod));
 		
       
    }

     public function guardarAction()
    {
    	

		$validation = new Phalcon\Validation();
       
		$validation->add('tip_des', new PresenceOf(array(
		    'message' => 'El campo Nombre Es Requerido',

		)));

        


		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$fuente = new Tipos();
    	$fuente->tip_des  = $this->request->getPost("tip_des");
        $fuente->tip_tip  = "rec";

    	
    	if($fuente->save()){
    		echo "1";
    	}else{
    		 foreach ($fuente->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    	

    }

     public function actualizarAction()
    {
    	

		$validation = new Phalcon\Validation();
       
		$validation->add('tip_des', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));


		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}



    	$fuente = Tipos::findFirst($this->request->getPost("tip_cod"));
    	$fuente->tip_des  = $this->request->getPost("tip_des");

    	
    	if($fuente->save()){
    		echo "1";
    	}else{
    		 foreach ($fuente->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    	

    }

     public function eliminarAction()
    {
    	
    	$fuente = Tipos::findFirst($this->request->getPost("cod"));
    	if($fuente->delete()){
    		echo "1";
    	}else{
    		 foreach ($fuente->getMessages() as $message) {
		        echo "Message: ", $message->getMessage();
		        echo "Field: ", $message->getField();
		        echo "Type: ", $message->getType();
   		 	}
    	}
    }

}

