<?php

class VerPFController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/EvaluarPrueba.js');
    }

     public function indexAction($per_cod)
    {
          // $persona = Personas::findFirst($per_cod);
          // $this->view->setVar("persona", $persona);
          $int = split("-", $per_cod);
          if (count($int) > 1) {
            $per_cod = $int[0];
            $req_cod = $int[1];
          }
        
          $persona = new Personas();
          $persona_datos = $persona->getEdadSexo($per_cod);
          $this->view->setVar("persona", $persona_datos);
          $this->view->setVar("per_cod", $per_cod);

          $pf = Personas16pf::findFirst(array("per_cod=$per_cod ORDER BY p16_cod DESC"))->p16_cod;
          $this->view->setVar("pf", $pf);



    }

    public function eliminarAction()
    {
           $pf = Personas16pf::findFirst($this->request->getPost("cod"));
           if ($pf) {
             if ($pf->delete() == false) {
                echo "Lo sentimos, hubo un error: \n";

                foreach ($robot->getMessages() as $message) {
                    echo $message, "\n";
                }
            } else {
                echo "1";
            }
           }

    }
}