<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Personas extends \Phalcon\Mvc\Model
{

    public function getEdadSexo($per_cod)
    {
        $sql = "SELECT per_ide, per_sex,  per_cod, per_pno, per_sno, per_pap, per_sap, per_pro, YEAR(CURDATE())-YEAR(per_fna) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(per_fna,'%m-%d'), 0, -1) 
				AS EDAD_ACTUAL FROM Personas WHERE per_cod=$per_cod";
        // return $this->modelsManager->executeQuery($sql);
        return $this->getModelsManager()->executeQuery($sql);

    }

    public function listar()
    {
        $sql = "SELECT p.*,e.*,r.*,c.*,m.*, p.per_cod AS cod,r.req_cod AS req FROM personas p LEFT JOIN estados e ON e.est_cod=p.est_cod INNER JOIN muni m ON p.per_ciu=m.mun_cod LEFT JOIN postulaciones po ON po.per_cod=p.per_cod LEFT JOIN requisiciones r ON r.req_cod=po.req_cod LEFT JOIN cargos c ON c.car_cod=r.car_cod WHERE p.per_est = '1' ORDER BY p.per_cod DESC";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function listarBus($where)
    {
        $sql = "SELECT p.*,e.*,r.*,c.*,m.*,v.*, p.per_cod AS cod,r.req_cod AS req FROM personas p LEFT JOIN estados e ON e.est_cod=p.est_cod LEFT JOIN postulaciones po ON po.per_cod=p.per_cod LEFT JOIN requisiciones r ON r.req_cod=po.req_cod LEFT JOIN cargos c ON c.car_cod=r.car_cod LEFT JOIN muni m ON p.per_ciu=m.mun_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) {$where} ORDER BY p.per_cod DESC";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function listarBusC($where)
    {
        $sql = "SELECT p.*,e.*,r.*,c.*,m.*,v.*, p.per_cod AS cod,r.req_cod AS req FROM personas p INNER JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN cargos c ON c.car_cod=r.car_cod LEFT JOIN muni m ON p.per_ciu=m.mun_cod LEFT JOIN estados e ON e.est_cod=p.est_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) {$where} ORDER BY p.per_cod DESC";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function listarInt($where)
    {
        $sql = "SELECT p.*,e.*,c.*,m.*,r.*,v.*, p.per_cod AS cod,r.req_cod AS req FROM personas p INNER JOIN cargos c ON c.car_cod=p.car_cod LEFT JOIN muni m ON p.per_ciu=m.mun_cod LEFT JOIN estados e ON e.est_cod=p.est_cod LEFT JOIN postulaciones po ON po.per_cod=p.per_cod LEFT JOIN requisiciones r ON r.req_cod=po.req_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) {$where} ORDER BY p.per_cod DESC";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function listarBusCM($where)
    {
        $sql = "SELECT p.*,e.*,r.*,c.*,m.*,v.*, p.per_cod AS cod,r.req_cod AS req FROM personas p INNER JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN cargos c ON c.car_cod=r.car_cod INNER JOIN muni m ON p.per_ciu=m.mun_cod LEFT JOIN estados e ON e.est_cod=p.est_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) {$where} ORDER BY p.per_cod DESC";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function listarBusM($where)
    {
        $sql = "SELECT p.*,e.*,r.*,c.*,m.*,v.*, p.per_cod AS cod,r.req_cod AS req FROM personas p INNER JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN cargos c ON c.car_cod=r.car_cod INNER JOIN muni m ON p.per_ciu=m.mun_cod LEFT JOIN estados e ON e.est_cod=p.est_cod LEFT JOIN valoraciones v ON v.pos_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod) {$where} ORDER BY p.per_cod DESC";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function actualizarEstado($est_cod, $per_cod)
    {
        $sql = "UPDATE personas SET est_cod=$est_cod WHERE per_cod=$per_cod";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function consultarCedula($ced)
    {
        $sql = "SELECT p.per_cod, p.per_pno, p.per_sno, p.per_pap, p.per_sap, e.emp_cod, e.emp_raz, o.ord_cod, o.ord_fec, o.ord_car, t.tip_des, m.mun_nom FROM personas p INNER JOIN ordenes o ON o.pos_cod=p.per_cod INNER JOIN empresas e ON e.emp_cod=o.emp_cod INNER JOIN tipos t ON t.tip_cod=o.tip_cod INNER JOIN usuarios u ON u.usu_cod=o.usu_cod INNER JOIN muni m ON m.mun_cod=u.mun_cod WHERE p.per_ide='$ced' ORDER BY o.ord_cod DESC LIMIT 1";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function consultarCedulaC($ced)
    {
        $sql = "SELECT o.nombre1 as per_pno, o.nombre2 as per_sno, o.apellido1 as per_pap, o.apellido2 as per_sap, e.emp_cod, e.emp_raz, o.fecha as ord_fec, o.cargo as ord_car FROM contratados o INNER JOIN empresas e ON e.emp_cod=o.usuaria WHERE o.cedula='$ced' LIMIT 1";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosT($fechai, $fechaf)
    {
        $sql = "SELECT DATE(per_fin) as fecha, count(per_cod) as total FROM personas WHERE DATE(per_fin) BETWEEN '$fechai' AND '$fechaf'  GROUP BY DATE(per_fin)";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosTD($fechai, $fechaf, $dpto)
    {
        $sql = "SELECT DATE(p.per_fin) as fecha, count(p.per_cod) as total FROM personas p INNER JOIN muni m ON m.mun_cod=p.per_ciu INNER JOIN dpto d ON d.dep_cod=m.dep_cod AND d.dep_cod=$dpto WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf'  GROUP BY DATE(p.per_fin)";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosA($fechai, $fechaf)
    {
        $sql = "SELECT count(per_cod) as total FROM personas WHERE DATE(per_fin) BETWEEN '$fechai' AND '$fechaf' AND (est_cod NOT IN (7,8,9,11,12,13,14,21,22,24,25,26,27,29) OR est_cod IS NULL) ";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosAD($fechai, $fechaf, $dpto)
    {
        $sql = "SELECT count(p.per_cod) as total FROM personas p INNER JOIN muni m ON m.mun_cod=p.per_ciu INNER JOIN dpto d ON d.dep_cod=m.dep_cod AND d.dep_cod=$dpto WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf' AND (p.est_cod NOT IN (7,8,9,11,12,13,14,21,22,24,25,26,27,29) OR p.est_cod IS NULL) ";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosF($fechai, $fechaf)
    {
        $sql = "SELECT per_fre, count(per_fre) as total FROM personas WHERE per_fin BETWEEN '$fechai' AND '$fechaf' AND (per_fre='Página Web Contactamos' OR per_fre='Redes Sociales (Facebook, Twitter)' OR per_fre='Computrabajo' OR per_fre='El Empleo' OR per_fre='Reclutador Contactamos' OR per_fre='Líderes de Comunidad Contactamos' OR per_fre='Empresa Cliente' OR per_fre='Referido' OR per_fre='Entidad Educativa' OR per_fre='Centro de oportunidades') GROUP BY per_fre";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosFD($fecha, $tip_cod)
    {
        $sql = "SELECT DATE(p.per_fin) AS fecha, t.tip_des, COUNT(p.per_fre) AS total FROM tipos t LEFT JOIN personas p ON t.tip_cod=p.per_fre WHERE t.tip_cod='$tip_cod' AND DATE(p.per_fin)='$fecha'";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosFDC($fecha, $tip_cod)
    {
        $sql = "SELECT DATE(p.per_fin) AS fecha, t.tip_des, COUNT(p.per_fre) AS total FROM tipos t LEFT JOIN personas p ON t.tip_cod=p.per_fre WHERE t.tip_cod='$tip_cod' AND p.per_ciu<>'' AND DATE(p.per_fin)='$fecha'";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosC($fechai, $fechaf)
    {
        $sql = "SELECT m.mun_nom, count(p.per_ciu) as total FROM personas p INNER JOIN muni m ON m.mun_cod=p.per_ciu WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf' GROUP BY p.per_ciu ORDER BY m.mun_nom";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosS($fechai, $fechaf, $per_sex, $dpto)
    {
        if ($dpto == "TODOS") {
            $sql = "SELECT t.tip_des, count(p.per_cod) as total FROM personas p INNER JOIN tipos t ON t.tip_cod=p.per_sex WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf'  AND (est_cod NOT IN (7,8,9,11,12,13,14,21,22,24,25,26,27,29) OR est_cod IS NULL) AND p.per_sex='$per_sex' ";
        } else {
            $sql = "SELECT t.tip_des, count(p.per_cod) as total FROM personas p INNER JOIN muni m ON m.mun_cod=p.per_ciu INNER JOIN dpto d ON d.dep_cod=m.dep_cod AND d.dep_cod=$dpto INNER JOIN tipos t ON t.tip_cod=p.per_sex WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf'  AND (est_cod NOT IN (7,8,9,11,12,13,14,21,22,24,25,26,27,29) OR est_cod IS NULL) AND p.per_sex='$per_sex' ";
        }
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosN($fechai, $fechaf, $per_niv, $dpto)
    {
        if ($dpto == "TODOS") {
            $sql = "SELECT t.tip_des, count(p.per_cod) as total FROM personas p INNER JOIN tipos t ON t.tip_cod=p.per_niv WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf'  AND (est_cod NOT IN (7,8,9,11,12,13,14,21,22,24,25,26,27,29) OR est_cod IS NULL) AND p.per_niv='$per_niv' ";
        } else {
            $sql = "SELECT t.tip_des, count(p.per_cod) as total FROM personas p INNER JOIN muni m ON m.mun_cod=p.per_ciu INNER JOIN dpto d ON d.dep_cod=m.dep_cod AND d.dep_cod=$dpto INNER JOIN tipos t ON t.tip_cod=p.per_niv WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf'  AND (est_cod NOT IN (7,8,9,11,12,13,14,21,22,24,25,26,27,29) OR est_cod IS NULL) AND p.per_niv='$per_niv' ";
        }
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosE($fechai, $fechaf, $est_cod, $dpto)
    {
        if ($dpto == "TODOS") {
            $sql = "SELECT e.est_nom, count(p.est_cod) as total FROM personas p RIGHT JOIN estados e ON e.est_cod=p.est_cod WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf' AND e.est_cod='$est_cod'";
        } else {
            $sql = "SELECT e.est_nom, count(p.est_cod) as total FROM personas p INNER JOIN muni m ON m.mun_cod=p.per_ciu INNER JOIN dpto d ON d.dep_cod=m.dep_cod AND d.dep_cod=$dpto RIGHT JOIN estados e ON e.est_cod=p.est_cod WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf' AND e.est_cod='$est_cod'";
        }
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosI($fechai, $fechaf, $car_cod, $dpto)
    {
        if ($dpto == "TODOS") {
            $sql = "SELECT e.car_des, count(p.car_cod) as total FROM personas p RIGHT JOIN cargos e ON e.car_cod=p.car_cod WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf' AND e.car_cod='$car_cod'";
        } else {
            $sql = "SELECT e.car_des, count(p.car_cod) as total FROM personas p INNER JOIN muni m ON m.mun_cod=p.per_ciu INNER JOIN dpto d ON d.dep_cod=m.dep_cod AND d.dep_cod=$dpto RIGHT JOIN cargos e ON e.car_cod=p.car_cod WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf' AND e.car_cod='$car_cod'";
        }
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function registrosP($fechai, $fechaf, $car_cod, $dpto)
    {
        if ($dpto == "TODOS") {
            $sql = "SELECT e.car_des, count(e.car_cod) as total FROM personas p RIGHT JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN cargos e ON e.car_cod=r.car_cod WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf' AND e.car_cod='$car_cod'";
        } else {
            $sql = "SELECT e.car_des, count(e.car_cod) as total FROM personas p INNER JOIN muni m ON m.mun_cod=p.per_ciu INNER JOIN dpto d ON d.dep_cod=m.dep_cod AND d.dep_cod=$dpto RIGHT JOIN postulaciones po ON po.per_cod=p.per_cod INNER JOIN requisiciones r ON r.req_cod=po.req_cod INNER JOIN cargos e ON e.car_cod=r.car_cod WHERE DATE(p.per_fin) BETWEEN '$fechai' AND '$fechaf' AND e.car_cod='$car_cod'";
        }
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function consolidadoingresos($fechai, $fechaf)
    {
        $sql = "SELECT oc.con_fre,e.emp_raz,t.tip_des,m.mun_nom AS cic,o.ord_cod,o.ord_dir, o.ord_car,o.ord_sal, o.ord_arl,o.ord_sot,o.ord_cto,o.ord_fec,o.ord_ftr,o.ord_dot,o.ord_doc,o.ord_fdo,o.fecha_correo,o.ord_fec_sot,mn.mun_nom,tp.tip_des AS razon,ti.tip_des AS tdocu,p.per_cod,p.per_ciu,p.per_ide, p.per_pno,p.per_sno,p.per_pap,p.per_sap,p.per_fna,p.per_ema,p.per_tel,p.per_cel,ag.cit_fec,pf.p16_cod,c.cas_cod,ct.cmt_cod,iv.ias_cod,pl.pna_cod,v.val_cod,s.ent_fec,va.val_fch, us.usu_nom AS nom_usu, us.usu_ape AS ape_usu, u.usu_nom, u.usu_ape, af.banco, af.arl, af.caja, af.eps, af.pension FROM ordenes o
        INNER JOIN tipos tp ON tp.tip_cod=o.tip_cod
        INNER JOIN personas p ON p.per_cod=o.pos_cod AND o.ord_fec BETWEEN '$fechai' AND '$fechaf' AND p.per_est='1'
        INNER JOIN empresas e ON e.emp_cod=o.emp_cod 
        INNER JOIN usuarios u ON u.usu_cod=o.usu_cod 
        INNER JOIN muni m ON m.mun_cod=u.mun_cod
        LEFT JOIN muni mn ON mn.mun_cod=p.per_ciu
        LEFT JOIN tipos t ON t.tip_cod=p.per_fre
        LEFT JOIN tipos ti ON ti.tip_cod=p.per_tid
        LEFT JOIN agenda ag ON ag.per_cod=p.per_cod AND ag.cit_cod=(SELECT MAX(cit_cod) FROM agenda WHERE per_cod=p.per_cod)
        LEFT JOIN 16pf_per pf ON pf.per_cod=p.per_cod AND pf.p16_cod=(SELECT MAX(p16_cod) FROM 16pf_per WHERE per_cod=p.per_cod)
        LEFT JOIN caras_per c ON c.per_cod=p.per_cod AND c.cas_cod=(SELECT MAX(cas_cod) FROM caras_per WHERE per_cod=p.per_cod)
        LEFT JOIN cmt ct ON ct.per_cod=p.per_cod AND ct.cmt_cod=(SELECT MAX(cmt_cod) FROM cmt WHERE per_cod=p.per_cod)
        LEFT JOIN ipv_per iv ON iv.per_cod=p.per_cod AND iv.ias_cod=(SELECT MAX(ias_cod) FROM ipv_per WHERE per_cod=p.per_cod)
        LEFT JOIN pnl_per pl ON pl.per_cod=p.per_cod AND pl.pna_cod=(SELECT MAX(pna_cod) FROM pnl_per WHERE per_cod=p.per_cod)
        LEFT JOIN valanti v ON v.per_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valanti WHERE per_cod=p.per_cod)
        LEFT JOIN entrevistas s ON s.pos_cod=p.per_cod AND s.ent_cod=(SELECT MAX(ent_cod) FROM entrevistas WHERE pos_cod=p.per_cod)
        LEFT JOIN valoraciones va ON va.pos_cod=p.per_cod AND va.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod)
        LEFT JOIN usuarios us ON us.usu_cod=va.usu_cod
        LEFT JOIN ordenes_confirmar oc ON oc.pos_cod=o.pos_cod AND oc.con_cod=(SELECT MAX(con_cod) FROM ordenes_confirmar WHERE pos_cod=p.per_cod)
        LEFT JOIN afiliaciones af ON af.ord_cod=o.ord_cod";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function consolidado($fechai, $fechaf, $cargos)
    {
        $sql = "SELECT e.est_nom,u.usu_nom,u.usu_ape,cr.car_des,po.pos_fec,ca.car_des AS interes,oc.con_fre,t.tip_des, o.ord_fec,o.ord_ftr,mn.mun_nom,p.per_ide, p.per_pno,p.per_sno,p.per_pap,p.per_sap,p.per_fna,p.per_ema,p.per_tel,p.per_cel,p.per_ciu,ag.cit_fec,pf.p16_cod,c.cas_cod,ct.cmt_cod,iv.ias_cod,pl.pna_cod,v.val_cod,s.ent_fec,va.val_fch, us.usu_nom AS nom_usu, us.usu_ape AS ape_usu FROM personas p
        INNER JOIN cargos ca ON ca.car_cod=p.car_cod AND p.car_cod IN (SELECT car_cod FROM cargos WHERE car_cod IN($cargos))
        LEFT JOIN muni mn ON mn.mun_cod=p.per_ciu
        LEFT JOIN estados e ON e.est_cod=p.est_cod
        
        LEFT JOIN postulaciones po ON po.per_cod=p.per_cod AND po.pos_cod=(SELECT MAX(pos_cod) FROM postulaciones WHERE per_cod=p.per_cod)
        LEFT JOIN requisiciones r ON r.req_cod=po.req_cod
        LEFT JOIN cargos cr ON cr.car_cod=r.car_cod
        LEFT JOIN tipos t ON t.tip_cod=p.per_fre
        LEFT JOIN agenda ag ON ag.per_cod=p.per_cod AND ag.cit_cod=(SELECT MAX(cit_cod) FROM agenda WHERE per_cod=p.per_cod)
        LEFT JOIN usuarios u ON u.usu_cod=ag.cod_usu
        LEFT JOIN 16pf_per pf ON pf.per_cod=p.per_cod AND pf.p16_cod=(SELECT MAX(p16_cod) FROM 16pf_per WHERE per_cod=p.per_cod)
        LEFT JOIN caras_per c ON c.per_cod=p.per_cod AND c.cas_cod=(SELECT MAX(cas_cod) FROM caras_per WHERE per_cod=p.per_cod)
        LEFT JOIN cmt ct ON ct.per_cod=p.per_cod AND ct.cmt_cod=(SELECT MAX(cmt_cod) FROM cmt WHERE per_cod=p.per_cod)
        LEFT JOIN ipv_per iv ON iv.ias_cod=p.per_cod AND iv.ias_cod=(SELECT MAX(ias_cod) FROM ipv_per WHERE per_cod=p.per_cod)
        LEFT JOIN pnl_per pl ON pl.per_cod=p.per_cod AND pl.pna_cod=(SELECT MAX(pna_cod) FROM pnl_per WHERE per_cod=p.per_cod)
        LEFT JOIN valanti v ON v.per_cod=p.per_cod AND v.val_cod=(SELECT MAX(val_cod) FROM valanti WHERE per_cod=p.per_cod)
        LEFT JOIN entrevistas s ON s.pos_cod=p.per_cod AND s.ent_cod=(SELECT MAX(ent_cod) FROM entrevistas WHERE pos_cod=p.per_cod)
        LEFT JOIN valoraciones va ON va.pos_cod=p.per_cod AND va.val_cod=(SELECT MAX(val_cod) FROM valoraciones WHERE pos_cod=p.per_cod)
        LEFT JOIN usuarios us ON us.usu_cod=va.usu_cod
        LEFT JOIN ordenes o ON o.pos_cod=p.per_cod
        LEFT JOIN ordenes_confirmar oc ON oc.pos_cod=o.pos_cod AND oc.con_cod=(SELECT MAX(con_cod) FROM ordenes_confirmar WHERE pos_cod=p.per_cod)
        WHERE p.per_fin BETWEEN '$fechai' AND '$fechaf' AND p.per_est='1'";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function consolidadomai($fechai, $fechaf)
    {
        $sql = "SELECT p.per_pno,p.per_sno,p.per_pap,p.per_sap,p.per_ema,p.per_ciu,c.car_des,e.est_nom,m.mun_nom,t.tip_des FROM personas p 
        LEFT JOIN cargos c ON c.car_cod=p.car_cod 
        LEFT JOIN muni m ON m.mun_cod=p.per_ciu 
        LEFT JOIN tipos t ON tip_cod=p.per_fre 
        LEFT JOIN estados e ON e.est_cod=p.est_cod 
        WHERE p.per_fin BETWEEN '$fechai' AND '$fechaf' AND p.per_est='1'";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function encuestaSalida($fechai, $fechaf)
    {
        $sql = "SELECT *, DATE(en.enc_fec) as fecha FROM encuesta en 
        INNER JOIN personas p ON p.per_cod=en.per_cod
        INNER JOIN ordenes o ON o.pos_cod=p.per_cod AND o.ord_cod = (SELECT MAX(ord_cod) FROM ordenes WHERE pos_cod=p.per_cod)
        INNER JOIN empresas e ON e.emp_cod=o.emp_cod
        INNER JOIN tipos t ON t.tip_cod=o.tip_cod
        INNER JOIN usuarios u ON u.usu_cod=o.usu_cod
        INNER JOIN muni m ON u.mun_cod=m.mun_cod
        WHERE DATE(en.enc_fec) BETWEEN '$fechai' AND '$fechaf'";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function encuestaLogytech($fechai, $fechaf)
    {
        $sql = "SELECT *, DATE(en.date_create) as fecha FROM logytech en 
        INNER JOIN personas p ON p.per_cod=en.per_cod
        INNER JOIN ordenes o ON o.pos_cod=p.per_cod
        INNER JOIN empresas e ON e.emp_cod=o.emp_cod
        INNER JOIN tipos t ON t.tip_cod=o.tip_cod
        INNER JOIN usuarios u ON u.usu_cod=o.usu_cod
        INNER JOIN muni m ON u.mun_cod=m.mun_cod
        WHERE DATE(en.date_create) BETWEEN '$fechai' AND '$fechaf'";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public function encuestaSalidaC($fechai, $fechaf)
    {
        $sql = "SELECT *, DATE(en.enc_fec) as fecha FROM encuesta en 
        INNER JOIN personas p ON p.per_cod=en.per_cod
        INNER JOIN contratados c ON c.cedula=p.per_ide
        INNER JOIN empresas e ON e.emp_cod=c.usuaria
        WHERE DATE(en.enc_fec) BETWEEN '$fechai' AND '$fechaf'";
        $obj = new Personas();
        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

}
