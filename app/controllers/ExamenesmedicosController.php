<?php
use Phalcon\Validation\Validator\PresenceOf,
        Phalcon\Validation\Validator\Email,
        Phalcon\Paginator\Adapter\Model as PaginatorModel;
class ExamenesmedicosController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/examenes.js');
         
            
    }

    public function indexAction()
    {

        $this->view->setVar("examenes", ExamenesMedicos::find(array(
            "exa_est='1'"
        )));

    }


    public function editarAction($exa_cod)
    {
        

        $this->view->setVar("examen", ExamenesMedicos::findFirst($exa_cod));
        
       
    }

    public function nuevoAction()
    {
        

        
       
    }

     public function guardarAction()
    {
        

        $validation = new Phalcon\Validation();
       
        $validation->add('exa_nom', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));

        $validation->add('exa_tip', new PresenceOf(array(
            'message' => 'El campo Tipo Es Requerido',

        )));

        


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $examenes = new ExamenesMedicos();
        $examenes->exa_nom  = $this->request->getPost("exa_nom");
        $examenes->exa_tip  = $this->request->getPost("exa_tip");
        $examenes->exa_est = new \Phalcon\Db\RawValue('default');

        
        if($examenes->save()){
            echo "1";
        }else{
             foreach ($examenes->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
        

    }

     public function actualizarAction()
    {
        

        $validation = new Phalcon\Validation();
       
        $validation->add('exa_nom', new PresenceOf(array(
            'message' => 'El campo Nombre Es Requerido',

        )));

        $validation->add('exa_tip', new PresenceOf(array(
            'message' => 'El campo Tipo Es Requerido',

        )));


        $messages = $validation->validate($_POST);
        if (count($messages)) {
            foreach ($messages as $message) {
                echo $message;
                return false;
            }
        }



        $examenes = ExamenesMedicos::findFirst($this->request->getPost("exa_cod"));
        $examenes->exa_nom  = $this->request->getPost("exa_nom");
        $examenes->exa_tip  = $this->request->getPost("exa_tip");

        
        if($examenes->save()){
            echo "1";
        }else{
             foreach ($examenes->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
        

    }

     public function eliminarAction()
    {
        
        $examenes = ExamenesMedicos::findFirst($this->request->getPost("exa_cod"));
        $examenes->exa_est="0";
        if($examenes->save()){
            echo "1";
        }else{
             foreach ($examenes->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

}

