jQuery(document).ready(function($) {
	$("#btn_guardar").click(function(event) {
		$("#frm").submit();
	});

	$.ajax({
			url: $("#url_consultar").val(),
			type: 'POST',
			dataType: 'html',
			data: "txt_pag="+$("#txt_pag").val(),
			 beforeSend: function() {
				$("#respuesta").html("Cargando...")
  			 }
		})

		.done(function(datos) {
			console.log(datos)
			$("#respuesta").html(datos)

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	$("#txt_bus").keypress(function(e) {
		if(e.which == 13) {
			$.ajax({
				url: $("#url_consultar").val(),
				type: 'POST',
				dataType: 'html',
				data: "txt_bus="+$("#txt_bus").val()+"&txt_pag="+$("#txt_pag").val()+"&filas="+$("#filas").val(),
				 beforeSend: function() {
					$("#respuesta").html("Cargando...")
	  			 }
			})

			.done(function(datos) {
				console.log(datos)
				$("#respuesta").html(datos)

				
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
	});
	
});

function abrirModal(asi_cod, per_cod){
	$(".negro").css("display", "block")
	$("#modal_documentos").css("display", "block")

	$("#per_cod").val(per_cod)
	$("#asi_cod").val(asi_cod)
	
}

function cerrarModal(per_cod){
	$(".negro").css("display", "none")
	$("#modal_documentos").css("display", "none")


}