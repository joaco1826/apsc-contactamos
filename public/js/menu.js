jQuery(document).ready(function($) {

	$(window).resize(function(){
		$('#contenedor').height($(window).height());
		$('#izq, #central').height($('#contenedor').height() - $('#pie').height() - $('#encabezado').height());
	});
	$(window).trigger('resize');

	$("#menu_postulante").click(function(event) {
		$(".submenu").slideUp(500);
		if($("#submenu_postulante").is(":visible")){
			$("#submenu_postulante").slideUp(500);
		}else{
			$("#submenu_postulante").slideDown(500);
		}
	});

	$("#menu_proceso").click(function(event) {
		$(".submenu").slideUp(500);
		if($("#submenu_proceso").is(":visible")){
			$("#submenu_proceso").slideUp(500);
		}else{
			$("#submenu_proceso").slideDown(500);
		}
	});

	$("#menu_ordenes").click(function(event) {
		$(".submenu").slideUp(500);
		if($("#submenu_ordenes").is(":visible")){
			$("#submenu_ordenes").slideUp(500);
		}else{
			$("#submenu_ordenes").slideDown(500);
		}
	});

	$("#menu_informes").click(function(event) {
		$(".submenu").slideUp(500);
		if($("#submenu_informes").is(":visible")){
			$("#submenu_informes").slideUp(500);
		}else{
			$("#submenu_informes").slideDown(500);
		}
	});




	$("#menu_plataforma").click(function(event) {
		$(".submenu").slideUp(500);
		if($("#submenu_plataforma").is(":visible")){
			$("#submenu_plataforma").slideUp(500);
		}else{
			$("#submenu_plataforma").slideDown(500);
		}
	});

    $("#menu_gestor").click(function(event) {
        $(".submenu").slideUp(500);
        if($("#submenu_gestor").is(":visible")){
            $("#submenu_gestor").slideUp(500);
        }else{
            $("#submenu_gestor").slideDown(500);
        }
    });





});