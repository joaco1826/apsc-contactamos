<?php

$loader = new \Phalcon\Loader();

// Register some classes
$loader->registerClasses(
    array(
        "PHPMailer" => $config->application->libraryDir."PHPMailer/class.phpmailer.php",
        "General" => $config->application->libraryDir."General.php",
    )
);

$loader->registerFiles(
    [
        "vendor/autoload.php",
    ]
);

// register autoloader
$loader->register();


$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->libraryDir
    )
)->register();


