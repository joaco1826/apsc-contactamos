<?php

class Dpto extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $dep_cod;

    /**
     *
     * @var string
     */
    public $dep_cdg;

    /**
     *
     * @var string
     */
    public $dep_nom;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'dep_cod' => 'dep_cod', 
            'dep_cdg' => 'dep_cdg', 
            'dep_nom' => 'dep_nom'
        );
    }

     public function initialize()
    {
         $this->hasMany("dep_cod", "Muni", "dep_cod");      
    }

}
