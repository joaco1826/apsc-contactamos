<?php

class IpvPerDet extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $ide_cod;

    /**
     *
     * @var integer
     */
    public $ias_cod;

    /**
     *
     * @var integer
     */
    public $ipr_cod;

    /**
     *
     * @var string
     */
    public $ire_num;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'ide_cod' => 'ide_cod', 
            'ias_cod' => 'ias_cod', 
            'ipr_cod' => 'ipr_cod', 
            'ire_num' => 'ire_num'
        );
    }

}
