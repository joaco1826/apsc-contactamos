jQuery(document).ready(function($) {
	$("#btn_guardar").click(function(event) {


		$(this).attr('disabled', true);
		$.ajax({
			url: $("#url").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			$("#btn_guardar").attr('disabled', false);
			console.log(datos)
			if(datos==1){
				mensaje.jnAlert("!Registro Guardado!", "Los registros se han guardado con exito");
				// location.reload()
			}else{

				alert(datos);

			}


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});

	});

	$(".eliminar").click(function(event) {

			if (confirm("Desea realmente eliminar este registro?")){
			$.ajax({
				url: $("#url_eliminar").val(),
				type: 'POST',
				dataType: 'html',
				data: "cod="+$(this).attr('id'),
			})
			.done(function(datos) {
				console.log(datos)
				if(datos==1){
					alert("Registro Eliminado");
					location.reload()
				}else{
					alert(datos)
				}


			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
	});

    $(".modal-cargos").click(function(event) {

        $.ajax({
            url: $("#url_listar").val(),
            type: 'POST',
            dataType: 'html',
            data: "empresa_id="+$("#emp_cod").val()+"&cargo_id="+$(this).attr('data-id'),
        })
            .done(function(datos) {

                $(".negro").css("display", "block")
                $("#modal_documentos").html(datos)
                $("#modal_documentos").css("display", "block")

            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

    });

});

function cerrarModal(){
    $(".negro").css("display", "none")
    $("#modal_documentos").css("display", "none")
}

function competencias() {
    $.ajax({
        url: $("#url_comp").val(),
        type: 'POST',
        dataType: 'html',
        data: $("#frm").serialize(),
    })
	.done(function(datos) {

		if ($.trim(datos) == "1") {
			alert("Registro guardado con éxito")
		} else {
			alert("Hubo un error")
		}

	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

}