<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class PreguntasPnl extends \Phalcon\Mvc\Model
{


    public function initialize()
    {
        $this->setSource('pnl_pre');
    }

     public static function obtenerRespuesta($id_pregunta, $pos_cod)
    {
        // A raw SQL statement
        $hoy = date("Y-m-d");
        $fecha = strtotime ( '-90 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
        $sql = "SELECT d.`pnl_cod` AS pregunta, r.`rpn_enu` , r.`rpn_nom` AS respuesta
		FROM pnl_per pos 
		JOIN pnl_per_det d ON pos.`pna_cod`=d.`pna_cod`
		LEFT JOIN pnl_res r ON d.`rpn_cod`=r.`rpn_cod`
		WHERE pos.`pna_cod`=(SELECT MAX(pna_cod) FROM pnl_per WHERE per_cod='$pos_cod')  AND d.`pnl_cod`=$id_pregunta AND pos.pna_fch BETWEEN '$fecha' AND '$hoy'";

        // Base model
        $pf = new PreguntasPnl();

        // Execute the query
        return new Resultset(null, $pf, $pf->getReadConnection()->query($sql));
    }

   

}
