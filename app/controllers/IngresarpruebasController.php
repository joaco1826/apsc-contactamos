<?php

class IngresarPruebasController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
            ->addJs('js/IngresarPruebas.js');
         
            
    }

    public function indexAction()
    {
          // $tipoDocumentos =  TipoDocumento::find();
          // $this->view->setVar("tipoDocumentos", $tipoDocumentos);
    }

     public function consultarAction()
    {
        $txt_bus = $this->request->getPost("txt_bus");
        $personas =  Personas::find(array(
               "per_est = '1' AND per_pno LIKE '$txt_bus%' OR per_pap LIKE '$txt_bus%' OR per_ide LIKE '$txt_bus%'"
        ));
        ?>
        <table class="lista">
             <tr>
                <th>No. Documento</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Profesión</th>
                <th>Accion</th>
             </tr>
        <?
        foreach ($personas as  $per) {

            ?>
                <tr>
                    <td><?=$per->per_ide?></td>
                    <td><?=$per->per_pno?></td>
                    <td><?=$per->per_pap?></td>
                    <td><?=$per->per_pro?></td>
                    <td><a href="#" onclick="abrirModal('<?=$per->per_cod?>');">Ingresar Pruebas</a></td>
                </tr>
            <?
        }
        ?>
        </table>
        <?
    }

}

?>