jQuery(document).ready(function($) {
	$("#btn_guardar").click(function(event) {
		

		
		$.ajax({
			url: $("#url").val(),
			type: 'POST',
			dataType: 'html',
			data: $("#frm").serialize(),
		})
		.done(function(datos) {
			console.log(datos)
			if(datos==1){
				alert("Registro Guardado");
				window.location=$("#url_index").val()
			}else{
				alert(datos)
			}

			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	});

	$(".eliminar").click(function(event) {
			
		if (confirm("Desea realmente eliminar este tipo de usuario?")){
			$.ajax({
				url: $("#url").val(),
				type: 'POST',
				dataType: 'html',
				data: "cod="+$(this).attr('id'),
			})
			.done(function(datos) {
				console.log(datos)
				if(datos==1){
					alert("Registro Eliminado");
					location.reload()
				}else{
					alert(datos)
				}

				
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}
	});

	$("#todos").click(function(event) {
		/* Act on the event */

		if($("[name=todos]").is(":checked")) { 

			$("input[type='checkbox']").each(function() {
				$(this).prop('checked', true)
			});
		} else {
			$("input[type='checkbox']").each(function() {
				$(this).prop('checked', false)
			});
		}
	});
});