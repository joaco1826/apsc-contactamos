<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class CarasPer extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $cas_cod;

    /**
     *
     * @var integer
     */
    public $per_cod;

    /**
     *
     * @var integer
     */
    public $req_cod;

    /**
     *
     * @var integer
     */
    public $usu_cod;

    /**
     *
     * @var string
     */
    public $cas_fecha;

    /**
     *
     * @var string
     */
    public $cas_web;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'cas_cod' => 'cas_cod', 
            'per_cod' => 'per_cod', 
            'req_cod' => 'req_cod', 
            'usu_cod' => 'usu_cod', 
            'cas_fecha' => 'cas_fecha', 
            'cas_web' => 'cas_web'
        );
    }

    public function resultado($per_cod, $req_cod){
         $hoy = date("Y-m-d");
        $fecha = strtotime ( '-90 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
        $sql = "SELECT cas_cod 
                FROM caras_per 
                WHERE per_cod=$per_cod AND DATE(cas_fecha) BETWEEN '$fecha' AND '$hoy' ORDER BY cas_cod DESC LIMIT 1";
       $caras = new CarasPer();

        // Execute the query
        return new Resultset(null, $caras, $caras->getReadConnection()->query($sql));

    }

    public static function npruebas($fecha)
    {
        // A raw SQL statement

       $sql ="SELECT COUNT(*) as total
            FROM caras_per 
            WHERE DATE(cas_fecha) = '$fecha'";

        // Base model
       $caras = new CarasPer();

        // Execute the query
        return new Resultset(null, $caras, $caras->getReadConnection()->query($sql));
    }

}
