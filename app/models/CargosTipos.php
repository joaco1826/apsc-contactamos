<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class CargosTipos extends \Phalcon\Mvc\Model
{

    public  function tipoCargo($pos_cod){
            $sql ="SELECT c.car_des, tc.tic_nom,   tc.`tic_ada`, tc.`tic_cap`, tc.`tic_com`, tc.`tic_lid`, tc.`tic_mot`, tc.`tic_anh`, tc.`tic_rip` FROM postulaciones p 
            JOIN requisiciones r ON p.`req_cod`=r.`req_cod`
            JOIN cargos c ON r.`car_cod`=c.`car_cod`
            JOIN cargos_tipos tc ON c.`tic_cod`=tc.`tic_cod`
            WHERE  p.`req_cod`=$pos_cod LIMIT 1";

        // Base model
        $obj = new CargosTipos();

        // Execute the query
        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }
   

}
