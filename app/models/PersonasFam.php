<?php

class PersonasFam extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $fam_cod;

    /**
     *
     * @var string
     */
    public $fam_nom;

    /**
     *
     * @var string
     */
    public $fam_par;

    /**
     *
     * @var string
     */
    public $fam_tel;

    /**
     *
     * @var string
     */
    public $fam_ocu;

    /**
     *
     * @var integer
     */
    public $per_cod;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'fam_cod' => 'fam_cod', 
            'fam_nom' => 'fam_nom', 
            'fam_par' => 'fam_par', 
            'fam_tel' => 'fam_tel',
            'fam_ocu' => 'fam_ocu',
            'per_cod' => 'per_cod'
        );
    }

}
