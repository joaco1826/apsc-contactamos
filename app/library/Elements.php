<?php 
	
	class Elements extends Phalcon\Mvc\User\Component
	{

	    public function getMuniDpto()
	    {
	        $municipios = $this->db->query("SELECT * FROM dpto d JOIN muni m ON d.dep_cod = m.dep_cod");
	        return $municipios;
	    }

	    public function envioEmail($cuerpo, $asunto, $destinatario)
	    {
	    	 $mail = new PHPMailer();
            //instancio u$mail = new PHPMailer(); // defaults to using php "mail()"

            //defino el cuerpo del mensaje en una variable $body
            //se trae el contenido de un archivo de texto
            //también podríamos hacer $body="contenido...";
            $body = $cuerpo;
            //Esta línea la he tenido que comentar
            //porque si la pongo me deja el $bodyn objeto de la clase PHPMailer
             // vacío
            // $body = preg_replace('/[]/i','',$body);

            //defino el email y nombre del remitente del mensaje
            $mail->SetFrom('info@agenciamid.com', 'Info APSC');

            //defino la dirección de email de "reply", a la que responder los mensajes
            //Obs: es bueno dejar la misma dirección que el From, para no caer en spam
            $mail->AddReplyTo("info@agenciamid.com","Info APSC");
            //Defino la dirección de correo a la que se envía el mensaje
            $address = $destinatario;
            //la añado a la clase, indicando el nombre de la persona destinatario
            $mail->AddAddress($address, "");

            //Añado un asunto al mensaje
            $mail->Subject = $asunto;

            //Puedo definir un cuerpo alternativo del mensaje, que contenga solo texto
            $mail->AltBody = "Cuerpo alternativo del mensaje";

            //inserto el texto del mensaje en formato HTML
            $mail->MsgHTML($body);

            $mail->CharSet = 'UTF-8';

            //asigno un archivo adjunto al mensaje
            // $mail->AddAttachment("ruta/archivo_adjunto.gif");

            //envío el mensaje, comprobando si se envió correctamente
            if(!$mail->Send()) {
            	return "Error al enviar el mensaje: " . $mail->ErrorInfo;
            } else {
           	    return true;
            }    
	    }
	    

	}

 ?>