<?php

class Empresas extends \Phalcon\Mvc\Model
{

    

    /**
     *
     * @var integer
     */
    protected $emp_cod;

    /**
     *
     * @var string
     */
    protected $emp_nit;

    /**
     *
     * @var string
     */
    protected $emp_raz;

    /**
     *
     * @var string
     */
    protected $emp_con;

    /**
     *
     * @var string
     */
    protected $emp_tel;

    /**
     *
     * @var string
     */
    protected $emp_ema;

    /**
     *
     * @var string
     */
    protected $emp_dir;

    /**
     *
     * @var string
     */
    protected $emp_pas;

    /**
     *
     * @var string
     */
    protected $emp_exa;

    /**
     *
     * @var string
     */
    protected $emp_ing;

    /**
     *
     * @var string
     */
    protected $emp_tip;

    /**
     *
     * @var string
     */
    protected $emp_est;

    /**
     * Method to set the value of field emp_cod
     *
     * @param integer $emp_cod
     * @return $this
     */
    public function setEmpCod($emp_cod)
    {
        $this->emp_cod = $emp_cod;

        return $this;
    }

    /**
     * Method to set the value of field emp_nit
     *
     * @param string $emp_nit
     * @return $this
     */
    public function setEmpNit($emp_nit)
    {
        $this->emp_nit = $emp_nit;

        return $this;
    }

    /**
     * Method to set the value of field emp_raz
     *
     * @param string $emp_raz
     * @return $this
     */
    public function setEmpRaz($emp_raz)
    {
        $this->emp_raz = $emp_raz;

        return $this;
    }

    /**
     * Method to set the value of field emp_con
     *
     * @param string $emp_con
     * @return $this
     */
    public function setEmpCon($emp_con)
    {
        $this->emp_con = $emp_con;

        return $this;
    }

    /**
     * Method to set the value of field emp_tel
     *
     * @param string $emp_tel
     * @return $this
     */
    public function setEmpTel($emp_tel)
    {
        $this->emp_tel = $emp_tel;

        return $this;
    }

    /**
     * Method to set the value of field emp_ema
     *
     * @param string $emp_ema
     * @return $this
     */
    public function setEmpEma($emp_ema)
    {
        $this->emp_ema = $emp_ema;

        return $this;
    }

    /**
     * Method to set the value of field emp_pas
     *
     * @param string $emp_pas
     * @return $this
     */
    public function setEmpDir($emp_dir)
    {
        $this->emp_dir = $emp_dir;

        return $this;
    }

    /**
     * Method to set the value of field emp_pas
     *
     * @param string $emp_pas
     * @return $this
     */
    public function setEmpPas($emp_pas)
    {
        $this->emp_pas = $emp_pas;

        return $this;
    }

    /**
     * Method to set the value of field emp_pas
     *
     * @param string $emp_pas
     * @return $this
     */
    public function setEmpExa($emp_exa)
    {
        $this->emp_exa = $emp_exa;

        return $this;
    }

     /**
     * Method to set the value of field emp_pas
     *
     * @param string $emp_pas
     * @return $this
     */
    public function setEmpIng($emp_ing)
    {
        $this->emp_ing = $emp_ing;

        return $this;
    }

    /**
     * Method to set the value of field emp_tip
     *
     * @param string $emp_tip
     * @return $this
     */
    public function setEmpTip($emp_tip)
    {
        $this->emp_tip = $emp_tip;

        return $this;
    }

    /**
     * Method to set the value of field emp_est
     *
     * @param string $emp_est
     * @return $this
     */
    public function setEmpEst($emp_est)
    {
        $this->emp_est = $emp_est;

        return $this;
    }

    /**
     * Returns the value of field emp_cod
     *
     * @return integer
     */
    public function getEmpCod()
    {
        return $this->emp_cod;
    }

    /**
     * Returns the value of field emp_nit
     *
     * @return string
     */
    public function getEmpNit()
    {
        return $this->emp_nit;
    }

    /**
     * Returns the value of field emp_raz
     *
     * @return string
     */
    public function getEmpRaz()
    {
        return $this->emp_raz;
    }

    /**
     * Returns the value of field emp_con
     *
     * @return string
     */
    public function getEmpCon()
    {
        return $this->emp_con;
    }

    /**
     * Returns the value of field emp_tel
     *
     * @return string
     */
    public function getEmpTel()
    {
        return $this->emp_tel;
    }

    /**
     * Returns the value of field emp_ema
     *
     * @return string
     */
    public function getEmpEma()
    {
        return $this->emp_ema;
    }

    /**
     * Returns the value of field emp_ema
     *
     * @return string
     */
    public function getEmpExa()
    {
        return $this->emp_exa;
    }

     /**
     * Returns the value of field emp_ema
     *
     * @return string
     */
    public function getEmpDir()
    {
        return $this->emp_dir;
    }

    /**
     * Returns the value of field emp_pas
     *
     * @return string
     */
    public function getEmpPas()
    {
        return $this->emp_pas;
    }

     /**
     * Returns the value of field emp_pas
     *
     * @return string
     */
    public function getEmpIng()
    {
        return $this->emp_ing;
    }

    /**
     * Returns the value of field emp_tip
     *
     * @return string
     */
    public function getEmpTip()
    {
        return $this->emp_tip;
    }

    /**
     * Returns the value of field emp_est
     *
     * @return string
     */
    public function getEmpEst()
    {
        return $this->emp_est;
    }



    

     public function initialize()
    {
         $this->hasMany("emp_cod", "Proveedores", "emp_cod"); 
         $this->hasMany("emp_cod", "Requisiciones", "emp_cod"); //relacion con requisiciones
    }

}
