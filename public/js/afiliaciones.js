/**
 * Created by joacomidsoluciones on 15/08/17.
 */

jQuery(document).ready(function ($) {

    $("#btn_guardar").click(function (event) {


        $.ajax({

            url: $("#url").val(),

            type: 'POST',

            dataType: 'html',

            data: $("#frm").serialize(),

        })

            .done(function (datos) {

                console.log(datos)

                if (datos == 1) {

                    alert("Registro Guardado");
                    history.back(1);

                } else {

                    alert(datos)

                }


            })

            .fail(function () {

                console.log("error");

            })

            .always(function () {

                console.log("complete");

            });


    });

    $("#txt_bus").keypress(function (e) {
        if (e.which == 13) {
            $.ajax({
                url: $("#url_consultar").val(),
                type: 'POST',
                dataType: 'html',
                data: "txt_pag=" + $("#txt_pag").val() + "&txt_bus=" + $("#txt_bus").val() + "&car_cod=" + $("#car_cod").val() + "&emp_cod=" + $("#emp_cod").val() + "&filas=" + $("#filas").val() + "&oficina=" + $("#oficina").val() + "&estad=" + $("#estad").val() + "&dotacion=" + $("#dotacion").val(),
                beforeSend: function () {
                    $("#respuesta").html("Cargando...")
                }
            })

                .done(function (datos) {
                    // console.log(datos)
                    $("#respuesta").html(datos)


                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
        }

    });


});


function consultarO() {
    $.ajax({
        url: $("#url_consultar").val(),
        type: 'POST',
        dataType: 'html',
        data: "txt_pag=" + $("#txt_pag").val(),
        beforeSend: function () {
            $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            // console.log(datos)
            $("#respuesta").html(datos)


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}


function filtrar(pag) {
    $("#txt_pag").val(pag);
    $.ajax({
        url: $("#url_consultar").val(),
        type: 'POST',
        dataType: 'html',
        data: "txt_pag=" + $("#txt_pag").val() + "&txt_bus=" + $("#txt_bus").val() + "&emp_cod=" + $("#emp_cod").val() + "&filas=" + $("#filas").val() + "&oficina=" + $("#oficina").val() + "&estad=" + $("#estad").val() + "&dotacion=" + $("#dotacion").val(),
        beforeSend: function () {
            $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            $("#respuesta").html(datos)


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function fechas(campo, valor, ord_cod) {
    $.ajax({
        url: $("#url_fechas").val(),
        type: 'POST',
        dataType: 'html',
        data: "campo=" + campo + "&valor=" + valor + "&ord_cod=" + ord_cod,
        beforeSend: function () {

        }
    })

        .done(function (datos) {
            console.log(datos)


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}


function cambiarestado(per_cod, est_cod, ord_cod) {
    $.ajax({
        url: $("#url_estado").val(),
        type: 'POST',
        dataType: 'html',
        data: "per_cod=" + per_cod + "&est_cod=" + est_cod + "&ord_cod=" + ord_cod,
        beforeSend: function () {
            // $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            if (datos == 1) {
                // alert("Estado cambiado");
            } else {
                alert(datos);
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function confirmar(ord_cod, obj) {
    $.ajax({
        url: $("#url_confirmar").val(),
        type: 'POST',
        dataType: 'html',
        data: "ord_cod=" + ord_cod,
        beforeSend: function () {
            // $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            if (datos == 1) {
                $(obj).parent().parent().remove();
            } else {
                alert(datos);
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}




