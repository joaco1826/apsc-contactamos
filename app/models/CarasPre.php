<?php
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class CarasPre extends \Phalcon\Mvc\Model
{

   public static function respuestasPos($pos_cod)
    {
        // A raw SQL statement
        $hoy = date("Y-m-d");
        $fecha = strtotime ( '-720 day' , strtotime ( $hoy ) ) ;
        $fecha = date ( 'Y-m-d' , $fecha );
       $sql = "SELECT caras_per_det.`car_cod`
        AS pregunta, caras_per_det.car_rta AS Rta_Aspirante,caras_pre.car_rta 
        AS rta_correcta, caras_pre.car_rtb AS Rta_B 
        FROM caras_pre, caras_per_det, caras_per 
        WHERE caras_per_det.car_cod=caras_pre.car_cod AND caras_per.cas_cod=(SELECT MAX(cas_cod) FROM caras_per WHERE per_cod=$pos_cod)  
        AND caras_per.cas_cod=caras_per_det.cas_cod AND DATE(caras_per.cas_fecha) BETWEEN '$fecha' AND '$hoy' ";
        // Base model
        $caras = new CarasPre();

        // Execute the query
        return new Resultset(null, $caras, $caras->getReadConnection()->query($sql));
    }

}
