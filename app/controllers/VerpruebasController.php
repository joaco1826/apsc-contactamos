<?php

class VerpruebasController extends \Phalcon\Mvc\Controller
{
	 public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js');
            
    }

    public function indexAction()
    {
    	// $this->assets
     //        ->addJs('js/jquery.js')
     //        ->addJs('js/verOfertas.js');
    	$req = new Requisiciones();
        $this->view->setVar("pruebas", $req->getPruebasReq($this->session->get("req_cod")));
    }

}

