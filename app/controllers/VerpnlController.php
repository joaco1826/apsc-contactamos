<?php

class VerPNLController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/jnalert.js')
            ->addJs('js/menu.js')
             ->addJs('js/EvaluarPrueba.js');
    }

     public function indexAction($per_cod)
    {
          $int = split("-", $per_cod);
        $per_cod = $int[0];
        $req_cod = $int[1];
          $persona = Personas::findFirst($per_cod);
          $this->view->setVar("persona", $persona);
          $pnl = PnlPer::findFirst(array("per_cod=$per_cod ORDER BY pna_cod DESC"))->pna_cod;
          $this->view->setVar("pnl", $pnl);

    }

    public function eliminarAction()
    {
           $pnl = PnlPer::findFirst($this->request->getPost("cod"));
           if ($pnl) {
             if ($pnl->delete() == false) {
                echo "Lo sentimos, hubo un error: \n";

                foreach ($robot->getMessages() as $message) {
                    echo $message, "\n";
                }
            } else {
                echo "1";
            }
           }

    }
}