<?php
use Phalcon\Validation\Validator\PresenceOf,
    	Phalcon\Validation\Validator\Email;
class PermisosController extends \Phalcon\Mvc\Controller
{

     public function initialize()
    {
         $this->assets
             ->addCss('//fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900', false)
             ->addCss('css/estilos.css');

        $this->assets
            ->addJs('js/jquery.js')
            ->addJs('js/menu.js')
            ->addJs('js/TipoUsuarios.js');

    }


    public function indexAction()
    {

            
    	 $this->view->setVar("usuarios", Usuarios::find(array(
		    	"usu_est = '1'"
		 )));
        
    }

     public function nuevoAction()
    {
    	
            
    	 $this->view->setVar("menus", Menus::find(array(
		    	"men_est = '1'"
		 )));
    }

     public function editarAction($usu_cod)
    {
    	

 		 $this->view->setVar("cargos", Cargos::find(array(
		    	"car_est = '1'"
		 )));


 		 $this->view->setVar("permisos", Permisos::UsuariosCargos($usu_cod));
 		 $this->view->setVar("usu", Usuarios::findFirst($usu_cod));
    }

      public function guardarAction()
    {
    	

		$validation = new Phalcon\Validation();
       
		$validation->add('tus_des', new PresenceOf(array(
		    'message' => 'El campo Nombre Es Requerido',

		)));

		$messages = $validation->validate($_POST);
		if (count($messages)) {
		    foreach ($messages as $message) {
		        echo $message;
		        return false;
		    }
		}

		$this->db->begin();

    	$tip = new TipoUsuarios();
    	$tip->tus_des  = $this->request->getPost("tus_des");
    	$tip->tus_est = new \Phalcon\Db\RawValue('default');
    	$opciones  = $this->request->getPost("opciones");

    	if ($tip->save() == false) {
            $this->db->rollback();
            return;
        }

    	$tus_cod = $tip->tus_cod;
    	
    	
    	foreach ($opciones as $key => $men_cod) {
    		$per = new Permisos();
    		$per->men_cod = $men_cod;
    		$per->tus_cod = $tus_cod;

    		if ($per->save() == false) {
	            $this->db->rollback();
	            return;
        	}

    	}

         $this->db->commit();
         echo "1";
    	
    	

    }

     public function actualizarAction()
    {
    	

		$this->db->begin();
		//ACTUALZIAR DESCRIPCION TIPO DE USUARIO
		$usu_cod = $this->request->getPost("usu_cod");
    	$opciones  = $this->request->getPost("opciones");

    	   	
    	//BORRAR PERMISOS
    	foreach (Consempresas::find("usu_id=".$usu_cod) as $per) {
		    if ($per->delete() == false) {
		         $this->db->rollback();
		    }
		}
		//INSERTAR PERMISOS
    	foreach ($opciones as $key => $car_id) {
    		$per = new Consempresas();
    		$per->car_id = $car_id;
    		$per->usu_id = $usu_cod;

    		if ($per->save() == false) {
	            $this->db->rollback();
	            return;
        	}

    	}

         $this->db->commit();
         echo "1";
    	
    	

    }

     public function eliminarAction()
    {
        
        $tipo = TipoUsuarios::findFirst($this->request->getPost("cod"));
        $tipo->tus_est="0";
        if($tipo->save()){
            echo "1";
        }else{
             foreach ($tipo->getMessages() as $message) {
                echo "Message: ", $message->getMessage();
                echo "Field: ", $message->getField();
                echo "Type: ", $message->getType();
            }
        }
    }

}

