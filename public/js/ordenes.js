jQuery(document).ready(function ($) {

    $("#btn_guardar").click(function (event) {


        $.ajax({

            url: $("#url").val(),

            type: 'POST',

            dataType: 'html',

            data: $("#frm").serialize(),

        })

            .done(function (datos) {

                console.log(datos)

                if (datos == 1) {

                    alert("Registro Guardado");
                    history.back(1);

                } else {

                    alert(datos)

                }


            })

            .fail(function () {

                console.log("error");

            })

            .always(function () {

                console.log("complete");

            });


    });

    $("#txt_bus").keypress(function (e) {
        if (e.which == 13) {
            $.ajax({
                url: $("#url_consultar").val(),
                type: 'POST',
                dataType: 'html',
                data: "txt_pag=" + $("#txt_pag").val() + "&txt_bus=" + $("#txt_bus").val() + "&car_cod=" + $("#car_cod").val() + "&emp_cod=" + $("#emp_cod").val() + "&filas=" + $("#filas").val() + "&oficina=" + $("#oficina").val() + "&estad=" + $("#estad").val() + "&dotacion=" + $("#dotacion").val(),
                beforeSend: function () {
                    $("#respuesta").html("Cargando...")
                }
            })

                .done(function (datos) {
                    // console.log(datos)
                    $("#respuesta").html(datos)


                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
        }

    });
    $("#txt_doc").keypress(function (e) {
        if (e.which == 13) {
            $.ajax({
                url: $("#url_consultar").val(),
                type: 'POST',
                dataType: 'html',
                data: "txt_pag=" + $("#txt_pag").val() + "&txt_bus=" + $("#txt_doc").val() + "&car_cod=" + $("#car_cod").val() + "&emp_cod=" + $("#emp_cod").val() + "&filas=" + $("#filas").val() + "&oficina=" + $("#oficina").val() + "&estad=" + $("#estad").val() + "&dotacion=" + $("#dotacion").val() + "&completo=" + $("#completo").val(),
                beforeSend: function () {
                    $("#respuesta").html("Cargando...")
                }
            })

                .done(function (datos) {
                    // console.log(datos)
                    $("#respuesta").html(datos)


                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
        }

    });
    $("#req_fci").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá']
    })

    $(".postu").click( function () {
        var postu = "";
        if ($(this).is(':checked')) {
            postu = $(this).val();
        }
        $("#personas").val($("#personas").val() + postu);
        alert($(this).parent().text());
    });


});

function desPerso(obj) {
    var postu = $(obj).val();
    var id = $(obj).attr("data-id");
    var cadena = $("#personas").val();
    var res, sorftime;
    if ($(obj).is(':checked')) {
        $("#personas").val($("#personas").val() + postu);
        $(obj).parent().parent().css("background", "#7bcd7b");
        sorftime = "SI";
    } else {
        $(obj).parent().parent().css("background", "#ffffff");
        res = cadena.replace(postu, "");
        $("#personas").val(res);
        sorftime = "NO";
    }

    $.ajax({
        url: $("#url_descargar").val(),
        type: 'POST',
        dataType: 'html',
        data: "id=" + id + "&sorftime="+sorftime,
        beforeSend: function () {

        }
    })

        .done(function (datos) {



        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });

}

function eliminar(id) {
    if (confirm("Desea realmente eliminar esta orden?")) {
        $.ajax({
            url: $("#url_eliminar").val(),
            type: 'POST',
            dataType: 'html',
            data: "id=" + id,
            beforeSend: function () {
                $("#respuesta").html("Cargando...")
            }
        })

            .done(function (datos) {
                console.log(datos)
                alert("Orden eliminada")
                location.reload();


            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });
    }
}

function editarO(ord_cod) {
    location.href = $("#url_editar").val() + "/" + ord_cod;
}

function asignar_examenes(tipo) {



    //alert($("#per_cod").val())


    $.ajax({

        url: $("#url_base").val() + "Ordenes/asignar_examenes",

        type: 'POST',

        dataType: 'html',

        data: $("#frm").serialize() + "&tipo=" + tipo,

    })

        .done(function (datos) {

            if ($.trim(datos) == "1") {

                alert("Se han asignados los exámenes con éxito")

                location.reload()

            } else {

                alert(datos)

            }


        })

        .fail(function () {

            console.log("error");

        })

        .always(function () {

            console.log("complete");

        });

}


function consultarO() {
    $.ajax({
        url: $("#url_consultar").val(),
        type: 'POST',
        dataType: 'html',
        data: "txt_pag=" + $("#txt_pag").val(),
        beforeSend: function () {
            $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            // console.log(datos)
            $("#respuesta").html(datos)


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}


function filtrar(pag) {
    $("#txt_pag").val(pag);
    $.ajax({
        url: $("#url_consultar").val(),
        type: 'POST',
        dataType: 'html',
        data: "txt_pag=" + $("#txt_pag").val() + "&txt_bus=" + $("#txt_bus").val() + "&car_cod=" + $("#car_cod").val() + "&emp_cod=" + $("#emp_cod").val() + "&filas=" + $("#filas").val() + "&oficina=" + $("#oficina").val() + "&estad=" + $("#estad").val() + "&dotacion=" + $("#dotacion").val(),
        beforeSend: function () {
            $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            $("#respuesta").html(datos)


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function filtrarDoc(pag) {
    $("#txt_pag").val(pag);
    $.ajax({
        url: $("#url_consultar").val(),
        type: 'POST',
        dataType: 'html',
        data: "txt_pag=" + $("#txt_pag").val() + "&txt_bus=" + $("#txt_doc").val() + "&car_cod=" + $("#car_cod").val() + "&emp_cod=" + $("#emp_cod").val() + "&filas=" + $("#filas").val() + "&oficina=" + $("#oficina").val() + "&estad=" + $("#estad").val() + "&dotacion=" + $("#dotacion").val() + "&completo=" + $("#completo").val(),
        beforeSend: function () {
            $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            $("#respuesta").html(datos)


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function eliminarDoc(tdo_cod, ord_cod, obj) {
    $.ajax({
        url: $("#url_eliminar_doc").val(),
        type: 'POST',
        dataType: 'html',
        data: "tdo_cod=" + tdo_cod + "&ord_cod=" + ord_cod,
        beforeSend: function () {

        }
    })

        .done(function (datos) {
            console.log(datos)
            if ($.trim(datos) == 1) {
                alert("El documentado ha sido eliminado con éxito!")
                $(obj).parent().html("Sin adjunto")
            } else {
                alert(datos)
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function eliminarDocDrive(tdo_cod, ord_cod, obj) {
    $.ajax({
        url: $("#url_eliminar_docdrive").val(),
        type: 'POST',
        dataType: 'html',
        data: "tdo_cod=" + tdo_cod + "&ord_cod=" + ord_cod,
        beforeSend: function () {

        }
    })

        .done(function (datos) {
            console.log(datos)
            if ($.trim(datos) == 1) {
                alert("El documentado ha sido eliminado con éxito!")
                $(obj).parent().html("Sin adjunto")
            } else {
                alert(datos)
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function quitarCompletado(ord_cod, obj) {
    $.ajax({
        url: $("#url_quitar").val(),
        type: 'POST',
        dataType: 'html',
        data: "id=" + ord_cod,
        beforeSend: function () {

        }
    })

        .done(function (datos) {
            console.log(datos)
            if ($.trim(datos) == 1) {
                alert("Accion realizada exitosamente")
                $(obj).parent().parent().remove()
            } else {
                alert(datos)
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function abrirModalDoc(ord_cod) {
    $(".negro").css("display", "block")
    $("#modal_documentos_doc").css("display", "block")
    $("#ord_doc").val(ord_cod)
    $.ajax({
        url: $("#url_obtener").val(),
        type: 'POST',
        dataType: 'html',
        data: "ord_cod=" + ord_cod,
        beforeSend: function () {
            $("#modal_documentos_doc").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            $("#modal_documentos_doc").html(datos)


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function SubirDoc(ord_doc) {
    var data = new FormData($("#frm-doc")[0]);
    data.append("ord_doc", ord_doc);
    $.ajax({

        url: $("#url_upload").val(),

        type: 'POST',

        contentType: false, //Debe estar en false para que pase el objeto sin procesar
        data: data, //Le pasamos el objeto que creamos con los archivos
        processData: false, //Debe estar en false para que JQuery no procese los datos a enviar
        cache: false, //Para que el formulario no guarde cache


    })

        .done(function (datos) {

            console.log(datos)

            if (datos == 1) {

                alert("Documentos subidos");

            } else {

                alert(datos)

            }


        })

        .fail(function () {

            console.log("error");

        })

        .always(function () {

            console.log("complete");

        });

}


function abrirModal(ord_cod, per_cod, nombre_aspirante, exa_tip) {

    $(".negro").css("display", "block")

    $("#modal_documentos").css("display", "block")

    $("#per_cod").val(per_cod);

    $("#ord_cod").val(ord_cod);


    $.ajax({

        url: $("#url_base").val() + "Ordenes/cargar_examenes",

        type: 'POST',

        dataType: 'html',

        data: "ord_cod=" + ord_cod + "&per_cod=" + per_cod + "&nombre_aspirante=" + nombre_aspirante + "&exa_tip=" + exa_tip,

        beforeSend: function () {

            $("#modal_documentos").html("Cargando...")

        }

    })


        .done(function (datos) {

            console.log(datos)

            $("#modal_documentos").html(datos)


        })

        .fail(function () {

            console.log("error");

        })

        .always(function () {

            console.log("complete");

        });

}


function cerrarModal(per_cod) {

    $(".negro").css("display", "none")

    $("#modal_documentos").css("display", "none")
    $("#modal_documentos_o").css("display", "none")
    $("#modal_documentos_doc").css("display", "none")


}

function cambiarestado(per_cod, est_cod, ord_cod) {
    $.ajax({
        url: $("#url_estado").val(),
        type: 'POST',
        dataType: 'html',
        data: "per_cod=" + per_cod + "&est_cod=" + est_cod + "&ord_cod=" + ord_cod,
        beforeSend: function () {
            // $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            if (datos == 1) {
                // alert("Estado cambiado");
            } else {
                alert(datos);
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function ediExa(examenes, ord_cod) {
    $.ajax({
        url: $("#url_editarex").val(),
        type: 'POST',
        dataType: 'html',
        data: "examenes=" + examenes + "&ord_cod=" + ord_cod,
        beforeSend: function () {
            // $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            if (datos == 1) {
                // alert("Estado de exámenes cambiado");
            } else {
                alert(datos);
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function ediEst(estudios, ord_cod) {
    $.ajax({
        url: $("#url_editares").val(),
        type: 'POST',
        dataType: 'html',
        data: "estudios=" + estudios + "&ord_cod=" + ord_cod,
        beforeSend: function () {
            // $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            if (datos == 1) {
                // alert("Estado de estudios cambiado");
            } else {
                alert(datos);
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function confirmar(ord_cod, obj) {
    $.ajax({
        url: $("#url_confirmar").val(),
        type: 'POST',
        dataType: 'html',
        data: "ord_cod=" + ord_cod,
        beforeSend: function () {
            // $("#respuesta").html("Cargando...")
        }
    })

        .done(function (datos) {
            console.log(datos)
            if (datos == 1) {
                $(obj).parent().parent().remove();
            } else {
                alert(datos);
            }


        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}



