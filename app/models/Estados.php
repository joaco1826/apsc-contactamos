<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class Estados extends \Phalcon\Mvc\Model
{

     public function initialize()
    {
        $this->belongsTo("per_cod", "personas", "per_cod"); //relacion Personas
       
    }

    public  function listar(){
        $sql = "SELECT * FROM estados WHERE est_est='1'";
        $obj = new Estados();



        // Execute the query

        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarTip($est_pro){
        $sql = "SELECT * FROM estados WHERE est_pro='$est_pro' AND est_est='1'";
        $obj = new Estados();



        // Execute the query

        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarNotIn($est_pro){
        $sql = "SELECT * FROM estados WHERE est_pro NOT IN ('{$est_pro}') AND est_est='1' ORDER BY est_nom";
        $obj = new Estados();



        // Execute the query

        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarCont(){
        $sql = "SELECT * FROM estados WHERE est_pro NOT IN ('agenda', 'seleccion') AND est_est='1' ORDER BY est_nom";
        $obj = new Estados();



        // Execute the query

        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

    public  function listarNot(){
        $sql = "SELECT * FROM estados WHERE est_pro NOT IN ('agenda', 'contratacion') AND est_est='1'";
        $obj = new Estados();



        // Execute the query

        return new Resultset(null, $obj, $obj->getReadConnection()->query($sql));
    }

}