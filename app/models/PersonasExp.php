<?php

class PersonasExp extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $exp_cod;

    /**
     *
     * @var string
     */
    public $exp_emp;

    /**
     *
     * @var string
     */
    public $exp_car;

    /**
     *
     * @var string
     */
    public $exp_ini;

    /**
     *
     * @var string
     */
    public $exp_ffi;

    /**
     *
     * @var string
     */
    public $exp_mot;

    /**
     *
     * @var string
     */
    public $exp_des;

    /**
     *
     * @var integer
     */
    public $per_cod;

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'exp_cod' => 'exp_cod', 
            'exp_emp' => 'exp_emp', 
            'exp_car' => 'exp_car', 
            'exp_ini' => 'exp_ini', 
            'exp_ffi' => 'exp_ffi', 
            'exp_mot' => 'exp_mot', 
            'exp_des' => 'exp_des', 
            'per_cod' => 'per_cod'
        );
    }

}
